#!/bin/sh

# serviceConfigs directory should be sibling to this project directory

JAR_MASTER_PREFIX=emory-ciscoasr-service-master
JAR_SOURCE_VERSION=1.0
LOCAL_JARS_DIR=../deploy/build-test/libs/CiscoAsrService
SOURCE_JAR=$LOCAL_JARS_DIR/$JAR_MASTER_PREFIX-$JAR_SOURCE_VERSION.jar

COPY_JAR=true
COPY_APP_CONFIGS=false
COPY_TEST_CONFIGS=false
COPY_TEST_PROPERTIES=false

######################################
# esb-dev
# subversion:
# svn list  https://svn.service.emory.edu:8443/repos/emoryoit/deployment/esb/dev/
#
######################################

if [ "$COPY_JAR" = "true" ]; then

    if [ -z "$1" ]; then
        echo "Please provide the jar master version number."
        exit 1
    fi
fi

MASTER_JAR_VERSION=$1

export SERVICE_CONFIGS_DIR='../../serviceConfigs'

if [ ! -d "$SERVICE_CONFIGS_DIR" ]; then

    echo "No service config directory."
    exit 2
fi

touch "../deploy/build-test/libs/CiscoAsrService/$JAR_MASTER_PREFIX-$JAR_SOURCE_VERSION.jar"

#if [ -e ../deploy/build-test/libs/CiscoAsrService/$JAR_MASTER_PREFIX-$JAR_SOURCE_VERSION.jar ]; then

if [ "$COPY_JAR" = "true" ]; then
    if [ -e $SOURCE_JAR ]; then
        echo "Jar exists."
    else
        echo "No source ciscoasr jar exists"
        exit 3
    fi
fi

echo "Service config directory found - proceeding..."

if [ "$COPY_APP_CONFIGS" = "true" ]; then
    echo "Copying app configs..."
    # esb-dev/configs - app configs

    RESULT=

    cp ../deploy/esb-dev/configs/CiscoAsr1Service.xml $SERVICE_CONFIGS_DIR/Deployments/.
    cp ../deploy/esb-dev/configs/CiscoAsr2Service.xml $SERVICE_CONFIGS_DIR/Deployments/.
    cp ../deploy/esb-dev/configs/TestSuiteApplication-CiscoAsrService.xml $SERVICE_CONFIGS_DIR/Deployments/.

fi

if [ "$COPY_TEST_CONFIGS" = "true" ]; then

    echo "Copying test configs..."
    # esb-dev/configs & esb-dev/tests - test configs

    cp ../deploy/esb-dev/tests/TestSuite-CiscoAsrService.xml $SERVICE_CONFIGS_DIR/TestSuites/CiscoAsrService/.
    cp ../deploy/esb-dev/tests/TestSuite-CiscoAsrService.xslt $SERVICE_CONFIGS_DIR/TestSuites/CiscoAsrService/.

fi

if [ "$COPY_TEST_PROPERTIES" = "true" ]; then
    echo "Copying test  properties..."
    # esb-dev/props - test properties

    cp ../deploy/esb-dev/props/TestSuiteApplication-CiscoAsrService.properties $SERVICE_CONFIGS_DIR/props/CiscoAsrService/.
fi

if [ "$COPY_JAR" = "true" ]; then
    echo "Copying CiscoAsrService jar..."
    # esb-dev jar

    cp $SOURCE_JAR $SERVICE_CONFIGS_DIR/Jars/CiscoAsr1Service/$JAR_MASTER_PREFIX-$MASTER_JAR_VERSION.jar
fi

############################################
# svn commit
############################################


exit 1

# $SERVICE_CONFIGS_DIR/Deployments

cd $SERVICE_CONFIGS_DIR/Deployments
#svn commit -m "Pushing latest app configs" CiscoAsr1Service.xml CiscoAsr2Service.xml

# $SERVICE_CONFIGS_DIR/TestSuites/CiscoAsrService

cd $SERVICE_CONFIGS_DIR/TestSuites/CiscoAsrService
#svn commit -m "Pushing latest test configs" TestSuiteApplication-CiscoAsr1Service.xml TestSuiteApplication-CiscoAsr2Service.xml TestSuite-CiscoAsr1Service-minimal.xml TestSuite-CiscoAsr2Service-minimal.xml

# $SERVICE_CONFIGS_DIR/props/CiscoAsrService
cd $SERVICE_CONFIGS_DIR/props/CiscoAsrService

#svn commit -m "Pushing latest test properties" TestSuiteApplication-CiscoAsr1Service.properties TestSuiteApplication-CiscoAsr2Service.properties
