#!/bin/sh

export G=/mnt/e/Workspaces/Surge
export D=/mnt/e/Workspaces/Surge/emory-network-ops-service
export S=NetworkOperationsService

#cp -r $G/emory-greeting-service/deploy/esb-dev   $D/deploy
#cp -r $G/emory-greeting-service/deploy/esb-test  $D/deploy
#cp -r $G/emory-greeting-service/deploy/esb-stage $D/deploy
#cp -r $G/emory-greeting-service/deploy/esb-prod  $D/deploy
#
#mv $D/deploy/esb-dev/libs/GreetingService   $D/deploy/esb-dev/libs/$S
#mv $D/deploy/esb-test/libs/GreetingService  $D/deploy/esb-test/libs/$S
#mv $D/deploy/esb-stage/libs/GreetingService $D/deploy/esb-stage/libs/$S
#mv $D/deploy/esb-prod/libs/GreetingService  $D/deploy/esb-prod/libs/$S
#
#sed -i "" -e "s/GreetingService/$S/g" $D/deploy/esb-dev/libs/$S/jndi.properties
#sed -i "" -e "s/GreetingService/$S/g" $D/deploy/esb-test/libs/$S/jndi.properties
#
#sed -e "s/GreetingService/$S/g" $G/emory-greeting-service/Dockerfile > $D/Dockerfile
#
#mkdir $D/deploy/build-test/servicegen-configs
#cp $G/emory-greeting-service/deploy/build-test/servicegen-configs/build.webservice.xml $D/deploy/build-test/servicegen-configs
#cp $G/emory-greeting-service/deploy/build-test/servicegen-configs/build.default.properties $D/deploy/build-test/servicegen-configs
#sed -e "s/GreetingService/$S/g" \
#    -e "s/actionables=Greeting/#actionables=/" \
#        $G/emory-greeting-service/deploy/build-test/servicegen-configs/greeting.properties > $D/deploy/build-test/servicegen-configs/$S.properties
#sed -e "s/emory-greeting-service-dev/emory-emailvalidation-service-dev/g" \
#    -e "s/emory-greeting-service-test/emory-emailvalidation-service-test/" \
#    -e "s/emory-greeting-service-stage/emory-emailvalidation-service-stage/" \
#    -e "s/emory-greeting-service-prod/emory-emailvalidation-service-prod/" \
#        $G/emory-greeting-service/deploy/build-test/servicegen-configs/build.custom.properties > $D/deploy/build-test/servicegen-configs/build.custom.properties
