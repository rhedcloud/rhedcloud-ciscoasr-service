#!/bin/sh
# File has been changed to Unix LF

############### pipeline variables #################
export OPENEAI_HOME=$HOME/deploy/build-test
export OPENEAI_JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
export BITBUCKET_BUILD_NUMBER=1.0
export BITBUCKET_BRANCH=master
####################################################

#pipelines:
#  default:

################### step ##################
#     - step:
#        name: Build NetworkOpsService ESB service
#        caches:
#        - maven
#        script:

cat /etc/issue
mvn -B -DskipTests package

# artifacts:
# - target/**

################### step ##################
#     - step:
#        name: Integration tests
#        max-time: 90
#        caches:
#        - maven
#        script:
##### TODO: Integration Tests

################### step ##################
# - step:
#        name: Deploy and test locally
#        script:


cp target/*.jar deploy/build-test/libs/CiscoAsrService
cp -R deploy/* $HOME/deploy

cd
#git clone https://${BB_AUTH_STRING}@bitbucket.org/itarch/emory-aws-pipeline-scripts.git

. ~/.profile   #source .profile
# activemq start
sleep 5
# tail -1000 /opt/activemq/apache-activemq-5.15.2/data/activemq.log
export OPENEAI_HOME=$HOME/deploy/build-test
cd $OPENEAI_HOME
echo Current dir: `pwd`
mkdir logs

# Start service
./invokeTarget.sh CiscoAsr1Service
./invokeTarget.sh CiscoAsr2Service

sleep 10


if [ -r System.out ]; then tail -1000 System.out ; fi
 tail -1000 logs/CiscoAsrService.log
# Start test suite
./invokeTarget.sh CiscoAsrServiceTestSuite
# Wait for completion and check for failure
sleep 10
tail -1000 logs/TestSuiteApplication-CiscoAsrService.log
tail -1000 TestSuite-CiscoAsrService-summary.xml
