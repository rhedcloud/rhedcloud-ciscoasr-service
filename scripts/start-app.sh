#!/bin/sh -x

cd /mnt/e/Workspaces/Surge/emory-network-ops-service

pwd

CLASSPATH=/mnt/e/Workspaces/Surge/emory-network-ops-service/.:/mnt/e/Workspaces/Surge/emory-network-ops-service/lib/*:/mnt/e/Workspaces/Surge/emory-network-ops-service/lib-external/*:/mnt/e/Workspaces/Surge/emory-network-ops-service/src/main/resources:/mnt/e/Workspaces/Surge/emory-network-ops-service/target/classes
properties-file=/mnt/e/Workspaces/Surge/emory-network-ops-service/deploy/build-test/props/NetworkOpsService.properties

#set CLASSPATH=.:./lib
#set cwd=/mnt/e/Workspaces/Surge/emory-network-ops-service

java -cp "${CLASSPATH}" -Duser.dir="/mnt/e/Workspaces/Surge/emory-network-ops-service/deploy/build-test"  org.openeai.jms.consumer.MessageConsumerClient "/mnt/e/Workspaces/Surge/emory-network-ops-service/deploy/build-test/props/NetworkOpsService.properties"

