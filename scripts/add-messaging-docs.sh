#!/bin/bash

# 1) Park yourself in emory-ciscoasr-service/deploy/build-test/
# 2) Create configs/messaging.save and eos.save directories, just in case!

pushd ../deploy/build-test

echo `pwd`

mkdir -p configs/messaging/Environments/Examples/EnterpriseObjects/3.0/edu/emory/Resources/1.0
mkdir -p configs/messaging/Environments/Examples/EnterpriseObjects/3.0/edu/emory/Network/1.0
mkdir -p configs/messaging/Environments/Examples/EnterpriseObjects/3.0/org/openeai/Resources/1.0
mkdir -p ./eos/org/openeai/TestSuite/1.0/
mkdir -p ./eos/org/openeai/Resources/1.0/
mkdir -p ./eos/3.0/org/openeai/Resources/1.0/

cp ./eos/edu/emory/Resources/1.0/*.xml ./configs/messaging/Environments/Examples/EnterpriseObjects/3.0/edu/emory/Resources/1.0/.
cp ./eos/edu/emory/Network/1.0/*.xml ./configs/messaging/Environments/Examples/EnterpriseObjects/3.0/edu/emory/Network/1.0/.

# Artifacts not found in moas dist - some other?
cp ./configs/messaging.save/Environments/Examples/EnterpriseObjects/3.0/org/openeai/Resources/1.0/ControlAreaEO.xml  ./configs/messaging/Environments/Examples/EnterpriseObjects/3.0/org/openeai/Resources/1.0/ControlAreaEO.xml
cp ./eos.save/org/openeai/TestSuite/1.0/TestSuiteEO.xml ./eos/org/openeai/TestSuite/1.0/.
cp ./eos.save/org/openeai/Resources/1.0/ControlAreaEO.xml ./eos/org/openeai/Resources/1.0/
cp ./eos.save/3.0/org/openeai/Resources/1.0/ControlAreaEO.xml ./eos/3.0/org/openeai/Resources/1.0/ControlAreaEO.xml
