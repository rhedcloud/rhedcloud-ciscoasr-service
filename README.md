# This project has been closed and is no longer supported
##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

##Description
The Emory Cisco Asr Service implements Static Nat and VPN Connection lifecycle management. It's indirectly a part of the Emory AWS Account Provisioning chain in that it is a service used by Emory Network Operations Service during its VPN Connection and Static Nat provisioning orchestration.

This service interacts directly with the Cisco Asr 1002 routers using the tail-f Java NETCONF Client application programming interface.

### Basic Service Architecture
There are two instances of the Cisco Asr Service, each dedicated to a single router. The same application uses two deployment descriptors, a.k.a. application configuration files to assign each instance to its associated physical router. Router specific configuration includes router url, port number, and credentials, and are specified below.
### Message Flow
#### Static Nat
CaStaticNatRequestCommand -> CiscoAsr1002NetconfStaticNatProvider
#### VPN Connection
CaVpnConnectionRequestCommand -> CiscoAsr1002NetconfVpnConnectionProvider

### Components
* CaStaticNatRequestCommand : Handles StaticNatRequestCommand requests and delegates request operations to the associated provider shown in the message flow. The provider uses the tail-f JNC api to communicate with the configured router to perform router object functions.
* CaVpnConnectionRequestCommand : Handles VpnConnectionRequestCommand requests and delegates request operations to the associated provider shown in the message flow. The provider uses the tail-f JNC api to communicate with the configured router to perform router object functions.

### Setup instructions:
As mentioned above, there are two deployment descriptors (app configuration files) associated with this service, one for each instance:
* CiscoAsr1Service.xml
* CiscoAsr2Service.xml

All Configuration specified below must be performed for both commands, in the Command/Configuration section of each.

#### Router Specific Configuration
Each descriptor contains router specific configuration:

* netconfRouterHost: URL for the assigned router; used by the JNC API to connect to the router via SSH
* netconfRouterPort: Port number for the assigned router; again, for use by the JNC API to connect to the router via SSH
* adminUsername: User for SSH authentication to router
* adminPassword: Password for SSH authentication to router
* tunnelNumber: Ordinal number of each router; used when constructing router-specific YANG models used in the protocol

Configuration specifics:

    <PropertyConfig name="GeneralProperties" refresh="false">
        <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
             .
             .
             .
        <Property>
            <PropertyName>netconfRouterHost</PropertyName>
            <PropertyValue>csrappdev1.cc.emory.edu</PropertyValue>
        </Property>
        <Property>
            <PropertyName>netconfRouterPort</PropertyName>
            <PropertyValue>830</PropertyValue>
        </Property>
        <Property>
            <PropertyName>adminUsername</PropertyName>
            <PropertyValue>aws-ipsec</PropertyValue>
        </Property>
        <Property>
            <PropertyName>adminPassword</PropertyName>
            <PropertyValue>CredentialRedactedAndRotated!</PropertyValue>
        </Property>
        <Property>
            <PropertyName>tunnelNumber</PropertyName>
            <PropertyValue>1</PropertyValue>
        </Property>
             .
             .
             .
    </PropertyConfig>

#### General Configuration
Operational configuration items used by the business logic in the providers. There are command-level retries, error-level retries and router "settle" times that are configured to take into account router behaviors beyond the control of the service:

* waitForOperationRetryCount: Configuration requests to the routers often require multiple attempts based on return status from a NETCONF config command - this is the number of retries to attempt to complete an operation before signalling an error situtation
* waitForOperationRetryWaitInMilliseconds: This is the sleep period between operation retries
* operationErrorRetryCount: Some configuration requests return NETCONF error status indicating the the router is busy - for these types of errors, retries occur and this is the retry count config
* operationErrorRetryWaitInMilliseconds: This is the sleep period between error retries
* routerSettlingWaitInMilliseconds: VPN Connection configuration on the router causes the router to lock out all subsequent configuration requests until the configuration has been "processed." This configuration value compensates for that unavailability of the router firmware. Thirty seconds seems to be the optimal "settling" time, however, if new router firmware is installed, or other external factors exist, this value can be used to account for any changes in router processing time.

Configuration specifics:

    <PropertyConfig name="GeneralProperties" refresh="false">
             .
             .
             .
        <Property>
            <PropertyName>waitForOperationRetryCount</PropertyName>
            <PropertyValue>5</PropertyValue>
        </Property>
        <Property>
            <PropertyName>waitForOperationRetryWaitInMilliseconds</PropertyName>
            <PropertyValue>2000</PropertyValue>
        </Property>
        <Property>
            <PropertyName>operationErrorRetryCount</PropertyName>
            <PropertyValue>50</PropertyValue>
        </Property>
        <Property>
            <PropertyName>operationErrorRetryWaitInMilliseconds</PropertyName>
            <PropertyValue>10000</PropertyValue>
        </Property>
        <Property>
            <PropertyName>routerSettlingWaitInMilliseconds</PropertyName>
            <PropertyValue>30000</PropertyValue>
        </Property>
             .
             .
             .
    </PropertyConfig>

#### Command Locking
The NETCONF protocol on the routers is single session (thread) based and is enforced at the Cisco Asr Service level via OpenEAI framework's command locking mechanism. A configuration item was added to aid development, but should be set to "true."

Lock retry and sleep intervals can be set here as well.

These property settings are in the following "GeneralProperties" sections under Command/Configuration (there are five instances):

    <PropertyConfig name="GeneralProperties" refresh="false">
        <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
            .
            .
            .
        <Property>
            <PropertyName>useLock</PropertyName>
            <PropertyValue>true</PropertyValue>
        </Property>
        <Property>
            <PropertyName>obtainLockMaxRetries</PropertyName>
            <PropertyValue>5</PropertyValue>
        </Property>
        <Property>
            <PropertyName>obtainLockRetrySleepInterval</PropertyName>
            <PropertyValue>2000</PropertyValue>
        </Property>
    </PropertyConfig>

#### Deployments
There are two instances of CiscoAsrServices deployed in the same container here: CiscoAsr1Service and CiscoAsr2Service.  Each has its own deployment appConfig file ), nothing magical.  These two .aar file are generated by the same openeai-servicegen just like any other deployment without any modification.  The only other difference here is that we added another .aar file emory-healthcheck-webservice-1.0.aar.  This provides a single url used by health checking, which in turn kick-starts each of the  two instances.

The emory-healthcheck-webservice search a file named healthcheck.properties in its classpath.  The file has a single property named urls, its values are comma delimited urls.

#### AWS Secrets for router credentials
For a site's deploy only instance of CiscoAsrService, a user, role and access key set needs to be created for the router credentials. See the script file ```README-base-00.sh``` in the RHEDcloud repository ```https://sbrodeur@bitbucket.org/rhedcloud/rhedcloud-aws-admin-master-cfn.git```

Procedure:
* Create the Router Credential Secrets for all environments. Currently, only DEV and PROD are supported for CiscoAsrService. The secret name uses the following format: ```ciscoasr-creds-<ENV>```. Go to the AWS Secrets Manager Console and start with DEV: ```ciscoasr-creds-DEV```
    * Use DefaultEncryptionKey
    * The secrets themselves are plain text and represent Java Properties:
```text
        router.username=fix-me-username
        router.password=fix-me-password
```

* Once the secret has been created, retrieve its AWS ARN.  It will look something like this:

    arn:aws:secretsmanager:us-east-1:12345678901:secret:ciscoasr-creds-DEV-abcdX1

* Run the user, policy and access key creation script. Setup the script variables for default region and admin account by adding them to your environment, or put them in a temp script along with the creation script.

* Once the user, policy and access key are created, open the AWS IAM Console and locate the ```ciscoasr-service-policy``` and update trailing ARN values from the secrets ARN retrieved above.

* Here is a sample of the creation script that's been modified for Rice site deployment:

```bash
cat <<EOF > ciscoasr-service--policy.json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "secretsmanager:GetSecretValue",
            "Resource": [
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-DEV-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R1-NAT-PROD-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R2-NAT-PROD-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R1-VPN-PROD-XXXXX",
                "arn:aws:secretsmanager:$AWS_DEFAULT_REGION:$AWS_ADMIN_ACT:secret:ciscoasr-creds-R2-VPN-PROD-XXXXX"
            ]
        }
    ]
}
EOF
(
aws iam create-user --user-name ciscoasr-service

aws iam create-policy \
  --policy-name RiceCiscoAsrServiceLabRouterAccess \
  --policy-document file://ciscoasr-service--policy.json \
  --description "Policy to allow access to Cisco ASR Lab Router Credentials."
aws iam attach-user-policy --user-name ciscoasr-service \
  --policy-arn "arn:aws:iam::$AWS_ADMIN_ACT:policy/RiceCiscoAsrServiceLabRouterAccess"

aws iam create-access-key --user-name ciscoasr-service > ciscoasr-service--accesskey

) &>ciscoasr-service--user.log

```
#### Router Version Algorithm
The method for determining router version is essentially two steps:
* Send a NETCONF hello command using ganymed-ssh2 library, directly after JNC connect
* Search the returned capabilities list for specific capabilities at known revisions

#####The NETCONF hello command to send:

```
private String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?>\n" +
            "<hello><capabilities>\n" +
            "        <capability>urn:ietf:params:netconf:capability:startup:1.0</capability>\n" +
            "   </capabilities>\n" +
            "</hello>]]>]]>\n" +
            " ";

```

#####Capabilities to Router Version Map

* Version IOS-XE 16.6.2 (1662)

        #Cisco-IOS-XE-tunnel: 2017-07-11
        #Cisco-IOS-XE-crypto: 2017-05-10
        #Cisco-IOS-XE-native: 2017-08-30
        #Cisco-IOS-XE-bgp: 2017-04-28

* Version IOS-XE 16.9.4 (1694)

        #Cisco-IOS-XE-tunnel: 2017-08-28
        #Cisco-IOS-XE-crypto: 2019-04-25
        #Cisco-IOS-XE-native: 2018-07-27
        #Cisco-IOS-XE-bgp: 2019-01-09

#### OwnerId Overloading
OwnerId has been changed to a colon separated two value field. The first value represents the Cloud Service Provider discriminator, and is optional. The second field is the original Vpc Id from version v1.0

#####Parsing Rules
* If a colon ":" is present in the OwnerId value, the text to the left of the colon is parsed and used to represent the Cloud Service Provider discriminator
* If a colon is present and the text to the left of the colon is empty (Owner Id starts with a colon), it is considered an error, and an exception is thrown
* If the text to the left of the colon doesn't match a configured CSP map value entry, it is considered an illegal discriminator value and an exception is thrown
* If no colon is present, the discriminator value is defaulted to "AWS" for backward compatibility issues and the entire Owner Id is used as the incoming Vpc Id
* If a colon is present, and the text to the left of the colon is a valid CSP discriminator, the text to the right of the colon is used as the incoming Vpc Id

#### Moving Router Host Names To IP Addresses
Bsvcscox1 = 10.254.4.221

Bsvcsws1 = 10.254.4.220
