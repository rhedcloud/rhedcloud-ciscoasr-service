package edu.emory.it.service.ca.provider.mapper;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.mapper._1662._1662AwsVpnMapper;
import edu.emory.it.service.ca.provider.mapper._1662._1662GcpVpnMapper;
import edu.emory.it.service.ca.provider.mapper._1662._1662StaticNatMapper;
import edu.emory.it.service.ca.provider.mapper._1693._1693AwsVpnMapper;
import edu.emory.it.service.ca.provider.mapper._1693._1693GcpVpnMapper;
import edu.emory.it.service.ca.provider.mapper._1693._1693StaticNatMapper;

public class MapperFactory {

    public static VpnMapper createVpnMapper(String mapperType) throws ProviderException {
        VpnMapper mapper;

        switch (mapperType) {
            case "1662AWS":
                mapper = new _1662AwsVpnMapper();
                break;

            case "1693AWS":
                mapper = new _1693AwsVpnMapper();
                break;
            case "1662GCP":
                mapper = new _1662GcpVpnMapper();
                break;

            case "1693GCP":
                mapper = new _1693GcpVpnMapper();
                break;
            default:
                throw new ProviderException("Unsupported router/CSP: " + mapperType);
        }

       return mapper;
    }

    public static boolean isMapperOfType(VpnMapper vpnMapper, String mapperType) {
        if (vpnMapper != null && vpnMapper.getClass().getSimpleName().contains(mapperType)) {
            return true;
        } else {
            return false;
        }
    }

    public static StaticNatMapper createStaticNatMapper(String mapperType) throws ProviderException {
        StaticNatMapper mapper = null;

        switch (mapperType) {
            case "1662":
                mapper = new _1662StaticNatMapper();
                break;

            case "1693":
                mapper = new _1693StaticNatMapper();
                break;
            default:
                throw new ProviderException("Unsupported YANG version/CSP: " + mapperType);
        }

        return mapper;
    }

    public static boolean isMapperOfType(StaticNatMapper staticNatMapper, String mapperType) {
        if (staticNatMapper != null && staticNatMapper.getClass().getSimpleName().contains(mapperType)) {
            return true;
        } else {
            return false;
        }
    }

}
