package edu.emory.it.service.ca.provider.util;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.BgpPrefixes;
import edu.emory.moa.objects.resources.v1_0.BgpState;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecTransformSet;
import edu.emory.moa.objects.resources.v1_0.CryptoIsakmpProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoKeyring;
import edu.emory.moa.objects.resources.v1_0.LocalAddress;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.TunnelInterface;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

public class AppConfigUtil {

    private static final Logger logger = OpenEaiObject.logger;
    private static String LOGTAG = "[AppConfigUtil] ";

    private static final String STATIC_NAT_CLASS_NAME = "staticnat.v1_0";
    private static final String STATIC_NAT_QUERY_SPECIFICATION_CLASS_NAME = "StaticNatQuerySpecification.v1_0";
    private static final String VPN_CONNECTION_CLASS_NAME = "vpnconnection.v1_0";
    private static final String VPN_CONNECTION_QUERY_SPECIFICATION_CLASS_NAME = "vpnconnectionqueryspecification.v1_0";
    private static final String LOCAL_ADDRESS_CLASS_NAME = "localaddress.v1_0";
    private static final String CRYPTO_IPSEC_PROFILE_CLASS_NAME = "cryptoipsecprofile.v1_0";
    private static final String CRYPTO_IPSEC_TRANSFORM_SET_CLASS_NAME = "cryptoipsectransformset.v1_0";
    private static final String CRYPTO_ISAKMP_PROFILE_CLASS_NAME = "cryptoisakmpprofile.v1_0";
    private static final String CRYPTO_KEYRING_CLASS_NAME = "cryptokeyring.v1_0";
    private static final String TUNNEL_INTERFACE_CLASS_NAME = "tunnelinterface.v1_0";
    private static final String BGP_STATE_CLASS_NAME = "bgpstate.v1_0";
    private static final String BGP_PREFIXES_CLASS_NAME = "bgpprefixes.v1_0";
    private static final String MATCH_IDENTITY_CLASS_NAME = "matchidentity.v1_0";


    public static StaticNat getStaticNatFromConfiguration(AppConfig appConfig) throws ProviderException {
        StaticNat staticNat;

        try {
            staticNat = (StaticNat) appConfig.getObject(STATIC_NAT_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting static nat object from app config for class name " + STATIC_NAT_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return staticNat;
    }

    public static StaticNatQuerySpecification getStaticNatQuerySpecificationFromConfiguration(AppConfig appConfig) throws ProviderException {
        StaticNatQuerySpecification staticNatQuerySpec;

        try {
            staticNatQuerySpec = (StaticNatQuerySpecification) appConfig.getObject(STATIC_NAT_QUERY_SPECIFICATION_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting static nat query specification object from app config for class name " + STATIC_NAT_QUERY_SPECIFICATION_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return staticNatQuerySpec;
    }

    public static VpnConnectionQuerySpecification getVpnConnectionQuerySpecificationFromConfiguration(AppConfig appConfig) throws ProviderException {
        VpnConnectionQuerySpecification vpnConnectionQuerySpecification;

        try {
            vpnConnectionQuerySpecification = (VpnConnectionQuerySpecification) appConfig.getObject(VPN_CONNECTION_QUERY_SPECIFICATION_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection query specification object from app config for class name " + VPN_CONNECTION_QUERY_SPECIFICATION_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
        return vpnConnectionQuerySpecification;
    }

    public static VpnConnection getVpnConnectionFromConfiguration(AppConfig appConfig) throws ProviderException {
        VpnConnection vpnConnection;

        try {
            vpnConnection = (VpnConnection) appConfig.getObject(VPN_CONNECTION_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection object from app config for class name " + VPN_CONNECTION_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return vpnConnection;
    }

    public static LocalAddress getLocalAddressProfileFromConfiguration(AppConfig appConfig) throws ProviderException {
        LocalAddress localAddress;

        try {
            localAddress = (LocalAddress) appConfig.getObject(LOCAL_ADDRESS_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting local address object from app config for class name " + LOCAL_ADDRESS_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return localAddress;
    }

    public static CryptoIpsecProfile getCryptoIpsecProfileFromConfiguration(AppConfig appConfig) throws ProviderException {
        CryptoIpsecProfile cryptoIpsecProfile;

        try {
            cryptoIpsecProfile = (CryptoIpsecProfile) appConfig.getObject(CRYPTO_IPSEC_PROFILE_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting crypto ipsec profile object from app config for class name " + CRYPTO_IPSEC_PROFILE_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return cryptoIpsecProfile;
    }

    public static CryptoIpsecTransformSet getCryptoIpsecTransformSetFromConfiguration(AppConfig appConfig) throws ProviderException {
        CryptoIpsecTransformSet cryptoIpsecTransformSet;

        try {
            cryptoIpsecTransformSet = (CryptoIpsecTransformSet) appConfig.getObject(CRYPTO_IPSEC_TRANSFORM_SET_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting crypto ipsec transform set object from app config for class name " + CRYPTO_IPSEC_TRANSFORM_SET_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return cryptoIpsecTransformSet;
    }

    public static CryptoIsakmpProfile getCryptoIsakmpProfileFromConfiguration(AppConfig appConfig) throws ProviderException {
        CryptoIsakmpProfile cryptoIsakmpProfile;

        try {
            cryptoIsakmpProfile = (CryptoIsakmpProfile) appConfig.getObject(CRYPTO_ISAKMP_PROFILE_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting crypto isakmp profile object from app config for class name " + CRYPTO_ISAKMP_PROFILE_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return cryptoIsakmpProfile;
    }

    public static CryptoKeyring getCryptoKeyringFromConfiguration(AppConfig appConfig) throws ProviderException {
        CryptoKeyring cryptoKeyring;

        try {
            cryptoKeyring = (CryptoKeyring) appConfig.getObject(CRYPTO_KEYRING_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting crypto keyring object from app config for class name " + CRYPTO_KEYRING_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return cryptoKeyring;
    }

    public static TunnelInterface getTunnelInterfaceFromConfiguration(AppConfig appConfig) throws ProviderException {
        TunnelInterface tunnelInterface;

        try {
            tunnelInterface = (TunnelInterface) appConfig.getObject(TUNNEL_INTERFACE_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting tunnel interface object from app config for class name " + TUNNEL_INTERFACE_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return tunnelInterface;
    }

    public static BgpState getBgpStateFromConfiguration(AppConfig appConfig) throws ProviderException {
        BgpState bgpState;

        try {
            bgpState = (BgpState) appConfig.getObject(BGP_STATE_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting bgp state object from app config for class name " + BGP_STATE_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return bgpState;
    }

    public static BgpPrefixes getBgpPrefixesFromConfiguration(AppConfig appConfig) throws ProviderException {
        BgpPrefixes bgpPrefixes;

        try {
            bgpPrefixes = (BgpPrefixes) appConfig.getObject(BGP_PREFIXES_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting bgp prefixes object from app config for class name " + BGP_PREFIXES_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return bgpPrefixes;
    }

    public static MatchIdentity getMatchIdentityFromConfiguration(AppConfig appConfig) throws ProviderException {
        MatchIdentity matchIdentity;

        try {
            matchIdentity = (MatchIdentity) appConfig.getObject(MATCH_IDENTITY_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting match identity object from app config for class name " + "matchidentity.v1_0";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return matchIdentity;
    }


    private static String getLogtag() {
        return LOGTAG;
    }
}
