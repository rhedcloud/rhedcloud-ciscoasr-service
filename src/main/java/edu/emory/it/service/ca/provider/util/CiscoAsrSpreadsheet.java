package edu.emory.it.service.ca.provider.util;

import edu.emory.it.service.ca.provider.validator.VpnConnectionOperationContext;

import java.util.Map;

public interface CiscoAsrSpreadsheet {

    Map<String, String> buildSpreadsheetValues(String vpcId, Integer vpnId, String tunnelNumberAsString, VpnConnectionOperationContext operationContext);

    String getTunnelDescriptionPrefix();

    Integer getVpnIdFromTunnelName(String tunnelName);

    String getVpcIdFromTunnelDescription(String tunnelDescription);
}
