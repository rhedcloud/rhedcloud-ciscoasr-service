package edu.emory.it.service.ca.provider;

public enum ProviderDevice {
    ROUTER1("Cisco ASR1002 Router1", "1"),
    // With Orchestration service refactor, the tunnel index is ALWAYS 0
    ROUTER2("Cisco ASR1002 Router2", "2");

    private String name;
    private String tunnelNumber;

    ProviderDevice(String name, String tunnelNumber) {
        this.name = name;
        this.tunnelNumber = tunnelNumber;
    }

    public String getName() {
        return name;
    }

    public String getTunnelNumber() {
        return tunnelNumber;
    }
}
