package edu.emory.it.service.ca;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.util.LockWrapper;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.SyncCommand;
import org.openeai.jms.consumer.commands.SyncCommandImpl;
import org.openeai.utils.lock.Lock;
import org.openeai.utils.lock.LockException;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Properties;

public class CiscoAsrNotificationSyncCommand extends SyncCommandImpl implements SyncCommand {

    private static final String PROVIDER_CLASS_NAME = "caNotificationSyncProviderClassName";
    private static final String PROP_ROUTER_NUMBER = "routerNumber";
    private static final String PROP_USE_LOCK = "useLock";
    private static final String PROP_OBTAIN_LOCK_MAX_WAIT_TIME = "obtainLockMaxWaitTime";
    private static final String PROP_OBTAIN_LOCK_RETRY_SLEEP_INTERVAL = "obtainLockRetrySleepInterval";

    private LockWrapper lockWrapper;
    private Boolean useLock;
    private Lock commandLock;
    private Integer obtainLockMaxWaitTime;
    private Integer obtainLockRetrySleepInterval;
    private NotificationSyncProvider notificationSyncProvider;

    public CiscoAsrNotificationSyncCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        logger.info(getLogtag() + "Initializing...");
        try {
            try {
                Properties generalProperties = getAppConfig().getProperties("GeneralProperties");
                setProperties(generalProperties);
                notificationSyncProvider = initializeProvider();
            } catch (EnterpriseConfigurationObjectException e) {
                String errMsg = getLogtag() + "Error retrieving 'GeneralProperties' from AppConfig: The exception is: " + e.getMessage();
                logger.fatal(getLogtag() + errMsg, e);
                throw new InstantiationException(errMsg);
            }

            setupUseLockConfigValue();
            setupCommandLockConfigValues();

        } catch (InstantiationException e) {
            String errMsg = "Failed to initialize notification sync provider";
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        }

    }

    @Override
    public void execute(int messageNumber, Message aMessage) throws CommandException {
        if (useLock) {
            lockWrapper = acquireCommandLock();
            try {
                sendSaveConfigMessage(messageNumber, aMessage);
            } catch (CommandException e) {
                throw e;
            } finally {
                // Make sure the command lock is released.
                try {
                    lockWrapper.release();
                } catch (LockException e) {
                    // An error occurred releasing the command lock. Log it and
                    // throw
                    // and exception.
                    String errMsg = "An error occurred releasing the command lock. " + "The exception is: " + e.getMessage();
                    logger.fatal(getLogtag() + errMsg, e);
                    throw new CommandException(errMsg);
                }
            }

        } else {
            sendSaveConfigMessage(messageNumber, aMessage);
        }

    }

    protected LockWrapper acquireCommandLock() throws CommandException {

        if (useLock) {
            lockWrapper = new LockWrapper(commandLock, obtainLockMaxWaitTime, obtainLockRetrySleepInterval, getLogtag());
            try {
                lockWrapper.acquireLock();
            } catch (LockException e) {
                String errMsg = "Error acquiring command lock";
                logger.error(getLogtag(), e);
                throw new CommandException(errMsg, e);
            }
        } else {
            lockWrapper = new LockWrapper();
        }

        return lockWrapper;
    }

    private void sendSaveConfigMessage(int messageNumber, Message aMessage) throws CommandException {

        Document inDoc;

        logger.info("Received cisco asr service sync message");
        try {
            inDoc = initializeInput(messageNumber, aMessage);
        } catch (JMSException jmse) {
            String errMsg = "Exception occurred processing input message in " + "org.openeai.jms.consumer.commands.Command.  Exception: "
                    + jmse.getMessage();
            logger.error(getLogtag() + errMsg);
            throw new CommandException(getLogtag() + errMsg);
        }

        logger.info("Notification Sync Command message sent in is: \n" + getMessageBody(inDoc));

        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Get messageAction and messageObject attributes from the
        // ControlArea element.
        String msgAction = eControlArea.getAttribute("messageAction").getValue();
        String msgObject = eControlArea.getAttribute("messageObject").getValue();

        logger.info(getLogtag() + "Notification sync command for message object: " + msgObject + " - action: " + msgAction);

        if (!msgObject.equalsIgnoreCase("VpnConnection")
                && !msgObject.equalsIgnoreCase("StaticNat")
                && !msgObject.equalsIgnoreCase("VpnConnectionRequisition")) {
            logger.warn(getLogtag() + "Unknown message object type: " + msgObject);
            // Save config anyway...
        }

        // For now, sync command objects unneeded
        logger.info(getLogtag() + "Invoking notification sync command's sendSaveConfig");
        if (!notificationSyncProvider.sendSaveConfig()) {
            String errMsg = "Send save config failed";
            logger.error(getLogtag() + errMsg);
            // TODO: Send sync error
        }

    }

    private void setupUseLockConfigValue() throws InstantiationException {

        try {
            String useLockAsString = getAppConfig().getProperties("GeneralProperties").getProperty(PROP_USE_LOCK);
            if ("true".equalsIgnoreCase(useLockAsString) || "false".equalsIgnoreCase(useLockAsString)) {
                useLock = Boolean.valueOf(useLockAsString);
            } else {
                String errMsg = "The 'useLock' property must be present, and have a value of 'true' or 'false'";
                logger.error(getLogtag() + errMsg);
                throw new InstantiationException(errMsg);
            }
        } catch (Exception e) {
            String errMsg = "Failed to obtain 'useLock' property from config";
            logger.error(getLogtag() + errMsg, e);
            throw new InstantiationException(errMsg);
        }
    }

    private void setupCommandLockConfigValues() throws InstantiationException {
        if (useLock) {
            try {
                Properties generalProperties = getAppConfig().getProperties("GeneralProperties");
                String routerNumberAsString = generalProperties.getProperty(PROP_ROUTER_NUMBER);
                String commandLockName = "CiscoAsr" + routerNumberAsString + "CommandLock";
                Lock lock = (Lock) getAppConfig().getObject(commandLockName);
                commandLock = lock;
                obtainLockMaxWaitTime = Integer.valueOf(generalProperties.getProperty(PROP_OBTAIN_LOCK_MAX_WAIT_TIME));
                obtainLockRetrySleepInterval = Integer.valueOf(generalProperties.getProperty(PROP_OBTAIN_LOCK_RETRY_SLEEP_INTERVAL));
            } catch (EnterpriseConfigurationObjectException ecoe) {
                // An error occurred retrieving an object from AppConfig. Log it
                // and
                // throw an exception.
                String errMsg = "An error occurred retrieving a Lock object from " + "AppConfig: " + ecoe.getMessage();
                logger.fatal(getLogtag() + errMsg, ecoe);
                throw new InstantiationException(errMsg);
            }
        }

    }

    private NotificationSyncProvider initializeProvider() throws InstantiationException {

        String providerClassName = getProperties().getProperty(PROVIDER_CLASS_NAME);
        if (providerClassName == null || providerClassName.equals("")) {
            String errMsg = getLogtag() + "No " + PROVIDER_CLASS_NAME + " property specified. Can't continue.";
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            logger.info(getLogtag() + "Getting provider class for name: " + providerClassName);
            Class<?> providerClass = Class.forName(providerClassName);

            logger.info(getLogtag() + "Making a new instance of " + providerClass.getName());
            NotificationSyncProvider providerInstance = (NotificationSyncProvider) providerClass.newInstance();

            logger.info(getLogtag() + "Initializing provider: " + providerInstance.getClass().getName());
            providerInstance.init(getAppConfig());
            logger.info(getLogtag() + "Initialization complete.");

            return providerInstance;
        } catch (ClassNotFoundException e) {
            String errMsg = "Class named " + providerClassName + "not found on the classpath.  The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + "- " + errMsg);
            throw new InstantiationException(errMsg);
        } catch (IllegalAccessException e) {
            String errMsg = "An error occurred getting a class for name: " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + "- " + errMsg);
            throw new InstantiationException(errMsg);
        } catch (ProviderException e) {
            String errMsg = "An error occurred initializing the provider " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + "- " + errMsg);
            throw new InstantiationException(errMsg);
        }
    }

    private String getLogtag() {
        return "[" + this.getClass().getSimpleName() + "] ";
    }
}
