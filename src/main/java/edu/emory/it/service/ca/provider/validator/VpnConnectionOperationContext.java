package edu.emory.it.service.ca.provider.validator;

public enum VpnConnectionOperationContext {

    QUERY_REQUEST("Query", "query"),
    GENERATE_SEND("GenerateSend", "generate-send"),
    GENERATE_RESPONSE("GenerateResponse", "generate-response"),
    DELETE_REQUEST("DeleteSend", "delete-send");


    private String name;
    private String operation;

    VpnConnectionOperationContext(String name, String index) {
        this.name = name;
        this.operation = index;
    }

    public String getName() {
        return name;
    }

    public String getOperation() {
        return operation;
    }


}
