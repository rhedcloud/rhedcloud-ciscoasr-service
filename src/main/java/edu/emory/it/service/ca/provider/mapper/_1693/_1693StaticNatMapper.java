package edu.emory.it.service.ca.provider.mapper._1693;

import edu.emory.it.service.ca.provider.mapper.AbstractStaticNatMapper;
import edu.emory.it.service.ca.provider.mapper.StaticNatMapper;

/**
 * Specialization for 1693AWS static nat templates.
 *
 * The only difference from default behavior in AbstractStaticNatMapper is 3 xpaths to primary data.
 */
public class _1693StaticNatMapper extends AbstractStaticNatMapper implements StaticNatMapper {

    private String xpathLocalIp = "ip/nat/inside/source/static/nat-static-transport-list-with-vrf/local-ip";
    private String xpathGlobalIp = "ip/nat/inside/source/static/nat-static-transport-list-with-vrf/global-ip";
    private String xpathNativeTransportList = "native/ip/nat/inside/source/static/nat-static-transport-list-with-vrf";
    private String xpathTransportList = "ip/nat/inside/source/static/nat-static-transport-list-with-vrf";

    @Override
    public String xpathLocalIp() {
        return xpathLocalIp;
    }

    @Override
    public String xpathGlobalIp() {
        return xpathGlobalIp;
    }

    @Override
    public String xpathNativeTransportList() {
        return xpathNativeTransportList;
    }

    @Override
    public String xpathTransportList() { return xpathTransportList; }
}
