package edu.emory.it.service.ca;

import com.openii.openeai.commands.MessageMetaData;
import com.openii.openeai.commands.OpeniiRequestCommand;
import edu.emory.it.service.ca.provider.CiscoAsrProvider;
import edu.emory.it.service.ca.provider.util.LockWrapper;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.OpenEaiObject;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Error;
import org.openeai.utils.lock.Lock;
import org.openeai.utils.lock.LockException;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public abstract class CaRequestCommand extends OpeniiRequestCommand implements RequestCommand {
    static final Logger logger = OpenEaiObject.logger;

    private static final String PROP_ROUTER_NUMBER = "routerNumber";
    private static final String PROP_USE_LOCK = "useLock";
    private static final String PROP_OBTAIN_LOCK_MAX_WAIT_TIME = "obtainLockMaxWaitTime";
    private static final String PROP_OBTAIN_LOCK_RETRY_SLEEP_INTERVAL = "obtainLockRetrySleepInterval";

    static final String GENERATE_ACTION = "generate";

    protected CiscoAsrProvider ciscoAsrProvider;

    private Boolean useLock;
    private Lock commandLock;
    private Integer obtainLockMaxWaitTime;
    private Integer obtainLockRetrySleepInterval;

    private Document genericResponseReplyDoc;
    private ProducerPool serviceSyncProducerPool;
    ProducerPool ciscoAsrSyncCommandProducerPool;
    private String routerNumber;

    public CaRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(getLogtag() + "Initializing " + ReleaseTag.getReleaseInfo());

        try {
            Properties generalProperties = getAppConfig().getProperties("GeneralProperties");
            setProperties(generalProperties);
            routerNumber = generalProperties.getProperty("routerNumber");
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = getLogtag() + "Error retrieving 'GeneralProperties' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + errMsg, e);
            throw new InstantiationException(errMsg);
        }

        setupUseLockConfigValue();
        setupCommandLockConfigValues();

        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            genericResponseReplyDoc = xmlReader.initializeDocument(
                    getProperties().getProperty("GenericResponseDocumentUri"), getOutboundXmlValidation());
        } catch (XmlDocumentReaderException e) {
            String errMsg = getLogtag() + "Error initializing 'GenericResponseDocumentUri'.  Can't continue.";
            logger.fatal(getLogtag() + errMsg, e);
            throw new InstantiationException(e.getMessage());
        }
        if (genericResponseReplyDoc == null) {
            String errMsg = getLogtag() + "Missing 'GenericResponseDocumentUri' property in configuration document.  Can't continue.";
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        }


        // retrieve Save config sync producer from AppConfig. This object will be used to publish
        // sync messages when create/delete message actions are performed to initiate a saveConfig on the routers.
//        try {
//            ciscoAsrSyncCommandProducerPool = (ProducerPool) getAppConfig().getObject("SyncSaveConfigCommandProducer");
//        } catch (Exception e) {
//            String errMsg = "No 'SyncSaveConfigCommandProducer' PubSubProducer found in AppConfig.";
//            logger.fatal(getLogtag() + errMsg);
//            throw new InstantiationException(errMsg);
//        }

        // retrieve Sync Publisher from AppConfig. This object will be used to publish
        // sync messages when create, delete or update message actions are performed
        // in this authoritative system.
        String syncProducerName = "CiscoAsr" + routerNumber + "ServiceSyncProducer";
        try {
            serviceSyncProducerPool = (ProducerPool) getAppConfig().getObject(syncProducerName);
        } catch (Exception e) {
            logger.warn(getLogtag() + "No 'CiscoAsr1ServiceSyncProducer' PubSubProducer found in AppConfig.  "
                    + "Processing will continue but Sync Messages will not be published "
                    + "when changes are made via this Command.");
        }

        logger.info(getLogtag() + "instantiated successfully.");
    }

    protected LockWrapper acquireCommandLock() throws CommandException {

        LockWrapper lockWrapper;
        if (useLock) {
            lockWrapper = new LockWrapper(commandLock, obtainLockMaxWaitTime, obtainLockRetrySleepInterval, getLogtag());
            try {
                lockWrapper.acquireLock();
            } catch (LockException e) {
                String errMsg = "Error acquiring command lock";
                logger.error(getLogtag(), e);
                throw new CommandException(errMsg, e);
            }
        } else {
            lockWrapper = new LockWrapper();
        }

        return lockWrapper;
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        long startTime = System.currentTimeMillis();
        ciscoAsrProvider.setRouterExecutionTimeInMilliseconds(0L);

        logger.info(getLogtag() + "[execute] - beginning message number " + messageNumber + " ...");
        String errDescription;

        MessageMetaData mmd;
        try {
            mmd = initializeMetaData(messageNumber, aMessage);
        } catch (Exception e) {
            errDescription = getLogtag() + "Exception occurred processing input message in ConsumerCommand.  Exception: " + e.getMessage();
            logExecutionComplete("Unknown", e.getMessage(), startTime);
            throw new CommandException(errDescription, e);
        }

        logger.info(getLogtag() + "[execute] - Processing a: " + mmd.getMessageCategory() + "." + mmd.getMessageObject()
                + "." + mmd.getMessageAction() + "-" + mmd.getMessageType());

        // retrieve text portion of message passed in
        TextMessage msg = (TextMessage) aMessage;
        try {
            msg.clearBody(); // So we don't have to do it later...
        } catch (JMSException e) {
            logExecutionComplete(mmd.getMessageAction(), e.getMessage(), startTime);
            throw new CommandException(e.getMessage(), e);
        }

        // Verify that the message object we are dealing with is what we expect. If not, reply with an error.
        if (!isSupportedCommand(mmd.getMessageObject())) {
            errDescription = getLogtag() + "Unsupported message object: " + mmd.getMessageObject() + ". "
                    + "Require '" + getRequiredMessageObject() + "'.";
            String replyContents = createErrorResponseReply(mmd, "application", "OpenEAI-1001", errDescription, null);
            logExecutionComplete(mmd.getMessageAction(), replyContents, startTime);
            return getMessage(msg, replyContents);
        }

        // check the incoming action and take appropriate steps
        String replyContents;
        switch (mmd.getMessageAction().toLowerCase()) {
            case GENERATE_ACTION:
                replyContents = handleGenerate(mmd);
                break;
            case QUERY_ACTION:
                replyContents = handleQuery(mmd);
                break;
            case CREATE_ACTION:
                replyContents = handleCreate(mmd);
                break;
            case UPDATE_ACTION:
                replyContents = handleUpdate(mmd);
                break;
            case DELETE_ACTION:
                replyContents = handleDelete(mmd);
                break;
            default:
                replyContents = handleUnsupportedAction(mmd);
                break;
        }
        logExecutionComplete(mmd.getMessageAction(), replyContents, startTime);
        return getMessage(msg, replyContents);

    }

    private boolean isSupportedCommand(String messageObject) {
        return getRequiredMessageObject().equalsIgnoreCase(messageObject);
    }

    private void logExecutionComplete(String action, String replyContents, long startTime) {
        Long routerExecutionTime = ciscoAsrProvider.getRouterExecutionTimeInMilliseconds();
        long executionTime = System.currentTimeMillis() - startTime;
        logger.info(getLogtag() + action + "-Request execution complete in " + executionTime + " ms (Router execution time: " + routerExecutionTime + " ms) with replyContents:\n"
                + replyContents);
    }

    protected String getLogtag() {
        return "[CiscoAsr" + routerNumber + "][" + this.getClass().getSimpleName() + "] ";
    }

    abstract String getRequiredMessageObject();
    abstract String getSupportedCommands();
    abstract String handleGenerate(MessageMetaData mmd) throws CommandException;
    abstract String handleQuery(MessageMetaData mmd) throws CommandException;
    abstract String handleCreate(MessageMetaData mmd) throws CommandException;
    abstract String handleUpdate(MessageMetaData mmd) throws CommandException;
    abstract String handleDelete(MessageMetaData mmd) throws CommandException;

    protected String createErrorResponseReply(MessageMetaData mmd, String errType, String errNumber, String errDescription, Exception e) {
        Document genericDoc = (Document) genericResponseReplyDoc.clone();

        List<Error> errors = new ArrayList<>();
        errors.add(buildError(errType, errNumber, errDescription));

        logger.fatal(getLogtag() + errDescription, e);
        logger.fatal(getLogtag() + "Message sent in is:\n" + getMessageBody(mmd.getInDoc()));

        if (e != null && e.getMessage() != null) {
            // copied from RequestCommandImpl.buildReplyDocumentWithErrors(Element senderControlArea, Document replyDoc, List errors, Throwable e)
            // to deal with the unicode characters that can be in the LDAP API exception messages
            ByteArrayOutputStream bw = new ByteArrayOutputStream();
            PrintWriter pw = new PrintWriter(bw, true);
            e.printStackTrace(pw);

            // strip unicode from m1 and m2
            String m1 = e.getMessage().replaceAll("[^\\x01-\\x7F]", "");
            String m2 = bw.toString().replaceAll("[^\\x01-\\x7F]", "");

            errors.add(buildError("system", "COMMAND-1001", "Exception: " + m1 + "\n" + m2));
        }

        return this.buildReplyDocumentWithErrors(mmd.getControlArea(), genericDoc, errors);
    }

    protected String handleUnsupportedAction(MessageMetaData mmd) {
        String errDescription = "Unsupported message action: " + mmd.getMessageAction() + ". "
                + getLogtag() + "only supports actions: "
                + getSupportedCommands() + ".";
        return createErrorResponseReply(mmd, "application", "OpenEAI-1002", errDescription, null);
    }

    protected XmlEnterpriseObject retrieveObjectFromAppConfig(String comment, String msgObject, String msgObjectName)
            throws CommandException {

        XmlEnterpriseObject xeo;

        try {
            xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObjectName);
            logger.info(getLogtag() + "xeo retrieved with " + msgObjectName + ", enterpriseFields=" + xeo.getEnterpriseFields());
        } catch (Exception e) {
            try {
                logger.warn(getLogtag() + "Could not find object named '" + msgObjectName + "' in AppConfig, trying '" + msgObject + "'");
                xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObject);
            } catch (Exception e2) {
                // very bad error
                String msg = "Could not find an object named '" + msgObject + "' OR '" + msgObjectName
                        + "' in AppConfig.  Exception: " + e2.getMessage();
                throw new CommandException(msg, e);
            }
        }
        return xeo;
    }

    protected void publishCreateConfigSync(String msgObject, String action, ActionableEnterpriseObject object) throws CommandException {
        if (ciscoAsrSyncCommandProducerPool != null) {
            PubSubProducer pubSubProducer = null;
            String modality = "Sync";

            try {
                pubSubProducer = (PubSubProducer) ciscoAsrSyncCommandProducerPool.getExclusiveProducer();
                object.setCommandName("CiscoAsrNotificationSyncCommand");
                object.createSync(pubSubProducer);

            } catch (EnterpriseObjectSyncException e) {
                String errMsg = "Failed to publish save config sync message";
                logger.error(getLogtag() + errMsg, e);
                throw new CommandException(errMsg, e);
            } catch (Exception e) {
                String errMsg = "Failed to publish save config sync message - internal error";
                logger.error(getLogtag() + errMsg, e);
                throw new CommandException(errMsg, e);
            } finally {
                try {
                    ciscoAsrSyncCommandProducerPool.releaseProducer(pubSubProducer);
                } catch (Exception e) {
                    logger.error("Failed to release cisco asr command producer", e);
                }
            }

            logger.info(getLogtag() + "Published '" + msgObject + "-" + action + "-" + modality + "' message");
        }
    }

    void publishSync(String msgObject, String action, ActionableEnterpriseObject jeo, ActionableEnterpriseObject baseline)
            throws CommandException {

        if (serviceSyncProducerPool == null) {
            logger.info(getLogtag() + "No configured 'SyncPublisher' so syncs CANNOT be published."
                    + " Check the application's config doc if you want to publish sync messages.");
            return;
        }

        PubSubProducer pubSub = null;
        String modality = "Sync";

        try {
            pubSub = (PubSubProducer) serviceSyncProducerPool.getExclusiveProducer();

            logger.info(getLogtag() + "Publishing '" + msgObject + "-" + action + "-" + modality + "' message");

            if (action.equalsIgnoreCase(CREATE_ACTION)) {
                logger.info(getLogtag() + "NewData:\n" + jeo.toXmlString());
                jeo.createSync(pubSub);
            } else if (action.equalsIgnoreCase(UPDATE_ACTION)) {
                logger.info(getLogtag() + "NewData:\n" + jeo.toXmlString());
                logger.info(getLogtag() + "BaselineData:\n" + baseline.toXmlString());
                jeo.updateSync(pubSub);
            } else if (action.equalsIgnoreCase(DELETE_ACTION)) {
                logger.info(getLogtag() + "DeleteData:\n" + jeo.toXmlString());
                jeo.deleteSync("Delete", pubSub);
            } else {
                throw new CommandException(getLogtag() + "Can not publishSync for action: " + action);
            }

            logger.info(getLogtag() + "Published '" + msgObject + "-" + action + "-" + modality + "' message");
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new CommandException(e);
        } finally {
            try {
                if (pubSub != null)
                    serviceSyncProducerPool.releaseProducer(pubSub);
            } catch (Exception e) {
                logger.error(getLogtag() + "Error releasing PubSubProducer");
            }
        }
    }

    /**
     *
     */
    protected XmlEnterpriseObject retrieveAndBuildObject(String comment, String msgObject, String msgObjectName,
                                                         Element eData) throws CommandException {

        XmlEnterpriseObject xeo = retrieveObjectFromAppConfig(comment, msgObject, msgObjectName);

        // now, build the object from the xml document (DataArea)...
        try {
            xeo.buildObjectFromInput(eData);
            if (comment != null) {
                logger.info(getLogtag() + comment + " Object is: " + xeo.toXmlString());
            } else {
                logger.info(getLogtag() + "Object is: " + xeo.toXmlString());
            }
        } catch (Exception e) {
            logger.error(getLogtag() + "xeo=" + xeo);
            logger.error(getLogtag() + "data=" + new XMLOutputter().outputString(eData));
            throw new CommandException(e.getMessage(), e);
        }
        return xeo;
    }

    /**
     * Based on the 'messageAction' associated to the request we determine which
     * 'PrimedXmlDocument' associated to the message object being operated on
     * should be returned. This is specified in the Command's deployment
     * descriptor on each message object.
     * <p>
     * For example, our command supports
     * com.any-erp-vendor.Person/BasicPerson/1.0/Query-Request
     * and
     * com.any-erp-vendor.Person/BasicPerson/1.0/Create-Request.
     * We know based on the OpenEAI Message Protocol that the response to a
     * com.any-erp-vendor.Person/BasicPerson/1.0/Query-Request
     * is
     * com.any-erp-vendor.Person/BasicPerson/1.0/Provide-Reply.
     * We also know that the response to a
     * com.any-erp-vendor.Person/BasicPerson/1.0/Create-Request
     * is a
     * org.openeai.CoreMessaging/1.0/Response-Reply
     * with the appropriate status, action and error information filled in.
     * <p>
     * Because of all this knowledge, we configure the message objects used by
     * this Command to have the appropriate 'PrimedXmlDocument' entries that
     * allows us to simply get the appropriate reply document from the object
     * based on the action. If the Action is 'Query' we'll be returning the
     * 'primed' Provide document associated to the object. If the action is
     * anything else, we'll be returning the 'primed' generic Response document
     * associated to the object.
     **/
    protected final Document getReplyDoc(String msgAction, String msgObject, String msgRelease) throws CommandException {

        try {
            XmlEnterpriseObject xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObject + "." + generateRelease(msgRelease));
            Document rDoc;

            if (msgAction.equalsIgnoreCase(QUERY_ACTION) || msgAction.equalsIgnoreCase(GENERATE_ACTION)) {
                rDoc = xeo.getProvideDoc();
            } else {
                rDoc = xeo.getResponseDoc();
            }

            if (rDoc == null) {
                logger.fatal(getLogtag() + "Could not find a reply document for the '" + msgObject + "-" + msgAction + "' request.");
                throw new CommandException("Could not find an appropriate reply document for the '" + msgObject + "-"
                        + msgAction + " request.");
            } else {
                logger.info(getLogtag() + "Found a reply document for the '" + msgObject + "-" + msgAction + "' request.");
            }
            return rDoc;
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    protected void addXmlEnterpriseObjectToReplyDoc(MessageMetaData mmd, Document replyDoc, XmlEnterpriseObject xmlEnterpriseObject) throws EnterpriseLayoutException {
        // remove the contents of the DataArea element from the primed Provide-Reply document
        // before adding our output Element
        replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());

        xmlEnterpriseObject.setOutputLayoutManager(xmlEnterpriseObject.getOutputLayoutManager("xml"));
        Element dataAreaContent = (Element) xmlEnterpriseObject.buildOutputFromObject();
        replyDoc.getRootElement().getChild(DATA_AREA).addContent(dataAreaContent);


    }

    protected void addXmlEnterpriseObjectToReplyDoc(MessageMetaData mmd, Document replyDoc, List<? extends XmlEnterpriseObject> xmlEnterpriseObjects) throws EnterpriseLayoutException {
        // remove the contents of the DataArea element from the primed Provide-Reply document
        // before adding our output Element
        replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());

        ArrayList vpnConnectionProvisioningList = new ArrayList();
        for (XmlEnterpriseObject xmlEnterpriseObject : xmlEnterpriseObjects) {
            Element provisioningElement;
            try {
                provisioningElement = (Element) xmlEnterpriseObject.buildOutputFromObject();
                vpnConnectionProvisioningList.add(provisioningElement);
            } catch (EnterpriseLayoutException e) {
                String errMsg = "An error occurred serializing "
                        + "Stack object to an XML element. "
                        + "The exception is: " + e.getMessage();
                logger.error(getLogtag() + errMsg);
                throw e;
            }
        }
        replyDoc.getRootElement().getChild("DataArea").addContent(vpnConnectionProvisioningList);

    }


    private void setupUseLockConfigValue() throws InstantiationException {

        try {
            String useLockAsString = getAppConfig().getProperties("GeneralProperties").getProperty(PROP_USE_LOCK);
            if ("true".equalsIgnoreCase(useLockAsString) || "false".equalsIgnoreCase(useLockAsString)) {
                useLock = Boolean.valueOf(useLockAsString);
            } else {
                String errMsg = "The 'useLock' property must be present, and have a value of 'true' or 'false'";
                logger.error(getLogtag() + errMsg);
                throw new InstantiationException(errMsg);
            }
        } catch (Exception e) {
            String errMsg = "Failed to obtain 'useLock' property from config";
            logger.error(getLogtag() + errMsg, e);
            throw new InstantiationException(errMsg);
        }
    }

    private void setupCommandLockConfigValues() throws InstantiationException {
        if (useLock) {
            try {
                Properties generalProperties = getAppConfig().getProperties("GeneralProperties");
                String tunnelNumberAsString = generalProperties.getProperty(PROP_ROUTER_NUMBER);
                String commandLockName = "CiscoAsr" + tunnelNumberAsString + "CommandLock";
                Lock lock = (Lock)getAppConfig().getObject(commandLockName);
                commandLock = lock;
                obtainLockMaxWaitTime = Integer.valueOf(generalProperties.getProperty(PROP_OBTAIN_LOCK_MAX_WAIT_TIME));
                obtainLockRetrySleepInterval = Integer.valueOf(generalProperties.getProperty(PROP_OBTAIN_LOCK_RETRY_SLEEP_INTERVAL));
            }
            catch (EnterpriseConfigurationObjectException ecoe) {
                // An error occurred retrieving an object from AppConfig. Log it and
                // throw an exception.
                String errMsg = "An error occurred retrieving a Lock object from " +
                        "AppConfig: " + ecoe.getMessage();
                logger.fatal(getLogtag() + errMsg, ecoe);
                throw new InstantiationException(errMsg);
            }
        }

    }

}
