package edu.emory.it.service.ca.provider.mapper._1693;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NodeSet;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.mapper.AbstractVpnMapper;
import edu.emory.it.service.ca.provider.mapper.VpnMapper;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.ElementUtils;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.CryptoIsakmpProfile;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;
import org.apache.commons.lang.StringUtils;
import org.openeai.config.EnterpriseFieldException;

import java.util.Map;

public abstract class _1693AbstractVpnMapper extends AbstractVpnMapper implements VpnMapper {

    private String cryptoKeyringPresharedkeyAddressIpv4Ipv4addr = "crypto/keyring/local-address/bind-ip-address/ip-address/";

    private String cryptoIsakmpProfileMatchIdentityIpv4addressAddress = "crypto/isakmp/profile/match/identity/ipv4-address/address/ip";
    private String cryptoIsakmpProfileMatchIdentityIpv4addressMask = "crypto/isakmp/profile/match/identity/ipv4-address/mask";
    private String cryptoIsakmpProfileMatchIdentityIpv4addressVrf = "crypto/isakmp/profile/match/identity/ipv4-address/vrf";

    // Significant structural difference betwee 1662 and 1692: List of identities returned here.
    private String cryptoIsakmpProfileMatchIdentityAddress = "crypto/isakmp/profile/match/identity/address";
                                                    // crypto/isakmp/profile/match/identity/address/ip

    private String xpathInterfaceTunnelTunnelDestination = "interface/Tunnel/tunnel/destination/ipaddress-or-host";

    // 1693 contains "ipv4-unicast" segment
    private String nativeRouterBgpAddressfamilyWithvrfIpv4VrfNeighborContains = "native/router/bgp/address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor/id[text()[contains(., '${bgpNeighborId}')]]/..";


    /**
     * YANG version 1693 routers don't return bgp rouer info, so remove it from the descriptions expectation list.
     *
     * @param nativeResult       - NodeSet containing all crypto elements returned from a query
     * @param nativeTunnelResult - NodeSet containing the native/interface/tunnel result
     * @param bgpStateDataResult - NodeSet containing the bgp-state-data result
     * @return
     */
    @Override
    public Map<String, Element> buildDescriptionMap(NodeSet nativeResult, NodeSet nativeTunnelResult, NodeSet bgpStateDataResult) {
        Map<String, Element> descriptionMap = super.buildDescriptionMap(nativeResult, nativeTunnelResult, bgpStateDataResult);

        descriptionMap.remove(xpathRouterBgpAddressfamilyWithvrfIpv4VrfNeighborDescription());
        return descriptionMap;
    }

    /*
crypto/keyring

// LocalVpnIpAddress
1662:
cryptoKeyringPresharedkeyAddressIpv4Ipv4addr =
      "crypto/keyring/pre-shared-key/address/ipv4/ipv4-addr";
1693:
native/crypto/keyring/local-address/bind-ip-address/ip-address/

----------------------------------------------------------------------------

crypto/isakmp

1662:
      crypto/isakmp/profile/match/identity/ipv4-address/address/    <-- ${RemoteVpnIpAddress}
1693:
      crypto/isakmp/profile/match/identity/address/ip  <-- ${RemoteVpnIpAddress}

1662:
      <none>
1693:
      crypto/isakmp/profile/keyring/name

1662:
      <none>
1693:
      crypto/isakmp/profile/local-address/bind-ip-address/ip-address/

----------------------------------------------------------------------------

interface/Tunnel

1662:
      interface/Tunnel/tunnel/destination                           <-- ${RemoteVpnIpAddress}
1693:
      interface/Tunnel/tunnel/destination/ipaddress-or-host         <-- ${RemoteVpnIpAddress}


     */
    /**
     * Structural differences:
     *
     * ----------------------------------------------------------------------------
     * crypto/keyring
     *
     * // LocalVpnIpAddress
     * 1662:
     * cryptoKeyringPresharedkeyAddressIpv4Ipv4addr =
     *       "crypto/keyring/pre-shared-key/address/ipv4/ipv4-addr";
     * 1693:
     * native/crypto/keyring/local-address/bind-ip-address/ip-address/
     *
     * ----------------------------------------------------------------------------
     *
     * crypto/isakmp
     *
     * 1662:
     *       crypto/isakmp/profile/match/identity/ipv4-address/address/    <-- ${RemoteVpnIpAddress}
     * 1693:
     *       crypto/isakmp/profile/match/identity/address/ip  <-- ${RemoteVpnIpAddress}
     *
     * 1662:
     *       <none>
     * 1693:
     *       crypto/isakmp/profile/keyring/name
     *
     * 1662:
     *       <none>
     * 1693:
     *       crypto/isakmp/profile/local-address/bind-ip-address/ip-address/
     *
     * ----------------------------------------------------------------------------
     *
     * interface/Tunnel
     *
     * 1662:
     *       interface/Tunnel/tunnel/destination                           <-- ${RemoteVpnIpAddress}
     * 1693:
     *       interface/Tunnel/tunnel/destination/ipaddress-or-host         <-- ${RemoteVpnIpAddress}
     *
     * ----------------------------------------------------------------------------
     *
     * router
     *
     * 1662:
     *       router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/id        <-- ${BgpNeighborId}
     * 1693:
     *       router/bgp/address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor/id   <-- ${BgpNeighborId}
     *
     * ----------------------------------------------------------------------------
     *
     * Navigate to the associated MOA objects and override value with 1693 version.
     *
     * @param vpnConnection
     * @param resultMap
     * @return
     * @throws JNCException
     * @throws EnterpriseFieldException
     * @throws ProviderException
     */
    @Override
    public VpnConnection hydrateVpnConnection(VpnConnection vpnConnection, Map<String, Element> resultMap) throws JNCException, EnterpriseFieldException, ProviderException {
        vpnConnection = super.hydrateVpnConnection(vpnConnection, resultMap);

        if (vpnConnection != null) {
            String value;

            Element nativeInterface = resultMap.get(xpathNativeInterface());
            Element cryptoIsakmpProfileConfig = resultMap.get(xpathCryptoIsakmpProfile());

            CryptoIsakmpProfile cryptoIsakmpProfile = vpnConnection.getCryptoIsakmpProfile();
            MatchIdentity matchIdentity = AppConfigUtil.getMatchIdentityFromConfiguration(appConfig);

            // Use resultMap to get raw values.
            String remoteVpnIpAddress = nativeInterface.getValue(xpathInterfaceTunnelTunnelDestination()).toString();

            // Structurral change:  Single address record in 1662 is now a list. We match by RemoteVpnIpAddress.
            NodeSet nodes = cryptoIsakmpProfileConfig.get(xpathCryptoIsakmpProfileMatchIdentityAddress());
            for (Element node : nodes) {
                if (remoteVpnIpAddress.equals(node.getValue("ip").toString())) {
                    value = node.getValue("ip").toString();
                    if (StringUtils.isNotEmpty(value)) {
                        matchIdentity.setIpAddress(value);
                    }
                    value = ElementUtils.safeGetValue(node, "mask");
                    if (StringUtils.isNotEmpty(value)) {
                        matchIdentity.setNetmask(value);
                    }
                    value = ElementUtils.safeGetValue(node, "vrf");
                    if (StringUtils.isNotEmpty(value)) {
                        matchIdentity.setVirtualRouteForwarding(value);
                    }
                    cryptoIsakmpProfile.setMatchIdentity(matchIdentity);
                }
            }

        }
        return vpnConnection;
    }

    @Override
    public String xpathCryptoKeyringPresharedkeyAddressIpv4Ipv4addr() {
        return cryptoKeyringPresharedkeyAddressIpv4Ipv4addr;
    }

    @Override
    public String xpathCryptoIsakmpProfileMatchIdentityIpv4addressAddress() {
        return cryptoIsakmpProfileMatchIdentityIpv4addressAddress;
    }

    @Override
    public String xpathCryptoIsakmpProfileMatchIdentityIpv4addressMask() {
        return cryptoIsakmpProfileMatchIdentityIpv4addressMask;
    }

    @Override
    public String xpathCryptoIsakmpProfileMatchIdentityIpv4addressVrf() {
        return cryptoIsakmpProfileMatchIdentityIpv4addressVrf;
    }

    @Override
    public String xpathInterfaceTunnelTunnelDestination() {
        return xpathInterfaceTunnelTunnelDestination;
    }

    // Private to 1693 implementation.
    private String xpathCryptoIsakmpProfileMatchIdentityAddress() {
        return cryptoIsakmpProfileMatchIdentityAddress;
    }

}
