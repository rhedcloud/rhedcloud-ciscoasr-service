package edu.emory.it.service.ca.provider;

import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.Transport;
import edu.emory.it.service.ca.NotificationSyncProvider;
import org.openeai.config.AppConfig;

import java.io.IOException;
import java.net.SocketException;
import java.util.Properties;

public class CiscoAsrNotificationSyncProvider extends CiscoAsr1002Provider implements NotificationSyncProvider {

    private static final String PROVIDER_PROPERTIES = "caNotificationSyncProviderProperties";

    private static final String PROP_SAVE_CONFIG_RETRY_COUNT = "saveConfigMaxRetries";
    private static final String PROP_SAVE_CONFIG_SLEEP_INTERVAL = "saveConfigRetrySleepInterval";

    private Integer saveConfigMaxRetries;
    private Integer saveConfigRetrySleepInterval;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {

        logger.info(getLogtag() + "- Initializing");

        if (aConfig != null) {
            appConfig = aConfig;

            setProviderProperties(this, appConfig, PROVIDER_PROPERTIES);
            refreshLocalVariables();

            console(getLogtag() + " initialized.");
            console(getLogtag() + "Router: " + routerNumber);
        } else {
            String errMsg = "No deployment descriptor provided!";
            logger.fatal(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
    }

    @Override
    protected void refreshLocalVariables() throws ProviderException {
        try {
            super.refreshLocalVariables();
            Properties generalProperties = appConfig.getProperties("GeneralProperties");
            saveConfigMaxRetries = Integer.parseInt(generalProperties.getProperty(PROP_SAVE_CONFIG_RETRY_COUNT));
            saveConfigRetrySleepInterval = Integer.parseInt(generalProperties.getProperty(PROP_SAVE_CONFIG_SLEEP_INTERVAL));

            if (saveConfigMaxRetries == null) {
                throw new ProviderException(getLogtag() + "saveMaxConfigRetries not found in app config");
            }
            if (saveConfigRetrySleepInterval == null) {
                throw new ProviderException(getLogtag() + "saveConfigSleepInterval not found in app config");
            }
        } catch (Exception e) {
            String errMsg = "Failed to get save config max retries property";
            logger.fatal(getLogtag() + errMsg);
            throw new ProviderException(errMsg, e);
        }
    }

    @Override
    public boolean sendSaveConfig() {

        boolean sendSaveConfigSuccess = false;

        int retries = 0;

        NetconfSession session = null;

        while (++retries < saveConfigMaxRetries) {
            try {
                if (session == null) {
                    session = openNetconfSession();
                }
                if (saveConfigAttempt(session)) {
                    logger.info(getLogtag() + "router save config succeeded" + (retries == 1 ? " on first attempt" : " on retry #" + retries));
                    sendSaveConfigSuccess = true;
                    break;
                } else {
                    logger.error(getLogtag() + "router save config failed"  + (retries == 1 ? " on first attempt" : " on retry #" + retries));
                }
            } catch (Exception e) {
                String errMsg = "router save config attempt exception" + (retries == 1 ? " on first attempt" : " on retry #" + retries);
                logger.error(getLogtag() + errMsg, e);
            }

            // Some errors during save-config will shut down the session from the router Netconf stack.
            // There is no way of inspecting the session instance, so we'll just have to close every time.
            try {
                closeNetconfSession(session);
            } catch (ProviderException e) {
                logger.warn(getLogtag() + "Error while closing session: " + e.getMessage());
            }
            session = null;

            try {
                Thread.sleep(saveConfigRetrySleepInterval);
            } catch (InterruptedException e) {
                logger.error(getLogtag() + "Sleep interrupted", e);
                break;
            }
        }

        try {
            if (session != null) {
                closeNetconfSession(session);
            }
        } catch (ProviderException e) {
            logger.warn(getLogtag() + "Failed to close Netconf session", e);
        }

        if (retries >= saveConfigMaxRetries) {
            String errMsg = "Save config failure - max retries exceeded";
            logger.error(getLogtag() + errMsg);
            sendSaveConfigSuccess = false;
        }

        return sendSaveConfigSuccess;
    }

    private boolean saveConfigAttempt(NetconfSession session) throws ProviderException {

        boolean saveConfigSucceeded = false;

        try {
            saveConfigSucceeded = saveConfig(session);
         } catch (Exception e) {
            // Full stack trace has been logged by saveConfig() method - just supply some context in this log message.
            String errMsg = "Failed to save router config";
            if (e instanceof ProviderException && e.getCause() instanceof SocketException) {
                if (e.getCause().getMessage() != null && e.getCause().getMessage().equalsIgnoreCase("Connection reset")) {
                    errMsg += ": Connection reset by router - probably still busy";
                } else {
                    errMsg += ": Socket error";
                }
            } else if (e instanceof ProviderException && e.getCause() instanceof JNCException) {
                String message = null;
                if (e.getCause() != null) {
                    message = e.getCause().getMessage();
                } else {
                    message = e.getMessage();
                }
                errMsg += " - Failed to open netconf session to router" + (message != null ? ": " + message : "");
            } else {
                errMsg += ": " + e.getMessage();
            }
            logger.error(getLogtag() + errMsg);

        }
        return saveConfigSucceeded;
    }
}
