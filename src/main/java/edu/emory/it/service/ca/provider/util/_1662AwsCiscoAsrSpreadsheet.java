package edu.emory.it.service.ca.provider.util;

import edu.emory.it.service.ca.provider.validator.VpnConnectionOperationContext;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _1662AwsCiscoAsrSpreadsheet implements CiscoAsrSpreadsheet {

    private String tunnelDescriptionPrefix = "AWS Research VPC";

    public Map<String, String> buildSpreadsheetValues(String vpcId, Integer vpnId, String tunnelNumberAsString, VpnConnectionOperationContext operationContext) {

        Map<String, String> spreadsheetValues = new HashMap<>();

        String vpcNumberAsString = vpnId.toString();
        String paddedVpcNumberAsString = String.format("%03d", vpnId);
        String baseVpcTunnelString = paddedVpcNumberAsString + "-tun" + tunnelNumberAsString;

        spreadsheetValues.put("vpcId", vpcId);
        spreadsheetValues.put("vpcNumberAsString", vpcNumberAsString);
        spreadsheetValues.put("paddedVpcNumberAsString", paddedVpcNumberAsString);
        spreadsheetValues.put("cryptoKeyringName", "keyring-vpn-research-vpc" + baseVpcTunnelString);
        spreadsheetValues.put("isakmpProfileName", "isakmp-vpn-research-vpc" + baseVpcTunnelString);
        spreadsheetValues.put("ipsecTransformSetName", "ipsec-prop-vpn-research-vpc" + baseVpcTunnelString);
        spreadsheetValues.put("ipsecProfileName", "ipsec-vpn-research-vpc" + baseVpcTunnelString);
        spreadsheetValues.put("tunnelId", tunnelNumberAsString + "0" + paddedVpcNumberAsString);
        switch (operationContext.getOperation()) {
            case "query":
            case "generate-send":
                spreadsheetValues.put("tunnelDescription", tunnelDescriptionPrefix + paddedVpcNumberAsString + " Tunnel" + tunnelNumberAsString + " (" + vpcId + ")");
                break;
            case "delete-send":
            case "delete-response":
                spreadsheetValues.put("tunnelDescription", tunnelDescriptionPrefix + paddedVpcNumberAsString + " Tunnel" + tunnelNumberAsString + " (AVAILABLE)");
                break;

        }

        return spreadsheetValues;
    }

    public String getTunnelDescriptionPrefix() {
        return tunnelDescriptionPrefix;
    }

    public Integer getVpnIdFromTunnelName(String tunnelName) {
        Integer vpnId = null;

        Pattern pattern = Pattern.compile("[\\d]{5}");
        Matcher matcher = pattern.matcher(tunnelName);

        if (matcher.matches()) {
            vpnId = Integer.valueOf(tunnelName.substring(1));
        } else {
            throw new IllegalArgumentException("Tunnel name is not 5 digits");
        }

        return vpnId;
    }

    public String getVpcIdFromTunnelDescription(String tunnelDescription) {
        String vpcId = null;

        Pattern pattern = Pattern.compile(".+\\(([^\\)]+)\\)");
        Matcher matcher = pattern.matcher(tunnelDescription);

        if (matcher.matches()) {
            vpcId = matcher.group(1);
        } else {
            throw new IllegalArgumentException("Tunnel description is malformed: " + tunnelDescription);
        }
        return vpcId;
    }
}
