package edu.emory.it.service.ca.provider.mapper._1662;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.mapper.AbstractStaticNatMapper;
import edu.emory.it.service.ca.provider.mapper.StaticNatMapper;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;

import java.util.ArrayList;
import java.util.List;

public class _1662StaticNatMapper extends AbstractStaticNatMapper implements StaticNatMapper {

    // All default behavior in AbstractStaticNatMapper is 1662AWS
}
