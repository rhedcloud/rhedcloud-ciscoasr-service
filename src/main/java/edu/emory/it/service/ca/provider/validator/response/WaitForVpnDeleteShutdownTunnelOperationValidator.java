package edu.emory.it.service.ca.provider.validator.response;

import org.apache.logging.log4j.Logger;

public class WaitForVpnDeleteShutdownTunnelOperationValidator implements WaitForVpnOperationValidator {

    private final Logger logger;
    private final String tunnelNumber;

    public WaitForVpnDeleteShutdownTunnelOperationValidator(Logger logger, String tunnelNumber) {
        this.logger = logger;
        this.tunnelNumber = tunnelNumber;
    }

    @Override
    public boolean validate(VpnResponseValidatorCommand vpnResponseValidatorCommand) {

        boolean valid = false;

        VpnDeleteShutdownTunnelValidatorCommand vpnDeleteShutdownTunnelValidatorCommand = (VpnDeleteShutdownTunnelValidatorCommand) vpnResponseValidatorCommand;

        String tunnelShutdown = vpnDeleteShutdownTunnelValidatorCommand.getTunnelShutdown();

        do {
            if (tunnelShutdown == null) {
                logger.warn(getLogtag() + "No tunnel shutdown returned - tunnel shutdown still in progress");
                break;
            } else if (!tunnelShutdown.equalsIgnoreCase("shutdown")) {
                logger.error(getLogtag() + "tunnel shutdown mismatch - expected: shutdown received: " + tunnelShutdown);
                break;
            }

            valid = true;
            
        } while (false);
        return valid;
    }

    private String getLogtag() {
        return "[CiscoAsr" + tunnelNumber + "][" + this.getClass().getSimpleName() + "] ";
    }
}
