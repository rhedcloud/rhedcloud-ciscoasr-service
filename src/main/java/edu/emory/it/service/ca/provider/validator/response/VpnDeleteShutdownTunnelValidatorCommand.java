package edu.emory.it.service.ca.provider.validator.response;

public class VpnDeleteShutdownTunnelValidatorCommand extends VpnResponseValidatorCommand implements VpnResponseValidator {

    private String tunnelName;
    private String tunnelShutdown;

    @Override
    public boolean expectResult() {
        return true;
    }

    public String getTunnelShutdown() {
        return tunnelShutdown;
    }

    public void setTunnelShutdown(String tunnelShutdown) {
        this.tunnelShutdown = tunnelShutdown;
    }

    public String getTunnelName() {
        return tunnelName;
    }

    public void setTunnelName(String tunnelName) {
        this.tunnelName = tunnelName;
    }
}
