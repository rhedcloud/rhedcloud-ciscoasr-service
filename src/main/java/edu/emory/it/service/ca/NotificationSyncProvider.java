package edu.emory.it.service.ca;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import org.openeai.config.AppConfig;

public interface NotificationSyncProvider {

    void init(AppConfig aConfig) throws ProviderException;

    boolean sendSaveConfig();
}
