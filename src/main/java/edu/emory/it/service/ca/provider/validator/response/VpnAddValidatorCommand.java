package edu.emory.it.service.ca.provider.validator.response;

import edu.emory.it.service.ca.provider.ProviderDevice;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;

public class VpnAddValidatorCommand extends VpnResponseValidatorCommand implements VpnResponseValidator {

    private VpnConnectionRequisition vpnConnectionRequisition;

    @Override
    public boolean expectResult() {
        return true;
    }

    public VpnConnectionRequisition getVpnConnectionRequisition() {
        return vpnConnectionRequisition;
    }

    public void setVpnConnectionRequisition(VpnConnectionRequisition vpnConnectionRequisition) {
        this.vpnConnectionRequisition = vpnConnectionRequisition;
    }
}
