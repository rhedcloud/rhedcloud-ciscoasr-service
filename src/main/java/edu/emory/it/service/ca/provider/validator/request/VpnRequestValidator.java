package edu.emory.it.service.ca.provider.validator.request;

import edu.emory.it.service.ca.provider.ProviderException;

public interface VpnRequestValidator {

    void validate() throws ProviderException;

}
