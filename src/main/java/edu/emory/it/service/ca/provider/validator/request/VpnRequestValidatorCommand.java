package edu.emory.it.service.ca.provider.validator.request;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class VpnRequestValidatorCommand implements VpnRequestValidator {
//public class VpnRequestValidatorCommand {
    private String vpcId;
    private VpnConnectionQuerySpecification vpnConnectionQuerySpecification;
    private VpnConnection vpnConnection;
    private VpnConnectionRequisition vpnConnectionRequisition;

    private final int TUNNEL_ID_LENGTH = 5;

    public VpnRequestValidatorCommand() {

    }

    @Override
    public void validate() throws ProviderException {

    }

    protected void validateVpcId(String vpcId, StringBuilder builder) {
        if (StringUtils.isEmpty(vpcId)) {
            builder.append("No vpc id in query specification\n");
        }
    }

    protected void validateVpnId(String vpnId, StringBuilder builder) {
        if (StringUtils.isEmpty(vpnId)) {
            builder.append("No vpn connection profile id in query specification\n");
        } else {
            try {
                Integer.valueOf(vpnId);
            } catch (NumberFormatException e) {
                builder.append("vpn connection profile id is not a number: " + vpnId + "\n");
            }
        }
    }

    protected Map<String, ?> validateTunnelId(String tunnelId, StringBuilder builder) throws ProviderException {
        Map<String, Object> tunnelValues = new HashMap<>();
        if (tunnelId == null || tunnelId.length() != TUNNEL_ID_LENGTH) {
            builder.append("VpnConnection - TunnelInterface tunnel id is invalid: " + tunnelId + "\n");
        }
        String tunnelNumberAsString = tunnelId.substring(0, 1);

        String vpcNumberAsString = tunnelId.substring(2, 5);
        Integer vpcNumber = null;

        try {
            vpcNumber = Integer.parseInt(vpcNumberAsString);
        } catch (NumberFormatException e) {
            builder.append("Invalid vpc number in tunnel id: " + vpcNumberAsString + "\n");
        }

        tunnelValues.put("tunnelNumberAsString", tunnelNumberAsString);
        tunnelValues.put("vpcNumberAsString", vpcNumberAsString);
        tunnelValues.put("vpcNumber", vpcNumber);

        // Fatal errors encountered - can't validate without tunnelId and/or vpcNumber.
        if (builder.length() > 0) {
            throw new ProviderException("FATAL: Invalid tunnel interface\n" + builder.toString());
        }

        return tunnelValues;
    }

    public String getVpcId() {
        return vpcId;
    }

    public void setVpcId(String vpcId) {
        this.vpcId = vpcId;
    }

    public VpnConnectionQuerySpecification getVpnConnectionQuerySpecification() {
        return vpnConnectionQuerySpecification;
    }

    public void setVpnConnectionQuerySpecification(VpnConnectionQuerySpecification vpnConnectionQuerySpecification) {
        this.vpnConnectionQuerySpecification = vpnConnectionQuerySpecification;
    }

    public VpnConnection getVpnConnection() {
        return vpnConnection;
    }

    public void setVpnConnection(VpnConnection vpnConnection) {
        this.vpnConnection = vpnConnection;
    }

    public VpnConnectionRequisition getVpnConnectionRequisition() {
        return vpnConnectionRequisition;
    }

    public void setVpnConnectionRequisition(VpnConnectionRequisition vpnConnectionRequisition) {
        this.vpnConnectionRequisition = vpnConnectionRequisition;
    }

}
