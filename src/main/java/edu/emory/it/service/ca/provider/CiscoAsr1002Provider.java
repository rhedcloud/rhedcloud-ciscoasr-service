package edu.emory.it.service.ca.provider;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.AWSSecretsManagerException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;
import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.SSHConnection;
import com.tailf.jnc.SSHSession;
import com.tailf.jnc.Transport;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.util.RouterUtil;
import edu.emory.it.service.ca.provider.util.StackTraceAnalyzer;
import org.apache.commons.lang.StringUtils;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.ProducerPool;

import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Common provider behavior goes here.
 */
public abstract class CiscoAsr1002Provider extends OpenEaiObject implements CiscoAsrProvider {

    protected AppConfig appConfig;

    // PropertyConfig keys for this provider
    private static final String PROP_NETCONF_SAVE_REQUEST = "caNetconfSaveRequest";
    private static final String PROP_NETCONF_ROUTER_HOST = "netconfRouterHost";
    private static final String PROP_NETCONF_ROUTER_PORT = "netconfRouterPort";
    private static final String PROP_NETCONF_ADMIN_USERNAME = "adminUsername";
    private static final String PROP_NETCONF_ADMIN_PASSWORD = "adminPassword";
    private static final String PROP_ROUTER_NUMBER = "routerNumber";
    private static final String PROP_WAIT_FOR_OPERATION_RETRY_COUNT = "waitForOperationRetryCount";
    private static final String PROP_WAIT_FOR_OPERATION_RETRY_WAIT_IN_MILLISECONDS = "waitForOperationRetryWaitInMilliseconds";
    private static final String PROP_OPERATION_ERROR_RETRY_COUNT = "operationErrorRetryCount";
    private static final String PROP_OPERATION_ERROR_RETRY_WAIT_IN_MILLISECONDS = "operationErrorRetryWaitInMilliseconds";
    private static final String PROP_ROUTER_SETTLING_WAIT_IN_MILLISECONDS = "routerSettlingWaitInMilliseconds";


    // Router event types
    private static final String ROUTER_EVENT_SAVE_CONFIG_ERROR = "ROUTER_EVENT_SAVE_CONFIG_ERROR";

    /*
     * default values, if applicable, for properties are specified here,
     * but can be overridden at runtime via corresponding properties from config doc
     */

    protected ProducerPool ciscoAsrSyncCommandProducerPool;

    /** NETCONF host - PROP_NETCONF_HOST - no default */
    protected String netconfRouterHost;
    /** NETCONF port - PROP_NETCONF_PORT */
    protected int netconfRouterPort = 830;

    /** NETCONF admin username - PROP_NETCONF_ADMIN_USERNAME - no default */
    protected String netconfAdminUsername;
    /** NETCONF admin password - PROP_NETCONF_ADMIN_PASSWORD - no default */
    protected String netconfAdminPassword;

    protected String saveConfigXml;

    protected ProviderDevice providerDevice;

    protected Integer waitForOperationRetryCount;
    protected Integer waitForOperationRetryWaitInMilliseconds;

    protected Integer operationErrorRetryCount;
    protected Integer operationErrorRetryWaitInMilliseconds;
    protected Integer routerSettlingWaitInMilliseconds;

    protected Long routerExecutionTimeInMilliseconds;

    protected Map<String, Map<String, String>> routerCapabilityRevisionsMap;

    protected Integer sshConnectionTimeoutInMilliseconds = 60000;

    protected String routerNumber = "";

    protected String routerVersion;

    protected String cspDiscriminator;

    protected List<String> cspDiscriminators;

    protected String getLogtag() {
        return "[CiscoAsr" + routerNumber + "][" + this.getClass().getSimpleName() + "] ";
    }

    abstract public void init(AppConfig aConfig) throws ProviderException;

    /**
     * Set provider and general properties on super class.
     *
     * @param eaiObject - super class into which properties are set
     * @param appConfig - AppConfig hydrated from deployment descriptor
     * @param providerPropertyName - provider property name in deployment descriptor with which to retrieve properties
     * @throws ProviderException
     */
    protected void setProviderProperties(OpenEaiObject eaiObject, AppConfig appConfig, String providerPropertyName) throws ProviderException {
        try {
            eaiObject.setProperties(appConfig.getProperties(providerPropertyName));

        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving the " + providerPropertyName
                    + " PropertyConfig object from AppConfig: The exception is: " + e.getMessage();
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    /**
     * Reset all relevant local variables to account for any config doc
     * changes that may have occurred since last time provider was used.
     *
     * @throws ProviderException on error
     */
    protected void refreshLocalVariables() throws ProviderException {
        // do this here so config doc changes will be picked up without a service bounce
        try {
            Properties generalProperties = appConfig.getProperties("GeneralProperties");

            // Get router number first for better logging during property init.
            routerNumber = generalProperties.getProperty(PROP_ROUTER_NUMBER);
            switch (routerNumber) {
                case "1":
                    providerDevice = ProviderDevice.ROUTER1;
                    break;
                case "2":
                    providerDevice = ProviderDevice.ROUTER2;
                    break;
                default:
                    String errMsg = "Invalid router number configuration: " + routerNumber;
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);

            }

            getRouterCredentials();

            saveConfigXml = generalProperties.getProperty(PROP_NETCONF_SAVE_REQUEST);
            netconfRouterHost = generalProperties.getProperty(PROP_NETCONF_ROUTER_HOST, netconfRouterHost);
            netconfRouterPort = Integer.parseInt(generalProperties.getProperty(PROP_NETCONF_ROUTER_PORT, String.valueOf(netconfRouterPort)));
            netconfAdminUsername = generalProperties.getProperty(PROP_NETCONF_ADMIN_USERNAME, netconfAdminUsername);
            netconfAdminPassword = generalProperties.getProperty(PROP_NETCONF_ADMIN_PASSWORD, netconfAdminPassword);

            waitForOperationRetryCount = Integer.parseInt(generalProperties.getProperty(PROP_WAIT_FOR_OPERATION_RETRY_COUNT));
            waitForOperationRetryWaitInMilliseconds = Integer.parseInt(generalProperties.getProperty(PROP_WAIT_FOR_OPERATION_RETRY_WAIT_IN_MILLISECONDS));

            operationErrorRetryCount = Integer.parseInt(generalProperties.getProperty(PROP_OPERATION_ERROR_RETRY_COUNT));
            operationErrorRetryWaitInMilliseconds = Integer.parseInt(generalProperties.getProperty(PROP_OPERATION_ERROR_RETRY_WAIT_IN_MILLISECONDS));
            routerSettlingWaitInMilliseconds = Integer.parseInt(generalProperties.getProperty(PROP_ROUTER_SETTLING_WAIT_IN_MILLISECONDS));

            routerCapabilityRevisionsMap = new HashMap<>();

            try {
                List objects = appConfig.getObjectsLike("RouterRevisionMap_");

                for (Object revisionMapPropertyConfig : objects) {
                    if (revisionMapPropertyConfig instanceof PropertyConfig) {
                        PropertyConfig propertyConfig = (PropertyConfig)revisionMapPropertyConfig;
                        String revisionMapName = propertyConfig.getName();
                        logger.info(getLogtag() + "Router revision map name: " + revisionMapName);
                        Map<String, String> theMap = propertyConfig.getProperties().entrySet().stream().collect(
                                Collectors.toMap(
                                        e -> e.getKey().toString(),
                                        e -> e.getValue().toString()
                                )
                        );
                        routerCapabilityRevisionsMap.put(revisionMapName, theMap);
                    } else {
                        String errMsg = "Invalid configuration - router revision map object from config is not a PropertyConfig - type: " + revisionMapPropertyConfig.toString();
                        logger.error(getLogtag() + errMsg);
                        throw new ProviderException(errMsg);
                    }
                }
            } catch (Exception e) {
                String errMsg = "Failed to read router capability revision maps from configuration";
                logger.error(getLogtag() + errMsg, e);
                throw new ProviderException(errMsg, e);
            }

        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "EnterpriseConfigurationObjectException while getting refreshing local variables from provider configuration";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (AWSSecretsManagerException e) {
            String errMsg = "AWSSecretsManagerException while getting router credentials";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (Exception e) {
            String errMsg = "Exception while refreshing local variables from provider configuration";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    protected NetconfSession openNetconfSession() throws ProviderException {
        NetconfSession session = null;
        session = openNetconfSession(netconfRouterHost, netconfRouterPort, netconfAdminUsername, netconfAdminPassword);
        return session;
    }

    private NetconfSession openNetconfSession(String netconfServiceHost, int netconfServicePort, String netconfAdminUsername, String netconfAdminPassword) throws ProviderException {

        logger.info(getLogtag() + ">>>>>>>>>>>>>>>>>>>>>>>>>> OPEN NETCONF SESSION >>>>>>>>>>>>>>>>>>>>>>>>>>");
        NetconfSession session = null;
        try {
            SSHConnection c = new SSHConnection(netconfServiceHost, netconfServicePort, sshConnectionTimeoutInMilliseconds);
            c.authenticateWithPassword(netconfAdminUsername, netconfAdminPassword);
            SSHSession ssh = new SSHSession(c);
            session = new NetconfSession(ssh);
            session.setErrorOption(session.ROLLBACK_ON_ERROR);
        } catch (IOException e) {
            String errMsg = "I/O Exception opening netconf session to host: " + netconfServiceHost + " - port: " + netconfServicePort + " - for admin user: " + obfuscate(netconfAdminUsername);
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (JNCException e) {
            String errMsg = "Java Netconf Client Exception opening netconf session to host: " + netconfServiceHost + " - port: " + netconfServicePort + " - for admin user: " + obfuscate(netconfAdminUsername);
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
        return session;
    }

    private String obfuscate(String value) {
        String obfuscatedValue = "********";

        if (value != null && value.length() > 2) {
            byte[] buffer = new byte[value.length()];

            buffer[0] = (byte)value.charAt(0);
            for (int n = 1; n < value.length() -2; n++) {
                buffer[n] = '*';
            }
            buffer[value.length() - 1] = (byte)value.charAt(value.length() - 1);
            obfuscatedValue = new String(buffer);
        }
        return obfuscatedValue;
    }

    /**
     * Attempt to unlock specified data store on specified netconf session object.
     *
     * Exceptions occurring during the unlock are logged as warnings, since a session close operation follows this call
     * which will gracefully end the session.
     *
     * JNC docs for closeSession:
     * "The server will release any locks and resources associated with the session and gracefully close any associated connections."
     *
     * @param netconfSession
     * @param datastoreId
     * @throws ProviderException - on unknown datastore id
     */
    protected void unlockDatastore(NetconfSession netconfSession, int datastoreId) throws ProviderException {
        String datastoreName;
        switch (datastoreId) {
            case NetconfSession.RUNNING:
                datastoreName = "RUNNING";
                break;
            case NetconfSession.STARTUP:
                datastoreName = "STARTUP";
                break;
            case NetconfSession.CANDIDATE:
                datastoreName = "CANDIDATE";
                break;
            default:
                throw new ProviderException("Unknown datastore id: " + datastoreId);
        }
        try {
            if (null != netconfSession) {
                netconfSession.unlock(datastoreId);
            }
        } catch (JNCException e) {
            String errMsg = "Java Netconf Client Exception unlocking datastore " + datastoreName;
            logger.warn(getLogtag() + errMsg);
        } catch (IOException e ) {
            String errMsg = "I/O Exception unlocking datastore " + datastoreName;
            logger.warn(getLogtag() + errMsg);
        }
    }

    protected void closeNetconfSession(NetconfSession netconfSession) throws ProviderException {
        logger.info(getLogtag() + "<<<<<<<<<<<<<<<<<<<<<<<<<< CLOSE NETCONF SESSION <<<<<<<<<<<<<<<<<<<<<<<<<<");

        try {
            if (null != netconfSession) {
                netconfSession.closeSession();
                Transport transport = netconfSession.getTransport();
                if (transport instanceof SSHSession) {
                    ((SSHSession)transport).getSSHConnection().close();
                    logger.info(getLogtag() + "JNC NETCONF SSHConnection closed - socket reader thread shutdown");
                } else {
                    logger.warn(getLogtag() + "JNC NETCONF Transport should be an instance of SSHSession - socket reader thread leaked");
                }
            }
        } catch (JNCException e) {
            String errMsg = "Java Netconf Client Exception closing the netconf session";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (IOException e ) {
            String errMsg = "I/O Exception closing the netconf session";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (Exception e ) {
            String errMsg = "Exception closing the netconf session";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    protected void operationSleep(int waitPeriod) throws ProviderException {
        try {
            Thread.sleep(waitPeriod);
        } catch (InterruptedException e) {
            String errMsg = "Operation sleep interrupted";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    /**
     * Save device config using provided NetconfSession.
     *
     * This is a raw rpc call, so response is parsed for errors.
     *
     * Success:
     *
     * <result xmlns="http://cisco.com/yang/cisco-ia" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">Save running-config successful</result>
     *
     * Sample errors:
     *
     * Comm failure:
     *
     * <rpc-error xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
     *   <error-type>application</error-type>
     *   <error-tag>operation-failed</error-tag>
     *   <error-severity>error</error-severity>
     *   <nc:error-path xmlns:cisco-ia="http://cisco.com/yang/cisco-ia" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
     *     /nc:rpc/cisco-ia:save-config
     *   </nc:error-path>
     *   <error-message unknown:lang="en">application communication failure</error-message>
     *   <error-info>
     *     <bad-element>save-config</bad-element>
     *   </error-info>
     * </rpc-error>
     *
     * Sync in progress:
     * (Note that newer firmware is closing the Netconf session in this case)
     *
     * <rpc-error xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
     *   <error-type>application</error-type>
     *   <error-tag>resource-denied</error-tag>
     *   <error-severity>error</error-severity>
     *   <nc:error-path xmlns:cisco-ia="http://cisco.com/yang/cisco-ia" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
     *     /nc:rpc/cisco-ia:save-config
     *   </nc:error-path>
     *   <error-message unknown:lang="en">resource denied: Sync is in progress</error-message>
     *   <error-info>
     *     <bad-element>save-config</bad-element>
     *   </error-info>
     * </rpc-error>
     *
     * @throws ProviderException
     */
    public boolean saveConfig(NetconfSession session) throws ProviderException {

        boolean configSaved = false;

        try {
            XMLParser xmlParser = new XMLParser();
            Element saveConfigElement = xmlParser.parse(saveConfigXml);

            NodeSet reply = session.callRpc(saveConfigElement);
            logger.info(getLogtag() + "Saved config on device " + providerDevice.getName() + ":\n" + reply.toXMLString());

            Element errorMessage = reply.getFirst("error-message");
            if (errorMessage == null) {
                configSaved = true;
            } else {
                String errMsg = "Error while saving router config: " + errorMessage.getValue();
                logger.error(getLogtag() + errMsg);
            }
        } catch (Exception e) {
            String errMsg = "Failed to save config on device " + providerDevice.getName();
            logger.error(getLogtag() + errMsg, e);
        }
        return configSaved;
    }

    public void getRouterCredentials() throws ProviderException {

        // Named using the AWS docs for BasicAWSCredentials constructor.
        String accessKey;
        String secretKey;
        String secretName;
        String region;

        /**
         * {
         *     "AccessKey": {
         *         "Status": "Active",
         *         "SecretAccessKey": "CredentialRedactedAndRotated...",
         *         "CreateDate": "2020-01-23T22:15:20Z",
         *         "AccessKeyId": "CredentialRedactedAndRotated...",
         *         "UserName": "ciscoasr-service"
         *     }
         * }
         */

        try {
            Properties generalProperties = appConfig.getProperties("GeneralProperties");
            accessKey = generalProperties.getProperty("routerCredentialsAccessKey");
            secretKey = generalProperties.getProperty("routerCredentialsSecretKey");
            secretName = generalProperties.getProperty("routerCredentialsSecretName");
            region = generalProperties.getProperty("awsRegion", "us-east-1");

            if (StringUtils.isEmpty(accessKey) || StringUtils.isEmpty(secretKey) || StringUtils.isEmpty(secretName)) {
                String errMsg = "Router configuration secret values incomplete or missing entirely";
                logger.error(getLogtag() + errMsg);
                throw new ProviderException(errMsg);
            }
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Failed to get general properties from app config";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);

        GetSecretValueResult getSecretValueResult = null;

        try {
            logger.info(getLogtag() + "Attempting to get secret value using default AWS context...");

            // Try default AWS Context first.
            AWSSecretsManager client  = AWSSecretsManagerClientBuilder.standard()
                    .withRegion(region)
                    .build();

            getSecretValueResult = client.getSecretValue(getSecretValueRequest);

            logger.info("Successfully retrieved router credentials secret using default AWS context");

        } catch (Exception e ) {
            String infoMsg = "Failed to get secret value using default AWS context: " + e.getMessage();
            logger.info(infoMsg);
        }

        if (getSecretValueResult == null) {
            try {
                logger.info(getLogtag() + "Attempting to get secret value using access key and secret key...");

                BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
                ;

                AWSSecretsManager clientWithCredentials = AWSSecretsManagerClientBuilder
                        .standard()
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .withRegion(region)
                        .build();

                getSecretValueResult = clientWithCredentials.getSecretValue(getSecretValueRequest);

                logger.info(getLogtag() + "Get secret value result: " + getSecretValueResult.toString());

            } catch (InternalServiceErrorException e) {
                // An error occurred on the server side.
                // Deal with the exception here, and/or rethrow at your discretion.
                String errMsg = "Internal service error exception in AWS Secrets Manager";
                logger.error(getLogtag() + errMsg, e);
                throw new AWSSecretsManagerException(errMsg);
            } catch (InvalidParameterException e) {
                // You provided an invalid value for a parameter.
                // Deal with the exception here, and/or rethrow at your discretion.
                String errMsg = "Invalid parameter exception interacting with AWS Secrets Manager";
                logger.error(getLogtag() + errMsg, e);
                throw new AWSSecretsManagerException(errMsg);
            } catch (InvalidRequestException e) {
                // You provided a parameter value that is not valid for the current state of the resource.
                // Deal with the exception here, and/or rethrow at your discretion.
                String errMsg = "Invalid request exception interacting with AWS Secrets Manager";
                logger.error(getLogtag() + errMsg, e);
                throw new AWSSecretsManagerException(errMsg);
            } catch (ResourceNotFoundException e) {
                // We can't find the resource that you asked for.
                // Deal with the exception here, and/or rethrow at your discretion.
                String errMsg = "Resource not found exception interacting with AWS Secrets Manager";
                logger.error(getLogtag() + errMsg, e);
                throw new AWSSecretsManagerException(errMsg);
            } catch (Exception e) {
                String errMsg = "Exception interacting with AWS Secrets Manager";
                logger.error(getLogtag() + errMsg, e);
                throw new AWSSecretsManagerException(errMsg);
            }
        }

        logger.info(getLogtag() + "Secrets successfully retrieved");

        try {
            StringReader stringReader = new StringReader(getSecretValueResult.getSecretString());
            Properties routerCredentials = new Properties();
            routerCredentials.load(stringReader);

            netconfAdminUsername = routerCredentials.getProperty("router.username");
            netconfAdminPassword = routerCredentials.getProperty("router.password");

            logger.info(getLogtag() + "Successfully retrieved router credentials from AWS Secrets manager");

        } catch (IOException e) {
            String errMsg = "IO Exception while loading properties from secret value result";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

    }

    protected String buildTriesString() {
        return operationErrorRetryCount + (operationErrorRetryCount == 1 ? " try" : " tries");
    }

    protected String getExceptionMessage(Exception e) {
        String message = e.getMessage();
        if (StringUtils.isEmpty(message)) {
            Throwable cause = e.getCause();
            if (cause != null  && StringUtils.isNotEmpty(cause.getMessage())) {
                message = cause.getClass().getName() + ": " + cause.getMessage();
            } else {
                message = "<unknown cause>";
            }
        }
        return message;
    }

    protected boolean isRouterSettlingError(Exception e) {
        String[] settlingErrors = {
                "internal error",
                "lock-denied"
        };
        StackTraceAnalyzer stackTraceAnalyzer = new StackTraceAnalyzer();
        return stackTraceAnalyzer.isErrorPresent(settlingErrors, e);
    }

    protected boolean isConnectionResetError(Exception e) {
        String[] settlingErrors = {
                "Connection reset"
        };
        StackTraceAnalyzer stackTraceAnalyzer = new StackTraceAnalyzer();
        return stackTraceAnalyzer.isErrorPresent(settlingErrors, e);
    }

    protected boolean isRouterDataExistsError(Exception e) {
        StackTraceAnalyzer stackTraceAnalyzer = new StackTraceAnalyzer();
        return stackTraceAnalyzer.isErrorPresent("data-exists", e);
    }

    protected String getRouterVersion(NetconfSession session) throws ProviderException {
        RouterUtil routerUtil = new RouterUtil(netconfRouterHost, netconfRouterPort, netconfAdminUsername, netconfAdminPassword);

        int retryWait = waitForOperationRetryWaitInMilliseconds;
        int retryCount = waitForOperationRetryCount;
        String routerVersion = null;

        while (retryCount-- > 0) {
            try {
                routerVersion = routerUtil.getRouterVersion(routerCapabilityRevisionsMap, session);
                break;
            } catch (Exception e) {
                if (isConnectionResetError(e)) {
                    logger.warn(getLogtag() + "Connection reset while getting router version- retrying...");
                    operationSleep(retryWait);
                }
                String errMsg = "Failed to get router version";
                logger.error(getLogtag() + errMsg, e);
                throw new ProviderException(errMsg, e);
            }
        }

        if (retryCount < 0) {
            String errMsg = "Failed to get router version after " + waitForOperationRetryCount + " retries";
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

        return routerVersion;
    }

    // ####################################### Generic builders ########################################################

    public Long getRouterExecutionTimeInMilliseconds() {
        return routerExecutionTimeInMilliseconds;
    }

    public void setRouterExecutionTimeInMilliseconds(long time) {
        routerExecutionTimeInMilliseconds = time;
    }

    protected void console(String message) {
        System.out.println(message);
    }
}
