package edu.emory.it.service.ca.provider;

import com.tailf.jnc.NetconfSession;

public interface CiscoAsrProvider {

    Long getRouterExecutionTimeInMilliseconds();

    void setRouterExecutionTimeInMilliseconds(long time);

    boolean saveConfig(NetconfSession session) throws ProviderException;

}
