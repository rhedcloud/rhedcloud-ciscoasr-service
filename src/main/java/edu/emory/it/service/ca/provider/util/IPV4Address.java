package edu.emory.it.service.ca.provider.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPV4Address {

    final private static Long _16To0 = 1L;
    final private static Long _16To2 = 256L;
    final private static Long _16To4 = 65536L;
    final private static Long _16To6 = 16777216L;

    public static String adjustAddressByOffset(String address, Integer offset) {

        Integer[] octets = getOctets(address);

        // Convert to long value.
        // Everything must be Long values to prevent over/under flow errors.
        Long value = (octets[0].longValue() * _16To0) + (octets[1].longValue() * _16To2) + (octets[2].longValue() * _16To4) + (octets[3].longValue() * _16To6);

        // Add offset (can be negative).
        Long addjustedValue = value + offset.longValue();

        // Convert to HEX value and extract octets.
        String valueHexString = Long.toHexString(addjustedValue);

        int length = valueHexString.length();

        // Octet1 new value
        if (length > 1) {
            octets[0] = Integer.valueOf(valueHexString.substring(length - 2), 16);
            valueHexString = valueHexString.substring(0, length - 2);
        } else {
            octets[0] = Integer.valueOf(valueHexString.substring(length - 1), 16);
            valueHexString = valueHexString.substring(0, length - 1);
        }
        length = valueHexString.length();

        if (length > 1) {
            octets[1] = Integer.valueOf(valueHexString.substring(length - 2), 16);
            valueHexString = valueHexString.substring(0, length - 2);
        } else if (length > 0) {
            octets[1] = Integer.valueOf(valueHexString.substring(length - 1), 16);
            valueHexString = valueHexString.substring(0, length - 1);
        } else {
            octets[1] = 0;
        }
        length = valueHexString.length();
        if (length > 1) {
            octets[2] = Integer.valueOf(valueHexString.substring(length - 2), 16);
            valueHexString = valueHexString.substring(0, length - 2);
        } else if (length > 0) {
            octets[2] = Integer.valueOf(valueHexString.substring(length - 1), 16);
            valueHexString = valueHexString.substring(0, length - 1);
        } else {
            octets[2] = 0;
        }
        length = valueHexString.length();
        if (length > 1) {
            octets[3] = Integer.valueOf(valueHexString.substring(length - 2), 16);
        } else if (length > 0) {
            octets[3] = Integer.valueOf(valueHexString.substring(length - 1), 16);
        } else {
            octets[3] = 0;
        }

        address = octets[3].toString() + "." + octets[2] + "." + octets[1] + "." + octets[0];


        return address;
    }

    public static Integer[] getOctets(String address) {
        Integer[] octets = new Integer[4];

        // Extract and validate.
        try {
            Pattern p = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");
            Matcher m = p.matcher(address);
            if (m.matches()) {
                octets[3] = Integer.valueOf(m.group(1));
                octets[2] = Integer.valueOf(m.group(2));
                octets[1] = Integer.valueOf(m.group(3));
                octets[0] = Integer.valueOf(m.group(4));

            } else {
                p = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)\\/(\\d++)");
                m = p.matcher(address);
                if (m.matches()) {
                    octets[3] = Integer.valueOf(m.group(1));
                    octets[2] = Integer.valueOf(m.group(2));
                    octets[1] = Integer.valueOf(m.group(3));
                    octets[0] = Integer.valueOf(m.group(4));

                } else {
                    throw new IllegalArgumentException("Invalid IPV4 address: " + address);
                }

                if (octets[0] < 0 || octets[0] > 255) {
                    throw new IllegalArgumentException("Zeroeth power octet is out of range: " + octets[0]);
                }
                if (octets[1] < 0 || octets[1] > 255) {
                    throw new IllegalArgumentException("Second power octet is out of range: " + octets[1]);
                }
                if (octets[2] < 0 || octets[2] > 255) {
                    throw new IllegalArgumentException("Fourth power octet is out of range: " + octets[2]);
                }
                if (octets[3] < 0 || octets[3] > 255) {
                    throw new IllegalArgumentException("Sixth power octet is out of range: " + octets[3]);
                }

            }

        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid IPV4 address: " + address);
        }

        return octets;
    }

    /**
     * Get greatest common ip range for list of ip addresses passed in
     *
     * @param ipAddresses - list of ip addresses to consider
     * @return ipRange - greates common ip range for ip addresses passed in
     */
    public static String getIpRangeForAddresses(List<String> ipAddresses) {
        Integer[] ipRange = null;

        for (String ip: ipAddresses) {
            Integer[] octets = IPV4Address.getOctets(ip);
            if (ipRange == null) {
                ipRange = new Integer[4];
                ipRange[0] = octets[0];
                ipRange[1] = octets[1];
                ipRange[2] = octets[2];
                ipRange[3] = octets[3];
                continue;
            }

            if (ipRange[3] != null && !ipRange[3].equals(octets[3])) {
                ipRange[3] = null;
            }
            if (ipRange[2] != null && !ipRange[2].equals(octets[2])) {
                ipRange[2] = null;
            }
            if (ipRange[1] != null && !ipRange[1].equals(octets[1])) {
                ipRange[1] = null;
            }
            if (ipRange[0] != null && !ipRange[0].equals(octets[0])) {
                ipRange[0] = null;
            }
        }

        String resultIpRange = null;

        if (ipRange[3] != null) {
            resultIpRange = ipRange[3] + ".";
            if (ipRange[2] != null) {
                resultIpRange += ipRange[2] + ".";
                if (ipRange[1] != null) {
                    resultIpRange += ipRange[1] + ".";
                    if (ipRange[0] != null) {
                        resultIpRange += ipRange[0];
                    }
                }
            }
        }
        return resultIpRange;
    }

}
