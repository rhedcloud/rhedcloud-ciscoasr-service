package edu.emory.it.service.ca.provider.validator.response;

public interface WaitForVpnOperationValidator {

    boolean validate(VpnResponseValidatorCommand vpnAddValidatorCommand);
}
