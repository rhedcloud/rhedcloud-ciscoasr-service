package edu.emory.it.service.ca.provider.validator.response;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile;
import edu.emory.moa.objects.resources.v1_0.BgpPrefixes;
import edu.emory.moa.objects.resources.v1_0.BgpState;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecTransformSet;
import edu.emory.moa.objects.resources.v1_0.CryptoKeyring;
import edu.emory.moa.objects.resources.v1_0.TunnelInterface;
import edu.emory.moa.objects.resources.v1_0.TunnelProfile;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * From Jimmy Kincaid:
 *
 * I think the safest thing to do is to just grab all of the crypto and tunnel config and check that it's all there.
 * As for state, I would only look for Administrative status of UP and not the Operational status.
 *
 * Including:
 * All crypto and tunnel config values sent should be present in returned config, including all name/description values
 * built with vpc number and tunnel number
 */
public class WaitForVpnAddOperationValidator implements WaitForVpnOperationValidator {

    private final Logger logger;
    private final String tunnelNumber;
    private final StringBuilder errorBucket;

    public WaitForVpnAddOperationValidator(Logger logger, String tunnelNumber, StringBuilder errorBucket) {
        this.logger = logger;
        this.tunnelNumber = tunnelNumber;
        this.errorBucket = errorBucket == null ? new StringBuilder() : errorBucket;
    }

    @Override
    public boolean validate(VpnResponseValidatorCommand vpnResponseValidatorCommand) {
        boolean valid = false;

        errorBucket.delete(0, errorBucket.length());

        VpnAddValidatorCommand vpnAddOperationValidator = (VpnAddValidatorCommand) vpnResponseValidatorCommand;
        VpnConnection vpnConnectionFromRouter = vpnAddOperationValidator.getVpnConnection();
        VpnConnectionRequisition vpnConnectionRequisition = vpnAddOperationValidator.getVpnConnectionRequisition();

        String vpcId = vpnConnectionFromRouter.getVpcId();
        String vpnId = vpnConnectionFromRouter.getVpnId();
        TunnelInterface tunnelInterface = vpnConnectionFromRouter.getTunnelInterface(0);
        CryptoKeyring cryptoKeyring = vpnConnectionFromRouter.getCryptoKeyring();
        CryptoIpsecProfile cryptoIpsecProfile = vpnConnectionFromRouter.getCryptoIpsecProfile();
        CryptoIpsecTransformSet cryptoIpsecTransformSet = vpnConnectionFromRouter.getCryptoIpsecTransformSet();

        do {

            if (vpcId != null) {
                if (!vpcId.equals(vpnConnectionRequisition.getOwnerId())) {
                    String warnMsg = "Returned vpcId mismatch - expected: " + vpnConnectionRequisition.getOwnerId() + " received: " + vpcId + " - vpn add is still in progress";
                    logger.warn(getLogtag() + warnMsg);
                    break;
                }
            }

            if (vpnId != null) {
                if (!vpnId.equals(vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId())) {
                    String warnMsg = "Returned vpnId mismatch - expected: " + vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId() + " received: " + vpnId + " - vpn add is still in progress";
                    logger.warn(getLogtag() + warnMsg);
                    break;
                }
            }

            if (!validateTunnelInterface(tunnelInterface, vpnConnectionRequisition)) {
                break;
            }

            /*
               v1.1 now is template driven, so these validators will require a mapper reference and the hydrated add template to refactor.
             */

//            if (!validateCryptoKeyring(cryptoKeyring, vpnConnectionRequisition)) {
//                break;
//            }
//
//            if (!validateCryptoIpsecProfile(cryptoIpsecProfile, vpnConnectionRequisition)) {
//                break;
//            }
//
//            if (!validateCryptoIpsecTransformSet(cryptoIpsecTransformSet, vpnConnectionRequisition)) {
//                break;
//            }

            /**
             * BgpState cannot be part of the add validation, as the tunnel can be provisioned, but may take some
             * time to "come up" (which is what we're checking here). A separate monitoring sequence must be used
             * to watch the tunnel's state.
             *
            if (!validateBgpState(tunnelInterface.getBgpState(), tunnelInterface.getBgpPrefixes())) {
                break;
            }
             */

            valid = true;

        } while (false);

        return valid;
    }

    private boolean validateTunnelInterface(TunnelInterface tunnelInterfaceReturned, VpnConnectionRequisition vpnConnectionRequisition) {
        boolean valid = false;

        if (tunnelInterfaceReturned != null) {

            String name = tunnelInterfaceReturned.getName();
            String administrativeState = tunnelInterfaceReturned.getAdministrativeState();

            if (name != null && administrativeState != null) {

                VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();
                if (vpnConnectionProfile != null ) {
                    TunnelProfile tunnelProfile = vpnConnectionProfile.getTunnelProfile(0);
                    if (tunnelProfile != null) {
                        if (name.equals(tunnelProfile.getTunnelId())) {
                            if (administrativeState.equalsIgnoreCase("up")) {
                                valid = true;
                            } else {
                                logger.warn(getLogtag() + "Administrative status is NOT up: " + administrativeState + " vpn add is still in progress");
                            }
                        } else {
                            String errMsg = "Tunnel Id mismatch - expected: " + tunnelProfile.getTunnelId() + " returned: " + name + " vpn add is still in progress";
                            addMessageToErrorBucket(errMsg);
                            logger.error(getLogtag() +  errMsg);
                        }
                    } else {
                        String errMsg = "No tunnel profile in requisition!";
                        addMessageToErrorBucket(errMsg);
                        logger.error(getLogtag() + errMsg);
                    }
                }
            }
        }
        return valid;
    }

    /*
       v1.1 now is template driven, so these validators will require a mapper reference and the hydrated add template to refactor.
     */
//    private boolean validateCryptoKeyring(CryptoKeyring cryptoKeyringReturned, VpnConnectionRequisition vpnConnectionRequisition) {
//        boolean valid = false;
//
//        if (cryptoKeyringReturned != null) {
//
//            String name = cryptoKeyringReturned.getName();
//
//            if (name != null) {
//
//                VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();
//                if (vpnConnectionProfile != null ) {
//                    TunnelProfile tunnelProfile = vpnConnectionProfile.getTunnelProfile(0);
//                    if (tunnelProfile != null) {
//                        if (name.equals(tunnelProfile.getCryptoKeyringName())) {
//                            valid = true;
//                        } else {
//                            logger.warn(getLogtag() + "crypto keyring name mismatch - expected: " + tunnelProfile.getCryptoKeyringName() + " returned: " + name + " vpn add is still in progress");
//                        }
//                    } else {
//                        String errMsg = "No tunnel profile in requisition!";
//                        addMessageToErrorBucket(errMsg);
//                        logger.error(getLogtag() + errMsg);
//                    }
//                }
//            }
//        }
//        return valid;
//    }
//
//    private boolean validateCryptoIpsecProfile(CryptoIpsecProfile cryptoIpsecProfileReturned, VpnConnectionRequisition vpnConnectionRequisition) {
//        boolean valid = false;
//
//        if (cryptoIpsecProfileReturned != null) {
//
//            String name = cryptoIpsecProfileReturned.getName();
//
//            if (name != null) {
//
//                VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();
//                if (vpnConnectionProfile != null ) {
//                    TunnelProfile tunnelProfile = vpnConnectionProfile.getTunnelProfile(0);
//                    if (tunnelProfile != null) {
//                        if (name.equals(tunnelProfile.getIpsecProfileName())) {
//                            valid = true;
//                        } else {
//                            String errMsg = "crypto ipsec profile name mismatch - expected: " + tunnelProfile.getIpsecProfileName() + " returned: " + name + " vpn add is still in progress";
//                            addMessageToErrorBucket(errMsg);
//                            logger.error(getLogtag() + errMsg);
//                        }
//                    } else {
//                        String errMsg = "No tunnel profile in requisition!";
//                        addMessageToErrorBucket(errMsg);
//                        logger.error(getLogtag() + errMsg);
//                    }
//                }
//            }
//        }
//        return valid;
//    }
//
//    private boolean validateCryptoIpsecTransformSet(CryptoIpsecTransformSet cryptoIpsecTransformSetReturned, VpnConnectionRequisition vpnConnectionRequisition) {
//        boolean valid = false;
//
//        if (cryptoIpsecTransformSetReturned != null) {
//
//            String name = cryptoIpsecTransformSetReturned.getName();
//
//            if (name != null) {
//
//                VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();
//                if (vpnConnectionProfile != null ) {
//                    TunnelProfile tunnelProfile = vpnConnectionProfile.getTunnelProfile(0);
//                    if (tunnelProfile != null) {
//                        if (name.equals(tunnelProfile.getIpsecTransformSetName())) {
//                            valid = true;
//                        } else {
//                            String errMsg = "crypto ipsec transform set name mismatch - expected: " + tunnelProfile.getIpsecTransformSetName() + " returned: " + name;
//                            addMessageToErrorBucket(errMsg);
//                            logger.error(getLogtag() + errMsg);
//                        }
//                    } else {
//                        String errMsg = "";
//                        addMessageToErrorBucket(errMsg);
//                        logger.error(getLogtag() + errMsg);
//                    }
//                }
//            }
//        }
//        return valid;
//    }
//
    /**
     * Validations:
     * 1) status must be 'fsm-established'
     * 2) uptime must satisfy following regex: '\d\d\:\d\d\:\d\d' or '\dd:\dh'
     * 3) sentPrefix and receivedPrefix must be > 0
     * 4) neighborId must be valid ip address
     *
     * Determine all validation failures.
     *
     * NOTES:
     * 1) This should not be part of add operation validation due to the nature of tunnel bgp state
     * 2) This should be used as part of a monitoring service.
     *
     * @param bgpState - returned from router
     * @return true if valid, false otherwise
     */

    private boolean validateBgpState(BgpState bgpState, BgpPrefixes bgpPrefixes) {
        boolean isValid = true;

        String neighborId = bgpState.getNeighborId();
        String status = bgpState.getStatus();
        String uptime = bgpState.getUptime();
        Integer sent;
        Integer received;

        // BgpState
        if (!"fsm-established".equals(status)) {
            isValid = false;
            String errMsg = "Invalid bgp state status: " + status;
            logger.error(getLogtag() + errMsg);
            addMessageToErrorBucket(errMsg);
        }

        if (!(neighborId != null && isValidIpAddress(neighborId))) {
            isValid = false;
            String errMsg = "Invalid neighbor id ip address: " + neighborId;
            addMessageToErrorBucket(errMsg);
            logger.error(getLogtag() + errMsg);
        }

        if (!(uptime != null && isValidTimeValue(uptime))) {
            isValid = false;
            String errMsg = "Invalid bgp state up time: " + uptime;
            addMessageToErrorBucket(errMsg);
            logger.error(getLogtag() + errMsg);
        }

        // BpgPrefixes
        try {
            sent = Integer.valueOf(bgpPrefixes.getSent());

            if (!(sent > 0)) {
                isValid = false;
                String errMsg = "Invalid bgp prefix - sent: " + sent;
                addMessageToErrorBucket(errMsg);
                logger.error(getLogtag() + errMsg);
            }
        } catch (NumberFormatException e) {
            isValid = false;
            String errMsg = "Invalid bgp prefix - sent: " + bgpPrefixes.getSent();
            addMessageToErrorBucket(errMsg);
            logger.error(errMsg);
        }

        try {
            received = Integer.valueOf(bgpPrefixes.getReceived());

            if (!(received > 0)) {
                isValid = false;
                String errMsg = "Invalid bgp prefix received: " + received;
                addMessageToErrorBucket(errMsg);
                logger.error(getLogtag() + errMsg);
            }

        } catch (NumberFormatException e) {
            isValid = false;
            String errMsg = "Invalid bgp prefix received: " + bgpPrefixes.getReceived();
            addMessageToErrorBucket(errMsg);
            logger.error(errMsg);
        }

        return isValid;
    }

    private boolean isValidIpAddress(String ipAddress) {
        boolean valid = true;

        try {
            Pattern p = Pattern.compile("(\\d+?)\\.(\\d+?)\\.(\\d+?)\\.(\\d+?)");
            Matcher m = p.matcher(ipAddress.trim());
            if (m.matches()) {
                for (int n = 0; n < m.groupCount(); n++) {
                    int octet = Integer.parseInt(m.group(n + 1));
                    if (octet > 255) {
                        logger.error(getLogtag() + "Invalid IP address: " + ipAddress);
                        valid = false;
                        break;
                    }
                }
            } else {
                logger.error(getLogtag() + "Invalid IP address: " + ipAddress);
                valid = false;
            }
        } catch (Exception e) {
            logger.error( getLogtag() + "Invalid IP address: " + ipAddress, e);
            valid = false;
        }

        return valid;
    }

    private boolean isValidTimeValue(String timeValue) {
        boolean isValid = true;

        Pattern pattern = Pattern.compile("(?:((\\d{2})\\:(\\d{2})\\:(\\d{2})))|(?:(\\d{1,3}[dm]+\\d{1,3}[dh]+))");
        Matcher matcher = pattern.matcher(timeValue);
        if (matcher.find()) {
            int groupCount = matcher.groupCount();
            String threePartTime = matcher.group(1);
            String hourString = matcher.group(2);
            String minuteString = matcher.group(3);
            String secondString = matcher.group(4);
            String twoPartTime = matcher.group(5);
            if (threePartTime != null) {
                int hour = Integer.parseInt(hourString);
                int minute = Integer.parseInt(minuteString);
                int second = Integer.parseInt(secondString);
                if (hour < 0 || hour > 23) {
                    isValid = false;
                    logger.error("Invalid time value - hour: " + hour);
                }
                if (minute < 0 || minute > 59) {
                    isValid = false;
                    logger.error("Invalid time value - minute: " + minute);
                }
                if (second < 0 || second > 59) {
                    isValid = false;
                    logger.error("Invalid time value - second: " + second);
                }
            } else {
                // Valid.
                logger.info("Two part time: " + twoPartTime);
            }
        } else {
            isValid = false;
        }

        return isValid;
    }

    private void addMessageToErrorBucket(String message) {
        errorBucket.append(message + "\n");
    }

    private String getLogtag() {
        return "[CiscoAsr" + tunnelNumber + "][" + this.getClass().getSimpleName() + "] ";
    }

}

