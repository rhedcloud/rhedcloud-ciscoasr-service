package edu.emory.it.service.ca.provider;

import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;
import org.openeai.config.AppConfig;

import java.util.List;

public interface StaticNatProvider {

    /**
     * Initialize the provider.
     * <P>
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * <P>
     * @throws ProviderException with details of the initialization error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    // TODO: Other provider interface goes here.


    void add(StaticNat staticNat) throws ProviderException;

    void delete(StaticNat staticNat) throws ProviderException;

    List<StaticNat> query(StaticNatQuerySpecification staticNat) throws ProviderException;

}
