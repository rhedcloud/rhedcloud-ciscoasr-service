package edu.emory.it.service.ca.provider;

import edu.emory.it.service.ca.NotificationSyncProvider;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;

public class ExampleNotificationSyncProvider extends ExampleProvider implements NotificationSyncProvider {

    @Override
    public boolean sendSaveConfig() {
        return true;
    }

}
