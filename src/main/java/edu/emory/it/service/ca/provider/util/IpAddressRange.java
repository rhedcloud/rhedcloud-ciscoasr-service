package edu.emory.it.service.ca.provider.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class IpAddressRange {

    final public static Integer COLLATED_RANGES = 1;
    final public static Integer OVERLAPPED_RANGES = 2;

    private Boolean verbose = Boolean.FALSE;

    public IpAddressRange() {
    }

    /**
     * Build an or-ed text comparison clause with all possible IP address values for the range passed in.  The
     * ipAddressRange value passed in is "extended" ip address format allowing for octet ranges.  Only the 2 least
     * significant octets are supported. The format of the ipAddressRange value is:
     * <p>
     * x.x.n1-m1.n2-m2
     * <p>
     * where n1 < m1 and n2 < m3, and {n1, m1, n2, m3} in the set of {0..255}
     * <p>
     * This can potentially produce 64K elements in the clause, for the following range:
     * <p>
     * x.x.0-255.0-255
     *
     * @param ipAddressRanges - ip range value to process
     * @param filterPrefix    - xpath filter prefix, e.g., native/ip/nat/inside/source/static/nat-static-transport-list/global-ip
     * @return - complete xpath filter
     * @throws com.sun.javaws.exceptions.InvalidArgumentException - for invalid ranges or ranges in the 2 most
     *                                                            significant octets
     */
    public String buildFilterClause(Collection<String> ipAddressRanges, String filterPrefix) {
        StringBuilder builder = new StringBuilder(filterPrefix + "[");

        StringBuilder infoBuilder = new StringBuilder();

        Set<String> totalExpandedIpRanges = new TreeSet<>();

        ipAddressRanges.forEach(iar -> {
            Set<String> expandedIpRanges = expandIpRanges(iar);
            totalExpandedIpRanges.addAll(expandedIpRanges);
        });

        logInfo("buildFilterClause: expanded ip ranges count: " + totalExpandedIpRanges.size());

        totalExpandedIpRanges.forEach(teir -> {
            infoBuilder.append("\n    " + teir);
            if (builder.indexOf("text()") != -1) {
                builder.append(" or ");
            }
            builder.append("text() = '" + teir + "'");
        });
        builder.append("]");

        if (verbose) {
            logInfo(infoBuilder.toString());
        }
        return builder.toString();
    }


    public Set<String> expandIpRanges(String ipAddressRange) {
        Set<String> ipAddresses;

        OctetRanges ranges = buildOctetRanges(ipAddressRange);

        ipAddresses = buildRange(ranges);

        return ipAddresses;
    }

    private Map<Integer, Collection<OctetRanges>> collateOctetRanges(List<String> ipAddressRanges) {
        List<OctetRanges> rangesList = new ArrayList<>();


        if (verbose) {
            logInfo("IP Address ranges:");
        }
        ipAddressRanges.forEach(ipAddressRange -> {
            if (verbose) {
                logInfo("    " + ipAddressRange);
            }
            OctetRanges range = buildOctetRanges(ipAddressRange);
            rangesList.add(range);
        });

        List<OctetRanges> collatedRanges = new ArrayList<>();
        for (OctetRanges r : rangesList) {
            collatedRanges.add(r.clone());
        }
        ;

        Boolean iterate = Boolean.TRUE;

        while (iterate) {

            List<OctetRanges> savedRanges = new ArrayList<>();
            for (OctetRanges r : collatedRanges) {
                savedRanges.add(r.clone());
            }

            logInfo("Calling combine...");
            combine(collatedRanges);
            logInfo("Returned from combine.");

            if (collatedRanges.size() != savedRanges.size()) {
                continue;
            }

            iterate = Boolean.FALSE;

            for (OctetRanges r : collatedRanges) {
                if (!savedRanges.contains(r)) {
                    iterate = Boolean.TRUE;
                    break;
                }
            }
        }

        Set<OctetRanges> overlappingSet = new HashSet<>();

        for (OctetRanges r : rangesList) {
            if (!collatedRanges.contains(r)) {
                overlappingSet.add(r);
            }
        }
        if (verbose) {
            analyzeCollatedResutls(collatedRanges, overlappingSet, rangesList);
        }

        Map<Integer, Collection<OctetRanges>> rangesMap = new HashMap<>();
        rangesMap.put(COLLATED_RANGES, collatedRanges);
        rangesMap.put(OVERLAPPED_RANGES, overlappingSet);

        return rangesMap;
    }

    private void combine(List<OctetRanges> rangesList) {

        Set<OctetRanges> eliminatedSet = new HashSet<>();
        List<OctetRanges> finalList = new ArrayList<>();
        Boolean modified;
        do {
            modified = Boolean.FALSE;
            for (int n = 0; n < rangesList.size(); n++) {
                OctetRanges nth = rangesList.get(n);
                for (int m = n + 1; m < rangesList.size(); m++) {
                    OctetRanges mth = rangesList.get(m);

                    OctetRanges combined = nth.combine(mth);
                    if (combined != null) {
                        if (!finalList.contains(combined)) {
                            logInfo("Combining: " + nth.toString() + " " + mth.toString() + " => " + combined);
                            modified = Boolean.TRUE;
                            finalList.add(combined);
                            eliminatedSet.add(nth);
                            eliminatedSet.add(mth);
                            finalList.remove(nth);
                            finalList.remove(mth);
                        }
                    } else {
                        if (!finalList.contains(nth) && !eliminatedSet.contains(nth)) {
                            modified = Boolean.TRUE;
                            finalList.add(nth);
                        }
                        if (!finalList.contains(mth) && !eliminatedSet.contains(mth)) {
                            modified = Boolean.TRUE;
                            finalList.add(mth);
                        }
                    }
                }
            }
            if (modified) {
                rangesList.clear();
                rangesList.addAll(finalList);
                modified = Boolean.FALSE;
            }
        } while (modified);

    }

    private void analyzeCollatedResutls(Collection<OctetRanges> collatedRanges, Collection<OctetRanges> overlappingSet, Collection<OctetRanges> rangesList) {
        logInfo("Eliminated set:");
        overlappingSet.forEach(r -> {
            if (!collatedRanges.contains(r)) {
                logInfo(r.toString());
            }
        });

        logInfo("Collated list:");

        collatedRanges.forEach(r -> {
            logInfo(r.toString());
        });

        if (overlappingSet.size() > 0) {
            int coveredCount = 0;

            OctetRanges[] rangesArray = rangesList.toArray(new OctetRanges[rangesList.size()]);
            OctetRanges[] collatedRangesArray = collatedRanges.toArray(new OctetRanges[collatedRanges.size()]);
            for (int n = 0; n < rangesArray.length; n++) {
                for (int m = 0; m < collatedRangesArray.length; m++) {
                    if (collatedRangesArray[m].combine(rangesArray[n]) != null) {
                        coveredCount++;
                        break;
                    }
                }
            }
            if (coveredCount == rangesList.size()) {
                logInfo("All initial ip ranges covered by combined list.");
            } else {
                logError("***ERROR: Initial ip ranges are NOT covered by combined list.");
            }
        } else {
            logInfo("No overlapping ranges found.");
        }

    }

    private OctetRanges buildOctetRanges(String ipAddressRange) {
        Boolean verbose = Boolean.FALSE;

        final Pattern ipRangePattern = Pattern.compile("((\\d+\\-\\d+)|(\\d+))\\.((\\d+\\-\\d+)|(\\d+))\\.((\\d+\\-\\d+)|(\\d+))\\.((\\d+\\-\\d+)|(\\d+))");
        final Matcher ipRangeMatcher = ipRangePattern.matcher(ipAddressRange);

        OctetRanges octetRanges;

        if (ipRangeMatcher.matches()) {

            if (verbose) {
                for (int n = 1; n <= ipRangeMatcher.groupCount(); n++) {
                    logInfo("Group: " + n + " - value: " + ipRangeMatcher.group(n));
                }
            }
            octetRanges = new OctetRanges(ipRangeMatcher.group(1), ipRangeMatcher.group(4), ipRangeMatcher.group(7), ipRangeMatcher.group(10));

        } else {
            throw new IllegalArgumentException("Invalid ip address range: " + ipAddressRange);
        }

        return octetRanges;
    }

    private Set<String> buildRange(OctetRanges ranges) {
        Set<String> ipAddresses = new HashSet<>();

        for (Integer i = ranges.get(0).start; i <= ranges.get(0).end; i++) {
            for (Integer j = ranges.get(1).start; j <= ranges.get(1).end; j++) {
                for (Integer n = ranges.get(2).start; n <= ranges.get(2).end; n++) {
                    for (Integer m = ranges.get(3).start; m <= ranges.get(3).end; m++) {
                        String ipAddress = i + "." + j + "." + n + "." + m;
                        ipAddresses.add(ipAddress);
                    }
                }
            }
        }
        return ipAddresses;
    }

    public Long calculateIpAddressCount(String ipAddressRange) {
        Long count;

        OctetRanges ranges = buildOctetRanges(ipAddressRange);

        if (ranges.get(0).isRange()) {
            count = ranges.get(0).end - ranges.get(0).start + 1L;
        } else {
            count = 1L;
        }

        if (ranges.get(1).isRange()) {
            count *= ranges.get(1).end - ranges.get(1).start + 1L;
        } else {
            count *= 1L;
        }

        if (ranges.get(2).isRange()) {
            count *= ranges.get(2).end - ranges.get(2).start + 1L;
        } else {
            count *= 1L;
        }

        if (ranges.get(3).isRange()) {
            count *= ranges.get(3).end - ranges.get(3).start + 1L;
        } else {
            count *= 1L;
        }

        return count;
    }

    private void logInfo(String message) {
        System.out.println(message);
    }

    private void logError(String errorMessage) {
        System.err.println(errorMessage);
    }

    private class OctetRange implements Comparator<OctetRange> {

        String value;
        Integer octetNumber;
        Integer start;
        Integer end;

        @Override
        public int hashCode() {
            return Objects.hashCode(value + octetNumber + start + end);
        }

        public OctetRange(String value, Integer octetNumber) {
            this.value = value;
            this.octetNumber = octetNumber;
        }

        private OctetRange() {
            start = new Integer(-1);
            end = new Integer(256);
            value = "";
            octetNumber = new Integer(-1);
        }

        String validate() {
            String[] rangeParts;
            rangeParts = value.split("\\-");

            StringBuilder errors = new StringBuilder();

            if (rangeParts.length == 2) {

                start = Integer.valueOf(rangeParts[0]);
                end = Integer.valueOf(rangeParts[1]);

                if (start > 255) {
                    errors.append("\n\t-Range " + octetNumber + " start value exceeds 255");
                }
                if (end > 255) {
                    errors.append("\n\t-Range " + octetNumber + " end value exceeds 255");
                }
                if (start >= end) {
                    errors.append("\n\t-Range " + octetNumber + " start value is greater than or equal to end value");
                }

            } else {
                start = Integer.valueOf(value);
                end = Integer.valueOf(value);
                if (start > 255) {
                    errors.append("\n\t-Octet " + octetNumber + " value exceeds 255");
                }
            }

            return errors.toString();
        }

        @Override
        public boolean equals(Object o) {
            boolean isEquals = false;

            if (o instanceof OctetRange) {
                OctetRange octetRange = (OctetRange) o;
                if (this.octetNumber.equals(((OctetRange) o).octetNumber)
                        && this.value.equals(octetRange.value)
                        && this.start.equals(octetRange.start)
                        && this.end.equals(octetRange.end)) {
                    isEquals = true;
                } else {
                    isEquals = false;
                }
            }
            return isEquals;
        }

        public void buildValue() {
            if (null != value && !value.isEmpty()) {
                if (start.equals(end)) {
                    value = start.toString();
                } else {
                    value = start.toString() + "-" + end.toString();
                }
            }
        }

        public OctetRange clone() {
            OctetRange clone = new OctetRange();

            clone.octetNumber = this.octetNumber;
            clone.value = this.value;
            clone.start = this.start;
            clone.end = this.end;

            return clone;
        }

        public Boolean isRange() {
            return ((start != null && end != null) && !start.equals(end));
        }

        @Override
        public int compare(OctetRange o1, OctetRange o2) {
            int returnVal = 1;

            if (o1.value.equals(o2.value)
                    && o1.octetNumber.equals(o2.octetNumber)
                    && o1.start.equals(o2.start)
                    && o1.end.equals(o2.end)) {
                returnVal = 0;
            }
            return returnVal;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();

            builder.append("Value: " + this.value + " octetNumber: " + this.octetNumber + " start: " + this.start + " end: " + this.end);

            return builder.toString();
        }
    }

    private class OctetRanges implements Comparator<OctetRanges> {

        private OctetRange[] ranges;

        @Override
        public int hashCode() {
            return Objects.hashCode(ranges[0].hashCode() + ranges[1].hashCode() + ranges[2].hashCode() + ranges[3].hashCode());
        }

        public OctetRanges(String octet1Value, String octet2Value, String octet3Value, String octet4Value) {
            ranges = new OctetRange[4];

            ranges[0] = new OctetRange(octet1Value, 1);
            ranges[1] = new OctetRange(octet2Value, 2);
            ranges[2] = new OctetRange(octet3Value, 3);
            ranges[3] = new OctetRange(octet4Value, 4);

            validate();
        }

        private OctetRanges() {
        }

        private void validate() {
            StringBuilder errors = new StringBuilder();

            errors.append(ranges[0].validate());
            errors.append(ranges[1].validate());
            errors.append(ranges[2].validate());
            errors.append(ranges[3].validate());

            if (errors.length() > 0) {
                throw new IllegalArgumentException("Invalid ip address range " + ranges[0] + "." + ranges[1] + "." + ranges[2] + "." + ranges[3] + ": bad range values:" + errors.toString());
            }

        }

        @Override
        public boolean equals(Object o) {

            boolean isEqual = false;
            if (o instanceof OctetRanges) {
                OctetRanges octetRanges = (OctetRanges) o;
                isEqual = true;
                for (int n = 0; n < octetRanges.ranges.length; n++) {
                    if (!this.ranges[n].equals(octetRanges.ranges[n])) {
                        isEqual = false;
                        break;
                    }
                }
            }

            return isEqual;
        }

        @Override
        public int compare(OctetRanges o1, OctetRanges o2) {
            int returnVal = 1;

            if (o1.ranges[0].equals(o2.ranges[0])
                    && o1.ranges[1].equals(o2.ranges[1])
                    && o1.ranges[2].equals(o2.ranges[2])
                    && o1.ranges[3].equals(o2.ranges[3])) {
                returnVal = 0;
            }

            return returnVal;
        }

        @Override
        public OctetRanges clone() {
            OctetRanges clone = new OctetRanges();
            clone.ranges = new OctetRange[this.ranges.length];
            for (int n = 0; n < this.ranges.length; n++) {
                clone.ranges[n] = this.ranges[n].clone();
            }

            clone.validate();

            return clone;
        }

        public OctetRanges combine(OctetRanges that) {
            OctetRanges combined = null;

            if (this.ranges.length == that.ranges.length) {
                for (int n = 0; n < this.ranges.length; n++) {
                    OctetRange thisOctetRange = this.ranges[n];
                    OctetRange thatOctetRange = that.ranges[n];

                    Boolean overlap = ((thisOctetRange.start >= thatOctetRange.start && thisOctetRange.start <= thatOctetRange.end)
                            || (thisOctetRange.end >= thatOctetRange.start && thisOctetRange.end <= thatOctetRange.end))
                            || ((thatOctetRange.start >= thisOctetRange.start && thatOctetRange.start <= thisOctetRange.end)
                            || (thatOctetRange.end >= thisOctetRange.start && thatOctetRange.end <= thisOctetRange.end));

                    if (overlap) {
                        if (combined == null) {
                            combined = this.clone();
                        }
                        OctetRange combinedRange = thisOctetRange.clone();
                        combinedRange.start = thisOctetRange.start < thatOctetRange.start ? thisOctetRange.start : thatOctetRange.start;
                        combinedRange.end = thisOctetRange.end > thatOctetRange.end ? thisOctetRange.end : thatOctetRange.end;
                        combinedRange.buildValue();
                        combined.ranges[n] = combinedRange;
                    } else {
                        combined = null;
                        break;
                    }

                }
            }

            return combined;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();

            for (int n = 0; ranges != null && n < ranges.length; n++) {
                OctetRange octetRange = this.get(n);
                if (n > 0) {
                    builder.append(".");
                }
                if (octetRange.isRange()) {
                    builder.append(octetRange.start);
                    builder.append("-");
                    builder.append(octetRange.end);
                } else {
                    builder.append(octetRange.start);
                }
            }
            return builder.toString();

        }

        public String dump() {
            StringBuilder builder = new StringBuilder();
            for (int n = 0; n < ranges.length; n++) {
                builder.append("    " + n + ": " + ranges[n].toString());
            }
            return builder.toString();
        }

        OctetRange get(Integer index) {
            return ranges[index];
        }

        Integer size() {
            return ranges.length;
        }
    }

    public static void main(String[] args) {
        IpAddressRange utils = new IpAddressRange();

        utils.testOverlap();
//        utils.test();
//        utils.testSegmented();
    }

    public void test() {
        OctetRanges or1 = new OctetRanges("2", "1-15", "10", "4-100");
        OctetRanges or2 = new OctetRanges("2", "10-20", "10", "2-95");

        OctetRanges combined = or1.combine(or2);
        if (combined == null) {
            combined = or2.combine(or1);
        }

        if (combined != null) {
            logInfo("COMBINED: " + combined);
        }

    }

    public void testSegmented() {
        List<String> ipAddresses = new ArrayList<>();

        // Current configuration segmented into 4 ranges per
        ipAddresses.add("170.140.248.0-31");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.32-63");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.64-95");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.96-127");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.128-159");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.160-191");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.192-223");
        collate(ipAddresses);
        ipAddresses.add("170.140.248.224-255");
        collate(ipAddresses);

        ipAddresses.add("170.140.249.0-31");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.32-63");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.64-95");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.96-127");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.128-159");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.160-191");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.192-223");
        collate(ipAddresses);
        ipAddresses.add("170.140.249.224-255");
        collate(ipAddresses);

    }

    public void testOverlap() {
        List<String> ipAddresses = new ArrayList<>();

        /*
            2.1-20.10.2-80
            2.2.10.4
            2.1-15.10.90-100
         */
        ipAddresses.add("2.14.10.4");
        ipAddresses.add("2.10-20.10.2-80");
        ipAddresses.add("2.1-15.10.90-100");
        ipAddresses.add("2.3.11-13.4-12");
        ipAddresses.add("2.11.10.4-95");

//        ipAddresses.add("1.2.3.4");
//        ipAddresses.add("5.6.7.8");
//        ipAddresses.add("9.10.11.100-200");
//        ipAddresses.add("20.21.100-200.23");

        // This will bring back 256 separate IP addresses that need to be or'd together
//        ipAddresses.add("170.140.80.0-127");
//        ipAddresses.add("170.140.81.0-127");

        // Current configuration
//        ipAddresses.add("170.140.248.0-255");
//        ipAddresses.add("170.140.249.0-255");

        // These addresses will exceed 64K limit
//        ipAddresses.add("1-2.2-4.10-15.65-75");
//        ipAddresses.add("2-3.4-8.14-22.70-100");
//        ipAddresses.add("3-20.7-100.21-99.99-200");

        logInfo("IP Addresses:");
        ipAddresses.forEach(ia -> {
            logInfo("   " + ia);
        });
        collate(ipAddresses);
    }

    private void collate(List<String> ipAddresses) {
        logInfo("================================================================");
        Map<Integer, Collection<OctetRanges>> rangesInfo = collateOctetRanges(ipAddresses);
        Collection<OctetRanges> collatedOctetRanges = rangesInfo.get(IpAddressRange.COLLATED_RANGES);
        Collection overlappedOctedRanges = rangesInfo.get(IpAddressRange.OVERLAPPED_RANGES);

        if (overlappedOctedRanges.size() > 0) {
            logInfo("#### COLLATED OCTET RANGES ####");
        } else {
            logInfo("### OCTET RANGES ###");
        }
        Long totalCount = 0L;
        for (OctetRanges cor : collatedOctetRanges) {
            logInfo("   " + cor);
            totalCount += calculateIpAddressCount(cor.toString());
        }

        logInfo("Calculated total count: " + totalCount);

        if (totalCount <= 65536) {
            if (overlappedOctedRanges.size() > 0) {
                logInfo("!!!! OVERLAPPED OCTET RANGES !!!!");
                overlappedOctedRanges.forEach(oor -> {
                    logInfo("   " + oor);
                });
            }

            List<String> ocllatedIpRangesAsString = collatedOctetRanges.stream()
                    .map(cor -> cor.toString())
                    .collect(Collectors.toList());

            String xpathFilter = buildFilterClause(ocllatedIpRangesAsString, "native/ip/nat/inside/source/static/nat-static-transport-list/global-ip");

            logInfo(xpathFilter);
        } else {
            logError("Calculated total count exceeds 64K max! Aborting filter clause construction.");
        }
        ipAddresses.clear();
    }
}