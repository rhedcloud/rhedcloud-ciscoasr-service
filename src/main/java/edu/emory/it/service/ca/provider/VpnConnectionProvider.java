package edu.emory.it.service.ca.provider;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.openeai.config.AppConfig;

import java.util.List;

public interface VpnConnectionProvider {
    /**
     * Initialize the provider.
     * <P>
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * <P>
     * @throws ProviderException with details of the initialization error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    List<VpnConnection> query(VpnConnectionQuerySpecification querySpec) throws ProviderException;

    VpnConnection generate(VpnConnectionRequisition vpnConnectionRequisition) throws ProviderException;

    void delete(VpnConnection vpnConnection) throws ProviderException;

}
