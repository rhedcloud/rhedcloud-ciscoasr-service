package edu.emory.it.service.ca.provider.util;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RouterUtil {

    private String LOGTAG = "[RouterUtil] ";
    private final Logger logger = OpenEaiObject.logger;

    final String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?><hello/>]]>]]";

    private final String hostname;
    private final int port;
    private final String username;
    private final String password;

    public RouterUtil(String hostname, int port, String username, String password) {
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    private final String getRouterCapabilitiesRpc = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"1\">\n" +
            "    <nc:get>\n" +
            "        <nc:filter><netconf-state xmlns=\"urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring\">\n" +
            "            <capabilities/>\n" +
            "        </netconf-state></nc:filter>\n" +
            "    </nc:get>\n" +
            "</nc:rpc>\n";

    public String getRouterVersion(Map<String, Map<String, String>> routerCapabilityRevisionsMap, NetconfSession session) throws IOException, JNCException {
        String routerVersion;
        Map<String, String> routerCapabilitesMap;
        String capabilities = sendHelloCommandToRouter(session);
        if (capabilities != null) {
            routerCapabilitesMap = getCapabilityRevisionsMap(capabilities);

            Optional<Map.Entry<String, Map<String, String>>> optionalVersion = routerCapabilityRevisionsMap.entrySet().stream()
                    .filter(es -> {
                        Map<String, String> revisionMap = es.getValue();

                        return revisionMap.entrySet()
                                .stream()
                                .allMatch(e -> (routerCapabilitesMap.containsKey(e.getKey()) && routerCapabilitesMap.get(e.getKey()).equals(e.getValue())));
                    })
                    .findAny();
            if (optionalVersion.isPresent()) {
                routerVersion = optionalVersion.get().getKey();
            } else {
                throw new RuntimeException("Capabilities from router do not match any configured router version");
            }
        } else {
            throw new RuntimeException("No capabilities returned from router!");
        }

        // BY CONVENTION, router version is just what's right of the '_' (RouterRevisionMap_1662)
        return routerVersion.replace("RouterRevisionMap_", "");
    }

    private String sendHelloCommandToRouter(NetconfSession session) throws IOException, JNCException {

        Element capabilitiesElement = session.rpc(getRouterCapabilitiesRpc);

        capabilitiesElement = capabilitiesElement.getFirst("data/netconf-state/capabilities");

        String capabilities = capabilitiesElement.getChildren().stream()
                .map(c -> c.getValue().toString())
                .collect(Collectors.joining("\n"));

        return capabilities;
    }

    private Map<String, String> getCapabilityRevisionsMap(String capabilities) {
        Map<String, String> capabilityRevisionsMap = new HashMap<>();

        StringBuilder builder = new StringBuilder();
        List<Pattern> patterns = new ArrayList<>();

        // Patterns could be configured.
        patterns.add(Pattern.compile("Cisco-IOS-XE-(tunnel).+?module=Cisco-IOS-XE-tunnel.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));
        patterns.add(Pattern.compile("Cisco-IOS-XE-(crypto).+?module=Cisco-IOS-XE-crypto.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));
        patterns.add(Pattern.compile("Cisco-IOS-XE-(native).+?module=Cisco-IOS-XE-native.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));
        patterns.add(Pattern.compile("Cisco-IOS-XE-(bgp).+?module=Cisco-IOS-XE-bgp.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));

        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(capabilities);

            if (matcher.find()) {
                capabilityRevisionsMap.put(matcher.group(1), matcher.group(2));
            } else {
                builder.append("\n    No capability found for pattern: " + pattern.pattern());
            }
        }

        if (builder.length() > 0) {
            throw new RuntimeException("Failed to find required capabilities:" + builder.toString());
        }
        return capabilityRevisionsMap;
    }

    public static String getTunnelSuffix(String routerNumberAsString, Integer localTunnelNumberZeroBased) {
        return routerNumberAsString + (localTunnelNumberZeroBased.equals(0) ? "" : "B");
    }


    private String getLogtag() {
        return LOGTAG;
    }
}

