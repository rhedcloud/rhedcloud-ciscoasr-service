package edu.emory.it.service.ca.provider;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.NodeSet;
import edu.emory.it.service.ca.provider.mapper.MapperFactory;
import edu.emory.it.service.ca.provider.mapper.VpnMapper;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.ElementUtils;
import edu.emory.it.service.ca.provider.util.IPV4Address;
import edu.emory.it.service.ca.provider.util.RouterUtil;
import edu.emory.it.service.ca.provider.util.VpnConnectionDump;
import edu.emory.it.service.ca.provider.util.VpnWildcardQueryFacade;
import edu.emory.it.service.ca.provider.validator.request.VpnRequestValidator;
import edu.emory.it.service.ca.provider.validator.request.VpnRequestValidatorFactory;
import edu.emory.it.service.ca.provider.validator.response.VpnAddValidatorCommand;
import edu.emory.it.service.ca.provider.validator.response.VpnDeleteNativeConfigValidatorCommand;
import edu.emory.it.service.ca.provider.validator.response.VpnDeleteShutdownTunnelValidatorCommand;
import edu.emory.it.service.ca.provider.validator.response.VpnResponseValidatorCommand;
import edu.emory.it.service.ca.provider.validator.response.WaitForVpnAddOperationValidator;
import edu.emory.it.service.ca.provider.validator.response.WaitForVpnDeleteNativeConfigOperationValidator;
import edu.emory.it.service.ca.provider.validator.response.WaitForVpnDeleteShutdownTunnelOperationValidator;
import edu.emory.it.service.ca.provider.validator.response.WaitForVpnOperationValidator;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.RemoteVpnConnectionInfo;
import edu.emory.moa.objects.resources.v1_0.RemoteVpnTunnel;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * Vpn Connection provisioning provider for performing requests.
 *
 * Add/Delete requests perform retries when specific types of router errors occur - those categorized as "settling"
 * errors.  All other error categories result in operation abort.
 *
 * Router/Tunnel mapping - tunnels on each router start with router number.  All "A" suffixes will actually be blank
 * for backwards compatibility.
 *
 * Tunnel1 ---> Tunnel1A ---> (Router1)
 *              Tunnel1B ---> (Router1)
 * Tunnel2 ---> Tunnel2A ---> (Router2)
 *              Tunnel2B ---> (Router2)
 *
 * tunnelNumber and ${TunnelNumber} refers code variables/substitution values that are mapped to the one's based tunnel
 * number that is shown in the left column
 *
 * localTunnelNumberZeroBased and ${LocalTunnelNumberZeroBase} are the "sub-tunnels" on each router that are mapped to
 * the "1A" and "2A" suffixes in the center column
 *
 * TunnelSuffix is the the same two "sub-tunnel" suffixes, "1A" and "2A" for substitution into the templates. For this
 * release tunnel suffix "1A" is defined as "1" for backward compatibility for the Production release.
 *
 */
public class CiscoAsr1002NetconfVpnConnectionProvider extends CiscoAsr1002Provider implements VpnConnectionProvider, CiscoAsrProvider{

    private static final Logger logger = OpenEaiObject.logger;

    // May be refactored to super class if Static Nat moves to YANG model version + CSP
    final private String PROP_CLOUD_SERVICE_PROVIDERS = "CloudServiceProviders";

    protected VpnMapper mapper = null;

    private static final Pattern parenthesizedValuePattern = Pattern.compile("\\((.+?)\\)");

    private String bgpAsn;

    // TODO: Add insideCidrRangeMask to config.

    /**
     * Main entry point for Vpn Connection queries. Depending on the content of the query specification, this can be
     * a query by vpnId or a wildcard query to obtain all active tunnels on this router.
     *
     * Support for multiple router versions and multiple Cloud Service Providers is encapsulated within the Mapper
     * hierarchy, and is determined here.
     *
     * @param vpnConnectionQuerySpecification - query spec containing vpn id and vpc id - if both are null, a wildcard
     *                                        query is performed, otherwise,
     * @return List of all found VpnConnection objects.
     * @throws ProviderException - thrown on any failure to query for specification
     */
    public List<VpnConnection> query(VpnConnectionQuerySpecification vpnConnectionQuerySpecification) throws ProviderException {
        List<VpnConnection> vpnConnections = new ArrayList<>();

        String vpnProfileId = vpnConnectionQuerySpecification.getVpnId();
        String vpcId = vpnConnectionQuerySpecification.getVpcId();

        // Use OpenEAI standard way of performing wildcard query.
        if (StringUtils.isEmpty(vpcId) && StringUtils.isEmpty(vpnProfileId)) {
            vpnConnections = queryByWildcard(vpnConnectionQuerySpecification);
        } else {
            // Validators take care of required data.
            VpnConnection vpnConnection = queryByVpnId(vpnConnectionQuerySpecification);
            if (vpnConnection != null) {
                vpnConnections.add(vpnConnection);
            }
        }

        return vpnConnections;
    }

    /**
     * Main entry point for VpnConnectionRequisition generate requests.
     *
     * With the move to multi-YANG version support, the configured YANG templates drive the config requests sent to
     * the routers. In fact, there are about half a dozen values from the requisition object used to setup the config
     * request - the remaining values are all embedded in the YANG add template.
     *
     * A configured number of retries are used to perform the add, depending upon the type of response received from
     * the router for each add attempt.
     *
     * Finally, the validation sequence looks for the created tunnel using standard query/validation.
     *
     * Here is information about some of the important fields in the VpnConnectionRequisition MOA:
     *
     * 1 - TunnelProfile value CustomerGatewayIp:
     *
     *     The CustomerGatewayIp is the same as the ${LocalVpnIpAddress} that we use in the templates.
     *     CustomerGatewayIp is AWS specific terminology and references the customer's VPN endpoint from the AWS
     *     perspective.  From our perspective, it's a local address on a specific router, and the endpoint on the
     *     AWS side is remote i.e. ${RemoteVpnIpAddress}.  So it's just a matter of perspective.  Each router has
     *     200 globally unique LocalVpnIpAddresses - one per profile.  We can use the same LocalVpnIpAddress
     *     (CustomerGatewayIp) to establish multiple tunnels to the same router on the Emory side.  This is how the
     *     dual tunnel VPNs to AWS function.
     *
     * 2 - RemoteVpnConnectionInfo values RemoteVpnIpAddress, PresharedKey and VpnInsideIpCidr
     *
     *     Each tunnel will have a set of these three values.  RemoteVpnIpAddress/PresharedKey are the two items
     *     needed from the CSP to build and authenticate the tunnel.  The VpnInsideIpCidr represents the addresses
     *     that are used over a specific tunnel.  They're also used to establish dynamic BGP routing through the
     *     tunnel - the CSP gets the first address out of this CIDR and our router gets the second one.  The
     *     VpnInsideIpCidr's are all unique across both local routers.  So, in the AWS dual tunnel example, there
     *     will be 4 different VpnInsideCidr's needed - one per tunnel.  The PresharedKey is just a passphrase and
     *     should be the same on both ends of the tunnel.  It doesn't have to be unique but generally is due to random
     *     generation.  The RemoteVpnIpAddress lives on a physical router on the CSP side just like our
     *     LocalVpnIpAddress lives on a specific physical router on our side.  The CSP could provide the same
     *     RemoteVpnIpAddress to us for different tunnels.  It just means those tunnels will terminate on the same
     *     equipment on the CSP side.
     *
     * @param vpnConnectionRequisition - incoming request object for generate request
     * @return VpnConnection object generated on success
     * @throws ProviderException - thrown on any failure to perform generate
     */
    public VpnConnection generate(VpnConnectionRequisition vpnConnectionRequisition) throws ProviderException {

        VpnRequestValidator validator = VpnRequestValidatorFactory.create(vpnConnectionRequisition);
        validator.validate();

        VpnConnection vpnConnectionFromDevice = null;
        NetconfSession session = null;
        StringBuilder errorBucket = new StringBuilder();

        try {
            VpnConnectionQuerySpecification specification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
            specification.setVpcId(vpnConnectionRequisition.getOwnerId());
            specification.setVpnId(vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId());
            vpnConnectionFromDevice = queryByVpnId(specification);

        } catch (Exception e) {
            String warnMsg = "Failed to query for existing vpn connection";
            logger.warn(getLogtag() + warnMsg, e);
        }

        if (vpnConnectionFromDevice != null) {
            String errMsg = "Error during generate vpn connection - profile Id " + vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId() + " already assigned to vpc Id: " + vpnConnectionRequisition.getOwnerId();
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

        // For multi-tunnel support, this will be replaced by enumerating over RemoteVpnTunnels in requisition.
        Integer localTunnelNumberZeroBased = 0;

        try {
            session = openNetconfSession();
            session.lock(NetconfSession.RUNNING);
            logger.info(getLogtag() + "VPN provision - generate - for owner id: " + vpnConnectionRequisition.getOwnerId());

            // add has its own retry algorithm for the basic setConfig sequence.  If it returns, we can start the
            // verification sequence in the same Netconf session.
            add(vpnConnectionRequisition, session, localTunnelNumberZeroBased);

            int retryCount = operationErrorRetryCount;

            Exception exception = null;
            Exception lastRetryError = null;

            while (retryCount-- > 0) {
                try {
                    VpnAddValidatorCommand vpnAddValidatorCommand = new VpnAddValidatorCommand();
                    vpnAddValidatorCommand.setVpcId(vpnConnectionRequisition.getOwnerId());
                    vpnAddValidatorCommand.setVpnId(vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId());
                    vpnAddValidatorCommand.setVpnConnectionRequisition(vpnConnectionRequisition);

                    logger.info(getLogtag() + "Waiting for VPN provision - add - to complete for owner id: " + vpnConnectionRequisition.getOwnerId());

                    vpnConnectionFromDevice = waitForVpnOperationToComplete(vpnAddValidatorCommand, new WaitForVpnAddOperationValidator(logger, providerDevice.getTunnelNumber(), errorBucket), session);
                    if (vpnConnectionFromDevice == null) {
                        String errMsg = "No VpnConnection returned on success.";
                        if (errorBucket.length() > 0) {
                            errMsg += "\n" + errorBucket.toString();
                        }
                        logger.error(getLogtag() + errMsg);
                        throw new ProviderException(errMsg);
                    }
                    if (errorBucket.length() > 0) {
                        String errMsg = "Validations failed for generate vpn connection: \n" + errorBucket.toString();
                        logger.error(getLogtag() + errMsg);
                        throw new ProviderException(errMsg);
                    }
                    break;

                } catch (Exception e) {
                    String errMsg = "Failed to vpn provision - add - for owner id: " + vpnConnectionRequisition.getOwnerId();
                    if (errorBucket.length() > 0) {
                        errMsg += "\n" + errorBucket.toString();
                        logger.error(getLogtag() + errMsg, e);
                        exception = new ProviderException(errMsg, e);
                        break;
                    } else if (isRouterSettlingError(e)) {
                        lastRetryError = e;
                        logger.error(getLogtag() + "Router settling error after " + retryCount + " retries...");
                        operationSleep(operationErrorRetryWaitInMilliseconds);
                    }
                } finally {
                    long startTime = System.currentTimeMillis();
                    routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
                }
            }

            if (retryCount < 0) {
                String msg = "Failed to add vpn tunnel after " + buildTriesString();
                Exception lastException = lastRetryError != null ? lastRetryError : exception != null ? exception : new Exception("Unknown exception adding vpn tunnel");
                logger.error(getLogtag() + msg, lastException);
            }

        } catch (ProviderException e) {
            String msg = "Failed to vpn provisioning generate";
            logger.error(getLogtag() + msg, e);
            throw new ProviderException(msg, e);
        } catch (IOException | JNCException e) {
            unlockDatastore(session, NetconfSession.RUNNING);
            String msg = "Failed to lock netconf session for generate";
            logger.error(getLogtag() + msg, e);
            throw new ProviderException(msg, e);
        } catch (Exception e) {
            unlockDatastore(session, NetconfSession.RUNNING);
            String msg = "Unhandled exception while attempting vpn generate";
            logger.error(getLogtag() + msg, e);
            throw new ProviderException(msg, e);
        } finally {

            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

        logger.info(getLogtag() + "Waiting " + (routerSettlingWaitInMilliseconds / 1000) + " seconds for router to settle after successful add...");
        operationSleep(routerSettlingWaitInMilliseconds);

        PubSubProducer pubSubProducer = null;
        try {
            pubSubProducer = (PubSubProducer) ciscoAsrSyncCommandProducerPool.getProducer();
            vpnConnectionFromDevice.createSync(pubSubProducer);
        } catch (Exception e) {
            String errMsg = "Failed to publish static nat create sync message";
            logger.error(getLogtag() + errMsg, e);
        } finally {
            ciscoAsrSyncCommandProducerPool.releaseProducer(pubSubProducer);
        }

        return vpnConnectionFromDevice;
    }

    /**
     * Main entry point for VpnConnection delete requests.
     *
     * With the move to multi-YANG version support, the configured YANG templates drive the config requests sent to
     * the routers. In fact, there are about half a dozen values from the requisition object used to setup the config
     * request - the remaining values are all embedded in the YANG add template.
     *
     * The delete operation is the only multi-config request implementation for the three Vpn Connection Provisioning
     * requests. This is problematic in that the current router YANG versions do not support the CANDIDATE datastore,
     * which creates risk of corrupting the router config. There are two config requests required for delete:
     *
     * 1) shutdown tunnel
     * 2) set tunnel configureation to "deleted"
     *
     * Both require confirmation on the router (by reading config) and if either fail, the tunnel is left "half deleted."
     * The solution is to utilize the CANDIDATE data store to perform both request steps and then commit on successful
     * validation.  Or, the bare minimum would be to commit the shutdown on success, and attempt a reverse of step 1 if
     * step 2 does not validate.
     *
     *
     * @param vpnConnection - incoming request used for the add
     * @throws ProviderException - thrown on any failure to perform delete
     */
    public void delete(VpnConnection vpnConnection) throws ProviderException {

        long startTime;
        logger.info(getLogtag() + "VpnConnection for delete request:\n" + new VpnConnectionDump(vpnConnection).dump());

        // Check request message object - bad values can corrupt router config.
        VpnRequestValidator validator = VpnRequestValidatorFactory.create(vpnConnection);
        validator.validate();

        NetconfSession session = null;

        // TODO: Investigate where the best place to loop through tunnel numbers should be - here probably.
        Integer localTunnelNumberZeroBased = 0;

        String vpcId = vpnConnection.getVpcId();
        String vpnId = vpnConnection.getVpnId();
        String tunnelName = vpnConnection.getTunnelInterface(localTunnelNumberZeroBased).getName();

        logger.info(getLogtag() + "VPN provision - delete - for vpc id: " + vpcId);

        boolean isTunnelShutdown = false;
        try {
            session = openNetconfSession();
            session.lock(NetconfSession.RUNNING);

            getVersioningInfo(vpnConnection, session);
            createMapper();
            getBgpAsn(session);

            startTime = System.currentTimeMillis();

            shutdownTunnel(vpnConnection, session, localTunnelNumberZeroBased);

            routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

            VpnDeleteShutdownTunnelValidatorCommand vpnDeleteShutdownTunnelValidatorCommand = new VpnDeleteShutdownTunnelValidatorCommand();
            vpnDeleteShutdownTunnelValidatorCommand.setTunnelName(tunnelName);

            logger.info(getLogtag() + "Waiting for VPN provision - delete[shutdown tunnel] - to complete for vpc id: " + vpcId + " - vpn id: " + vpnId);
            waitForVpnTunnelShutdownToComplete(vpnDeleteShutdownTunnelValidatorCommand, new WaitForVpnDeleteShutdownTunnelOperationValidator(logger, providerDevice.getTunnelNumber()), session);

            isTunnelShutdown = true;

            startTime = System.currentTimeMillis();

            setTunnelConfigurationToDeleted(vpnConnection, session, localTunnelNumberZeroBased);

            routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

            VpnDeleteNativeConfigValidatorCommand vpnDeleteNativeConfigValidatorCommand = new VpnDeleteNativeConfigValidatorCommand();
            vpnDeleteNativeConfigValidatorCommand.setVpcId(vpcId);
            vpnDeleteNativeConfigValidatorCommand.setVpnId(vpnId);

            logger.info(getLogtag() + "Waiting for VPN provision - delete[native config] - to complete for vpc id: " + vpcId);
            waitForVpnOperationToComplete(vpnDeleteNativeConfigValidatorCommand, new WaitForVpnDeleteNativeConfigOperationValidator(logger, providerDevice.getTunnelNumber()), session);

        } catch (Exception e) {
            String errMsg;
            if (isTunnelShutdown) {
                errMsg = "Failed to delete vpn - partial delete: tunnel has been shutdown but failed to set configuration.  Manual intervention required. Vpc id: " + vpcId + " - vpn id: " + vpnId;
            } else {
                errMsg = "Failed to delete vpn - tunnel shutdown failed. This may require manual intervention to resolve. Vpc id: " + vpcId + " - vpn id: " + vpnId;
            }
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

        logger.info(getLogtag() + "Waiting " + (routerSettlingWaitInMilliseconds / 1000) + " seconds for router to settle after delete...");
        operationSleep(routerSettlingWaitInMilliseconds);

        logger.info(getLogtag() + "Successful vpn delete for vpc id: " + vpcId + " - vpn id: " + vpnId);

        PubSubProducer pubSubProducer = null;
        try {
            pubSubProducer = (PubSubProducer) ciscoAsrSyncCommandProducerPool.getProducer();
            vpnConnection.deleteSync("Delete", pubSubProducer);
        } catch (Exception e) {
            String errMsg = "Failed to publish vpn connection delete sync message for vpcId: " + vpcId;
            logger.error(getLogtag() + errMsg, e);
        } finally {
            ciscoAsrSyncCommandProducerPool.releaseProducer(pubSubProducer);
        }
    }

    /************************************* Implementation ******************************************/

    /**
     * 16-APR-2018
     *
     * As of this writing, the physical devices are not current, with respect to the NETCONF Protocol's latest RFC.
     * This causes issues, since the Java Netconf Client API is current (RFC 6536 or later). The problem arises with
     * operations that can be associated with specific elements, and the obsolescence of the "REMOVE" operation in the
     * latest RFC. JNC API only supports "DELETE" per the latest RFC, and "REMOVE" has subtle semantic differences in
     * the older RFC (6241). Therefore, even though the JNC API has a "marking" API to mark elements with "CREATE,"
     * "UPDATE" and "DELETE" operations, they can't be used. The operations are hardcoded in the templates below, and
     * the following boolean is used to bypass marking operations.
     *
     * Once the devices are updated with at least RFC 6536 implementations:
     *
     * 1) The hardcoded operations can be removed from the templates
     * 2) The nc namespace can be removed from the native element in each template
     * 3) The use of the JNC Marking API in the private provisioning methods provisionVpnCreate() and
     *    provisionVpnDelete() can be reinstated
     *
     * NETCONF capability for CANDIDATE datastore support:
     *
     * urn:ietf:params:netconf:capability:candidate:1.0
     *
     */
    private final boolean RFC_6536 = false;

    public void init(AppConfig aConfig) throws ProviderException {
        // CiscoAsr1002NetconfVpnConnectionProvider init
        logger.info(getLogtag() + "- Initializing");

        if (aConfig != null) {
            appConfig = aConfig;

            try {

                refreshLocalVariables();

                ciscoAsrSyncCommandProducerPool = (ProducerPool) appConfig.getObject("SyncSaveConfigCommandProducer");

                Properties cspProperties = appConfig.getProperties(PROP_CLOUD_SERVICE_PROVIDERS);

                Map<String, String> providerMap  = cspProperties.entrySet().stream()
                        .collect(Collectors.toMap(
                            e -> e.getKey().toString(),
                            e -> e.getValue().toString()));

                StringBuilder builder = new StringBuilder("Configured Cloud Service Provider map: ");
                providerMap.entrySet().forEach(es -> {
                    builder.append("\n    " + es.getKey() + " (" + es.getValue() + ")");
                });
                logger.info(getLogtag() + builder.toString());

                cspDiscriminators = cspProperties.entrySet().stream()
                        .map(es -> es.getValue().toString())
                        .collect(Collectors.toList());

                if (cspDiscriminators.size() == 0) {
                    String errMsg = "No Cloud Service Provider map found in app config!";
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);
                }

            } catch (Exception e) {
                String errMsg = "Error loading properties from app config";
                logger.fatal(getLogtag() + errMsg, e);
                throw new ProviderException(errMsg, e);
            }

            console(getLogtag() + " initialized.");
        } else {
            String errMsg = "No deployment descriptor provided!";
            logger.fatal(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
    }

    /**
     * Create a mapper instance based on router version and CloudServiceProvider:
     *   1) Router version is obtained directly from the router using SSH2 and examining router capability revisions.
     *   2) CSP is determined TBD. It SHOULD be a discriminator field directly on the request object, but it might end
     *      up being field overloading.
     *
     * The combination of the router version number and the CSP is used as a parameter to the Mapping factory method.
     *
     * @param request - A query, generate or delete request used to obtain the CSP
     * @throws ProviderException
     */
    protected void getVersioningInfo(XmlEnterpriseObject request, NetconfSession session) throws ProviderException {

        logger.info(getLogtag() + "Creating mapper for " + request.getClass().getSimpleName() + " request object...");

        routerVersion = super.getRouterVersion(session);

        String ownerId;
        if (request instanceof VpnConnectionQuerySpecification) {
            ownerId = ((VpnConnectionQuerySpecification)request).getVpcId();
        } else if (request instanceof VpnConnectionRequisition) {
            ownerId = ((VpnConnectionRequisition)request).getOwnerId();
        } else if (request instanceof VpnConnection) {
            ownerId = ((VpnConnection)request).getVpcId();
        } else {
            String errMsg = "Invalid request object for Cloud Service Provider discrimination: " + request.getClass().getSimpleName();
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
        cspDiscriminator = "AWS";
        int index = (ownerId != null ? ownerId.indexOf(":") : -1);
        if (index > -1) {
            cspDiscriminator = ownerId.substring(0, index).toUpperCase();
        }
        if (!cspDiscriminators.contains(cspDiscriminator)) {
            String errMsg = "Invalid Cloud Service Provider discriminator: " + (cspDiscriminator.isEmpty() ? "<empty>" : cspDiscriminator);
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

    }

    protected void createMapper() throws ProviderException {
        createMapper(cspDiscriminator);
    }

    protected void createMapper(String cspd) throws ProviderException {

        String mapperType = routerVersion + cspd;

        if (MapperFactory.isMapperOfType(mapper, mapperType)) {
            return;
        }

        try {
            mapper = MapperFactory.createVpnMapper(mapperType);
        } catch (Exception e) {
            String errMsg = "Failed to create mapper from router version: " + routerVersion + " CSP: " + cspd;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        // Load configuration - at this point, it's just the mapper specific templates.
        try {
            String propertyKey = routerVersion + cspd + "_Properties";
            Properties mapperProps = appConfig.getProperties(propertyKey);
            mapper.initializeTemplates(mapperProps);
            mapper.init(appConfig, routerNumber);

        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Failed to load mapper configuration from router version: " + routerVersion + " CSP: " + cspd;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        logger.info(getLogtag() + "Mapper type: " + mapperType);
    }

    protected void getBgpAsn(NetconfSession session ) throws ProviderException {

        logger.info(getLogtag() + "Getting BGP Association...");

        try {
            String xpath = mapper.xpathNativeRouterBgpId();
            String xpathWithoutNative = xpath.replace("native/", "");
            NodeSet result = session.get(mapper.xpathNativeRouterBgpId());
            bgpAsn = result.getFirst(xpathWithoutNative).getValue().toString();
            logger.info(getLogtag() + "Obtained Bgp Association: " + bgpAsn);

        } catch (Exception e) {
            String errMsg = "Failed to get router bgp association";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

    }

    private Element queryTunnelInterface(String tunnelName, NetconfSession session) throws ProviderException {
        Element tunnelInterfaceElement = null;
        logger.info(getLogtag() + "VPN provision - query tunnel interface - for tunnel name: " + tunnelName);

        try {

            NodeSet result = session.get(mapper.xpathNativeInterfaceTunnelNameWithValue(tunnelName));

            if (result != null && result.size() > 0) {
                tunnelInterfaceElement = result.get(0);
            }

        } catch (Exception e) {
            String errMsg = "Exception while attempting to query tunnel interface with tunnel name: " + tunnelName;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return tunnelInterfaceElement;
    }

    /**
     * Query by vpn id.  vpc id is optional, and if present, is used to match responses from the router. The vpn id
     * is used to build query parameters to match the corresponding active tunnel, since there is a 1 to 1 relationship
     * between vpn connection profile ids (vpn ids) and configured tunnels on the router.
     *
     * Addtional info:
     *
     * From Jimmy K.  6/8/2018:
     *
     * He states that returned status from the query only represents status/state:
     *
     *   tunnel admin/oper up-down/up-down, BGP peer up/down, up time, prefixes sent/received
     *   The tunnel/bgp info queried here is NOT config.  This needs to be added above as mentioned
     *
     * According to Jimmy's new "status" Python script, here are the elements used to get status:
     *
     * 1) native/crypto/ipsec/profile
     * 2) native/interface/Tunnel
     * 3) native/router/bgp
     * 4) bgp-state-data
     * 5) interfaces-state
     *
     * In order to use Jimmy's config template, the VpnConnectionQuerySpecification document would need the following:
     *
     * 1) BgpNeighborId
     * 2) All the descriptions/tags from the VpnConnection object that use tunnelNumber and vpcNumber, e.g. ipsec-transformset's tag value:
     *    ipsec-prop-vpn-research-vpc050-tun1
     */
    private VpnConnection queryByVpnId(VpnConnectionQuerySpecification vpnConnectionQuerySpecification) throws ProviderException {

        VpnRequestValidator validator = VpnRequestValidatorFactory.create(vpnConnectionQuerySpecification);
        validator.validate();

        int retryCount = operationErrorRetryCount;

        VpnConnection vpnConnection = null;
        NetconfSession session = null;
        mapper = null;

        logger.info(getLogtag() + "VPN provision - query - by vpc id: " + vpnConnectionQuerySpecification.getVpcId());

        Exception lastRetryError = null;
        while (retryCount-- > 0) {
            try {
                session = openNetconfSession();

                if (mapper == null) {
                    getVersioningInfo(vpnConnectionQuerySpecification, session);
                    createMapper();
                    getBgpAsn(session);
                }

                vpnConnection = query(vpnConnectionQuerySpecification, session);

                logger.info(getLogtag() + "Successful vpn provision query after attempt #" + (operationErrorRetryCount - retryCount));
                break;

            } catch (Exception e) {
                String errMsg;

                if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    errMsg = "Router settling error while attempting to vpn provision query - retry #" + (operationErrorRetryCount - retryCount) + "...";
                    logger.error(getLogtag() + errMsg, e);

                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    throw new ProviderException("Error while attempting to vpn provision query", e);
                }
            } finally {
                long startTime = System.currentTimeMillis();
                closeNetconfSession(session);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }

        if (retryCount < 0) {
            throw new ProviderException("Failed to vpn provision query after " + buildTriesString(), lastRetryError);
        }

        return vpnConnection;
    }

    private VpnConnection query(VpnConnectionQuerySpecification vpnConnectionQuerySpecification, NetconfSession session) throws ProviderException {
        VpnConnection vpnConnection = null;
        long startTime;

        ElementUtils elementUtils = new ElementUtils();

        try {

            // One vpn connection with a list of tunnel interface elements. Note that due to a data modeling error,
            // the native elements do not represent the router state, in that there should be a one to one correspondence
            // between tunnel interface instances and native element instances. Currently, the last native element suite
            // overwrites the first.
            vpnConnection = AppConfigUtil.getVpnConnectionFromConfiguration(appConfig);

            // For multi-tunnel support, we'll need to somehow infer this value.
            Integer maxTunnels = 1;

            Integer tunnelCount = getTunnelCountForRouter(session);

            // Two tunnels per router
            for (Integer localTunnelNumberZeroBased = 0; localTunnelNumberZeroBased < maxTunnels; localTunnelNumberZeroBased++) {

                String tunnelSuffix = RouterUtil.getTunnelSuffix(routerNumber, localTunnelNumberZeroBased);
                String vpcId = vpnConnectionQuerySpecification.getVpcId();
                String vpnId = vpnConnectionQuerySpecification.getVpnId();
                Integer vpnIdAsInteger = Integer.valueOf(vpnId);

                // This needs rework!
                Map<String, String> valuesMap = new HashMap<>();

                // ${RouterNumber}0${VpnId-Padded}
                String tunnelId = mapper.buildQueryTunnelId(Integer.valueOf(routerNumber), localTunnelNumberZeroBased, vpnIdAsInteger);
                Integer tunnelNumber = localTunnelNumberZeroBased + 1;
                Map<String, Element> resultMap = new HashMap<>();

                startTime = System.currentTimeMillis();

                // TODO: This pre-query is required to calculate primaryIp, which is used to calculate neighborId.  Should be part of the request.
                NodeSet nativeTunnelResult = session.get(mapper.xpathNativeInterfaceTunnelNameWithValue(tunnelId));
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                String primaryIp = (String) nativeTunnelResult.getFirst(mapper.xpathInterfaceTunnelIpAddressPrimaryAddress()).getValue();
                String neighborId = IPV4Address.adjustAddressByOffset(primaryIp, -1);

                Element queryFromXml;

                /**
                 * Tokens used in query templates:
                 *
                 * ${TunnelNumber}
                 * ${RouterNumber}
                 * ${TunnelId}
                 * ${BgpNeighborId}
                 * ${VpnId-Padded}
                 * ${localTunnelNumberZeroBased}
                 * ${BgpAsn}
                 *
                 */
                valuesMap.put(mapper.getSubstitutionToken("paddedVpnId"), mapper.buildPaddedVpnId(vpnIdAsInteger));
                valuesMap.put(mapper.getSubstitutionToken("bgpNeighborId"), neighborId);
                valuesMap.put(mapper.getSubstitutionToken("tunnelId"), tunnelId);
                valuesMap.put(mapper.getSubstitutionToken("tunnelNumber"), routerNumber);
                valuesMap.put(mapper.getSubstitutionToken("tunnelSuffix"), tunnelSuffix);
                valuesMap.put(mapper.getSubstitutionToken("localTunnelNumberZeroBased"), localTunnelNumberZeroBased.toString());
                valuesMap.put(mapper.getSubstitutionToken("bgpAsn"), bgpAsn);

                Map<String, String> pathValueMap = new HashMap<>();
                queryFromXml = mapper.hydrateTemplate(valuesMap, "queryTemplate", pathValueMap);
                mapper.validatePathToValuesMap(pathValueMap);

                StringBuilder infoBuilder = new StringBuilder("Hydrated query template paths/values:");
                pathValueMap.entrySet().forEach(e -> {
                    infoBuilder.append("\n    " + e.getKey() + " -> " + e.getValue());
                });
                logger.info(getLogtag() + infoBuilder.toString());

                logger.info(getLogtag() + "query: \n" + queryFromXml.toXMLString());

                startTime = System.currentTimeMillis();
                NodeSet nativeResult = session.get(queryFromXml.getFirst(mapper.xpathNative()));
                NodeSet bgpStateDataResult = session.get(queryFromXml.getFirst(mapper.xpathBgpStateData()));
                NodeSet interfacesStateResult = session.get(queryFromXml.getFirst(mapper.xpathInterfacesState()));

                boolean responseValidated = validateResponse(pathValueMap, nativeResult, bgpStateDataResult, interfacesStateResult);

                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                logger.info(getLogtag() + "native result: \n" + nativeResult.toXMLString());
                logger.info(getLogtag() + "bgp-state-data result: \n" + bgpStateDataResult.toXMLString());
                logger.info(getLogtag() + "interfaces-state result: \n" + interfacesStateResult.toXMLString());

                if (responseValidated && nativeResult.size() == 1 && nativeTunnelResult.size() == 1 && interfacesStateResult.size() == 1) {

                    Map<String, Element> descriptionMap = mapper.buildDescriptionMap(nativeResult, nativeTunnelResult, bgpStateDataResult);

                    Map<String, Object> matchResultMap = matchVpcIdInResponse(descriptionMap, vpcId);

                    VpcIdMatchResult matchResult = (VpcIdMatchResult) matchResultMap.get("matchResult");

                    String errMsg;
                    switch (matchResult) {

                        case MATCH:
                            Element cryptoKeyringElement = elementUtils.createElement(nativeResult.getFirst(mapper.xpathCryptoKeyring()));
                            Element cryptoIsakmpProfileElement = elementUtils.createElement(nativeResult.getFirst(mapper.xpathCryptoIsakmpProfile()));
                            Element cryptoIpsecTransformsetElement = elementUtils.createElement(nativeResult.getFirst(mapper.xpathCryptoIpsecTransformset()));
                            Element cryptoIpsecProfileElement = elementUtils.createElement(nativeResult.getFirst(mapper.xpathCryptoIpsecProfile()));

                            resultMap.put(mapper.xpathNative(), nativeResult.get(0));
                            resultMap.put(mapper.xpathCryptoKeyring(), cryptoKeyringElement);
                            resultMap.put(mapper.xpathCryptoIsakmpProfile(), cryptoIsakmpProfileElement);
                            resultMap.put(mapper.xpathCryptoIpsecTransformset(), cryptoIpsecTransformsetElement);
                            resultMap.put(mapper.xpathCryptoIpsecProfile(), cryptoIpsecProfileElement);
                            resultMap.put(mapper.xpathNativeInterface(), nativeTunnelResult.get(0));
                            resultMap.put(mapper.xpathInterfacesState(), interfacesStateResult.get(0));
                            // Not present on verify delete, for some YANG versions.
                            if (bgpStateDataResult.size() > 0) {
                                resultMap.put(mapper.xpathBgpStateData(), bgpStateDataResult.get(0));
                            }

                            if (StringUtils.isEmpty(vpcId)) {
                                vpcId = (String) matchResultMap.get("foundVpcId");
                            }
                            vpnConnection.setVpcId(vpcId);
                            vpnConnection.setVpnId(vpnId);

                            vpnConnection = mapper.hydrateVpnConnection(vpnConnection, resultMap);

                            if (vpnConnection != null) {
                                logger.info(getLogtag() + "vpn connection returned for vpn id: " + vpcId + (vpnId != null ? " - vpn id: " + vpnId : ""));
                            }
                            break;
                        case NOT_FOUND:
                            logger.info(getLogtag() + "No matching vpn connection found for vpc id: " + vpcId + " - vpn id: " + vpnId);
                            vpnConnection = null;
                            break;
                        case MISMATCH:
                            errMsg = "Returned vpn connection does not match provided vpc id: " + vpcId + " - profile id mapped to another vpc id";
                            logger.error(getLogtag() + errMsg);
                            throw new ProviderException(errMsg);
                        case BROKEN_TUNNEL:
                            errMsg = "Broken vpn connection for provided " + (StringUtils.isNotEmpty(vpcId) ? "vpc id: " + vpcId : "vpn id: " + vpnId) + " - inconsistent vpc ids in returned descriptions";
                            logger.error(getLogtag() + errMsg);
                            throw new ProviderException(errMsg);
                    }
                } else {
                    String errMsg = "Invalid query template - missing return elements!";
                    logger.fatal(getLogtag() + errMsg);
                }
            }

        } catch (Exception e) {
            String errMsg = "Exception while attempting to query vpn connection info";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return vpnConnection;

    }

    public List<VpnConnection> queryByWildcard(VpnConnectionQuerySpecification querySpecification) throws ProviderException {

        NetconfSession session = null;


        List<VpnConnection> vpnConnections = new ArrayList<>();

        Map<String, Element> resultMap = new HashMap<>();
        try {

            session = openNetconfSession();
            getVersioningInfo(querySpecification, session);

            for (String cspd : cspDiscriminators) {
                createMapper(cspd);

                logger.info(getLogtag() + "Performing wildcard query for " + cspd + " CSP discriminator...");
                VpnWildcardQueryFacade facade = new VpnWildcardQueryFacade(session, mapper, getLogtag());

                vpnConnections.addAll(facade.performWildcardQuery(resultMap, appConfig, routerNumber));

            }

        } catch (Exception e) {
            logger.error(getLogtag() + "Wildcard query Failed", e);
        } finally {
            closeNetconfSession(session);
        }

        return vpnConnections;
    }

    private void add(VpnConnectionRequisition vpnConnectionRequisition, NetconfSession session, Integer localTunnelNumberZeroBased) throws ProviderException {

        int retryCount = operationErrorRetryCount;
        long startTime;

        // TODO: To be used for iteration over the RemoteVpnTunnel objects in the requisition.
        Integer tunnelCount = getTunnelCountForRouter(vpnConnectionRequisition);
        List<Exception> retryErrors = new ArrayList<>();
        Exception lastRetryError = null;

        // Setup YANG template for edit-config sequence

        // Setup map of values required for hydration:
        // TunnelNumber, VpnId-Padded, TunnelId, bgpNeighborId, remoteVpnIpAddress, presharedKey
        Map<String, String> valuesMap = new HashMap<>();
        String tunnelSuffix = RouterUtil.getTunnelSuffix(routerNumber, localTunnelNumberZeroBased);
        String tunnelId = getTunnelIdFromRequisition(vpnConnectionRequisition, localTunnelNumberZeroBased);
        String ownerId = vpnConnectionRequisition.getOwnerId();
        Integer vpnIdAsInteger= mapper.getVpnIdFromTunnelName(tunnelId);
        String vpnIdPadded = mapper.buildPaddedVpnId(vpnIdAsInteger);
        Integer tunnelNumber = localTunnelNumberZeroBased + 1;
        String preSharedKey = vpnConnectionRequisition.getRemoteVpnConnectionInfo(localTunnelNumberZeroBased).getRemoteVpnTunnel(localTunnelNumberZeroBased).getPresharedKey();
        String remoteVpnIpAddress = vpnConnectionRequisition.getRemoteVpnConnectionInfo(localTunnelNumberZeroBased).getRemoteVpnTunnel(localTunnelNumberZeroBased).getRemoteVpnIpAddress();
        String localVpnIpAddress = vpnConnectionRequisition.getVpnConnectionProfile().getTunnelProfile(localTunnelNumberZeroBased).getCustomerGatewayIp();
        String neighborId;

        if (tunnelNumber.equals(1)) {
            neighborId = IPV4Address.adjustAddressByOffset(vpnConnectionRequisition.getVpnConnectionProfile().getTunnelProfile(localTunnelNumberZeroBased).getVpnInsideIpCidr1(), 1);
        } else {
            neighborId = IPV4Address.adjustAddressByOffset(vpnConnectionRequisition.getVpnConnectionProfile().getTunnelProfile(localTunnelNumberZeroBased).getVpnInsideIpCidr2(), 1);
        }

        /**
         * Tokens used in add templates:
         *
         * ${VpcId}
         * ${TunnelNumber}
         * ${TunnelId}
         * ${BgpNeighborId}
         * ${RemoteVpnIpAddress}
         * ${PresharedKey}
         * ${LocalVpnIpAddress}
         * ${VpnId-Padded}
         * ${BgpAsn}
         */

        valuesMap.put(mapper.getSubstitutionToken("vpcId"), ownerId);
        valuesMap.put(mapper.getSubstitutionToken("paddedVpnId"), vpnIdPadded);
        valuesMap.put(mapper.getSubstitutionToken("bgpNeighborId"), neighborId);
        valuesMap.put(mapper.getSubstitutionToken("tunnelId"), tunnelId);
        valuesMap.put(mapper.getSubstitutionToken("tunnelNumber"), routerNumber);
        valuesMap.put(mapper.getSubstitutionToken("tunnelSuffix"), tunnelSuffix);
        valuesMap.put(mapper.getSubstitutionToken("presharedKey"), preSharedKey);
        valuesMap.put(mapper.getSubstitutionToken("remoteVpnIpAddress"), remoteVpnIpAddress);
        valuesMap.put(mapper.getSubstitutionToken("localVpnIpAddress"), localVpnIpAddress);
        valuesMap.put(mapper.getSubstitutionToken("bgpAsn"), bgpAsn);

        Map<String, String> pathValueMap = new HashMap<>();
        Element nativeFromXml = mapper.hydrateTemplate(valuesMap, "addTemplate", pathValueMap);
        mapper.validatePathToValuesMap(pathValueMap);

        StringBuilder infoBuilder = new StringBuilder("Hydrated add template paths/values:");
        pathValueMap.entrySet().forEach(e -> {
            infoBuilder.append("\n    " + e.getKey() + " -> " + e.getValue());
        });
        logger.info(getLogtag() + infoBuilder.toString());

        // Send edit config with retries.
        if (lastRetryError != null) {
            retryErrors.add(lastRetryError);
        }
        lastRetryError = null;
        while (retryCount-- > 0) {
            try {
                logger.info(getLogtag() + "native add config to send to router " + providerDevice.getName() + ":\n" + nativeFromXml.toXMLString());

                startTime = System.currentTimeMillis();
                session.editConfig(nativeFromXml);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                logger.info(getLogtag() + "Successful vpn provision add after attempt #" + (operationErrorRetryCount - retryCount));
                break;

            } catch (Exception e) {
                String errMsg;

                if (isRouterDataExistsError(e)) {
                    errMsg = "Attempting to vpn provision add with existing data - vpcId: " + vpnConnectionRequisition.getOwnerId();
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);
                } else if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    errMsg = "Error while attempting vpn provision add - vpcId: " + vpnConnectionRequisition.getOwnerId();
                    logger.error(getLogtag() + errMsg, e);
                    throw new ProviderException(errMsg, e);
                }
            } finally {
                startTime = System.currentTimeMillis();
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }

        if (retryCount < 0) {
            throw new ProviderException("Failed to vpn provision add after " + buildTriesString(), lastRetryError);
        }

    }

    private void shutdownTunnel(VpnConnection vpnConnection, NetconfSession session, Integer localTunnelNumberZeroBased) throws ProviderException {

        int retryCount = operationErrorRetryCount;
        long startTime;

        Map<String, String> valuesMap = new HashMap<>();

        /**
         * Tokens used in shutdown templates:
         *
         * ${TunnelId}
         */
        String tunnelId = vpnConnection.getTunnelInterface(localTunnelNumberZeroBased).getName();

        valuesMap.put(mapper.getSubstitutionToken("tunnelId"), tunnelId);

        Map<String, String> pathValueMap = new HashMap<>();
        Element shutdownTunnelFromXml = mapper.hydrateTemplate(valuesMap, "shutdownTemplate", pathValueMap);
        mapper.validatePathToValuesMap(pathValueMap);

        StringBuilder infoBuilder = new StringBuilder("Hydrated shutdown template paths/values:");
        pathValueMap.entrySet().forEach(e -> {
            infoBuilder.append("\n    " + e.getKey() + " -> " + e.getValue());
        });
        logger.info(getLogtag() + infoBuilder.toString());

        Exception lastRetryError = null;
        while (retryCount-- > 0) {
            try {

                logger.info(getLogtag() + "Tunnel shutdown config: \n\n" + shutdownTunnelFromXml.toXMLString());

                startTime = System.currentTimeMillis();
                session.editConfig(shutdownTunnelFromXml);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                logger.info(getLogtag() + "Successful delete/shutdown tunnel after attempt #" + (operationErrorRetryCount - retryCount));
                break;
            } catch (Exception e) {

                if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    String errMsg = "Unexpected error while attempting vpn delete/shutdown tunnel - vpcId: " + vpnConnection.getVpcId();
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg, e);
                }

            } finally {
                startTime = System.currentTimeMillis();
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }

        if (retryCount < 0) {
            throw new ProviderException("Failed to delete/shutdown tunnel after " + buildTriesString(), lastRetryError);
        }
    }

    /**
     *
     *     router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/id    <-- bgp-neighborid
     *
     *     interface/Tunnel/name    <-- tunnelName
     *     interface/Tunnel/description   <-- AWS Research VPCFIXME_VpcNum TunnelFIXME_TunnelNum (AVAILABLE)
     *     interface/Tunnel/tunnel/protection/ipsec/profile   <-- ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum
     *
     *     crypto/keyring/name  <-- keyring-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum
     *     crypto/keyring/name/pre-shared-key/address/ipv4/ipv4-addr   <-- FIXME_RemoteVpnIpAddress
     *     crypto/keyring/name/pre-shared-key/address/ipv4/unencryt-key  <-- FIXME_PresharedKey
     *
     *     crypto/isakmp/profile/name  <-- isakmp-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum
     *     crypto/isakmp/profile/match/identity/ipv4-address/address  <-- FIXME_RemoteVpnIpAddress
     *
     *     crypto/ipsec/transform-set/tag   <-- ipsec-prop-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum
     *     crypto/ipsec/profile/name   <-- ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum
     *
     * @param vpnConnection
     * @param session  - shared NetconfSession for both phases of delete, tunnel shutdown, and config edit (this)
     * @throws ProviderException
     */
    private void setTunnelConfigurationToDeleted(VpnConnection vpnConnection, NetconfSession session, Integer localTunnelNumberZeroBased) throws ProviderException {

        int retryCount = operationErrorRetryCount;

        Map<String, String> valuesMap = new HashMap<>();
        String tunnelSuffix = RouterUtil.getTunnelSuffix(routerNumber, localTunnelNumberZeroBased);
        String vpnId = vpnConnection.getVpnId();
        String vpnIdPadded = mapper.buildPaddedVpnId(Integer.valueOf(vpnId));
        String tunnelId = vpnConnection.getTunnelInterface(localTunnelNumberZeroBased).getName();
        Integer tunnelNumber = localTunnelNumberZeroBased + 1;
        String preSharedKey = vpnConnection.getCryptoKeyring().getPresharedKey();
        String remoteVpnIpAddress = vpnConnection.getTunnelInterface(localTunnelNumberZeroBased).getTunnelDestination();
        String neighborId = vpnConnection.getTunnelInterface(localTunnelNumberZeroBased).getBgpState().getNeighborId();

        /**
         * Tokens used in delete templates:
         *
         * ${TunnelNumber}
         * ${TunnelId}
         * ${BgpNeighborId}
         * ${RemoteVpnIpAddress}
         * ${PresharedKey}
         * ${VpnId-Padded}
         * ${BgpAsn}
         *
         */
        valuesMap.put(mapper.getSubstitutionToken("paddedVpnId"), vpnIdPadded);
        valuesMap.put(mapper.getSubstitutionToken("remoteVpnIpAddress"), remoteVpnIpAddress);
        valuesMap.put(mapper.getSubstitutionToken("bgpNeighborId"), neighborId);
        valuesMap.put(mapper.getSubstitutionToken("tunnelId"), tunnelId);
        // TODO: Fix the value Id to be "routerNumber"
        valuesMap.put(mapper.getSubstitutionToken("tunnelNumber"), routerNumber);
        valuesMap.put(mapper.getSubstitutionToken("tunnelSuffix"), tunnelSuffix);
        valuesMap.put(mapper.getSubstitutionToken("presharedKey"), preSharedKey);
        valuesMap.put(mapper.getSubstitutionToken("bgpAsn"), bgpAsn);

//        Element nativeFromXml = mapper.hydrateVpnDeleteNativeConfigTemplate(vpnConnection, tunnelNumber);
        Map<String, String> pathValueMap = new HashMap<>();
        Element nativeFromXml = mapper.hydrateTemplate(valuesMap, "deleteTemplate", pathValueMap);
        mapper.validatePathToValuesMap(pathValueMap);

        StringBuilder infoBuilder = new StringBuilder("Hydrated delete template paths/values:");
        pathValueMap.entrySet().forEach(e -> {
            infoBuilder.append("\n    " + e.getKey() + " -> " + e.getValue());
        });
        logger.info(getLogtag() + infoBuilder.toString());

        Exception lastRetryError = null;
        while (retryCount-- > 0) {
            try {

                logger.info(getLogtag() + "Delete native config: \n\n" + nativeFromXml.toXMLString());

                session.editConfig(nativeFromXml);

                logger.info(getLogtag() + "Successful delete/native config after attempt #" + (operationErrorRetryCount - retryCount));
                break;

            } catch (Exception e) {
                if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    String errMsg = "Unexpected error while attempting vpn provision delete/native config - vpcId: " + vpnConnection.getVpcId();
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg, e);
                }
            } finally {
                long startTime = System.currentTimeMillis();
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }
        if (retryCount < 0) {
            throw new ProviderException("Failed to provision delete/native config after " + buildTriesString(), lastRetryError);
        }

    }

    private void waitForVpnTunnelShutdownToComplete(VpnDeleteShutdownTunnelValidatorCommand validatorCommand, WaitForVpnOperationValidator validator, NetconfSession session) throws ProviderException {
        int retryCount = waitForOperationRetryCount;
        int retryWait = waitForOperationRetryWaitInMilliseconds;
        String tunnelName = validatorCommand.getTunnelName();

        while (retryCount-- > 0) {

            try {
                Element tunnelInterfaceConfig =  queryTunnelInterface(tunnelName, session);
                if (tunnelInterfaceConfig != null) {

                    validatorCommand.setTunnelShutdown(ElementUtils.safeGetElementName(tunnelInterfaceConfig, mapper.xpathInterfaceTunnelShutdown()));
                    if (validator.validate(validatorCommand)) {
                        break;
                    }
                } else if (!validatorCommand.expectResult()){
                    break;
                }
            } catch (ProviderException e) {
                logger.error(getLogtag() + "Error querying for tunnel interface config", e);
            } catch (JNCException e) {
                logger.error(getLogtag() + "JNC Error querying for tunnel interface config", e);
            }

            try {
                logger.info(getLogtag() + "No tunnel interface shutdown results yet, retrying...");
                Thread.sleep(retryWait);
            } catch (InterruptedException e) {
                logger.error(getLogtag() + "Sleep interrupted", e);
                break;
            }
        }
        if (retryCount == 0) {
            String errMsg = "Wait for operation failed to find tunnel interface for tunnel Id: " + tunnelName + " - max retries";
            logger.error(getLogtag() + errMsg);
            throw new RuntimeException(errMsg);
        }

    }

    private VpnConnection waitForVpnOperationToComplete(VpnResponseValidatorCommand validatorCommand, WaitForVpnOperationValidator validator, NetconfSession session) throws ProviderException {

        VpnConnection vpnConnectionfromDevice = null;
        int retryCount = waitForOperationRetryCount;
        int retryWait = waitForOperationRetryWaitInMilliseconds;

        VpnConnectionQuerySpecification vpnConnectionQuerySpecification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
        String vpcId = validatorCommand.getVpcId();
        String vpnId = validatorCommand.getVpnId();

        while (retryCount-- > 0) {

            try {
                vpnConnectionQuerySpecification.setVpcId(vpcId);
                vpnConnectionQuerySpecification.setVpnId(vpnId);
                logger.info(getLogtag() + "Waiting for vpn operation to complete - querying for vpn connection with vpcId: " + vpcId);
                vpnConnectionfromDevice = query(vpnConnectionQuerySpecification, session);
                if (vpnConnectionfromDevice != null) {
                    validatorCommand.setVpnConnection(vpnConnectionfromDevice);
                    if (validator.validate(validatorCommand)) {
                        break;
                    }
                } else if (!validatorCommand.expectResult()){
                    break;
                }
            } catch (EnterpriseFieldException e) {
                logger.error(getLogtag() + "EnterpriseFieldException while querying for Vpn config", e);
            } catch (ProviderException e) {
                logger.error(getLogtag() + "Error querying for Vpn config", e);
            }

            try {
                logger.info(getLogtag() + "Failed to get vpn provision results, retrying...");
                Thread.sleep(retryWait);
            } catch (InterruptedException e) {
                logger.error(getLogtag() + "Sleep interrupted", e);
                break;
            }
        }
        if (retryCount == 0) {
            String errMsg = "Wait for operation failed to find vpn configuration for vpc Id: " + vpcId + " - max retries";
            logger.error(getLogtag() + errMsg);
            throw new RuntimeException(errMsg);
        }

        return vpnConnectionfromDevice;
    }

    /**
     * Given a map of key -> YANG element that contains a description, return a map of string to object that essentially
     * is the status of whether a matched vpcId was found in ALL description elements.
     *
     * For the multi-version router support, this can stay here (not moved to Mapper) iff the vpcId is a parenthesized
     * value in the description.  If this changes, concrete mapper implementations will be required.
     *
     * @param descriptionMap - A String to Element map where the key is the xpath to the child of the Element containing
     *                       the desired Description value
     * @param vpcId - The vpcId to match against
     * @return - A map that contains the match result enum (VpcIdMatchResult) and the target vpcId to match
     * @throws ProviderException - If the Description element is malformed or invalid
     */
    private Map<String, Object> matchVpcIdInResponse(Map<String, Element> descriptionMap, String vpcId) throws ProviderException {
        Map<String, Object> resultMap = new HashMap<>();
        VpcIdMatchResult matchResult = VpcIdMatchResult.MATCH;

        StringBuilder builder = new StringBuilder("Returned descriptions:\n");
        Map<String, String> foundDescriptionMap = new HashMap<>();
        for (Map.Entry<String, Element> entry : descriptionMap.entrySet()) {
            try {
                String description = ElementUtils.safeGetValue(entry.getValue(), entry.getKey());
                if (!description.equals("[unknown]")) {
                    foundDescriptionMap.put(entry.getKey(), description);
                    builder.append("    " + entry.getKey() + " -> " + description + "\n");
                }
            } catch (JNCException e) {
                String errMsg = "Failed to get description for " + entry.getKey();
                logger.error(getLogtag() + errMsg);
                throw new ProviderException(errMsg, e);
            }
        };
        logger.info(getLogtag() + builder.toString());


        // Create a list of all nodes containing "AVAILABLE" (tunnel shutdown)
        List<String> allAvailables = foundDescriptionMap.entrySet().stream()
                .filter(e -> e.getValue().contains("AVAILABLE"))
                .map(e -> e.getKey() + " -> " + e.getValue())
                .collect(Collectors.toList());

        String foundVpcId = null;

        do {
            // Missing desciptions means tunnel associated with profile has been shutdown.
            if (descriptionMap.size() != foundDescriptionMap.entrySet().size()) {
                matchResult = VpcIdMatchResult.NOT_FOUND;
                break;
            }

            // Any "AVAILABLE" description content means tunnel associated with profile has been shutdown.
            if (allAvailables.size() > 0) {
                matchResult = VpcIdMatchResult.NOT_FOUND;
                StringBuilder buffer = new StringBuilder("Returned nodes containing \"AVAILABLE\":\n");
                allAvailables.forEach(a -> {
                    buffer.append("    " + a + "\n");
                });
                logger.info(getLogtag() + buffer.toString());
                break;
            }

            // All six vpc ids must be identical
            List<String> pureVpcIds = foundDescriptionMap.values().stream()
                    .map(d -> {
                        Matcher matcher = parenthesizedValuePattern.matcher(d);
                        if (matcher.find()) {
                            return matcher.group(1);
                        } else {
                            return d;
                        }
                    })
                    .collect(Collectors.toList());

            Set<String> uniqueVpcIds = pureVpcIds.stream()
                    .collect(Collectors.toSet());

            if (uniqueVpcIds.size() != 1) {
                logger.error(getLogtag() + "Broken tunnel - inconsistent vpc id values found.  See list above.");
                matchResult = VpcIdMatchResult.BROKEN_TUNNEL;
                break;
            }

            String returnedVpcId = uniqueVpcIds.iterator().next();

            // Check for mismatched vpc id, otherwise - FOUND.
            if (StringUtils.isNotEmpty(vpcId)) {

                if (!vpcId.equals(returnedVpcId)) {
                        matchResult = VpcIdMatchResult.MISMATCH;
                        logger.warn(getLogtag() + "Vpc id mismatch - returned vpc id: " + returnedVpcId + " - expected: " + vpcId + " (see list above)");
                }
                break;
            }

            // Empty vpcId - set found value.
            foundVpcId = returnedVpcId;

        } while (false);

        resultMap.put("matchResult", matchResult);
        resultMap.put("foundVpcId", foundVpcId);

        return resultMap;
    }

    private boolean validateResponse(Map<String, String> pathValueMap, NodeSet nativeResult, NodeSet bgpStateDataResult, NodeSet interfacesStateResult) {
        boolean validated = true;

        StringBuilder builder = new StringBuilder();
        StringBuilder infoBuilder = new StringBuilder("Validate Response - checking native result:");
        // Native config

        for (Map.Entry<String, String> entry : pathValueMap.entrySet()) {
            String xpath = entry.getKey();

            if (!xpath.startsWith("native/")) {
                continue;
            }
            xpath = xpath.replace("native/", "");
            infoBuilder.append("\n    " + xpath + " -> " + entry.getValue() + " ...");
            try {
                NodeSet nodes = nativeResult.get(xpath);
                for (Element element : nodes) {
                    if (!entry.getValue().equals(element.getValue())) {
                        if (builder.length() == 0) {
                            builder.append("\n===> Native Config errors:");
                        }
                        builder.append("\n    " + xpath + " - mismatch: expected: " + entry.getValue() + " returned: " + element.getValue());
                    }
                }
            } catch (JNCException e) {
                if (builder.length() == 0) {
                    builder.append("\n===> Native Config errors:");
                }
                builder.append("    " + xpath + ": error retrieving result: " + e.getMessage());
            }

        }

        infoBuilder.append("\n\nChecking bgp state data results...");
        // bgp-state-data
        for (Map.Entry<String, String> entry : pathValueMap.entrySet()) {
            String xpath = entry.getKey();

            if (!xpath.startsWith("bgp-state-data/")) {
                continue;
            }
            xpath = xpath.replace("bgp-state-data/", "");
            infoBuilder.append("\n    " + xpath + " -> " + entry.getValue() + " ...");
            try {
                NodeSet nodes = bgpStateDataResult.get(xpath);
                for (Element element : nodes) {
                    if (!entry.getValue().equals(element.getValue())) {
                        if (builder.length() == 0) {
                            builder.append("\n===> Bgp State Data errors:");
                        }
                        builder.append("\n    " + xpath + " - mismatch: expected: " + entry.getValue() + " returned: " + element.getValue());
                    }
                }
            } catch (JNCException e) {
                if (builder.length() == 0) {
                    builder.append("\n===> Bgp State Data errors:");
                }
                builder.append("    " + xpath + ": error retrieving result: " + e.getMessage());
            }

        }

        infoBuilder.append("\n\nChecking interfaces state results...");
        // interfaces-state

        for (Map.Entry<String, String> entry : pathValueMap.entrySet()) {
            String xpath = entry.getKey();

            if (!xpath.startsWith("interfaces-state/")) {
                continue;
            }
            xpath = xpath.replace("interfaces-state/", "");
            infoBuilder.append("\n    " + xpath + " -> " + entry.getValue() + " ...");
            try {
                NodeSet nodes = interfacesStateResult.get(xpath);
                for (Element element : nodes) {
                    if (!entry.getValue().equals(element.getValue())) {
                        if (builder.length() == 0) {
                            builder.append("\n===> Interfaces State errors:");
                        }
                        builder.append("\n    " + xpath + " - mismatch: expected: " + entry.getValue() + " returned: " + element.getValue());
                    }
                }
            } catch (JNCException e) {
                if (builder.length() == 0) {
                    builder.append("\n===> Interfaces State errors:");
                }
                builder.append("    " + xpath + ": error retrieving result: " + e.getMessage());
            }

        }

        logger.info(getLogtag() + infoBuilder.toString());

        if (builder.length() > 0) {
            logger.error(getLogtag() + "Failed to match results: " + builder.toString());
            validated = false;
        }

        return validated;
    }

    /**
     * For now, use the TunnelProfile for the provided zero based tunnel index
     * @param vpnConnectionRequisition
     * @return
     */
    private String getTunnelIdFromRequisition(VpnConnectionRequisition vpnConnectionRequisition, Integer zeroBasedTunnelIndex) throws ProviderException {
        List<RemoteVpnConnectionInfo> remoteVpnConnectionInfoList = vpnConnectionRequisition.getRemoteVpnConnectionInfo();

        if (remoteVpnConnectionInfoList.size() <= zeroBasedTunnelIndex) {
            String errMsg = "Tunnel index out of bounds: Tunnel index: " + zeroBasedTunnelIndex + " RemoteVpnConnectionInfo list size: " + remoteVpnConnectionInfoList.size();
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

        RemoteVpnConnectionInfo remoteVpnConnectionInfo = remoteVpnConnectionInfoList.get(zeroBasedTunnelIndex);

        List<RemoteVpnTunnel> remoteVpnTunnelList = remoteVpnConnectionInfo.getRemoteVpnTunnel();
        RemoteVpnTunnel remoteVpnTunnel = remoteVpnTunnelList.get(zeroBasedTunnelIndex);

        return remoteVpnTunnel.getLocalTunnelId();

    }

    /**
     * This calculation should be as simple as getting the number of RemoteVpnTunnel objects in the list.
     *
     * @param vpnConnectionRequisition
     * @return
     * @throws ProviderException
     */
    private Integer getTunnelCountForRouter(VpnConnectionRequisition vpnConnectionRequisition) {

        List<RemoteVpnTunnel> remoteVpnTunnelList = vpnConnectionRequisition.getRemoteVpnConnectionInfo();

        return remoteVpnTunnelList.size();
    }

    private Integer getTunnelCountForRouter(NetconfSession session) throws ProviderException {
        Integer tunnelCount = 1;
        try {
            String tunnelId = mapper.buildQueryTunnelId(Integer.valueOf(routerNumber), 0, 1);
            String xpath = mapper.xpathNativeInterfaceTunnelNameWithValue(tunnelId);
            NodeSet result = session.get(xpath);
            tunnelCount = result.size();
            tunnelId = mapper.buildQueryTunnelId(Integer.valueOf(routerNumber), 1, 1);
            xpath = mapper.xpathNativeInterfaceTunnelNameWithValue(tunnelId);
            result = session.get(xpath);
            tunnelCount += result.size();
        } catch (Exception e) {
            String errMsg = "Failed to get tunnel count";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
        return tunnelCount;
    }

    private enum VpcIdMatchResult {
        MATCH("match"),
        NOT_FOUND("not_found"),
        MISMATCH("mismatch"),
        BROKEN_TUNNEL( "broken_tunnel");

        VpcIdMatchResult(String name) {
        }

        private String name;

        public String getName() {
            return name;
        }
    }


}

