package edu.emory.it.service.ca.provider.util;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.BgpPrefixes;
import edu.emory.moa.objects.resources.v1_0.BgpState;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecTransformSet;
import edu.emory.moa.objects.resources.v1_0.CryptoIsakmpProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoKeyring;
import edu.emory.moa.objects.resources.v1_0.LocalAddress;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;
import edu.emory.moa.objects.resources.v1_0.TunnelInterface;

public class VpnConnectionDump extends Dump {
    final private VpnConnection vpnConnection;
    private StringBuilder builder;

    public VpnConnectionDump(VpnConnection vpnConnection) {
        this.vpnConnection = vpnConnection;
        builder = new StringBuilder();
    }


    public String dump() {

        dumpHeader("VpnConnection", builder);
        dump("vpcId: " + vpnConnection.getVpcId(), builder);
        dump("vpnId: " + vpnConnection.getVpnId(), builder);

        CryptoKeyring cryptoKeyring = vpnConnection.getCryptoKeyring();
        dumpCryptoKeyring(cryptoKeyring);

        CryptoIsakmpProfile cryptoIsakmpProfile = vpnConnection.getCryptoIsakmpProfile();

        if (cryptoIsakmpProfile != null) {
            dumpHeader("CryptoIsakmpProfile values", builder);

            if (cryptoIsakmpProfile.getName() == null
                    && cryptoIsakmpProfile.getDescription() == null
                    && cryptoIsakmpProfile.getVirtualRouteForwarding() == null
                    && cryptoIsakmpProfile.getCryptoKeyring() == null
                    && cryptoIsakmpProfile.getMatchIdentity() == null) {
                dumpNoValues("CRYPTO ISAKMP PROFILE", builder);
            } else {
                dump("Name: " + cryptoIsakmpProfile.getName(), builder);
                dump("Description: " + cryptoIsakmpProfile.getDescription(), builder);
                dump("Vrf: " + cryptoIsakmpProfile.getVirtualRouteForwarding(), builder);

                cryptoKeyring = cryptoIsakmpProfile.getCryptoKeyring();
                increaseIndent();
                dumpCryptoKeyring(cryptoKeyring);

                MatchIdentity matchIdentity = cryptoIsakmpProfile.getMatchIdentity();
                if (matchIdentity != null) {
                    dumpHeader("MatchIdentity values", builder);
                    dump("Ip Address: " + matchIdentity.getIpAddress(), builder);
                    dump("Netmask: " + matchIdentity.getNetmask(), builder);
                    dump("Vrf: " + matchIdentity.getVirtualRouteForwarding(), builder);
                } else {
                    dump("No match identity", builder);
                }
            }

        }

        decreaseIndent();

        CryptoIpsecTransformSet cryptoIpsecTransformSet = vpnConnection.getCryptoIpsecTransformSet();

        if (cryptoIpsecTransformSet != null) {
            dumpHeader("CryptoIpsecTransformSet values", builder);
            if (cryptoIpsecTransformSet.getName() == null
                    && cryptoIpsecTransformSet.getBits() == null
                    && cryptoIpsecTransformSet.getCipher() == null
                    && cryptoIpsecTransformSet.getMode() == null) {
                dumpNoValues("CRYPTO IPSEC TRANSFORM SET", builder);
            } else {
                dump("Name: " + cryptoIpsecTransformSet.getName(), builder);
                dump("Bits: " + cryptoIpsecTransformSet.getBits(), builder);
                dump("Cipher: " + cryptoIpsecTransformSet.getCipher(), builder);
                dump("Mode: " + cryptoIpsecTransformSet.getMode(), builder);
            }
        } else {
            dump("No crypto ipsec transform set", builder);
        }

        CryptoIpsecProfile cryptoIpsecProfile = vpnConnection.getCryptoIpsecProfile();

        dumpCryptoIpsecProfile(cryptoIpsecProfile);

        TunnelInterface tunnelInterface = vpnConnection.getTunnelInterface(0);
        if (tunnelInterface != null) {
            dumpHeader("TunnelInterface values", builder);
            if (tunnelInterface.getName() == null
                    && tunnelInterface.getDescription() == null
                    && tunnelInterface.getVirtualRouteForwarding() == null
                    && tunnelInterface.getIpAddress() == null
                    && tunnelInterface.getNetmask() == null
                    && tunnelInterface.getTcpMaximumSegmentSize() == null
                    && tunnelInterface.getTunnelSource() == null
                    && tunnelInterface.getTunnelMode() == null
                    && tunnelInterface.getTunnelDestination() == null
                    && tunnelInterface.getIpVirtualReassembly() == null
                    && tunnelInterface.getAdministrativeState() == null
                    && tunnelInterface.getOperationalStatus() == null
                    && tunnelInterface.getCryptoIpsecProfile() == null
                    && tunnelInterface.getBgpState() == null
                    && tunnelInterface.getBgpPrefixes() == null) {
                dumpNoValues("TUNNEL INTERFACE", builder);
            } else {
                dump("Name: " + tunnelInterface.getName(), builder);
                dump("Description: " + tunnelInterface.getDescription(), builder);
                dump("Vrf: " + tunnelInterface.getVirtualRouteForwarding(), builder);
                dump("IpAddress: " + tunnelInterface.getIpAddress(), builder);
                dump("Netmask: " + tunnelInterface.getNetmask(), builder);
                dump("TcpMaximumSegmentSize: " + tunnelInterface.getTcpMaximumSegmentSize(), builder);
                dump("TunnelSource: " + tunnelInterface.getTunnelSource(), builder);
                dump("TunnelMode: " + tunnelInterface.getTunnelMode(), builder);
                dump("TunnelDestination: " + tunnelInterface.getTunnelDestination(), builder);
                dump( "IpVirtualReassembly: " + tunnelInterface.getIpVirtualReassembly(), builder);
                dump("AdminstrativeState: " + tunnelInterface.getAdministrativeState(), builder);
                dump("OperationalStatus: " + tunnelInterface.getOperationalStatus(), builder);

                cryptoIpsecProfile = tunnelInterface.getCryptoIpsecProfile();

                increaseIndent();
                dumpCryptoIpsecProfile(cryptoIpsecProfile);

                BgpState bgpState = tunnelInterface.getBgpState();
                if (bgpState != null) {
                    dumpHeader("BgpState values", builder);
                    if (bgpState.getStatus() == null
                            && bgpState.getUptime() == null) {
                        dumpNoValues("BGP STATE", builder);
                    } else {
                        dump("Status: " + bgpState.getStatus(), builder);
                        dump("Uptime: " + bgpState.getUptime(), builder);
                    }
                }
                BgpPrefixes bgpPrefixes = tunnelInterface.getBgpPrefixes();
                if (bgpPrefixes != null) {
                    dumpHeader("BgpPrefixes values", builder);
                    if (bgpPrefixes.getSent() == null
                            && bgpPrefixes.getReceived() == null) {
                        dumpNoValues("BGP PREFIXES", builder);
                    } else {
                        dump("Sent: " + bgpPrefixes.getSent(), builder);
                        dump("Received: " + bgpPrefixes.getReceived(), builder);
                    }
                }
            }
        }
        return builder.toString();
    }

    private void dumpCryptoKeyring(CryptoKeyring cryptoKeyring) {
        if (cryptoKeyring != null) {
            dumpHeader("CryptoKeyring values", builder);
            if (cryptoKeyring.getName() == null
                    && cryptoKeyring.getDescription() == null
                    && cryptoKeyring.getPresharedKey() == null
                    && cryptoKeyring.getLocalAddress() == null) {
                dumpNoValues("CRYPTO KEYRING", builder);
            } else {
                dump("Name: " + cryptoKeyring.getName(), builder);
                dump("Descripton: " + cryptoKeyring.getDescription(), builder);
                dump("Preshared Key: " + cryptoKeyring.getPresharedKey(), builder);

                LocalAddress localAddress = cryptoKeyring.getLocalAddress();
                if (localAddress != null) {
                    dump("LocalAdress values", builder);
                    dump("IpAddress: " + localAddress.getIpAddress(), builder);
                    dump("vrf: " + localAddress.getVirtualRouteForwarding(), builder);
                } else {
                    dump("no crypto keyring local address", builder);
                }
            }
        } else {
            dump("no crypto keyring", builder);
        }

    }

    private void dumpCryptoIpsecProfile(CryptoIpsecProfile cryptoIpsecProfile) {
        if (cryptoIpsecProfile != null) {
            if (cryptoIpsecProfile.getName() == null
                    && cryptoIpsecProfile.getDescription() == null
                    && cryptoIpsecProfile.getPerfectForwardSecrecy() == null) {
                dumpNoValues("CRYPTO IPSEC PROFILE", builder);
            } else {
                dumpHeader("CryptoIpsecProfile values", builder);
                dump("Name: " + cryptoIpsecProfile.getName(), builder);
                dump("Description: " + cryptoIpsecProfile.getDescription(), builder);
                dump("PerfectForwardSecrecy: " + cryptoIpsecProfile.getPerfectForwardSecrecy(), builder);
            }
        } else {
            dump("No crypto ipsec profile", builder);
        }

    }



}
