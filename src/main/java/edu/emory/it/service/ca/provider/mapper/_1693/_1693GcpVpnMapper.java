package edu.emory.it.service.ca.provider.mapper._1693;

import edu.emory.it.service.ca.provider.mapper.VpnMapper;

/**
 * All default behavior is implemented in abstract vpn mapper
 *
 */
public class _1693GcpVpnMapper extends _1693AbstractVpnMapper implements VpnMapper {
}
