package edu.emory.it.service.ca.provider.validator.response;

public class VpnDeleteNativeConfigValidatorCommand extends VpnResponseValidatorCommand implements VpnResponseValidator {

    @Override
    public boolean expectResult() {
        return false;
    }

}
