package edu.emory.it.service.ca.provider;

import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.IPV4Address;
import edu.emory.it.service.ca.provider.validator.VpnConnectionOperationContext;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile;
import edu.emory.moa.objects.resources.v1_0.BgpPrefixes;
import edu.emory.moa.objects.resources.v1_0.BgpState;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecTransformSet;
import edu.emory.moa.objects.resources.v1_0.CryptoIsakmpProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoKeyring;
import edu.emory.moa.objects.resources.v1_0.LocalAddress;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;
import edu.emory.moa.objects.resources.v1_0.TunnelInterface;
import edu.emory.moa.objects.resources.v1_0.TunnelProfile;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExampleVpnConnectionProvider extends ExampleProvider implements VpnConnectionProvider {

    private static final Logger logger = OpenEaiObject.logger;
    protected String labRemoteVpnIpAddress = "72.21.209.225";
    private String exampleProviderBanner =
            "########################### XXXXXXXXXXXXXXXX ############################\n" +
            "#                                                                       #\n" +
            "#        ######   #   #      #     #     #  ####   #       ######       #\n" +
            "#        #         # #      # #    ##   ##  #   #  #       #            #\n" +
            "#        #####      #      #####   # # # #  ####   #       #####        #\n" +
            "#        #         # #    #     #  #  #  #  #      #       #            #\n" +
            "#        ######   #   #  #       # #     #  #      ######  ######       #\n" +
            "#                                                                       #\n" +
            "#                      V P N    C O N N E C T I O N                     #\n" +
            "#                                                                       #\n" +
            "#        ####   ####     ##    #     #  #  ####    ######  ####         #\n" +
            "#        #   #  #   #   #  #   #     #  #  #   #   #       #   #        #\n" +
            "#        ####   ####   #    #   #   #   #  #   #   #####   ####         #\n" +
            "#        #      #   #   #  #     # #    #  #   #   #       #   #        #\n" +
            "#        #      #   #    ##       #     #  ####    ######  #   #        #\n" +
            "#                                                                       #\n" +
            "########################### XXXXXXXXXXXXXXXX ############################\n";

    private VPNs vpns = new VPNs();

    protected int generateCounter;
    protected int queryCounter;

    @Override
    public void init(AppConfig appConfig) throws ProviderException {
        if (appConfig != null) {
            logger.info(getLogtag() + "init with AppConfig");
            try {
                super.appConfig = appConfig;
                Properties generalProperties  = appConfig.getProperties("GeneralProperties");
                routerNumber = (String)generalProperties.get("routerNumber");

                queryCounter = 0;
                generateCounter = 0;

                displayBanner(exampleProviderBanner);
            } catch (EnterpriseConfigurationObjectException e) {
                logger.error(getLogtag() + "Failed to init example vpn connection provider", e);
                throw new ProviderException(e.getMessage());
            }
        }

    }

    @Override
    public List<VpnConnection> query(VpnConnectionQuerySpecification querySpec) throws ProviderException {

        String vpnProfileId = querySpec.getVpnId();
        String vpcId = querySpec.getVpcId();

        // To fail on first call ONLY to query (this method), but only for tunnel 1:
//        fail(errorConditionFailOnCount, ++queryCounter, 1, "1", "Failed to query on first call only for tunnel 1");
        // To fail on second 2 queries, but only on tunnel 1
//        fail(errorConditionFailOnCount, ++queryCounter, 2, "1", "Failed to query on first call only for tunnel 1");

        //########################### For sweeper/reconciler #########################################
//        fail(errorRangeCondition, ++queryCounter, 3, 3, "2", "Failed to generate on call " + queryCounter);
        //########################### For sweeper/reconciler #########################################

        // Change tunnelNumber to tunnel of choice to cause a test exception to be thrown.
//        fail("1", "This is a test exception vpn query exception");

        List<VpnConnection> vpnConnections = new ArrayList<>();

        VpnConnection vpnConnection;


        // To generate a bogus connection for first query:
        if (StringUtils.isEmpty(vpcId) && StringUtils.isEmpty(vpnProfileId)) {
            // TODO: Here's where you can simulate the return of multiple vpnConnection objects for a wildcard query.
            VpnConnectionRequisition requisition = new VpnConnectionRequisition();
            VpnConnectionProfile profile = new VpnConnectionProfile();
            try {
                profile.setVpnConnectionProfileId("1");
                requisition.setVpnConnectionProfile(profile);
                requisition.setOwnerId("fake-owner-id");
                try {

                    vpnConnection = buildVpnConnectionResponseObject(requisition);
                } catch (ProviderException e) {
                    String errMsg = "Failed create vpn connection for vpcId: " + vpcId + " - vpnProfileId: " + vpnProfileId;
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);
                }

                vpnConnections.add(vpnConnection);
            } catch (EnterpriseFieldException e) {

            }

        } else {
            vpns.validateVpnConnectionParamsForQuery(vpcId, vpnProfileId);
            vpnConnection = vpns.get(vpnProfileId);

            if (vpnConnection != null && StringUtils.isNotEmpty(vpcId)) {
                vpnConnections.add(vpnConnection);
            }
        }
        // Cause confirmation step rollback - enable both timeout conditions.
//        timeoutCondition.trigger("1", 60000);
//        timeoutCondition.trigger("2", 60000);

        // To fail on first call only to each router - allows sweeper/reconciler to succeed.
        // This one is kinda tricky: generateCounter is 1 and 2 for provisioning's two calls, and allow query for profile id algorithm.
//        fail(errorRangeCondition, ++queryCounter, 2, 2, tunnelNumber, "Failed to query on call " + queryCounter);

        routerExecutionTimeInMilliseconds = getExecutionTime();

        return vpnConnections;
    }

    private String exception = "<rpc-error>\n" +
            "    <error-type>application</error-type>\n" +
            "    <error-tag>invalid-value</error-tag>\n" +
            "    <error-severity>error</error-severity>\n" +
            "    <error-message unknown:lang=\"en\">inconsistent value: Device refused one or more commands</error-message>\n" +
            "    <error-info>\n" +
            "      <severity xmlns=\"http://cisco.com/yang/cisco-ia\">error_cli</severity>\n" +
            "      <detail xmlns=\"http://cisco.com/yang/cisco-ia\">\n" +
            "        <bad-cli>\n" +
            "          <bad-command>  neighbor 169.254.255.249 description vpc-0cba9c802785016c6</bad-command>\n" +
            "          <error-location>60</error-location>\n" +
            "        </bad-cli>\n" +
            "      </detail>\n" +
            "    </error-info>\n" +
            "  </rpc-error>" +
            "<rpc-error>\n" +
            "    <error-type>application</error-type>\n" +
            "    <error-tag>invalid-value</error-tag>\n" +
            "    <error-severity>error</error-severity>\n" +
            "    <error-message unknown:lang=\"en\">inconsistent value: Device refused one or more commands</error-message>\n" +
            "    <error-info>\n" +
            "      <severity xmlns=\"http://cisco.com/yang/cisco-ia\">error_cli</severity>\n" +
            "      <detail xmlns=\"http://cisco.com/yang/cisco-ia\">\n" +
            "        <bad-cli>\n" +
            "          <bad-command>  neighbor 169.254.255.249 description vpc-0cba9c802785016c6</bad-command>\n" +
            "          <error-location>60</error-location>\n" +
            "        </bad-cli>\n" +
            "      </detail>\n" +
            "    </error-info>\n" +
            "  </rpc-error>";

    @Override
    public VpnConnection generate(VpnConnectionRequisition vpnConnectionRequisition) throws ProviderException {

        String[] tunnelNumbers = {};
//        fail(tunnelNumbers, "Test exception for generate");


        // ****** PARTIAL SUCCESS ******
        // To fail on first call to generate (this method) but only for tunnel 2:
//        fail(errorCondition, ++generateCounter, 1, "2", "Test error for first generate vpn");

        // To fail on second call to generate (this method) but only for tunnel 2:
//        fail(errorCondition, ++generateCounter, 2, "2", "Test error for second generate vpn");

        //############################## Sweeper ###########################################################
        // To fail on first call ONLY to generate (this method), but only for tunnel 1:
//        fail(errorConditionFailOnCount, ++generateCounter, 1, "1", "Failed to generate on first call only to generate vpn for tunnel 2");
        //##################################################################################################

        //############################## Sweeper ###########################################################
        // To fail on first call ONLY to generate (this method), but only for tunnel 2:
//        fail(errorConditionFailOnCount, ++generateCounter, 1, "2", "Failed to generate on first call only to generate vpn for tunnel 2");
        //##################################################################################################

        // To fail on second call ONLY to generate (this method), but only for tunnel 1:
//        fail(errorConditionFailOnCount, ++generateCounter, 2, "1", "Failed to generate on second call only to generate vpn for tunnel 1");

        // To fail for all calls after first, to generate, but only on tunnel 1:
//        fail(errorConditionFailAfterCount, ++generateCounter, 0, "1", "Failed to generate vpn for tunnel 1 for all calls after first call");

        // To fail on first 3 generates, but only on tunnel 2
//        fail(errorRangeCondition, ++generateCounter, 1, 3, "2", "Failed to generate on call " + generateCounter);

            //########################### For sweeper failure #################################
            // To fail on first 2 generates, but only on tunnel 2
//          fail(errorRangeCondition, ++generateCounter, 1, 2, "2", "Failed to generate on call " + generateCounter);
            //########################### For sweeper #########################################

        // To cause a rollback for VpnConnectionProvisioning.Generate-Request:
//        fail("1", "Test error on router 1 for generate vpn");
//        fail("2", "Test error on router 2 for generate vpn");

        // To cause a partial success.
//        fail("2", "Test error on router 2 for generate vpn");

        // To fail on first call only to each generate - allows sweeper/reconciler to succeed.
        // This one is kinda tricky: generateCounter is 1 and 2 for provisioning's two calls.
//        fail(errorConditionFailOnCount, ++generateCounter, 1, "1", "Failed to generate on first call only to generate vpn for tunnel 1");
//        fail(errorConditionFailOnCount, ++generateCounter, 2, "2", "Failed to generate on first call only to generate vpn for tunnel 2");

        vpns.validateVpnConnectionForAdd(vpnConnectionRequisition);

        String vpcId = vpnConnectionRequisition.getOwnerId();
        String vpnProfileId = vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId();

        logger.info(getLogtag() + "generating vpn connection by vpcId: " + vpcId + " - vpnProfileId: " + vpnProfileId);

        if (vpns.containsKey(vpnProfileId)) {
            String errMsg = "Vpn configuration already contains vpcId: " + vpcId + " - vpnProfileId: " + vpnProfileId;
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

        VpnConnection vpnConnection;

        try {

            vpnConnection = buildVpnConnectionResponseObject(vpnConnectionRequisition);
        } catch (ProviderException e) {
            String errMsg = "Failed create vpn connection for vpcId: " + vpcId + " - vpnProfileId: " + vpnProfileId;
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

        vpns.put(vpnProfileId, vpnConnection);
        routerExecutionTimeInMilliseconds = getExecutionTime();

        // To cause a rollback for VpnConnectionProvisioning.Generate-Request, with successful rollbacks:
//        fail("1", "Test error on router 1 for generate vpn");
//        fail("2", "Test error on router 2 for generate vpn");

        // Cause confirmation step rollback - enable both timeout conditions.
//        timeoutCondition.trigger("1", 60000);
//        timeoutCondition.trigger("2", 60000);


        return vpnConnection;
    }

    @Override
    public void delete(VpnConnection vpnConnection) throws ProviderException {


        // To throw an exception on first delete of tunnel1:
//        fail("1", "Test exception for delete vpn");

        // To throw an exception on first delete of tunnel2:
        //fail("2", "Test exception for delete vpn");

        vpns.validateVpnConnectionForDelete(vpnConnection);

        String vpcId = vpnConnection.getVpcId();
        String vpnProfileId = vpnConnection.getVpnId();

        logger.info(getLogtag() + "deleting vpn connection for vpcId: " + vpcId + " - vpnProfileId: " + vpnProfileId);
        if (!vpns.containsKey(vpnProfileId)) {
            String errMsg = "Vpn configuration does not contain vpcId: " + vpcId + " or vpnProfileId: " + vpnProfileId;
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
        vpns.remove(vpnProfileId);
        routerExecutionTimeInMilliseconds = getExecutionTime();
    }

    // ##################################### Example Provider Implementation ###########################################

    private VpnConnection buildVpnConnectionResponseObject(VpnConnectionRequisition vpnConnectionRequisition) throws ProviderException {

        VpnConnection vpnConnection;

        try {

            String vpcId = vpnConnectionRequisition.getOwnerId();
            Integer vpcNumber = Integer.parseInt(vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId());

            Map<String, String> addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    vpcNumber,
                    routerNumber,
                    VpnConnectionOperationContext.GENERATE_RESPONSE,
                    true
            );

            // Notes:
            // Remote VPN Address is AWS
            vpnConnection = AppConfigUtil.getVpnConnectionFromConfiguration(appConfig);

            CryptoKeyring cryptoKeyring = AppConfigUtil.getCryptoKeyringFromConfiguration(appConfig);
            CryptoIsakmpProfile cryptoIsakmpProfile = AppConfigUtil.getCryptoIsakmpProfileFromConfiguration(appConfig);
            CryptoIpsecTransformSet cryptoIpsecTransformSet = AppConfigUtil.getCryptoIpsecTransformSetFromConfiguration(appConfig);
            CryptoIpsecProfile cryptoIpsecProfile = AppConfigUtil.getCryptoIpsecProfileFromConfiguration(appConfig);
            TunnelInterface tunnelInterface = AppConfigUtil.getTunnelInterfaceFromConfiguration(appConfig);
            LocalAddress localAddress = AppConfigUtil.getLocalAddressProfileFromConfiguration(appConfig);
            MatchIdentity matchIdentity = AppConfigUtil.getMatchIdentityFromConfiguration(appConfig);
            BgpState bgpState = AppConfigUtil.getBgpStateFromConfiguration(appConfig);
            BgpPrefixes bgpPrefixes = AppConfigUtil.getBgpPrefixesFromConfiguration(appConfig);

            cryptoKeyring.setLocalAddress(localAddress);
            cryptoIsakmpProfile.setLocalAddress(localAddress);
            vpnConnection.setCryptoKeyring(cryptoKeyring);
            vpnConnection.setCryptoIsakmpProfile(cryptoIsakmpProfile);
            vpnConnection.setCryptoIpsecTransformSet(cryptoIpsecTransformSet);
            vpnConnection.setCryptoIpsecProfile(cryptoIpsecProfile);
            vpnConnection.addTunnelInterface(tunnelInterface);

            String tunnelInterfaceIpAddress = addDeleteParams.get("tunnelIpAddress");

            vpnConnection.setVpcId(addDeleteParams.get("vpcId"));
            vpnConnection.setVpnId(addDeleteParams.get("profileId"));

            localAddress.setIpAddress(addDeleteParams.get("remoteVpnIpAddress"));
            localAddress.setVirtualRouteForwarding("AWS");
            cryptoKeyring.setName(addDeleteParams.get("cryptoKeyringName"));
            cryptoKeyring.setDescription(addDeleteParams.get("vpcId"));
            cryptoKeyring.setPresharedKey(addDeleteParams.get("presharedKey"));
            cryptoIsakmpProfile.setName(addDeleteParams.get("isakmpProfileName"));
            cryptoIsakmpProfile.setDescription(addDeleteParams.get("vpcId"));
            cryptoIsakmpProfile.setVirtualRouteForwarding("AWS");
            cryptoIsakmpProfile.setCryptoKeyring(cryptoKeyring);
            cryptoIsakmpProfile.setMatchIdentity(matchIdentity);
            cryptoIpsecProfile.setPerfectForwardSecrecy("group2");
            cryptoIpsecProfile.setDescription(addDeleteParams.get("vpcId"));
            cryptoIpsecProfile.setCryptoIpsecTransformSet(cryptoIpsecTransformSet);
            cryptoIpsecTransformSet.setName(addDeleteParams.get("ipsecTransformSetName"));
            cryptoIpsecTransformSet.setMode("tunnel");
            cryptoIpsecTransformSet.setBits("256");
            cryptoIpsecTransformSet.setCipher("esp-sha256-hmac");
            cryptoIpsecProfile.setName(addDeleteParams.get("ipsecProfileName"));
            tunnelInterface.setName(addDeleteParams.get("tunnelId"));
            tunnelInterface.setDescription(addDeleteParams.get("tunnelDescription"));
            tunnelInterface.setTunnelSource(addDeleteParams.get("customerGatewayIp"));
            tunnelInterface.setTunnelDestination(addDeleteParams.get("remoteVpnIpAddress"));
            tunnelInterface.setIpAddress(tunnelInterfaceIpAddress);
            tunnelInterface.setVirtualRouteForwarding("AWS");
            tunnelInterface.setNetmask("255.255.255.252");
            tunnelInterface.setTcpMaximumSegmentSize("1387");
            tunnelInterface.setAdministrativeState("up");     // TODO: up/down for add or delete?
            tunnelInterface.setTunnelMode("tunnel");
            tunnelInterface.setIpVirtualReassembly("[unknown]");
            tunnelInterface.setOperationalStatus("up");
            tunnelInterface.setCryptoIpsecProfile(cryptoIpsecProfile);
            tunnelInterface.setBgpState(bgpState);
            tunnelInterface.setBgpPrefixes(bgpPrefixes);

            matchIdentity.setVirtualRouteForwarding("AWS");
            matchIdentity.setIpAddress(addDeleteParams.get("remoteVpnIpAddress"));
            matchIdentity.setNetmask("255.255.255.255");

            bgpState.setStatus("fsm-established");
            bgpState.setUptime("00:00:00");
            bgpState.setNeighborId(addDeleteParams.get("neighborId"));

            bgpPrefixes.setSent("0");
            bgpPrefixes.setReceived("0");
        } catch (Exception e) {
            logger.error(getLogtag() + "Failed to build vpn connection object", e);
            throw new ProviderException(e.getMessage());
        }
        return vpnConnection;
    }

    private Map<String, String> buildAddDeleteParamsFromSpreadsheet(String vpcId, Integer vpcNumber, String tunnelNumberAsString, VpnConnectionOperationContext operationContext, boolean verbose) {
        Map<String, String> addDeleteParams = new HashMap<>();

        try {
            final Map<String, Map<String, String>> spreadsheetMap = getSpreadsheetMap();
            String paddedVpcNumberAsString = String.format("%03d", vpcNumber);

            Map<String, String> lineMap = spreadsheetMap.get(paddedVpcNumberAsString);
            if (lineMap == null) {
                throw new RuntimeException("Bad vpc number - no map entry!");
            }

            addDeleteParams.put("operation", operationContext.getOperation());
            String remoteVpnIpAddress = labRemoteVpnIpAddress;
            String vpcNumberAsString = vpcNumber.toString();
            String testPresharedKey = buildLabUnencryptKey(Integer.valueOf(vpcNumberAsString));

            addDeleteParams.put("vpcId", vpcId);
            addDeleteParams.put("vpnId", vpcId);
            addDeleteParams.put("vpcNumberAsString", vpcNumberAsString);
            addDeleteParams.put("paddedVpcNumberAsString", paddedVpcNumberAsString);
            addDeleteParams.put("vpcNetworkIpRange", lineMap.get("VPC_NET"));
            addDeleteParams.put("presharedKey", testPresharedKey);
            addDeleteParams.put("remoteVpnIpAddress", remoteVpnIpAddress);

            String insideCidrIpRange;
            String profileId;
            switch (tunnelNumberAsString) {
                case "1":
                    addDeleteParams.put("customerGatewayIp", lineMap.get("CGW1_IP"));
                    addDeleteParams.put("cryptoKeyringName", lineMap.get("CRYPTO_KEYRING_1"));
                    addDeleteParams.put("isakmpProfileName", lineMap.get("ISAKMP_PROFILE_1"));
                    addDeleteParams.put("ipsecTransformSetName", lineMap.get("IPSEC_TRANSFORM_SET_1"));
                    addDeleteParams.put("ipsecProfileName", lineMap.get("IPSEC_PROFILE_1"));
                    addDeleteParams.put("tunnelId", lineMap.get("TUNNEL_INTERFACE_1"));
                    profileId = inferProfileId(lineMap.get("TUNNEL_INTERFACE_1"));
                    insideCidrIpRange = lineMap.get("VPN1_INSIDE_IP_CIDR_1");
                    break;
                case "2":
                    addDeleteParams.put("customerGatewayIp", lineMap.get("CGW2_IP"));
                    addDeleteParams.put("cryptoKeyringName", lineMap.get("CRYPTO_KEYRING_2"));
                    addDeleteParams.put("isakmpProfileName", lineMap.get("ISAKMP_PROFILE_2"));
                    addDeleteParams.put("ipsecTransformSetName", lineMap.get("IPSEC_TRANSFORM_SET_2"));
                    addDeleteParams.put("ipsecProfileName", lineMap.get("IPSEC_PROFILE_2"));
                    addDeleteParams.put("tunnelId", lineMap.get("TUNNEL_INTERFACE_2"));
                    profileId = inferProfileId(lineMap.get("TUNNEL_INTERFACE_2"));
                    insideCidrIpRange = lineMap.get("VPN2_INSIDE_IP_CIDR_1");
                    break;
                default:
                    throw new RuntimeException("Bad tunnel number: " + tunnelNumberAsString);
            }


            addDeleteParams.put("profileId", profileId);
            addDeleteParams.put("insideCidrIpRange", insideCidrIpRange);
            addDeleteParams.put("tunnelIpAddress", IPV4Address.adjustAddressByOffset(insideCidrIpRange, 2));
            addDeleteParams.put("neighborId", IPV4Address.adjustAddressByOffset(insideCidrIpRange, 1));

            switch (operationContext) {
                case QUERY_REQUEST:
                case GENERATE_SEND:
                case GENERATE_RESPONSE:
                    addDeleteParams.put("tunnelDescription", String.format("AWS Research VPC%s Tunnel%s (%s)", paddedVpcNumberAsString, tunnelNumberAsString, vpcId));
                    break;
                case DELETE_REQUEST:
                    addDeleteParams.put("tunnelDescription", String.format("AWS Research VPC%s Tunnel%s (AVAILABLE)", paddedVpcNumberAsString, tunnelNumberAsString, vpcId));
                    break;
            }

            if (verbose) {
                dumpAddDeleteParams(addDeleteParams);
            }

        } catch (Exception e ) {
            logger.error(getLogtag() + "Failed to build Add/Delete params from spreadsheet", e);
        }

        return addDeleteParams;
    }

    private String inferProfileId(String tunnelId) {
        try {
            if (!tunnelId.matches("[\\d]{5}")) {
                throw new Exception("Tunnel Name is not a number");
            }
            Integer profileIdAsInteger = Integer.valueOf(tunnelId.substring(2));
            String profileId =  profileIdAsInteger.toString();
            return profileId;
        } catch (Exception e) {
            String errMsg = "Bad tunnel name \"" + tunnelId + "\" : " + e.getMessage();
            logger.error(getLogtag() + errMsg);
            throw new RuntimeException(errMsg, e);
        }

    }
    private String buildLabUnencryptKey(Integer vpcNumber) {
        String unencryptKey;

        unencryptKey = String.format("test%03d", vpcNumber);

        return unencryptKey;
    }

    private long getExecutionTime() {
        Random r = new Random();
        return r.nextInt((999 - 1) + 1) + 1;
    }

    protected Map<String, Map<String, String>> getSpreadsheetMap() {
        final Map<String, Map<String, String>> spreadsheetMap = new HashMap<>();
        List<String> header = new ArrayList<>();

        try {
            List<String> spreadsheetLinesList = Arrays.asList(spreadsheetLines);
            spreadsheetLinesList.forEach(line -> {

                List<String> valuesList = new ArrayList<>(Arrays.asList(line.split(",")));
                if (valuesList.get(0).equals("VPC_NUM")) {
                    for (int n = 0; n < valuesList.size(); n++) {
                        header.add(valuesList.get(n).trim());
                    }
                } else {
                    Map<String, String> lineMap = new HashMap<>();
                    for (int n = 0; n < valuesList.size(); n++) {
                        lineMap.put(header.get(n), valuesList.get(n).trim());
                    }
                    spreadsheetMap.put(valuesList.get(0), lineMap);
                }
            });

        } catch (Exception e) {
            String errMsg = "Failed to read in spreadsheet";
            logger.error(getLogtag() + errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        return spreadsheetMap;
    }


    private class VPNs {
        private final Map<String, VpnConnection> vpnConfigurationByVpnId = new ConcurrentHashMap<>();

        /**
         * Note: params prevalidated - only one must be set.
         * @param vpnId
         * @return
         */
        public VpnConnection get(String vpnId) throws ProviderException{
            VpnConnection vpnConnection;

            vpnConnection = vpnConfigurationByVpnId.get(vpnId);

            return vpnConnection;
        }

        /**
         * Note: params prevalidated - both must must be set.
         * @param vpnId
         * @return
         */
        public void remove(String vpnId) {

            vpnConfigurationByVpnId.remove(vpnId);
        }

        /**
         * Note: params prevalidated - only one must be set.
         * @param vpnId
         * @return
         */
        public boolean containsKey(String vpnId) {
            boolean containsKey;

            containsKey = vpnConfigurationByVpnId.containsKey(vpnId);

            return containsKey;
        }

        /**
         * @param vpnId
         * @param vpnConnection
         */
        public void put(String vpnId, VpnConnection vpnConnection) {
            vpnConfigurationByVpnId.put(vpnId, vpnConnection);
        }

        public void validateVpnConnectionParamsForQuery(String vpcId, String vpnProfileId) throws ProviderException {
            if (StringUtils.isEmpty(vpnProfileId)) {
                throw new ProviderException("Illegal arguments - required field vpnId (profile id) is not present");
            }

            logger.info("vpcId param value" + (StringUtils.isEmpty(vpcId) ? " is empty" : ": " + vpcId) + " - vpnProfileId param value: " + vpnProfileId);
        }

        public void validateVpnConnectionForAdd(VpnConnectionRequisition vpnConnectionRequisition) throws ProviderException {
            if (vpnConnectionRequisition != null) {
                VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();
                List<TunnelProfile> tunnelProfiles = vpnConnectionProfile.getTunnelProfile();
                if (tunnelProfiles.size() == 1) {
                    TunnelProfile tunnelProfile = tunnelProfiles.get(0);
                    Pattern pattern = Pattern.compile("(AWS Research VPC[\\d]{3} Tunnel[\\d]) \\(" + vpnConnectionRequisition.getOwnerId() + "\\)");
                    Matcher matcher = pattern.matcher(tunnelProfile.getTunnelDescription());
                    if (!matcher.matches()) {
                        String errMsg = "tunnel interface description incorrect for add: " + tunnelProfile.getTunnelDescription();
                        logger.error(getLogtag() + errMsg);
                        throw new ProviderException(errMsg);
                    }
                } else {
                    String errMsg = "tunnel profiles list does not contain exactly one tunnel profile - size: " + tunnelProfiles.size();
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);
                }
            } else {
                String errMsg = "";
                logger.error(getLogtag() + errMsg);
                throw new ProviderException(errMsg);
            }

        }

        public void validateVpnConnectionForDelete(VpnConnection vpnConnection) throws ProviderException {

            if (vpnConnection != null) {
                List<TunnelInterface> tunnelInterfaces = vpnConnection.getTunnelInterface();
                if (tunnelInterfaces.size() == 1) {
                    TunnelInterface tunnelInterface = tunnelInterfaces.get(0);
                    if (!tunnelInterface.getDescription().contains("(AVAILABLE)")) {
                        String errMsg = "tunnel interface description incorrect for delete: " + tunnelInterface.getDescription();
                        logger.error(getLogtag() + errMsg);
                        throw new ProviderException(errMsg);
                    }
                    String vpcId = vpnConnection.getVpcId();
                    String vpnId = vpnConnection.getVpnId();
                    if (!StringUtils.isNotEmpty(vpcId) || !StringUtils.isNotEmpty(vpnId)) {
                        String errMsg = "Both vpcId and vpnId must be set for delete";
                        logger.error(getLogtag() + errMsg);
                        throw new ProviderException(errMsg);
                    }
                } else {
                    String errMsg = "tunnel interfaces list does not contain exactly one tunnel interface - size: " + tunnelInterfaces.size();
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);
                }
            } else {
                String errMsg = "no tunnel interfaces list!";
                logger.error(getLogtag() + errMsg);
                throw new ProviderException(errMsg);
            }
        }

    }

    private void dumpAddDeleteParams(Map<String, String> addDeleteParams) {
        logger.info(getLogtag() + "=================== Add/Delete Params  ====================");
        logger.info(getLogtag() + "vpcId: " + addDeleteParams.get("vpcId"));
        logger.info(getLogtag() + "vpcNumberAsString: " + addDeleteParams.get("vpcNumberAsString"));
        logger.info(getLogtag() + "paddedVpcNumberAsString: " + addDeleteParams.get("paddedVpcNumberAsString"));
        logger.info(getLogtag() + "vpcNetworkIpRange: " + addDeleteParams.get("vpcNetworkIpRange"));
        logger.info(getLogtag() + "customerGatewayIp: " + addDeleteParams.get("customerGatewayIp"));
        logger.info(getLogtag() + "presharedKey: " + addDeleteParams.get("presharedKey"));
        logger.info(getLogtag() + "insideCidrIpRange: " + addDeleteParams.get("insideCidrIpRange"));
        logger.info(getLogtag() + "tunnelId: " + addDeleteParams.get("tunnelId"));
        logger.info(getLogtag() + "remoteVpnIpAddress: " + addDeleteParams.get("remoteVpnIpAddress"));
        logger.info(getLogtag() + "cryptoKeyringName: " + addDeleteParams.get("cryptoKeyringName"));
        logger.info(getLogtag() + "isakmpProfileName: " + addDeleteParams.get("isakmpProfileName"));
        logger.info(getLogtag() + "ipsecTransformSetName: " + addDeleteParams.get("ipsecTransformSetName"));
        logger.info(getLogtag() + "ipsecProfileName: " + addDeleteParams.get("ipsecProfileName"));
        logger.info(getLogtag() + "tunnelDescription: " + addDeleteParams.get("tunnelDescription"));
        logger.info(getLogtag() + "remoteVpnIpAddress: " + addDeleteParams.get("remoteVpnIpAddress"));

    }

    final private String[] spreadsheetLines = {
            "VPC_NUM,    VPC_NET,        TUNNEL_INTERFACE_1, CRYPTO_KEYRING_1,                   ISAKMP_PROFILE_1,                   IPSEC_TRANSFORM_SET_1,                  IPSEC_PROFILE_1,                CGW1_IP,        VPN1_INSIDE_IP_CIDR_1,  VPN1_INSIDE_IP_CIDR_2,  TUNNEL_INTERFACE_2, CRYPTO_KEYRING_2,                   ISAKMP_PROFILE_2,IPSEC_TRANSFORM_SET_2,IPSEC_PROFILE_2,CGW2_IP,VPN2_INSIDE_IP_CIDR_1,VPN2_INSIDE_IP_CIDR_2,ENVIRONMENT",
            "001,        10.65.0.0/23,   10001,              keyring-vpn-research-vpc001-tun1,   isakmp-vpn-research-vpc001-tun1,    ipsec-prop-vpn-research-vpc001-tun1,    ipsec-vpn-research-vpc001-tun1, 170.140.76.1,   169.254.248.0/30,       169.254.251.248/30,     20001,              keyring-vpn-research-vpc001-tun2,   isakmp-vpn-research-vpc001-tun2,ipsec-prop-vpn-research-vpc001-tun2,ipsec-vpn-research-vpc001-tun2,170.140.77.1,169.254.252.0/30,169.254.255.248/30,DEV",
            "002,10.65.2.0/23,10002,keyring-vpn-research-vpc002-tun1,isakmp-vpn-research-vpc002-tun1,ipsec-prop-vpn-research-vpc002-tun1,ipsec-vpn-research-vpc002-tun1,170.140.76.2,169.254.248.4/30,169.254.251.248/30,20002,keyring-vpn-research-vpc002-tun2,isakmp-vpn-research-vpc002-tun2,ipsec-prop-vpn-research-vpc002-tun2,ipsec-vpn-research-vpc002-tun2,170.140.77.2,169.254.252.4/30,169.254.255.248/30,DEV",
            "003,10.65.4.0/23,10003,keyring-vpn-research-vpc003-tun1,isakmp-vpn-research-vpc003-tun1,ipsec-prop-vpn-research-vpc003-tun1,ipsec-vpn-research-vpc003-tun1,170.140.76.3,169.254.248.8/30,169.254.251.248/30,20003,keyring-vpn-research-vpc003-tun2,isakmp-vpn-research-vpc003-tun2,ipsec-prop-vpn-research-vpc003-tun2,ipsec-vpn-research-vpc003-tun2,170.140.77.3,169.254.252.8/30,169.254.255.248/30,DEV",
            "004,10.65.6.0/23,10004,keyring-vpn-research-vpc004-tun1,isakmp-vpn-research-vpc004-tun1,ipsec-prop-vpn-research-vpc004-tun1,ipsec-vpn-research-vpc004-tun1,170.140.76.4,169.254.248.12/30,169.254.251.248/30,20004,keyring-vpn-research-vpc004-tun2,isakmp-vpn-research-vpc004-tun2,ipsec-prop-vpn-research-vpc004-tun2,ipsec-vpn-research-vpc004-tun2,170.140.77.4,169.254.252.12/30,169.254.255.248/30,DEV",
            "005,10.65.8.0/23,10005,keyring-vpn-research-vpc005-tun1,isakmp-vpn-research-vpc005-tun1,ipsec-prop-vpn-research-vpc005-tun1,ipsec-vpn-research-vpc005-tun1,170.140.76.5,169.254.248.16/30,169.254.251.248/30,20005,keyring-vpn-research-vpc005-tun2,isakmp-vpn-research-vpc005-tun2,ipsec-prop-vpn-research-vpc005-tun2,ipsec-vpn-research-vpc005-tun2,170.140.77.5,169.254.252.16/30,169.254.255.248/30,DEV",
            "006,10.65.10.0/23,10006,keyring-vpn-research-vpc006-tun1,isakmp-vpn-research-vpc006-tun1,ipsec-prop-vpn-research-vpc006-tun1,ipsec-vpn-research-vpc006-tun1,170.140.76.6,169.254.248.20/30,169.254.251.248/30,20006,keyring-vpn-research-vpc006-tun2,isakmp-vpn-research-vpc006-tun2,ipsec-prop-vpn-research-vpc006-tun2,ipsec-vpn-research-vpc006-tun2,170.140.77.6,169.254.252.20/30,169.254.255.248/30,DEV",
            "007,10.65.12.0/23,10007,keyring-vpn-research-vpc007-tun1,isakmp-vpn-research-vpc007-tun1,ipsec-prop-vpn-research-vpc007-tun1,ipsec-vpn-research-vpc007-tun1,170.140.76.7,169.254.248.24/30,169.254.251.248/30,20007,keyring-vpn-research-vpc007-tun2,isakmp-vpn-research-vpc007-tun2,ipsec-prop-vpn-research-vpc007-tun2,ipsec-vpn-research-vpc007-tun2,170.140.77.7,169.254.252.24/30,169.254.255.248/30,DEV",
            "008,10.65.14.0/23,10008,keyring-vpn-research-vpc008-tun1,isakmp-vpn-research-vpc008-tun1,ipsec-prop-vpn-research-vpc008-tun1,ipsec-vpn-research-vpc008-tun1,170.140.76.8,169.254.248.28/30,169.254.251.248/30,20008,keyring-vpn-research-vpc008-tun2,isakmp-vpn-research-vpc008-tun2,ipsec-prop-vpn-research-vpc008-tun2,ipsec-vpn-research-vpc008-tun2,170.140.77.8,169.254.252.28/30,169.254.255.248/30,DEV",
            "009,10.65.16.0/23,10009,keyring-vpn-research-vpc009-tun1,isakmp-vpn-research-vpc009-tun1,ipsec-prop-vpn-research-vpc009-tun1,ipsec-vpn-research-vpc009-tun1,170.140.76.9,169.254.248.32/30,169.254.251.248/30,20009,keyring-vpn-research-vpc009-tun2,isakmp-vpn-research-vpc009-tun2,ipsec-prop-vpn-research-vpc009-tun2,ipsec-vpn-research-vpc009-tun2,170.140.77.9,169.254.252.32/30,169.254.255.248/30,DEV",
            "010,10.65.18.0/23,10010,keyring-vpn-research-vpc010-tun1,isakmp-vpn-research-vpc010-tun1,ipsec-prop-vpn-research-vpc010-tun1,ipsec-vpn-research-vpc010-tun1,170.140.76.10,169.254.248.36/30,169.254.251.248/30,20010,keyring-vpn-research-vpc010-tun2,isakmp-vpn-research-vpc010-tun2,ipsec-prop-vpn-research-vpc010-tun2,ipsec-vpn-research-vpc010-tun2,170.140.77.10,169.254.252.36/30,169.254.255.248/30,DEV",
            "011,10.65.20.0/23,10011,keyring-vpn-research-vpc011-tun1,isakmp-vpn-research-vpc011-tun1,ipsec-prop-vpn-research-vpc011-tun1,ipsec-vpn-research-vpc011-tun1,170.140.76.11,169.254.248.40/30,169.254.251.248/30,20011,keyring-vpn-research-vpc011-tun2,isakmp-vpn-research-vpc011-tun2,ipsec-prop-vpn-research-vpc011-tun2,ipsec-vpn-research-vpc011-tun2,170.140.77.11,169.254.252.40/30,169.254.255.248/30,DEV",
            "012,10.65.22.0/23,10012,keyring-vpn-research-vpc012-tun1,isakmp-vpn-research-vpc012-tun1,ipsec-prop-vpn-research-vpc012-tun1,ipsec-vpn-research-vpc012-tun1,170.140.76.12,169.254.248.44/30,169.254.251.248/30,20012,keyring-vpn-research-vpc012-tun2,isakmp-vpn-research-vpc012-tun2,ipsec-prop-vpn-research-vpc012-tun2,ipsec-vpn-research-vpc012-tun2,170.140.77.12,169.254.252.44/30,169.254.255.248/30,DEV",
            "013,10.65.24.0/23,10013,keyring-vpn-research-vpc013-tun1,isakmp-vpn-research-vpc013-tun1,ipsec-prop-vpn-research-vpc013-tun1,ipsec-vpn-research-vpc013-tun1,170.140.76.13,169.254.248.48/30,169.254.251.248/30,20013,keyring-vpn-research-vpc013-tun2,isakmp-vpn-research-vpc013-tun2,ipsec-prop-vpn-research-vpc013-tun2,ipsec-vpn-research-vpc013-tun2,170.140.77.13,169.254.252.48/30,169.254.255.248/30,DEV",
            "014,10.65.26.0/23,10014,keyring-vpn-research-vpc014-tun1,isakmp-vpn-research-vpc014-tun1,ipsec-prop-vpn-research-vpc014-tun1,ipsec-vpn-research-vpc014-tun1,170.140.76.14,169.254.248.52/30,169.254.251.248/30,20014,keyring-vpn-research-vpc014-tun2,isakmp-vpn-research-vpc014-tun2,ipsec-prop-vpn-research-vpc014-tun2,ipsec-vpn-research-vpc014-tun2,170.140.77.14,169.254.252.52/30,169.254.255.248/30,DEV",
            "015,10.65.28.0/23,10015,keyring-vpn-research-vpc015-tun1,isakmp-vpn-research-vpc015-tun1,ipsec-prop-vpn-research-vpc015-tun1,ipsec-vpn-research-vpc015-tun1,170.140.76.15,169.254.248.56/30,169.254.251.248/30,20015,keyring-vpn-research-vpc015-tun2,isakmp-vpn-research-vpc015-tun2,ipsec-prop-vpn-research-vpc015-tun2,ipsec-vpn-research-vpc015-tun2,170.140.77.15,169.254.252.56/30,169.254.255.248/30,DEV",
            "016,10.65.30.0/23,10016,keyring-vpn-research-vpc016-tun1,isakmp-vpn-research-vpc016-tun1,ipsec-prop-vpn-research-vpc016-tun1,ipsec-vpn-research-vpc016-tun1,170.140.76.16,169.254.248.60/30,169.254.251.248/30,20016,keyring-vpn-research-vpc016-tun2,isakmp-vpn-research-vpc016-tun2,ipsec-prop-vpn-research-vpc016-tun2,ipsec-vpn-research-vpc016-tun2,170.140.77.16,169.254.252.60/30,169.254.255.248/30,DEV",
            "017,10.65.32.0/23,10017,keyring-vpn-research-vpc017-tun1,isakmp-vpn-research-vpc017-tun1,ipsec-prop-vpn-research-vpc017-tun1,ipsec-vpn-research-vpc017-tun1,170.140.76.17,169.254.248.64/30,169.254.251.248/30,20017,keyring-vpn-research-vpc017-tun2,isakmp-vpn-research-vpc017-tun2,ipsec-prop-vpn-research-vpc017-tun2,ipsec-vpn-research-vpc017-tun2,170.140.77.17,169.254.252.64/30,169.254.255.248/30,DEV",
            "018,10.65.34.0/23,10018,keyring-vpn-research-vpc018-tun1,isakmp-vpn-research-vpc018-tun1,ipsec-prop-vpn-research-vpc018-tun1,ipsec-vpn-research-vpc018-tun1,170.140.76.18,169.254.248.68/30,169.254.251.248/30,20018,keyring-vpn-research-vpc018-tun2,isakmp-vpn-research-vpc018-tun2,ipsec-prop-vpn-research-vpc018-tun2,ipsec-vpn-research-vpc018-tun2,170.140.77.18,169.254.252.68/30,169.254.255.248/30,DEV",
            "019,10.65.36.0/23,10019,keyring-vpn-research-vpc019-tun1,isakmp-vpn-research-vpc019-tun1,ipsec-prop-vpn-research-vpc019-tun1,ipsec-vpn-research-vpc019-tun1,170.140.76.19,169.254.248.72/30,169.254.251.248/30,20019,keyring-vpn-research-vpc019-tun2,isakmp-vpn-research-vpc019-tun2,ipsec-prop-vpn-research-vpc019-tun2,ipsec-vpn-research-vpc019-tun2,170.140.77.19,169.254.252.72/30,169.254.255.248/30,DEV",
            "020,10.65.38.0/23,10020,keyring-vpn-research-vpc020-tun1,isakmp-vpn-research-vpc020-tun1,ipsec-prop-vpn-research-vpc020-tun1,ipsec-vpn-research-vpc020-tun1,170.140.76.20,169.254.248.76/30,169.254.251.248/30,20020,keyring-vpn-research-vpc020-tun2,isakmp-vpn-research-vpc020-tun2,ipsec-prop-vpn-research-vpc020-tun2,ipsec-vpn-research-vpc020-tun2,170.140.77.20,169.254.252.76/30,169.254.255.248/30,DEV",
            "021,10.65.40.0/23,10021,keyring-vpn-research-vpc021-tun1,isakmp-vpn-research-vpc021-tun1,ipsec-prop-vpn-research-vpc021-tun1,ipsec-vpn-research-vpc021-tun1,170.140.76.21,169.254.248.80/30,169.254.251.248/30,20021,keyring-vpn-research-vpc021-tun2,isakmp-vpn-research-vpc021-tun2,ipsec-prop-vpn-research-vpc021-tun2,ipsec-vpn-research-vpc021-tun2,170.140.77.21,169.254.252.80/30,169.254.255.248/30,QA",
            "022,10.65.42.0/23,10022,keyring-vpn-research-vpc022-tun1,isakmp-vpn-research-vpc022-tun1,ipsec-prop-vpn-research-vpc022-tun1,ipsec-vpn-research-vpc022-tun1,170.140.76.22,169.254.248.84/30,169.254.251.248/30,20022,keyring-vpn-research-vpc022-tun2,isakmp-vpn-research-vpc022-tun2,ipsec-prop-vpn-research-vpc022-tun2,ipsec-vpn-research-vpc022-tun2,170.140.77.22,169.254.252.84/30,169.254.255.248/30,QA",
            "023,10.65.44.0/23,10023,keyring-vpn-research-vpc023-tun1,isakmp-vpn-research-vpc023-tun1,ipsec-prop-vpn-research-vpc023-tun1,ipsec-vpn-research-vpc023-tun1,170.140.76.23,169.254.248.88/30,169.254.251.248/30,20023,keyring-vpn-research-vpc023-tun2,isakmp-vpn-research-vpc023-tun2,ipsec-prop-vpn-research-vpc023-tun2,ipsec-vpn-research-vpc023-tun2,170.140.77.23,169.254.252.88/30,169.254.255.248/30,QA",
            "024,10.65.46.0/23,10024,keyring-vpn-research-vpc024-tun1,isakmp-vpn-research-vpc024-tun1,ipsec-prop-vpn-research-vpc024-tun1,ipsec-vpn-research-vpc024-tun1,170.140.76.24,169.254.248.92/30,169.254.251.248/30,20024,keyring-vpn-research-vpc024-tun2,isakmp-vpn-research-vpc024-tun2,ipsec-prop-vpn-research-vpc024-tun2,ipsec-vpn-research-vpc024-tun2,170.140.77.24,169.254.252.92/30,169.254.255.248/30,QA",
            "025,10.65.48.0/23,10025,keyring-vpn-research-vpc025-tun1,isakmp-vpn-research-vpc025-tun1,ipsec-prop-vpn-research-vpc025-tun1,ipsec-vpn-research-vpc025-tun1,170.140.76.25,169.254.248.96/30,169.254.251.248/30,20025,keyring-vpn-research-vpc025-tun2,isakmp-vpn-research-vpc025-tun2,ipsec-prop-vpn-research-vpc025-tun2,ipsec-vpn-research-vpc025-tun2,170.140.77.25,169.254.252.96/30,169.254.255.248/30,QA",
            "026,10.65.50.0/23,10026,keyring-vpn-research-vpc026-tun1,isakmp-vpn-research-vpc026-tun1,ipsec-prop-vpn-research-vpc026-tun1,ipsec-vpn-research-vpc026-tun1,170.140.76.26,169.254.248.100/30,169.254.251.248/30,20026,keyring-vpn-research-vpc026-tun2,isakmp-vpn-research-vpc026-tun2,ipsec-prop-vpn-research-vpc026-tun2,ipsec-vpn-research-vpc026-tun2,170.140.77.26,169.254.252.100/30,169.254.255.248/30,QA",
            "027,10.65.52.0/23,10027,keyring-vpn-research-vpc027-tun1,isakmp-vpn-research-vpc027-tun1,ipsec-prop-vpn-research-vpc027-tun1,ipsec-vpn-research-vpc027-tun1,170.140.76.27,169.254.248.104/30,169.254.251.248/30,20027,keyring-vpn-research-vpc027-tun2,isakmp-vpn-research-vpc027-tun2,ipsec-prop-vpn-research-vpc027-tun2,ipsec-vpn-research-vpc027-tun2,170.140.77.27,169.254.252.104/30,169.254.255.248/30,QA",
            "028,10.65.54.0/23,10028,keyring-vpn-research-vpc028-tun1,isakmp-vpn-research-vpc028-tun1,ipsec-prop-vpn-research-vpc028-tun1,ipsec-vpn-research-vpc028-tun1,170.140.76.28,169.254.248.108/30,169.254.251.248/30,20028,keyring-vpn-research-vpc028-tun2,isakmp-vpn-research-vpc028-tun2,ipsec-prop-vpn-research-vpc028-tun2,ipsec-vpn-research-vpc028-tun2,170.140.77.28,169.254.252.108/30,169.254.255.248/30,QA",
            "029,10.65.56.0/23,10029,keyring-vpn-research-vpc029-tun1,isakmp-vpn-research-vpc029-tun1,ipsec-prop-vpn-research-vpc029-tun1,ipsec-vpn-research-vpc029-tun1,170.140.76.29,169.254.248.112/30,169.254.251.248/30,20029,keyring-vpn-research-vpc029-tun2,isakmp-vpn-research-vpc029-tun2,ipsec-prop-vpn-research-vpc029-tun2,ipsec-vpn-research-vpc029-tun2,170.140.77.29,169.254.252.112/30,169.254.255.248/30,QA",
            "030,10.65.58.0/23,10030,keyring-vpn-research-vpc030-tun1,isakmp-vpn-research-vpc030-tun1,ipsec-prop-vpn-research-vpc030-tun1,ipsec-vpn-research-vpc030-tun1,170.140.76.30,169.254.248.116/30,169.254.251.248/30,20030,keyring-vpn-research-vpc030-tun2,isakmp-vpn-research-vpc030-tun2,ipsec-prop-vpn-research-vpc030-tun2,ipsec-vpn-research-vpc030-tun2,170.140.77.30,169.254.252.116/30,169.254.255.248/30,QA",
            "031,10.65.60.0/23,10031,keyring-vpn-research-vpc031-tun1,isakmp-vpn-research-vpc031-tun1,ipsec-prop-vpn-research-vpc031-tun1,ipsec-vpn-research-vpc031-tun1,170.140.76.31,169.254.248.120/30,169.254.251.248/30,20031,keyring-vpn-research-vpc031-tun2,isakmp-vpn-research-vpc031-tun2,ipsec-prop-vpn-research-vpc031-tun2,ipsec-vpn-research-vpc031-tun2,170.140.77.31,169.254.252.120/30,169.254.255.248/30,QA",
            "032,10.65.62.0/23,10032,keyring-vpn-research-vpc032-tun1,isakmp-vpn-research-vpc032-tun1,ipsec-prop-vpn-research-vpc032-tun1,ipsec-vpn-research-vpc032-tun1,170.140.76.32,169.254.248.124/30,169.254.251.248/30,20032,keyring-vpn-research-vpc032-tun2,isakmp-vpn-research-vpc032-tun2,ipsec-prop-vpn-research-vpc032-tun2,ipsec-vpn-research-vpc032-tun2,170.140.77.32,169.254.252.124/30,169.254.255.248/30,QA",
            "033,10.65.64.0/23,10033,keyring-vpn-research-vpc033-tun1,isakmp-vpn-research-vpc033-tun1,ipsec-prop-vpn-research-vpc033-tun1,ipsec-vpn-research-vpc033-tun1,170.140.76.33,169.254.248.128/30,169.254.251.248/30,20033,keyring-vpn-research-vpc033-tun2,isakmp-vpn-research-vpc033-tun2,ipsec-prop-vpn-research-vpc033-tun2,ipsec-vpn-research-vpc033-tun2,170.140.77.33,169.254.252.128/30,169.254.255.248/30,QA",
            "034,10.65.66.0/23,10034,keyring-vpn-research-vpc034-tun1,isakmp-vpn-research-vpc034-tun1,ipsec-prop-vpn-research-vpc034-tun1,ipsec-vpn-research-vpc034-tun1,170.140.76.34,169.254.248.132/30,169.254.251.248/30,20034,keyring-vpn-research-vpc034-tun2,isakmp-vpn-research-vpc034-tun2,ipsec-prop-vpn-research-vpc034-tun2,ipsec-vpn-research-vpc034-tun2,170.140.77.34,169.254.252.132/30,169.254.255.248/30,QA",
            "035,10.65.68.0/23,10035,keyring-vpn-research-vpc035-tun1,isakmp-vpn-research-vpc035-tun1,ipsec-prop-vpn-research-vpc035-tun1,ipsec-vpn-research-vpc035-tun1,170.140.76.35,169.254.248.136/30,169.254.251.248/30,20035,keyring-vpn-research-vpc035-tun2,isakmp-vpn-research-vpc035-tun2,ipsec-prop-vpn-research-vpc035-tun2,ipsec-vpn-research-vpc035-tun2,170.140.77.35,169.254.252.136/30,169.254.255.248/30,QA",
            "036,10.65.70.0/23,10036,keyring-vpn-research-vpc036-tun1,isakmp-vpn-research-vpc036-tun1,ipsec-prop-vpn-research-vpc036-tun1,ipsec-vpn-research-vpc036-tun1,170.140.76.36,169.254.248.140/30,169.254.251.248/30,20036,keyring-vpn-research-vpc036-tun2,isakmp-vpn-research-vpc036-tun2,ipsec-prop-vpn-research-vpc036-tun2,ipsec-vpn-research-vpc036-tun2,170.140.77.36,169.254.252.140/30,169.254.255.248/30,QA",
            "037,10.65.72.0/23,10037,keyring-vpn-research-vpc037-tun1,isakmp-vpn-research-vpc037-tun1,ipsec-prop-vpn-research-vpc037-tun1,ipsec-vpn-research-vpc037-tun1,170.140.76.37,169.254.248.144/30,169.254.251.248/30,20037,keyring-vpn-research-vpc037-tun2,isakmp-vpn-research-vpc037-tun2,ipsec-prop-vpn-research-vpc037-tun2,ipsec-vpn-research-vpc037-tun2,170.140.77.37,169.254.252.144/30,169.254.255.248/30,QA",
            "038,10.65.74.0/23,10038,keyring-vpn-research-vpc038-tun1,isakmp-vpn-research-vpc038-tun1,ipsec-prop-vpn-research-vpc038-tun1,ipsec-vpn-research-vpc038-tun1,170.140.76.38,169.254.248.148/30,169.254.251.248/30,20038,keyring-vpn-research-vpc038-tun2,isakmp-vpn-research-vpc038-tun2,ipsec-prop-vpn-research-vpc038-tun2,ipsec-vpn-research-vpc038-tun2,170.140.77.38,169.254.252.148/30,169.254.255.248/30,QA",
            "039,10.65.76.0/23,10039,keyring-vpn-research-vpc039-tun1,isakmp-vpn-research-vpc039-tun1,ipsec-prop-vpn-research-vpc039-tun1,ipsec-vpn-research-vpc039-tun1,170.140.76.39,169.254.248.152/30,169.254.251.248/30,20039,keyring-vpn-research-vpc039-tun2,isakmp-vpn-research-vpc039-tun2,ipsec-prop-vpn-research-vpc039-tun2,ipsec-vpn-research-vpc039-tun2,170.140.77.39,169.254.252.152/30,169.254.255.248/30,QA",
            "040,10.65.78.0/23,10040,keyring-vpn-research-vpc040-tun1,isakmp-vpn-research-vpc040-tun1,ipsec-prop-vpn-research-vpc040-tun1,ipsec-vpn-research-vpc040-tun1,170.140.76.40,169.254.248.156/30,169.254.251.248/30,20040,keyring-vpn-research-vpc040-tun2,isakmp-vpn-research-vpc040-tun2,ipsec-prop-vpn-research-vpc040-tun2,ipsec-vpn-research-vpc040-tun2,170.140.77.40,169.254.252.156/30,169.254.255.248/30,QA",
            "041,10.65.80.0/23,10041,keyring-vpn-research-vpc041-tun1,isakmp-vpn-research-vpc041-tun1,ipsec-prop-vpn-research-vpc041-tun1,ipsec-vpn-research-vpc041-tun1,170.140.76.41,169.254.248.160/30,169.254.251.248/30,20041,keyring-vpn-research-vpc041-tun2,isakmp-vpn-research-vpc041-tun2,ipsec-prop-vpn-research-vpc041-tun2,ipsec-vpn-research-vpc041-tun2,170.140.77.41,169.254.252.160/30,169.254.255.248/30,QA",
            "042,10.65.82.0/23,10042,keyring-vpn-research-vpc042-tun1,isakmp-vpn-research-vpc042-tun1,ipsec-prop-vpn-research-vpc042-tun1,ipsec-vpn-research-vpc042-tun1,170.140.76.42,169.254.248.164/30,169.254.251.248/30,20042,keyring-vpn-research-vpc042-tun2,isakmp-vpn-research-vpc042-tun2,ipsec-prop-vpn-research-vpc042-tun2,ipsec-vpn-research-vpc042-tun2,170.140.77.42,169.254.252.164/30,169.254.255.248/30,QA",
            "043,10.65.84.0/23,10043,keyring-vpn-research-vpc043-tun1,isakmp-vpn-research-vpc043-tun1,ipsec-prop-vpn-research-vpc043-tun1,ipsec-vpn-research-vpc043-tun1,170.140.76.43,169.254.248.168/30,169.254.251.248/30,20043,keyring-vpn-research-vpc043-tun2,isakmp-vpn-research-vpc043-tun2,ipsec-prop-vpn-research-vpc043-tun2,ipsec-vpn-research-vpc043-tun2,170.140.77.43,169.254.252.168/30,169.254.255.248/30,QA",
            "044,10.65.86.0/23,10044,keyring-vpn-research-vpc044-tun1,isakmp-vpn-research-vpc044-tun1,ipsec-prop-vpn-research-vpc044-tun1,ipsec-vpn-research-vpc044-tun1,170.140.76.44,169.254.248.172/30,169.254.251.248/30,20044,keyring-vpn-research-vpc044-tun2,isakmp-vpn-research-vpc044-tun2,ipsec-prop-vpn-research-vpc044-tun2,ipsec-vpn-research-vpc044-tun2,170.140.77.44,169.254.252.172/30,169.254.255.248/30,QA",
            "045,10.65.88.0/23,10045,keyring-vpn-research-vpc045-tun1,isakmp-vpn-research-vpc045-tun1,ipsec-prop-vpn-research-vpc045-tun1,ipsec-vpn-research-vpc045-tun1,170.140.76.45,169.254.248.176/30,169.254.251.248/30,20045,keyring-vpn-research-vpc045-tun2,isakmp-vpn-research-vpc045-tun2,ipsec-prop-vpn-research-vpc045-tun2,ipsec-vpn-research-vpc045-tun2,170.140.77.45,169.254.252.176/30,169.254.255.248/30,QA",
            "046,10.65.90.0/23,10046,keyring-vpn-research-vpc046-tun1,isakmp-vpn-research-vpc046-tun1,ipsec-prop-vpn-research-vpc046-tun1,ipsec-vpn-research-vpc046-tun1,170.140.76.46,169.254.248.180/30,169.254.251.248/30,20046,keyring-vpn-research-vpc046-tun2,isakmp-vpn-research-vpc046-tun2,ipsec-prop-vpn-research-vpc046-tun2,ipsec-vpn-research-vpc046-tun2,170.140.77.46,169.254.252.180/30,169.254.255.248/30,QA",
            "047,10.65.92.0/23,10047,keyring-vpn-research-vpc047-tun1,isakmp-vpn-research-vpc047-tun1,ipsec-prop-vpn-research-vpc047-tun1,ipsec-vpn-research-vpc047-tun1,170.140.76.47,169.254.248.184/30,169.254.251.248/30,20047,keyring-vpn-research-vpc047-tun2,isakmp-vpn-research-vpc047-tun2,ipsec-prop-vpn-research-vpc047-tun2,ipsec-vpn-research-vpc047-tun2,170.140.77.47,169.254.252.184/30,169.254.255.248/30,QA",
            "048,10.65.94.0/23,10048,keyring-vpn-research-vpc048-tun1,isakmp-vpn-research-vpc048-tun1,ipsec-prop-vpn-research-vpc048-tun1,ipsec-vpn-research-vpc048-tun1,170.140.76.48,169.254.248.188/30,169.254.251.248/30,20048,keyring-vpn-research-vpc048-tun2,isakmp-vpn-research-vpc048-tun2,ipsec-prop-vpn-research-vpc048-tun2,ipsec-vpn-research-vpc048-tun2,170.140.77.48,169.254.252.188/30,169.254.255.248/30,QA",
            "049,10.65.96.0/23,10049,keyring-vpn-research-vpc049-tun1,isakmp-vpn-research-vpc049-tun1,ipsec-prop-vpn-research-vpc049-tun1,ipsec-vpn-research-vpc049-tun1,170.140.76.49,169.254.248.192/30,169.254.251.248/30,20049,keyring-vpn-research-vpc049-tun2,isakmp-vpn-research-vpc049-tun2,ipsec-prop-vpn-research-vpc049-tun2,ipsec-vpn-research-vpc049-tun2,170.140.77.49,169.254.252.192/30,169.254.255.248/30,QA",
            "050,10.65.98.0/23,10050,keyring-vpn-research-vpc050-tun1,isakmp-vpn-research-vpc050-tun1,ipsec-prop-vpn-research-vpc050-tun1,ipsec-vpn-research-vpc050-tun1,170.140.76.50,169.254.248.196/30,169.254.251.248/30,20050,keyring-vpn-research-vpc050-tun2,isakmp-vpn-research-vpc050-tun2,ipsec-prop-vpn-research-vpc050-tun2,ipsec-vpn-research-vpc050-tun2,170.140.77.50,169.254.252.196/30,169.254.255.248/30,QA",
            "051,10.65.100.0/23,10051,keyring-vpn-research-vpc051-tun1,isakmp-vpn-research-vpc051-tun1,ipsec-prop-vpn-research-vpc051-tun1,ipsec-vpn-research-vpc051-tun1,170.140.76.51,169.254.248.200/30,169.254.251.248/30,20051,keyring-vpn-research-vpc051-tun2,isakmp-vpn-research-vpc051-tun2,ipsec-prop-vpn-research-vpc051-tun2,ipsec-vpn-research-vpc051-tun2,170.140.77.51,169.254.252.200/30,169.254.255.248/30,QA",
            "052,10.65.102.0/23,10052,keyring-vpn-research-vpc052-tun1,isakmp-vpn-research-vpc052-tun1,ipsec-prop-vpn-research-vpc052-tun1,ipsec-vpn-research-vpc052-tun1,170.140.76.52,169.254.248.204/30,169.254.251.248/30,20052,keyring-vpn-research-vpc052-tun2,isakmp-vpn-research-vpc052-tun2,ipsec-prop-vpn-research-vpc052-tun2,ipsec-vpn-research-vpc052-tun2,170.140.77.52,169.254.252.204/30,169.254.255.248/30,QA",
            "053,10.65.104.0/23,10053,keyring-vpn-research-vpc053-tun1,isakmp-vpn-research-vpc053-tun1,ipsec-prop-vpn-research-vpc053-tun1,ipsec-vpn-research-vpc053-tun1,170.140.76.53,169.254.248.208/30,169.254.251.248/30,20053,keyring-vpn-research-vpc053-tun2,isakmp-vpn-research-vpc053-tun2,ipsec-prop-vpn-research-vpc053-tun2,ipsec-vpn-research-vpc053-tun2,170.140.77.53,169.254.252.208/30,169.254.255.248/30,QA",
            "054,10.65.106.0/23,10054,keyring-vpn-research-vpc054-tun1,isakmp-vpn-research-vpc054-tun1,ipsec-prop-vpn-research-vpc054-tun1,ipsec-vpn-research-vpc054-tun1,170.140.76.54,169.254.248.212/30,169.254.251.248/30,20054,keyring-vpn-research-vpc054-tun2,isakmp-vpn-research-vpc054-tun2,ipsec-prop-vpn-research-vpc054-tun2,ipsec-vpn-research-vpc054-tun2,170.140.77.54,169.254.252.212/30,169.254.255.248/30,QA",
            "055,10.65.108.0/23,10055,keyring-vpn-research-vpc055-tun1,isakmp-vpn-research-vpc055-tun1,ipsec-prop-vpn-research-vpc055-tun1,ipsec-vpn-research-vpc055-tun1,170.140.76.55,169.254.248.216/30,169.254.251.248/30,20055,keyring-vpn-research-vpc055-tun2,isakmp-vpn-research-vpc055-tun2,ipsec-prop-vpn-research-vpc055-tun2,ipsec-vpn-research-vpc055-tun2,170.140.77.55,169.254.252.216/30,169.254.255.248/30,QA",
            "056,10.65.110.0/23,10056,keyring-vpn-research-vpc056-tun1,isakmp-vpn-research-vpc056-tun1,ipsec-prop-vpn-research-vpc056-tun1,ipsec-vpn-research-vpc056-tun1,170.140.76.56,169.254.248.220/30,169.254.251.248/30,20056,keyring-vpn-research-vpc056-tun2,isakmp-vpn-research-vpc056-tun2,ipsec-prop-vpn-research-vpc056-tun2,ipsec-vpn-research-vpc056-tun2,170.140.77.56,169.254.252.220/30,169.254.255.248/30,QA",
            "057,10.65.112.0/23,10057,keyring-vpn-research-vpc057-tun1,isakmp-vpn-research-vpc057-tun1,ipsec-prop-vpn-research-vpc057-tun1,ipsec-vpn-research-vpc057-tun1,170.140.76.57,169.254.248.224/30,169.254.251.248/30,20057,keyring-vpn-research-vpc057-tun2,isakmp-vpn-research-vpc057-tun2,ipsec-prop-vpn-research-vpc057-tun2,ipsec-vpn-research-vpc057-tun2,170.140.77.57,169.254.252.224/30,169.254.255.248/30,QA",
            "058,10.65.114.0/23,10058,keyring-vpn-research-vpc058-tun1,isakmp-vpn-research-vpc058-tun1,ipsec-prop-vpn-research-vpc058-tun1,ipsec-vpn-research-vpc058-tun1,170.140.76.58,169.254.248.228/30,169.254.251.248/30,20058,keyring-vpn-research-vpc058-tun2,isakmp-vpn-research-vpc058-tun2,ipsec-prop-vpn-research-vpc058-tun2,ipsec-vpn-research-vpc058-tun2,170.140.77.58,169.254.252.228/30,169.254.255.248/30,QA",
            "059,10.65.116.0/23,10059,keyring-vpn-research-vpc059-tun1,isakmp-vpn-research-vpc059-tun1,ipsec-prop-vpn-research-vpc059-tun1,ipsec-vpn-research-vpc059-tun1,170.140.76.59,169.254.248.232/30,169.254.251.248/30,20059,keyring-vpn-research-vpc059-tun2,isakmp-vpn-research-vpc059-tun2,ipsec-prop-vpn-research-vpc059-tun2,ipsec-vpn-research-vpc059-tun2,170.140.77.59,169.254.252.232/30,169.254.255.248/30,QA",
            "060,10.65.118.0/23,10060,keyring-vpn-research-vpc060-tun1,isakmp-vpn-research-vpc060-tun1,ipsec-prop-vpn-research-vpc060-tun1,ipsec-vpn-research-vpc060-tun1,170.140.76.60,169.254.248.236/30,169.254.251.248/30,20060,keyring-vpn-research-vpc060-tun2,isakmp-vpn-research-vpc060-tun2,ipsec-prop-vpn-research-vpc060-tun2,ipsec-vpn-research-vpc060-tun2,170.140.77.60,169.254.252.236/30,169.254.255.248/30,QA",
            "061,10.65.120.0/23,10061,keyring-vpn-research-vpc061-tun1,isakmp-vpn-research-vpc061-tun1,ipsec-prop-vpn-research-vpc061-tun1,ipsec-vpn-research-vpc061-tun1,170.140.76.61,169.254.248.240/30,169.254.251.248/30,20061,keyring-vpn-research-vpc061-tun2,isakmp-vpn-research-vpc061-tun2,ipsec-prop-vpn-research-vpc061-tun2,ipsec-vpn-research-vpc061-tun2,170.140.77.61,169.254.252.240/30,169.254.255.248/30,PROD",
            "062,10.65.122.0/23,10062,keyring-vpn-research-vpc062-tun1,isakmp-vpn-research-vpc062-tun1,ipsec-prop-vpn-research-vpc062-tun1,ipsec-vpn-research-vpc062-tun1,170.140.76.62,169.254.248.244/30,169.254.251.248/30,20062,keyring-vpn-research-vpc062-tun2,isakmp-vpn-research-vpc062-tun2,ipsec-prop-vpn-research-vpc062-tun2,ipsec-vpn-research-vpc062-tun2,170.140.77.62,169.254.252.244/30,169.254.255.248/30,PROD",
            "063,10.65.124.0/23,10063,keyring-vpn-research-vpc063-tun1,isakmp-vpn-research-vpc063-tun1,ipsec-prop-vpn-research-vpc063-tun1,ipsec-vpn-research-vpc063-tun1,170.140.76.63,169.254.248.248/30,169.254.251.248/30,20063,keyring-vpn-research-vpc063-tun2,isakmp-vpn-research-vpc063-tun2,ipsec-prop-vpn-research-vpc063-tun2,ipsec-vpn-research-vpc063-tun2,170.140.77.63,169.254.252.248/30,169.254.255.248/30,PROD",
            "064,10.65.126.0/23,10064,keyring-vpn-research-vpc064-tun1,isakmp-vpn-research-vpc064-tun1,ipsec-prop-vpn-research-vpc064-tun1,ipsec-vpn-research-vpc064-tun1,170.140.76.64,169.254.248.252/30,169.254.251.248/30,20064,keyring-vpn-research-vpc064-tun2,isakmp-vpn-research-vpc064-tun2,ipsec-prop-vpn-research-vpc064-tun2,ipsec-vpn-research-vpc064-tun2,170.140.77.64,169.254.252.252/30,169.254.255.248/30,PROD",
            "065,10.65.128.0/23,10065,keyring-vpn-research-vpc065-tun1,isakmp-vpn-research-vpc065-tun1,ipsec-prop-vpn-research-vpc065-tun1,ipsec-vpn-research-vpc065-tun1,170.140.76.65,169.254.249.0/30,169.254.251.248/30,20065,keyring-vpn-research-vpc065-tun2,isakmp-vpn-research-vpc065-tun2,ipsec-prop-vpn-research-vpc065-tun2,ipsec-vpn-research-vpc065-tun2,170.140.77.65,169.254.253.0/30,169.254.255.248/30,PROD",
            "066,10.65.130.0/23,10066,keyring-vpn-research-vpc066-tun1,isakmp-vpn-research-vpc066-tun1,ipsec-prop-vpn-research-vpc066-tun1,ipsec-vpn-research-vpc066-tun1,170.140.76.66,169.254.249.4/30,169.254.251.248/30,20066,keyring-vpn-research-vpc066-tun2,isakmp-vpn-research-vpc066-tun2,ipsec-prop-vpn-research-vpc066-tun2,ipsec-vpn-research-vpc066-tun2,170.140.77.66,169.254.253.4/30,169.254.255.248/30,PROD",
            "067,10.65.132.0/23,10067,keyring-vpn-research-vpc067-tun1,isakmp-vpn-research-vpc067-tun1,ipsec-prop-vpn-research-vpc067-tun1,ipsec-vpn-research-vpc067-tun1,170.140.76.67,169.254.249.8/30,169.254.251.248/30,20067,keyring-vpn-research-vpc067-tun2,isakmp-vpn-research-vpc067-tun2,ipsec-prop-vpn-research-vpc067-tun2,ipsec-vpn-research-vpc067-tun2,170.140.77.67,169.254.253.8/30,169.254.255.248/30,PROD",
            "068,10.65.134.0/23,10068,keyring-vpn-research-vpc068-tun1,isakmp-vpn-research-vpc068-tun1,ipsec-prop-vpn-research-vpc068-tun1,ipsec-vpn-research-vpc068-tun1,170.140.76.68,169.254.249.12/30,169.254.251.248/30,20068,keyring-vpn-research-vpc068-tun2,isakmp-vpn-research-vpc068-tun2,ipsec-prop-vpn-research-vpc068-tun2,ipsec-vpn-research-vpc068-tun2,170.140.77.68,169.254.253.12/30,169.254.255.248/30,PROD",
            "069,10.65.136.0/23,10069,keyring-vpn-research-vpc069-tun1,isakmp-vpn-research-vpc069-tun1,ipsec-prop-vpn-research-vpc069-tun1,ipsec-vpn-research-vpc069-tun1,170.140.76.69,169.254.249.16/30,169.254.251.248/30,20069,keyring-vpn-research-vpc069-tun2,isakmp-vpn-research-vpc069-tun2,ipsec-prop-vpn-research-vpc069-tun2,ipsec-vpn-research-vpc069-tun2,170.140.77.69,169.254.253.16/30,169.254.255.248/30,PROD",
            "070,10.65.138.0/23,10070,keyring-vpn-research-vpc070-tun1,isakmp-vpn-research-vpc070-tun1,ipsec-prop-vpn-research-vpc070-tun1,ipsec-vpn-research-vpc070-tun1,170.140.76.70,169.254.249.20/30,169.254.251.248/30,20070,keyring-vpn-research-vpc070-tun2,isakmp-vpn-research-vpc070-tun2,ipsec-prop-vpn-research-vpc070-tun2,ipsec-vpn-research-vpc070-tun2,170.140.77.70,169.254.253.20/30,169.254.255.248/30,PROD",
            "071,10.65.140.0/23,10071,keyring-vpn-research-vpc071-tun1,isakmp-vpn-research-vpc071-tun1,ipsec-prop-vpn-research-vpc071-tun1,ipsec-vpn-research-vpc071-tun1,170.140.76.71,169.254.249.24/30,169.254.251.248/30,20071,keyring-vpn-research-vpc071-tun2,isakmp-vpn-research-vpc071-tun2,ipsec-prop-vpn-research-vpc071-tun2,ipsec-vpn-research-vpc071-tun2,170.140.77.71,169.254.253.24/30,169.254.255.248/30,PROD",
            "072,10.65.142.0/23,10072,keyring-vpn-research-vpc072-tun1,isakmp-vpn-research-vpc072-tun1,ipsec-prop-vpn-research-vpc072-tun1,ipsec-vpn-research-vpc072-tun1,170.140.76.72,169.254.249.28/30,169.254.251.248/30,20072,keyring-vpn-research-vpc072-tun2,isakmp-vpn-research-vpc072-tun2,ipsec-prop-vpn-research-vpc072-tun2,ipsec-vpn-research-vpc072-tun2,170.140.77.72,169.254.253.28/30,169.254.255.248/30,PROD",
            "073,10.65.144.0/23,10073,keyring-vpn-research-vpc073-tun1,isakmp-vpn-research-vpc073-tun1,ipsec-prop-vpn-research-vpc073-tun1,ipsec-vpn-research-vpc073-tun1,170.140.76.73,169.254.249.32/30,169.254.251.248/30,20073,keyring-vpn-research-vpc073-tun2,isakmp-vpn-research-vpc073-tun2,ipsec-prop-vpn-research-vpc073-tun2,ipsec-vpn-research-vpc073-tun2,170.140.77.73,169.254.253.32/30,169.254.255.248/30,PROD",
            "074,10.65.146.0/23,10074,keyring-vpn-research-vpc074-tun1,isakmp-vpn-research-vpc074-tun1,ipsec-prop-vpn-research-vpc074-tun1,ipsec-vpn-research-vpc074-tun1,170.140.76.74,169.254.249.36/30,169.254.251.248/30,20074,keyring-vpn-research-vpc074-tun2,isakmp-vpn-research-vpc074-tun2,ipsec-prop-vpn-research-vpc074-tun2,ipsec-vpn-research-vpc074-tun2,170.140.77.74,169.254.253.36/30,169.254.255.248/30,PROD",
            "075,10.65.148.0/23,10075,keyring-vpn-research-vpc075-tun1,isakmp-vpn-research-vpc075-tun1,ipsec-prop-vpn-research-vpc075-tun1,ipsec-vpn-research-vpc075-tun1,170.140.76.75,169.254.249.40/30,169.254.251.248/30,20075,keyring-vpn-research-vpc075-tun2,isakmp-vpn-research-vpc075-tun2,ipsec-prop-vpn-research-vpc075-tun2,ipsec-vpn-research-vpc075-tun2,170.140.77.75,169.254.253.40/30,169.254.255.248/30,PROD",
            "076,10.65.150.0/23,10076,keyring-vpn-research-vpc076-tun1,isakmp-vpn-research-vpc076-tun1,ipsec-prop-vpn-research-vpc076-tun1,ipsec-vpn-research-vpc076-tun1,170.140.76.76,169.254.249.44/30,169.254.251.248/30,20076,keyring-vpn-research-vpc076-tun2,isakmp-vpn-research-vpc076-tun2,ipsec-prop-vpn-research-vpc076-tun2,ipsec-vpn-research-vpc076-tun2,170.140.77.76,169.254.253.44/30,169.254.255.248/30,PROD",
            "077,10.65.152.0/23,10077,keyring-vpn-research-vpc077-tun1,isakmp-vpn-research-vpc077-tun1,ipsec-prop-vpn-research-vpc077-tun1,ipsec-vpn-research-vpc077-tun1,170.140.76.77,169.254.249.48/30,169.254.251.248/30,20077,keyring-vpn-research-vpc077-tun2,isakmp-vpn-research-vpc077-tun2,ipsec-prop-vpn-research-vpc077-tun2,ipsec-vpn-research-vpc077-tun2,170.140.77.77,169.254.253.48/30,169.254.255.248/30,PROD",
            "078,10.65.154.0/23,10078,keyring-vpn-research-vpc078-tun1,isakmp-vpn-research-vpc078-tun1,ipsec-prop-vpn-research-vpc078-tun1,ipsec-vpn-research-vpc078-tun1,170.140.76.78,169.254.249.52/30,169.254.251.248/30,20078,keyring-vpn-research-vpc078-tun2,isakmp-vpn-research-vpc078-tun2,ipsec-prop-vpn-research-vpc078-tun2,ipsec-vpn-research-vpc078-tun2,170.140.77.78,169.254.253.52/30,169.254.255.248/30,PROD",
            "079,10.65.156.0/23,10079,keyring-vpn-research-vpc079-tun1,isakmp-vpn-research-vpc079-tun1,ipsec-prop-vpn-research-vpc079-tun1,ipsec-vpn-research-vpc079-tun1,170.140.76.79,169.254.249.56/30,169.254.251.248/30,20079,keyring-vpn-research-vpc079-tun2,isakmp-vpn-research-vpc079-tun2,ipsec-prop-vpn-research-vpc079-tun2,ipsec-vpn-research-vpc079-tun2,170.140.77.79,169.254.253.56/30,169.254.255.248/30,PROD",
            "080,10.65.158.0/23,10080,keyring-vpn-research-vpc080-tun1,isakmp-vpn-research-vpc080-tun1,ipsec-prop-vpn-research-vpc080-tun1,ipsec-vpn-research-vpc080-tun1,170.140.76.80,169.254.249.60/30,169.254.251.248/30,20080,keyring-vpn-research-vpc080-tun2,isakmp-vpn-research-vpc080-tun2,ipsec-prop-vpn-research-vpc080-tun2,ipsec-vpn-research-vpc080-tun2,170.140.77.80,169.254.253.60/30,169.254.255.248/30,PROD",
            "081,10.65.160.0/23,10081,keyring-vpn-research-vpc081-tun1,isakmp-vpn-research-vpc081-tun1,ipsec-prop-vpn-research-vpc081-tun1,ipsec-vpn-research-vpc081-tun1,170.140.76.81,169.254.249.64/30,169.254.251.248/30,20081,keyring-vpn-research-vpc081-tun2,isakmp-vpn-research-vpc081-tun2,ipsec-prop-vpn-research-vpc081-tun2,ipsec-vpn-research-vpc081-tun2,170.140.77.81,169.254.253.64/30,169.254.255.248/30,PROD",
            "082,10.65.162.0/23,10082,keyring-vpn-research-vpc082-tun1,isakmp-vpn-research-vpc082-tun1,ipsec-prop-vpn-research-vpc082-tun1,ipsec-vpn-research-vpc082-tun1,170.140.76.82,169.254.249.68/30,169.254.251.248/30,20082,keyring-vpn-research-vpc082-tun2,isakmp-vpn-research-vpc082-tun2,ipsec-prop-vpn-research-vpc082-tun2,ipsec-vpn-research-vpc082-tun2,170.140.77.82,169.254.253.68/30,169.254.255.248/30,PROD",
            "083,10.65.164.0/23,10083,keyring-vpn-research-vpc083-tun1,isakmp-vpn-research-vpc083-tun1,ipsec-prop-vpn-research-vpc083-tun1,ipsec-vpn-research-vpc083-tun1,170.140.76.83,169.254.249.72/30,169.254.251.248/30,20083,keyring-vpn-research-vpc083-tun2,isakmp-vpn-research-vpc083-tun2,ipsec-prop-vpn-research-vpc083-tun2,ipsec-vpn-research-vpc083-tun2,170.140.77.83,169.254.253.72/30,169.254.255.248/30,PROD",
            "084,10.65.166.0/23,10084,keyring-vpn-research-vpc084-tun1,isakmp-vpn-research-vpc084-tun1,ipsec-prop-vpn-research-vpc084-tun1,ipsec-vpn-research-vpc084-tun1,170.140.76.84,169.254.249.76/30,169.254.251.248/30,20084,keyring-vpn-research-vpc084-tun2,isakmp-vpn-research-vpc084-tun2,ipsec-prop-vpn-research-vpc084-tun2,ipsec-vpn-research-vpc084-tun2,170.140.77.84,169.254.253.76/30,169.254.255.248/30,PROD",
            "085,10.65.168.0/23,10085,keyring-vpn-research-vpc085-tun1,isakmp-vpn-research-vpc085-tun1,ipsec-prop-vpn-research-vpc085-tun1,ipsec-vpn-research-vpc085-tun1,170.140.76.85,169.254.249.80/30,169.254.251.248/30,20085,keyring-vpn-research-vpc085-tun2,isakmp-vpn-research-vpc085-tun2,ipsec-prop-vpn-research-vpc085-tun2,ipsec-vpn-research-vpc085-tun2,170.140.77.85,169.254.253.80/30,169.254.255.248/30,PROD",
            "086,10.65.170.0/23,10086,keyring-vpn-research-vpc086-tun1,isakmp-vpn-research-vpc086-tun1,ipsec-prop-vpn-research-vpc086-tun1,ipsec-vpn-research-vpc086-tun1,170.140.76.86,169.254.249.84/30,169.254.251.248/30,20086,keyring-vpn-research-vpc086-tun2,isakmp-vpn-research-vpc086-tun2,ipsec-prop-vpn-research-vpc086-tun2,ipsec-vpn-research-vpc086-tun2,170.140.77.86,169.254.253.84/30,169.254.255.248/30,PROD",
            "087,10.65.172.0/23,10087,keyring-vpn-research-vpc087-tun1,isakmp-vpn-research-vpc087-tun1,ipsec-prop-vpn-research-vpc087-tun1,ipsec-vpn-research-vpc087-tun1,170.140.76.87,169.254.249.88/30,169.254.251.248/30,20087,keyring-vpn-research-vpc087-tun2,isakmp-vpn-research-vpc087-tun2,ipsec-prop-vpn-research-vpc087-tun2,ipsec-vpn-research-vpc087-tun2,170.140.77.87,169.254.253.88/30,169.254.255.248/30,PROD",
            "088,10.65.174.0/23,10088,keyring-vpn-research-vpc088-tun1,isakmp-vpn-research-vpc088-tun1,ipsec-prop-vpn-research-vpc088-tun1,ipsec-vpn-research-vpc088-tun1,170.140.76.88,169.254.249.92/30,169.254.251.248/30,20088,keyring-vpn-research-vpc088-tun2,isakmp-vpn-research-vpc088-tun2,ipsec-prop-vpn-research-vpc088-tun2,ipsec-vpn-research-vpc088-tun2,170.140.77.88,169.254.253.92/30,169.254.255.248/30,PROD",
            "089,10.65.176.0/23,10089,keyring-vpn-research-vpc089-tun1,isakmp-vpn-research-vpc089-tun1,ipsec-prop-vpn-research-vpc089-tun1,ipsec-vpn-research-vpc089-tun1,170.140.76.89,169.254.249.96/30,169.254.251.248/30,20089,keyring-vpn-research-vpc089-tun2,isakmp-vpn-research-vpc089-tun2,ipsec-prop-vpn-research-vpc089-tun2,ipsec-vpn-research-vpc089-tun2,170.140.77.89,169.254.253.96/30,169.254.255.248/30,PROD",
            "090,10.65.178.0/23,10090,keyring-vpn-research-vpc090-tun1,isakmp-vpn-research-vpc090-tun1,ipsec-prop-vpn-research-vpc090-tun1,ipsec-vpn-research-vpc090-tun1,170.140.76.90,169.254.249.100/30,169.254.251.248/30,20090,keyring-vpn-research-vpc090-tun2,isakmp-vpn-research-vpc090-tun2,ipsec-prop-vpn-research-vpc090-tun2,ipsec-vpn-research-vpc090-tun2,170.140.77.90,169.254.253.100/30,169.254.255.248/30,PROD",
            "091,10.65.180.0/23,10091,keyring-vpn-research-vpc091-tun1,isakmp-vpn-research-vpc091-tun1,ipsec-prop-vpn-research-vpc091-tun1,ipsec-vpn-research-vpc091-tun1,170.140.76.91,169.254.249.104/30,169.254.251.248/30,20091,keyring-vpn-research-vpc091-tun2,isakmp-vpn-research-vpc091-tun2,ipsec-prop-vpn-research-vpc091-tun2,ipsec-vpn-research-vpc091-tun2,170.140.77.91,169.254.253.104/30,169.254.255.248/30,PROD",
            "092,10.65.182.0/23,10092,keyring-vpn-research-vpc092-tun1,isakmp-vpn-research-vpc092-tun1,ipsec-prop-vpn-research-vpc092-tun1,ipsec-vpn-research-vpc092-tun1,170.140.76.92,169.254.249.108/30,169.254.251.248/30,20092,keyring-vpn-research-vpc092-tun2,isakmp-vpn-research-vpc092-tun2,ipsec-prop-vpn-research-vpc092-tun2,ipsec-vpn-research-vpc092-tun2,170.140.77.92,169.254.253.108/30,169.254.255.248/30,PROD",
            "093,10.65.184.0/23,10093,keyring-vpn-research-vpc093-tun1,isakmp-vpn-research-vpc093-tun1,ipsec-prop-vpn-research-vpc093-tun1,ipsec-vpn-research-vpc093-tun1,170.140.76.93,169.254.249.112/30,169.254.251.248/30,20093,keyring-vpn-research-vpc093-tun2,isakmp-vpn-research-vpc093-tun2,ipsec-prop-vpn-research-vpc093-tun2,ipsec-vpn-research-vpc093-tun2,170.140.77.93,169.254.253.112/30,169.254.255.248/30,PROD",
            "094,10.65.186.0/23,10094,keyring-vpn-research-vpc094-tun1,isakmp-vpn-research-vpc094-tun1,ipsec-prop-vpn-research-vpc094-tun1,ipsec-vpn-research-vpc094-tun1,170.140.76.94,169.254.249.116/30,169.254.251.248/30,20094,keyring-vpn-research-vpc094-tun2,isakmp-vpn-research-vpc094-tun2,ipsec-prop-vpn-research-vpc094-tun2,ipsec-vpn-research-vpc094-tun2,170.140.77.94,169.254.253.116/30,169.254.255.248/30,PROD",
            "095,10.65.188.0/23,10095,keyring-vpn-research-vpc095-tun1,isakmp-vpn-research-vpc095-tun1,ipsec-prop-vpn-research-vpc095-tun1,ipsec-vpn-research-vpc095-tun1,170.140.76.95,169.254.249.120/30,169.254.251.248/30,20095,keyring-vpn-research-vpc095-tun2,isakmp-vpn-research-vpc095-tun2,ipsec-prop-vpn-research-vpc095-tun2,ipsec-vpn-research-vpc095-tun2,170.140.77.95,169.254.253.120/30,169.254.255.248/30,PROD",
            "096,10.65.190.0/23,10096,keyring-vpn-research-vpc096-tun1,isakmp-vpn-research-vpc096-tun1,ipsec-prop-vpn-research-vpc096-tun1,ipsec-vpn-research-vpc096-tun1,170.140.76.96,169.254.249.124/30,169.254.251.248/30,20096,keyring-vpn-research-vpc096-tun2,isakmp-vpn-research-vpc096-tun2,ipsec-prop-vpn-research-vpc096-tun2,ipsec-vpn-research-vpc096-tun2,170.140.77.96,169.254.253.124/30,169.254.255.248/30,PROD",
            "097,10.65.192.0/23,10097,keyring-vpn-research-vpc097-tun1,isakmp-vpn-research-vpc097-tun1,ipsec-prop-vpn-research-vpc097-tun1,ipsec-vpn-research-vpc097-tun1,170.140.76.97,169.254.249.128/30,169.254.251.248/30,20097,keyring-vpn-research-vpc097-tun2,isakmp-vpn-research-vpc097-tun2,ipsec-prop-vpn-research-vpc097-tun2,ipsec-vpn-research-vpc097-tun2,170.140.77.97,169.254.253.128/30,169.254.255.248/30,PROD",
            "098,10.65.194.0/23,10098,keyring-vpn-research-vpc098-tun1,isakmp-vpn-research-vpc098-tun1,ipsec-prop-vpn-research-vpc098-tun1,ipsec-vpn-research-vpc098-tun1,170.140.76.98,169.254.249.132/30,169.254.251.248/30,20098,keyring-vpn-research-vpc098-tun2,isakmp-vpn-research-vpc098-tun2,ipsec-prop-vpn-research-vpc098-tun2,ipsec-vpn-research-vpc098-tun2,170.140.77.98,169.254.253.132/30,169.254.255.248/30,PROD",
            "099,10.65.196.0/23,10099,keyring-vpn-research-vpc099-tun1,isakmp-vpn-research-vpc099-tun1,ipsec-prop-vpn-research-vpc099-tun1,ipsec-vpn-research-vpc099-tun1,170.140.76.99,169.254.249.136/30,169.254.251.248/30,20099,keyring-vpn-research-vpc099-tun2,isakmp-vpn-research-vpc099-tun2,ipsec-prop-vpn-research-vpc099-tun2,ipsec-vpn-research-vpc099-tun2,170.140.77.99,169.254.253.136/30,169.254.255.248/30,PROD",
            "100,10.65.198.0/23,10100,keyring-vpn-research-vpc100-tun1,isakmp-vpn-research-vpc100-tun1,ipsec-prop-vpn-research-vpc100-tun1,ipsec-vpn-research-vpc100-tun1,170.140.76.100,169.254.249.140/30,169.254.251.248/30,20100,keyring-vpn-research-vpc100-tun2,isakmp-vpn-research-vpc100-tun2,ipsec-prop-vpn-research-vpc100-tun2,ipsec-vpn-research-vpc100-tun2,170.140.77.100,169.254.253.140/30,169.254.255.248/30,PROD",
            "101,10.65.200.0/23,10101,keyring-vpn-research-vpc101-tun1,isakmp-vpn-research-vpc101-tun1,ipsec-prop-vpn-research-vpc101-tun1,ipsec-vpn-research-vpc101-tun1,170.140.76.101,169.254.249.144/30,169.254.251.248/30,20101,keyring-vpn-research-vpc101-tun2,isakmp-vpn-research-vpc101-tun2,ipsec-prop-vpn-research-vpc101-tun2,ipsec-vpn-research-vpc101-tun2,170.140.77.101,169.254.253.144/30,169.254.255.248/30,PROD",
            "102,10.65.202.0/23,10102,keyring-vpn-research-vpc102-tun1,isakmp-vpn-research-vpc102-tun1,ipsec-prop-vpn-research-vpc102-tun1,ipsec-vpn-research-vpc102-tun1,170.140.76.102,169.254.249.148/30,169.254.251.248/30,20102,keyring-vpn-research-vpc102-tun2,isakmp-vpn-research-vpc102-tun2,ipsec-prop-vpn-research-vpc102-tun2,ipsec-vpn-research-vpc102-tun2,170.140.77.102,169.254.253.148/30,169.254.255.248/30,PROD",
            "103,10.65.204.0/23,10103,keyring-vpn-research-vpc103-tun1,isakmp-vpn-research-vpc103-tun1,ipsec-prop-vpn-research-vpc103-tun1,ipsec-vpn-research-vpc103-tun1,170.140.76.103,169.254.249.152/30,169.254.251.248/30,20103,keyring-vpn-research-vpc103-tun2,isakmp-vpn-research-vpc103-tun2,ipsec-prop-vpn-research-vpc103-tun2,ipsec-vpn-research-vpc103-tun2,170.140.77.103,169.254.253.152/30,169.254.255.248/30,PROD",
            "104,10.65.206.0/23,10104,keyring-vpn-research-vpc104-tun1,isakmp-vpn-research-vpc104-tun1,ipsec-prop-vpn-research-vpc104-tun1,ipsec-vpn-research-vpc104-tun1,170.140.76.104,169.254.249.156/30,169.254.251.248/30,20104,keyring-vpn-research-vpc104-tun2,isakmp-vpn-research-vpc104-tun2,ipsec-prop-vpn-research-vpc104-tun2,ipsec-vpn-research-vpc104-tun2,170.140.77.104,169.254.253.156/30,169.254.255.248/30,PROD",
            "105,10.65.208.0/23,10105,keyring-vpn-research-vpc105-tun1,isakmp-vpn-research-vpc105-tun1,ipsec-prop-vpn-research-vpc105-tun1,ipsec-vpn-research-vpc105-tun1,170.140.76.105,169.254.249.160/30,169.254.251.248/30,20105,keyring-vpn-research-vpc105-tun2,isakmp-vpn-research-vpc105-tun2,ipsec-prop-vpn-research-vpc105-tun2,ipsec-vpn-research-vpc105-tun2,170.140.77.105,169.254.253.160/30,169.254.255.248/30,PROD",
            "106,10.65.210.0/23,10106,keyring-vpn-research-vpc106-tun1,isakmp-vpn-research-vpc106-tun1,ipsec-prop-vpn-research-vpc106-tun1,ipsec-vpn-research-vpc106-tun1,170.140.76.106,169.254.249.164/30,169.254.251.248/30,20106,keyring-vpn-research-vpc106-tun2,isakmp-vpn-research-vpc106-tun2,ipsec-prop-vpn-research-vpc106-tun2,ipsec-vpn-research-vpc106-tun2,170.140.77.106,169.254.253.164/30,169.254.255.248/30,PROD",
            "107,10.65.212.0/23,10107,keyring-vpn-research-vpc107-tun1,isakmp-vpn-research-vpc107-tun1,ipsec-prop-vpn-research-vpc107-tun1,ipsec-vpn-research-vpc107-tun1,170.140.76.107,169.254.249.168/30,169.254.251.248/30,20107,keyring-vpn-research-vpc107-tun2,isakmp-vpn-research-vpc107-tun2,ipsec-prop-vpn-research-vpc107-tun2,ipsec-vpn-research-vpc107-tun2,170.140.77.107,169.254.253.168/30,169.254.255.248/30,PROD",
            "108,10.65.214.0/23,10108,keyring-vpn-research-vpc108-tun1,isakmp-vpn-research-vpc108-tun1,ipsec-prop-vpn-research-vpc108-tun1,ipsec-vpn-research-vpc108-tun1,170.140.76.108,169.254.249.172/30,169.254.251.248/30,20108,keyring-vpn-research-vpc108-tun2,isakmp-vpn-research-vpc108-tun2,ipsec-prop-vpn-research-vpc108-tun2,ipsec-vpn-research-vpc108-tun2,170.140.77.108,169.254.253.172/30,169.254.255.248/30,PROD",
            "109,10.65.216.0/23,10109,keyring-vpn-research-vpc109-tun1,isakmp-vpn-research-vpc109-tun1,ipsec-prop-vpn-research-vpc109-tun1,ipsec-vpn-research-vpc109-tun1,170.140.76.109,169.254.249.176/30,169.254.251.248/30,20109,keyring-vpn-research-vpc109-tun2,isakmp-vpn-research-vpc109-tun2,ipsec-prop-vpn-research-vpc109-tun2,ipsec-vpn-research-vpc109-tun2,170.140.77.109,169.254.253.176/30,169.254.255.248/30,PROD",
            "110,10.65.218.0/23,10110,keyring-vpn-research-vpc110-tun1,isakmp-vpn-research-vpc110-tun1,ipsec-prop-vpn-research-vpc110-tun1,ipsec-vpn-research-vpc110-tun1,170.140.76.110,169.254.249.180/30,169.254.251.248/30,20110,keyring-vpn-research-vpc110-tun2,isakmp-vpn-research-vpc110-tun2,ipsec-prop-vpn-research-vpc110-tun2,ipsec-vpn-research-vpc110-tun2,170.140.77.110,169.254.253.180/30,169.254.255.248/30,PROD",
            "111,10.65.220.0/23,10111,keyring-vpn-research-vpc111-tun1,isakmp-vpn-research-vpc111-tun1,ipsec-prop-vpn-research-vpc111-tun1,ipsec-vpn-research-vpc111-tun1,170.140.76.111,169.254.249.184/30,169.254.251.248/30,20111,keyring-vpn-research-vpc111-tun2,isakmp-vpn-research-vpc111-tun2,ipsec-prop-vpn-research-vpc111-tun2,ipsec-vpn-research-vpc111-tun2,170.140.77.111,169.254.253.184/30,169.254.255.248/30,PROD",
            "112,10.65.222.0/23,10112,keyring-vpn-research-vpc112-tun1,isakmp-vpn-research-vpc112-tun1,ipsec-prop-vpn-research-vpc112-tun1,ipsec-vpn-research-vpc112-tun1,170.140.76.112,169.254.249.188/30,169.254.251.248/30,20112,keyring-vpn-research-vpc112-tun2,isakmp-vpn-research-vpc112-tun2,ipsec-prop-vpn-research-vpc112-tun2,ipsec-vpn-research-vpc112-tun2,170.140.77.112,169.254.253.188/30,169.254.255.248/30,PROD",
            "113,10.65.224.0/23,10113,keyring-vpn-research-vpc113-tun1,isakmp-vpn-research-vpc113-tun1,ipsec-prop-vpn-research-vpc113-tun1,ipsec-vpn-research-vpc113-tun1,170.140.76.113,169.254.249.192/30,169.254.251.248/30,20113,keyring-vpn-research-vpc113-tun2,isakmp-vpn-research-vpc113-tun2,ipsec-prop-vpn-research-vpc113-tun2,ipsec-vpn-research-vpc113-tun2,170.140.77.113,169.254.253.192/30,169.254.255.248/30,PROD",
            "114,10.65.226.0/23,10114,keyring-vpn-research-vpc114-tun1,isakmp-vpn-research-vpc114-tun1,ipsec-prop-vpn-research-vpc114-tun1,ipsec-vpn-research-vpc114-tun1,170.140.76.114,169.254.249.196/30,169.254.251.248/30,20114,keyring-vpn-research-vpc114-tun2,isakmp-vpn-research-vpc114-tun2,ipsec-prop-vpn-research-vpc114-tun2,ipsec-vpn-research-vpc114-tun2,170.140.77.114,169.254.253.196/30,169.254.255.248/30,PROD",
            "115,10.65.228.0/23,10115,keyring-vpn-research-vpc115-tun1,isakmp-vpn-research-vpc115-tun1,ipsec-prop-vpn-research-vpc115-tun1,ipsec-vpn-research-vpc115-tun1,170.140.76.115,169.254.249.200/30,169.254.251.248/30,20115,keyring-vpn-research-vpc115-tun2,isakmp-vpn-research-vpc115-tun2,ipsec-prop-vpn-research-vpc115-tun2,ipsec-vpn-research-vpc115-tun2,170.140.77.115,169.254.253.200/30,169.254.255.248/30,PROD",
            "116,10.65.230.0/23,10116,keyring-vpn-research-vpc116-tun1,isakmp-vpn-research-vpc116-tun1,ipsec-prop-vpn-research-vpc116-tun1,ipsec-vpn-research-vpc116-tun1,170.140.76.116,169.254.249.204/30,169.254.251.248/30,20116,keyring-vpn-research-vpc116-tun2,isakmp-vpn-research-vpc116-tun2,ipsec-prop-vpn-research-vpc116-tun2,ipsec-vpn-research-vpc116-tun2,170.140.77.116,169.254.253.204/30,169.254.255.248/30,PROD",
            "117,10.65.232.0/23,10117,keyring-vpn-research-vpc117-tun1,isakmp-vpn-research-vpc117-tun1,ipsec-prop-vpn-research-vpc117-tun1,ipsec-vpn-research-vpc117-tun1,170.140.76.117,169.254.249.208/30,169.254.251.248/30,20117,keyring-vpn-research-vpc117-tun2,isakmp-vpn-research-vpc117-tun2,ipsec-prop-vpn-research-vpc117-tun2,ipsec-vpn-research-vpc117-tun2,170.140.77.117,169.254.253.208/30,169.254.255.248/30,PROD",
            "118,10.65.234.0/23,10118,keyring-vpn-research-vpc118-tun1,isakmp-vpn-research-vpc118-tun1,ipsec-prop-vpn-research-vpc118-tun1,ipsec-vpn-research-vpc118-tun1,170.140.76.118,169.254.249.212/30,169.254.251.248/30,20118,keyring-vpn-research-vpc118-tun2,isakmp-vpn-research-vpc118-tun2,ipsec-prop-vpn-research-vpc118-tun2,ipsec-vpn-research-vpc118-tun2,170.140.77.118,169.254.253.212/30,169.254.255.248/30,PROD",
            "119,10.65.236.0/23,10119,keyring-vpn-research-vpc119-tun1,isakmp-vpn-research-vpc119-tun1,ipsec-prop-vpn-research-vpc119-tun1,ipsec-vpn-research-vpc119-tun1,170.140.76.119,169.254.249.216/30,169.254.251.248/30,20119,keyring-vpn-research-vpc119-tun2,isakmp-vpn-research-vpc119-tun2,ipsec-prop-vpn-research-vpc119-tun2,ipsec-vpn-research-vpc119-tun2,170.140.77.119,169.254.253.216/30,169.254.255.248/30,PROD",
            "120,10.65.238.0/23,10120,keyring-vpn-research-vpc120-tun1,isakmp-vpn-research-vpc120-tun1,ipsec-prop-vpn-research-vpc120-tun1,ipsec-vpn-research-vpc120-tun1,170.140.76.120,169.254.249.220/30,169.254.251.248/30,20120,keyring-vpn-research-vpc120-tun2,isakmp-vpn-research-vpc120-tun2,ipsec-prop-vpn-research-vpc120-tun2,ipsec-vpn-research-vpc120-tun2,170.140.77.120,169.254.253.220/30,169.254.255.248/30,PROD",
            "121,10.65.240.0/23,10121,keyring-vpn-research-vpc121-tun1,isakmp-vpn-research-vpc121-tun1,ipsec-prop-vpn-research-vpc121-tun1,ipsec-vpn-research-vpc121-tun1,170.140.76.121,169.254.249.224/30,169.254.251.248/30,20121,keyring-vpn-research-vpc121-tun2,isakmp-vpn-research-vpc121-tun2,ipsec-prop-vpn-research-vpc121-tun2,ipsec-vpn-research-vpc121-tun2,170.140.77.121,169.254.253.224/30,169.254.255.248/30,PROD",
            "122,10.65.242.0/23,10122,keyring-vpn-research-vpc122-tun1,isakmp-vpn-research-vpc122-tun1,ipsec-prop-vpn-research-vpc122-tun1,ipsec-vpn-research-vpc122-tun1,170.140.76.122,169.254.249.228/30,169.254.251.248/30,20122,keyring-vpn-research-vpc122-tun2,isakmp-vpn-research-vpc122-tun2,ipsec-prop-vpn-research-vpc122-tun2,ipsec-vpn-research-vpc122-tun2,170.140.77.122,169.254.253.228/30,169.254.255.248/30,PROD",
            "123,10.65.244.0/23,10123,keyring-vpn-research-vpc123-tun1,isakmp-vpn-research-vpc123-tun1,ipsec-prop-vpn-research-vpc123-tun1,ipsec-vpn-research-vpc123-tun1,170.140.76.123,169.254.249.232/30,169.254.251.248/30,20123,keyring-vpn-research-vpc123-tun2,isakmp-vpn-research-vpc123-tun2,ipsec-prop-vpn-research-vpc123-tun2,ipsec-vpn-research-vpc123-tun2,170.140.77.123,169.254.253.232/30,169.254.255.248/30,PROD",
            "124,10.65.246.0/23,10124,keyring-vpn-research-vpc124-tun1,isakmp-vpn-research-vpc124-tun1,ipsec-prop-vpn-research-vpc124-tun1,ipsec-vpn-research-vpc124-tun1,170.140.76.124,169.254.249.236/30,169.254.251.248/30,20124,keyring-vpn-research-vpc124-tun2,isakmp-vpn-research-vpc124-tun2,ipsec-prop-vpn-research-vpc124-tun2,ipsec-vpn-research-vpc124-tun2,170.140.77.124,169.254.253.236/30,169.254.255.248/30,PROD",
            "125,10.65.248.0/23,10125,keyring-vpn-research-vpc125-tun1,isakmp-vpn-research-vpc125-tun1,ipsec-prop-vpn-research-vpc125-tun1,ipsec-vpn-research-vpc125-tun1,170.140.76.125,169.254.249.240/30,169.254.251.248/30,20125,keyring-vpn-research-vpc125-tun2,isakmp-vpn-research-vpc125-tun2,ipsec-prop-vpn-research-vpc125-tun2,ipsec-vpn-research-vpc125-tun2,170.140.77.125,169.254.253.240/30,169.254.255.248/30,PROD",
            "126,10.65.250.0/23,10126,keyring-vpn-research-vpc126-tun1,isakmp-vpn-research-vpc126-tun1,ipsec-prop-vpn-research-vpc126-tun1,ipsec-vpn-research-vpc126-tun1,170.140.76.126,169.254.249.244/30,169.254.251.248/30,20126,keyring-vpn-research-vpc126-tun2,isakmp-vpn-research-vpc126-tun2,ipsec-prop-vpn-research-vpc126-tun2,ipsec-vpn-research-vpc126-tun2,170.140.77.126,169.254.253.244/30,169.254.255.248/30,PROD",
            "127,10.65.252.0/23,10127,keyring-vpn-research-vpc127-tun1,isakmp-vpn-research-vpc127-tun1,ipsec-prop-vpn-research-vpc127-tun1,ipsec-vpn-research-vpc127-tun1,170.140.76.127,169.254.249.248/30,169.254.251.248/30,20127,keyring-vpn-research-vpc127-tun2,isakmp-vpn-research-vpc127-tun2,ipsec-prop-vpn-research-vpc127-tun2,ipsec-vpn-research-vpc127-tun2,170.140.77.127,169.254.253.248/30,169.254.255.248/30,PROD",
            "128,10.65.254.0/23,10128,keyring-vpn-research-vpc128-tun1,isakmp-vpn-research-vpc128-tun1,ipsec-prop-vpn-research-vpc128-tun1,ipsec-vpn-research-vpc128-tun1,170.140.76.128,169.254.249.252/30,169.254.251.248/30,20128,keyring-vpn-research-vpc128-tun2,isakmp-vpn-research-vpc128-tun2,ipsec-prop-vpn-research-vpc128-tun2,ipsec-vpn-research-vpc128-tun2,170.140.77.128,169.254.253.252/30,169.254.255.248/30,PROD",
            "129,10.66.0.0/23,10129,keyring-vpn-research-vpc129-tun1,isakmp-vpn-research-vpc129-tun1,ipsec-prop-vpn-research-vpc129-tun1,ipsec-vpn-research-vpc129-tun1,170.140.76.129,169.254.250.0/30,169.254.251.248/30,20129,keyring-vpn-research-vpc129-tun2,isakmp-vpn-research-vpc129-tun2,ipsec-prop-vpn-research-vpc129-tun2,ipsec-vpn-research-vpc129-tun2,170.140.77.129,169.254.254.0/30,169.254.255.248/30,PROD",
            "130,10.66.2.0/23,10130,keyring-vpn-research-vpc130-tun1,isakmp-vpn-research-vpc130-tun1,ipsec-prop-vpn-research-vpc130-tun1,ipsec-vpn-research-vpc130-tun1,170.140.76.130,169.254.250.4/30,169.254.251.248/30,20130,keyring-vpn-research-vpc130-tun2,isakmp-vpn-research-vpc130-tun2,ipsec-prop-vpn-research-vpc130-tun2,ipsec-vpn-research-vpc130-tun2,170.140.77.130,169.254.254.4/30,169.254.255.248/30,PROD",
            "131,10.66.4.0/23,10131,keyring-vpn-research-vpc131-tun1,isakmp-vpn-research-vpc131-tun1,ipsec-prop-vpn-research-vpc131-tun1,ipsec-vpn-research-vpc131-tun1,170.140.76.131,169.254.250.8/30,169.254.251.248/30,20131,keyring-vpn-research-vpc131-tun2,isakmp-vpn-research-vpc131-tun2,ipsec-prop-vpn-research-vpc131-tun2,ipsec-vpn-research-vpc131-tun2,170.140.77.131,169.254.254.8/30,169.254.255.248/30,PROD",
            "132,10.66.6.0/23,10132,keyring-vpn-research-vpc132-tun1,isakmp-vpn-research-vpc132-tun1,ipsec-prop-vpn-research-vpc132-tun1,ipsec-vpn-research-vpc132-tun1,170.140.76.132,169.254.250.12/30,169.254.251.248/30,20132,keyring-vpn-research-vpc132-tun2,isakmp-vpn-research-vpc132-tun2,ipsec-prop-vpn-research-vpc132-tun2,ipsec-vpn-research-vpc132-tun2,170.140.77.132,169.254.254.12/30,169.254.255.248/30,PROD",
            "133,10.66.8.0/23,10133,keyring-vpn-research-vpc133-tun1,isakmp-vpn-research-vpc133-tun1,ipsec-prop-vpn-research-vpc133-tun1,ipsec-vpn-research-vpc133-tun1,170.140.76.133,169.254.250.16/30,169.254.251.248/30,20133,keyring-vpn-research-vpc133-tun2,isakmp-vpn-research-vpc133-tun2,ipsec-prop-vpn-research-vpc133-tun2,ipsec-vpn-research-vpc133-tun2,170.140.77.133,169.254.254.16/30,169.254.255.248/30,PROD",
            "134,10.66.10.0/23,10134,keyring-vpn-research-vpc134-tun1,isakmp-vpn-research-vpc134-tun1,ipsec-prop-vpn-research-vpc134-tun1,ipsec-vpn-research-vpc134-tun1,170.140.76.134,169.254.250.20/30,169.254.251.248/30,20134,keyring-vpn-research-vpc134-tun2,isakmp-vpn-research-vpc134-tun2,ipsec-prop-vpn-research-vpc134-tun2,ipsec-vpn-research-vpc134-tun2,170.140.77.134,169.254.254.20/30,169.254.255.248/30,PROD",
            "135,10.66.12.0/23,10135,keyring-vpn-research-vpc135-tun1,isakmp-vpn-research-vpc135-tun1,ipsec-prop-vpn-research-vpc135-tun1,ipsec-vpn-research-vpc135-tun1,170.140.76.135,169.254.250.24/30,169.254.251.248/30,20135,keyring-vpn-research-vpc135-tun2,isakmp-vpn-research-vpc135-tun2,ipsec-prop-vpn-research-vpc135-tun2,ipsec-vpn-research-vpc135-tun2,170.140.77.135,169.254.254.24/30,169.254.255.248/30,PROD",
            "136,10.66.14.0/23,10136,keyring-vpn-research-vpc136-tun1,isakmp-vpn-research-vpc136-tun1,ipsec-prop-vpn-research-vpc136-tun1,ipsec-vpn-research-vpc136-tun1,170.140.76.136,169.254.250.28/30,169.254.251.248/30,20136,keyring-vpn-research-vpc136-tun2,isakmp-vpn-research-vpc136-tun2,ipsec-prop-vpn-research-vpc136-tun2,ipsec-vpn-research-vpc136-tun2,170.140.77.136,169.254.254.28/30,169.254.255.248/30,PROD",
            "137,10.66.16.0/23,10137,keyring-vpn-research-vpc137-tun1,isakmp-vpn-research-vpc137-tun1,ipsec-prop-vpn-research-vpc137-tun1,ipsec-vpn-research-vpc137-tun1,170.140.76.137,169.254.250.32/30,169.254.251.248/30,20137,keyring-vpn-research-vpc137-tun2,isakmp-vpn-research-vpc137-tun2,ipsec-prop-vpn-research-vpc137-tun2,ipsec-vpn-research-vpc137-tun2,170.140.77.137,169.254.254.32/30,169.254.255.248/30,PROD",
            "138,10.66.18.0/23,10138,keyring-vpn-research-vpc138-tun1,isakmp-vpn-research-vpc138-tun1,ipsec-prop-vpn-research-vpc138-tun1,ipsec-vpn-research-vpc138-tun1,170.140.76.138,169.254.250.36/30,169.254.251.248/30,20138,keyring-vpn-research-vpc138-tun2,isakmp-vpn-research-vpc138-tun2,ipsec-prop-vpn-research-vpc138-tun2,ipsec-vpn-research-vpc138-tun2,170.140.77.138,169.254.254.36/30,169.254.255.248/30,PROD",
            "139,10.66.20.0/23,10139,keyring-vpn-research-vpc139-tun1,isakmp-vpn-research-vpc139-tun1,ipsec-prop-vpn-research-vpc139-tun1,ipsec-vpn-research-vpc139-tun1,170.140.76.139,169.254.250.40/30,169.254.251.248/30,20139,keyring-vpn-research-vpc139-tun2,isakmp-vpn-research-vpc139-tun2,ipsec-prop-vpn-research-vpc139-tun2,ipsec-vpn-research-vpc139-tun2,170.140.77.139,169.254.254.40/30,169.254.255.248/30,PROD",
            "140,10.66.22.0/23,10140,keyring-vpn-research-vpc140-tun1,isakmp-vpn-research-vpc140-tun1,ipsec-prop-vpn-research-vpc140-tun1,ipsec-vpn-research-vpc140-tun1,170.140.76.140,169.254.250.44/30,169.254.251.248/30,20140,keyring-vpn-research-vpc140-tun2,isakmp-vpn-research-vpc140-tun2,ipsec-prop-vpn-research-vpc140-tun2,ipsec-vpn-research-vpc140-tun2,170.140.77.140,169.254.254.44/30,169.254.255.248/30,PROD",
            "141,10.66.24.0/23,10141,keyring-vpn-research-vpc141-tun1,isakmp-vpn-research-vpc141-tun1,ipsec-prop-vpn-research-vpc141-tun1,ipsec-vpn-research-vpc141-tun1,170.140.76.141,169.254.250.48/30,169.254.251.248/30,20141,keyring-vpn-research-vpc141-tun2,isakmp-vpn-research-vpc141-tun2,ipsec-prop-vpn-research-vpc141-tun2,ipsec-vpn-research-vpc141-tun2,170.140.77.141,169.254.254.48/30,169.254.255.248/30,PROD",
            "142,10.66.26.0/23,10142,keyring-vpn-research-vpc142-tun1,isakmp-vpn-research-vpc142-tun1,ipsec-prop-vpn-research-vpc142-tun1,ipsec-vpn-research-vpc142-tun1,170.140.76.142,169.254.250.52/30,169.254.251.248/30,20142,keyring-vpn-research-vpc142-tun2,isakmp-vpn-research-vpc142-tun2,ipsec-prop-vpn-research-vpc142-tun2,ipsec-vpn-research-vpc142-tun2,170.140.77.142,169.254.254.52/30,169.254.255.248/30,PROD",
            "143,10.66.28.0/23,10143,keyring-vpn-research-vpc143-tun1,isakmp-vpn-research-vpc143-tun1,ipsec-prop-vpn-research-vpc143-tun1,ipsec-vpn-research-vpc143-tun1,170.140.76.143,169.254.250.56/30,169.254.251.248/30,20143,keyring-vpn-research-vpc143-tun2,isakmp-vpn-research-vpc143-tun2,ipsec-prop-vpn-research-vpc143-tun2,ipsec-vpn-research-vpc143-tun2,170.140.77.143,169.254.254.56/30,169.254.255.248/30,PROD",
            "144,10.66.30.0/23,10144,keyring-vpn-research-vpc144-tun1,isakmp-vpn-research-vpc144-tun1,ipsec-prop-vpn-research-vpc144-tun1,ipsec-vpn-research-vpc144-tun1,170.140.76.144,169.254.250.60/30,169.254.251.248/30,20144,keyring-vpn-research-vpc144-tun2,isakmp-vpn-research-vpc144-tun2,ipsec-prop-vpn-research-vpc144-tun2,ipsec-vpn-research-vpc144-tun2,170.140.77.144,169.254.254.60/30,169.254.255.248/30,PROD",
            "145,10.66.32.0/23,10145,keyring-vpn-research-vpc145-tun1,isakmp-vpn-research-vpc145-tun1,ipsec-prop-vpn-research-vpc145-tun1,ipsec-vpn-research-vpc145-tun1,170.140.76.145,169.254.250.64/30,169.254.251.248/30,20145,keyring-vpn-research-vpc145-tun2,isakmp-vpn-research-vpc145-tun2,ipsec-prop-vpn-research-vpc145-tun2,ipsec-vpn-research-vpc145-tun2,170.140.77.145,169.254.254.64/30,169.254.255.248/30,PROD",
            "146,10.66.34.0/23,10146,keyring-vpn-research-vpc146-tun1,isakmp-vpn-research-vpc146-tun1,ipsec-prop-vpn-research-vpc146-tun1,ipsec-vpn-research-vpc146-tun1,170.140.76.146,169.254.250.68/30,169.254.251.248/30,20146,keyring-vpn-research-vpc146-tun2,isakmp-vpn-research-vpc146-tun2,ipsec-prop-vpn-research-vpc146-tun2,ipsec-vpn-research-vpc146-tun2,170.140.77.146,169.254.254.68/30,169.254.255.248/30,PROD",
            "147,10.66.36.0/23,10147,keyring-vpn-research-vpc147-tun1,isakmp-vpn-research-vpc147-tun1,ipsec-prop-vpn-research-vpc147-tun1,ipsec-vpn-research-vpc147-tun1,170.140.76.147,169.254.250.72/30,169.254.251.248/30,20147,keyring-vpn-research-vpc147-tun2,isakmp-vpn-research-vpc147-tun2,ipsec-prop-vpn-research-vpc147-tun2,ipsec-vpn-research-vpc147-tun2,170.140.77.147,169.254.254.72/30,169.254.255.248/30,PROD",
            "148,10.66.38.0/23,10148,keyring-vpn-research-vpc148-tun1,isakmp-vpn-research-vpc148-tun1,ipsec-prop-vpn-research-vpc148-tun1,ipsec-vpn-research-vpc148-tun1,170.140.76.148,169.254.250.76/30,169.254.251.248/30,20148,keyring-vpn-research-vpc148-tun2,isakmp-vpn-research-vpc148-tun2,ipsec-prop-vpn-research-vpc148-tun2,ipsec-vpn-research-vpc148-tun2,170.140.77.148,169.254.254.76/30,169.254.255.248/30,PROD",
            "149,10.66.40.0/23,10149,keyring-vpn-research-vpc149-tun1,isakmp-vpn-research-vpc149-tun1,ipsec-prop-vpn-research-vpc149-tun1,ipsec-vpn-research-vpc149-tun1,170.140.76.149,169.254.250.80/30,169.254.251.248/30,20149,keyring-vpn-research-vpc149-tun2,isakmp-vpn-research-vpc149-tun2,ipsec-prop-vpn-research-vpc149-tun2,ipsec-vpn-research-vpc149-tun2,170.140.77.149,169.254.254.80/30,169.254.255.248/30,PROD",
            "150,10.66.42.0/23,10150,keyring-vpn-research-vpc150-tun1,isakmp-vpn-research-vpc150-tun1,ipsec-prop-vpn-research-vpc150-tun1,ipsec-vpn-research-vpc150-tun1,170.140.76.150,169.254.250.84/30,169.254.251.248/30,20150,keyring-vpn-research-vpc150-tun2,isakmp-vpn-research-vpc150-tun2,ipsec-prop-vpn-research-vpc150-tun2,ipsec-vpn-research-vpc150-tun2,170.140.77.150,169.254.254.84/30,169.254.255.248/30,PROD",
            "151,10.66.44.0/23,10151,keyring-vpn-research-vpc151-tun1,isakmp-vpn-research-vpc151-tun1,ipsec-prop-vpn-research-vpc151-tun1,ipsec-vpn-research-vpc151-tun1,170.140.76.151,169.254.250.88/30,169.254.251.248/30,20151,keyring-vpn-research-vpc151-tun2,isakmp-vpn-research-vpc151-tun2,ipsec-prop-vpn-research-vpc151-tun2,ipsec-vpn-research-vpc151-tun2,170.140.77.151,169.254.254.88/30,169.254.255.248/30,PROD",
            "152,10.66.46.0/23,10152,keyring-vpn-research-vpc152-tun1,isakmp-vpn-research-vpc152-tun1,ipsec-prop-vpn-research-vpc152-tun1,ipsec-vpn-research-vpc152-tun1,170.140.76.152,169.254.250.92/30,169.254.251.248/30,20152,keyring-vpn-research-vpc152-tun2,isakmp-vpn-research-vpc152-tun2,ipsec-prop-vpn-research-vpc152-tun2,ipsec-vpn-research-vpc152-tun2,170.140.77.152,169.254.254.92/30,169.254.255.248/30,PROD",
            "153,10.66.48.0/23,10153,keyring-vpn-research-vpc153-tun1,isakmp-vpn-research-vpc153-tun1,ipsec-prop-vpn-research-vpc153-tun1,ipsec-vpn-research-vpc153-tun1,170.140.76.153,169.254.250.96/30,169.254.251.248/30,20153,keyring-vpn-research-vpc153-tun2,isakmp-vpn-research-vpc153-tun2,ipsec-prop-vpn-research-vpc153-tun2,ipsec-vpn-research-vpc153-tun2,170.140.77.153,169.254.254.96/30,169.254.255.248/30,PROD",
            "154,10.66.50.0/23,10154,keyring-vpn-research-vpc154-tun1,isakmp-vpn-research-vpc154-tun1,ipsec-prop-vpn-research-vpc154-tun1,ipsec-vpn-research-vpc154-tun1,170.140.76.154,169.254.250.100/30,169.254.251.248/30,20154,keyring-vpn-research-vpc154-tun2,isakmp-vpn-research-vpc154-tun2,ipsec-prop-vpn-research-vpc154-tun2,ipsec-vpn-research-vpc154-tun2,170.140.77.154,169.254.254.100/30,169.254.255.248/30,PROD",
            "155,10.66.52.0/23,10155,keyring-vpn-research-vpc155-tun1,isakmp-vpn-research-vpc155-tun1,ipsec-prop-vpn-research-vpc155-tun1,ipsec-vpn-research-vpc155-tun1,170.140.76.155,169.254.250.104/30,169.254.251.248/30,20155,keyring-vpn-research-vpc155-tun2,isakmp-vpn-research-vpc155-tun2,ipsec-prop-vpn-research-vpc155-tun2,ipsec-vpn-research-vpc155-tun2,170.140.77.155,169.254.254.104/30,169.254.255.248/30,PROD",
            "156,10.66.54.0/23,10156,keyring-vpn-research-vpc156-tun1,isakmp-vpn-research-vpc156-tun1,ipsec-prop-vpn-research-vpc156-tun1,ipsec-vpn-research-vpc156-tun1,170.140.76.156,169.254.250.108/30,169.254.251.248/30,20156,keyring-vpn-research-vpc156-tun2,isakmp-vpn-research-vpc156-tun2,ipsec-prop-vpn-research-vpc156-tun2,ipsec-vpn-research-vpc156-tun2,170.140.77.156,169.254.254.108/30,169.254.255.248/30,PROD",
            "157,10.66.56.0/23,10157,keyring-vpn-research-vpc157-tun1,isakmp-vpn-research-vpc157-tun1,ipsec-prop-vpn-research-vpc157-tun1,ipsec-vpn-research-vpc157-tun1,170.140.76.157,169.254.250.112/30,169.254.251.248/30,20157,keyring-vpn-research-vpc157-tun2,isakmp-vpn-research-vpc157-tun2,ipsec-prop-vpn-research-vpc157-tun2,ipsec-vpn-research-vpc157-tun2,170.140.77.157,169.254.254.112/30,169.254.255.248/30,PROD",
            "158,10.66.58.0/23,10158,keyring-vpn-research-vpc158-tun1,isakmp-vpn-research-vpc158-tun1,ipsec-prop-vpn-research-vpc158-tun1,ipsec-vpn-research-vpc158-tun1,170.140.76.158,169.254.250.116/30,169.254.251.248/30,20158,keyring-vpn-research-vpc158-tun2,isakmp-vpn-research-vpc158-tun2,ipsec-prop-vpn-research-vpc158-tun2,ipsec-vpn-research-vpc158-tun2,170.140.77.158,169.254.254.116/30,169.254.255.248/30,PROD",
            "159,10.66.60.0/23,10159,keyring-vpn-research-vpc159-tun1,isakmp-vpn-research-vpc159-tun1,ipsec-prop-vpn-research-vpc159-tun1,ipsec-vpn-research-vpc159-tun1,170.140.76.159,169.254.250.120/30,169.254.251.248/30,20159,keyring-vpn-research-vpc159-tun2,isakmp-vpn-research-vpc159-tun2,ipsec-prop-vpn-research-vpc159-tun2,ipsec-vpn-research-vpc159-tun2,170.140.77.159,169.254.254.120/30,169.254.255.248/30,PROD",
            "160,10.66.62.0/23,10160,keyring-vpn-research-vpc160-tun1,isakmp-vpn-research-vpc160-tun1,ipsec-prop-vpn-research-vpc160-tun1,ipsec-vpn-research-vpc160-tun1,170.140.76.160,169.254.250.124/30,169.254.251.248/30,20160,keyring-vpn-research-vpc160-tun2,isakmp-vpn-research-vpc160-tun2,ipsec-prop-vpn-research-vpc160-tun2,ipsec-vpn-research-vpc160-tun2,170.140.77.160,169.254.254.124/30,169.254.255.248/30,PROD",
            "161,10.66.64.0/23,10161,keyring-vpn-research-vpc161-tun1,isakmp-vpn-research-vpc161-tun1,ipsec-prop-vpn-research-vpc161-tun1,ipsec-vpn-research-vpc161-tun1,170.140.76.161,169.254.250.128/30,169.254.251.248/30,20161,keyring-vpn-research-vpc161-tun2,isakmp-vpn-research-vpc161-tun2,ipsec-prop-vpn-research-vpc161-tun2,ipsec-vpn-research-vpc161-tun2,170.140.77.161,169.254.254.128/30,169.254.255.248/30,PROD",
            "162,10.66.66.0/23,10162,keyring-vpn-research-vpc162-tun1,isakmp-vpn-research-vpc162-tun1,ipsec-prop-vpn-research-vpc162-tun1,ipsec-vpn-research-vpc162-tun1,170.140.76.162,169.254.250.132/30,169.254.251.248/30,20162,keyring-vpn-research-vpc162-tun2,isakmp-vpn-research-vpc162-tun2,ipsec-prop-vpn-research-vpc162-tun2,ipsec-vpn-research-vpc162-tun2,170.140.77.162,169.254.254.132/30,169.254.255.248/30,PROD",
            "163,10.66.68.0/23,10163,keyring-vpn-research-vpc163-tun1,isakmp-vpn-research-vpc163-tun1,ipsec-prop-vpn-research-vpc163-tun1,ipsec-vpn-research-vpc163-tun1,170.140.76.163,169.254.250.136/30,169.254.251.248/30,20163,keyring-vpn-research-vpc163-tun2,isakmp-vpn-research-vpc163-tun2,ipsec-prop-vpn-research-vpc163-tun2,ipsec-vpn-research-vpc163-tun2,170.140.77.163,169.254.254.136/30,169.254.255.248/30,PROD",
            "164,10.66.70.0/23,10164,keyring-vpn-research-vpc164-tun1,isakmp-vpn-research-vpc164-tun1,ipsec-prop-vpn-research-vpc164-tun1,ipsec-vpn-research-vpc164-tun1,170.140.76.164,169.254.250.140/30,169.254.251.248/30,20164,keyring-vpn-research-vpc164-tun2,isakmp-vpn-research-vpc164-tun2,ipsec-prop-vpn-research-vpc164-tun2,ipsec-vpn-research-vpc164-tun2,170.140.77.164,169.254.254.140/30,169.254.255.248/30,PROD",
            "165,10.66.72.0/23,10165,keyring-vpn-research-vpc165-tun1,isakmp-vpn-research-vpc165-tun1,ipsec-prop-vpn-research-vpc165-tun1,ipsec-vpn-research-vpc165-tun1,170.140.76.165,169.254.250.144/30,169.254.251.248/30,20165,keyring-vpn-research-vpc165-tun2,isakmp-vpn-research-vpc165-tun2,ipsec-prop-vpn-research-vpc165-tun2,ipsec-vpn-research-vpc165-tun2,170.140.77.165,169.254.254.144/30,169.254.255.248/30,PROD",
            "166,10.66.74.0/23,10166,keyring-vpn-research-vpc166-tun1,isakmp-vpn-research-vpc166-tun1,ipsec-prop-vpn-research-vpc166-tun1,ipsec-vpn-research-vpc166-tun1,170.140.76.166,169.254.250.148/30,169.254.251.248/30,20166,keyring-vpn-research-vpc166-tun2,isakmp-vpn-research-vpc166-tun2,ipsec-prop-vpn-research-vpc166-tun2,ipsec-vpn-research-vpc166-tun2,170.140.77.166,169.254.254.148/30,169.254.255.248/30,PROD",
            "167,10.66.76.0/23,10167,keyring-vpn-research-vpc167-tun1,isakmp-vpn-research-vpc167-tun1,ipsec-prop-vpn-research-vpc167-tun1,ipsec-vpn-research-vpc167-tun1,170.140.76.167,169.254.250.152/30,169.254.251.248/30,20167,keyring-vpn-research-vpc167-tun2,isakmp-vpn-research-vpc167-tun2,ipsec-prop-vpn-research-vpc167-tun2,ipsec-vpn-research-vpc167-tun2,170.140.77.167,169.254.254.152/30,169.254.255.248/30,PROD",
            "168,10.66.78.0/23,10168,keyring-vpn-research-vpc168-tun1,isakmp-vpn-research-vpc168-tun1,ipsec-prop-vpn-research-vpc168-tun1,ipsec-vpn-research-vpc168-tun1,170.140.76.168,169.254.250.156/30,169.254.251.248/30,20168,keyring-vpn-research-vpc168-tun2,isakmp-vpn-research-vpc168-tun2,ipsec-prop-vpn-research-vpc168-tun2,ipsec-vpn-research-vpc168-tun2,170.140.77.168,169.254.254.156/30,169.254.255.248/30,PROD",
            "169,10.66.80.0/23,10169,keyring-vpn-research-vpc169-tun1,isakmp-vpn-research-vpc169-tun1,ipsec-prop-vpn-research-vpc169-tun1,ipsec-vpn-research-vpc169-tun1,170.140.76.169,169.254.250.160/30,169.254.251.248/30,20169,keyring-vpn-research-vpc169-tun2,isakmp-vpn-research-vpc169-tun2,ipsec-prop-vpn-research-vpc169-tun2,ipsec-vpn-research-vpc169-tun2,170.140.77.169,169.254.254.160/30,169.254.255.248/30,PROD",
            "170,10.66.82.0/23,10170,keyring-vpn-research-vpc170-tun1,isakmp-vpn-research-vpc170-tun1,ipsec-prop-vpn-research-vpc170-tun1,ipsec-vpn-research-vpc170-tun1,170.140.76.170,169.254.250.164/30,169.254.251.248/30,20170,keyring-vpn-research-vpc170-tun2,isakmp-vpn-research-vpc170-tun2,ipsec-prop-vpn-research-vpc170-tun2,ipsec-vpn-research-vpc170-tun2,170.140.77.170,169.254.254.164/30,169.254.255.248/30,PROD",
            "171,10.66.84.0/23,10171,keyring-vpn-research-vpc171-tun1,isakmp-vpn-research-vpc171-tun1,ipsec-prop-vpn-research-vpc171-tun1,ipsec-vpn-research-vpc171-tun1,170.140.76.171,169.254.250.168/30,169.254.251.248/30,20171,keyring-vpn-research-vpc171-tun2,isakmp-vpn-research-vpc171-tun2,ipsec-prop-vpn-research-vpc171-tun2,ipsec-vpn-research-vpc171-tun2,170.140.77.171,169.254.254.168/30,169.254.255.248/30,PROD",
            "172,10.66.86.0/23,10172,keyring-vpn-research-vpc172-tun1,isakmp-vpn-research-vpc172-tun1,ipsec-prop-vpn-research-vpc172-tun1,ipsec-vpn-research-vpc172-tun1,170.140.76.172,169.254.250.172/30,169.254.251.248/30,20172,keyring-vpn-research-vpc172-tun2,isakmp-vpn-research-vpc172-tun2,ipsec-prop-vpn-research-vpc172-tun2,ipsec-vpn-research-vpc172-tun2,170.140.77.172,169.254.254.172/30,169.254.255.248/30,PROD",
            "173,10.66.88.0/23,10173,keyring-vpn-research-vpc173-tun1,isakmp-vpn-research-vpc173-tun1,ipsec-prop-vpn-research-vpc173-tun1,ipsec-vpn-research-vpc173-tun1,170.140.76.173,169.254.250.176/30,169.254.251.248/30,20173,keyring-vpn-research-vpc173-tun2,isakmp-vpn-research-vpc173-tun2,ipsec-prop-vpn-research-vpc173-tun2,ipsec-vpn-research-vpc173-tun2,170.140.77.173,169.254.254.176/30,169.254.255.248/30,PROD",
            "174,10.66.90.0/23,10174,keyring-vpn-research-vpc174-tun1,isakmp-vpn-research-vpc174-tun1,ipsec-prop-vpn-research-vpc174-tun1,ipsec-vpn-research-vpc174-tun1,170.140.76.174,169.254.250.180/30,169.254.251.248/30,20174,keyring-vpn-research-vpc174-tun2,isakmp-vpn-research-vpc174-tun2,ipsec-prop-vpn-research-vpc174-tun2,ipsec-vpn-research-vpc174-tun2,170.140.77.174,169.254.254.180/30,169.254.255.248/30,PROD",
            "175,10.66.92.0/23,10175,keyring-vpn-research-vpc175-tun1,isakmp-vpn-research-vpc175-tun1,ipsec-prop-vpn-research-vpc175-tun1,ipsec-vpn-research-vpc175-tun1,170.140.76.175,169.254.250.184/30,169.254.251.248/30,20175,keyring-vpn-research-vpc175-tun2,isakmp-vpn-research-vpc175-tun2,ipsec-prop-vpn-research-vpc175-tun2,ipsec-vpn-research-vpc175-tun2,170.140.77.175,169.254.254.184/30,169.254.255.248/30,PROD",
            "176,10.66.94.0/23,10176,keyring-vpn-research-vpc176-tun1,isakmp-vpn-research-vpc176-tun1,ipsec-prop-vpn-research-vpc176-tun1,ipsec-vpn-research-vpc176-tun1,170.140.76.176,169.254.250.188/30,169.254.251.248/30,20176,keyring-vpn-research-vpc176-tun2,isakmp-vpn-research-vpc176-tun2,ipsec-prop-vpn-research-vpc176-tun2,ipsec-vpn-research-vpc176-tun2,170.140.77.176,169.254.254.188/30,169.254.255.248/30,PROD",
            "177,10.66.96.0/23,10177,keyring-vpn-research-vpc177-tun1,isakmp-vpn-research-vpc177-tun1,ipsec-prop-vpn-research-vpc177-tun1,ipsec-vpn-research-vpc177-tun1,170.140.76.177,169.254.250.192/30,169.254.251.248/30,20177,keyring-vpn-research-vpc177-tun2,isakmp-vpn-research-vpc177-tun2,ipsec-prop-vpn-research-vpc177-tun2,ipsec-vpn-research-vpc177-tun2,170.140.77.177,169.254.254.192/30,169.254.255.248/30,PROD",
            "178,10.66.98.0/23,10178,keyring-vpn-research-vpc178-tun1,isakmp-vpn-research-vpc178-tun1,ipsec-prop-vpn-research-vpc178-tun1,ipsec-vpn-research-vpc178-tun1,170.140.76.178,169.254.250.196/30,169.254.251.248/30,20178,keyring-vpn-research-vpc178-tun2,isakmp-vpn-research-vpc178-tun2,ipsec-prop-vpn-research-vpc178-tun2,ipsec-vpn-research-vpc178-tun2,170.140.77.178,169.254.254.196/30,169.254.255.248/30,PROD",
            "179,10.66.100.0/23,10179,keyring-vpn-research-vpc179-tun1,isakmp-vpn-research-vpc179-tun1,ipsec-prop-vpn-research-vpc179-tun1,ipsec-vpn-research-vpc179-tun1,170.140.76.179,169.254.250.200/30,169.254.251.248/30,20179,keyring-vpn-research-vpc179-tun2,isakmp-vpn-research-vpc179-tun2,ipsec-prop-vpn-research-vpc179-tun2,ipsec-vpn-research-vpc179-tun2,170.140.77.179,169.254.254.200/30,169.254.255.248/30,PROD",
            "180,10.66.102.0/23,10180,keyring-vpn-research-vpc180-tun1,isakmp-vpn-research-vpc180-tun1,ipsec-prop-vpn-research-vpc180-tun1,ipsec-vpn-research-vpc180-tun1,170.140.76.180,169.254.250.204/30,169.254.251.248/30,20180,keyring-vpn-research-vpc180-tun2,isakmp-vpn-research-vpc180-tun2,ipsec-prop-vpn-research-vpc180-tun2,ipsec-vpn-research-vpc180-tun2,170.140.77.180,169.254.254.204/30,169.254.255.248/30,PROD",
            "181,10.66.104.0/23,10181,keyring-vpn-research-vpc181-tun1,isakmp-vpn-research-vpc181-tun1,ipsec-prop-vpn-research-vpc181-tun1,ipsec-vpn-research-vpc181-tun1,170.140.76.181,169.254.250.208/30,169.254.251.248/30,20181,keyring-vpn-research-vpc181-tun2,isakmp-vpn-research-vpc181-tun2,ipsec-prop-vpn-research-vpc181-tun2,ipsec-vpn-research-vpc181-tun2,170.140.77.181,169.254.254.208/30,169.254.255.248/30,PROD",
            "182,10.66.106.0/23,10182,keyring-vpn-research-vpc182-tun1,isakmp-vpn-research-vpc182-tun1,ipsec-prop-vpn-research-vpc182-tun1,ipsec-vpn-research-vpc182-tun1,170.140.76.182,169.254.250.212/30,169.254.251.248/30,20182,keyring-vpn-research-vpc182-tun2,isakmp-vpn-research-vpc182-tun2,ipsec-prop-vpn-research-vpc182-tun2,ipsec-vpn-research-vpc182-tun2,170.140.77.182,169.254.254.212/30,169.254.255.248/30,PROD",
            "183,10.66.108.0/23,10183,keyring-vpn-research-vpc183-tun1,isakmp-vpn-research-vpc183-tun1,ipsec-prop-vpn-research-vpc183-tun1,ipsec-vpn-research-vpc183-tun1,170.140.76.183,169.254.250.216/30,169.254.251.248/30,20183,keyring-vpn-research-vpc183-tun2,isakmp-vpn-research-vpc183-tun2,ipsec-prop-vpn-research-vpc183-tun2,ipsec-vpn-research-vpc183-tun2,170.140.77.183,169.254.254.216/30,169.254.255.248/30,PROD",
            "184,10.66.110.0/23,10184,keyring-vpn-research-vpc184-tun1,isakmp-vpn-research-vpc184-tun1,ipsec-prop-vpn-research-vpc184-tun1,ipsec-vpn-research-vpc184-tun1,170.140.76.184,169.254.250.220/30,169.254.251.248/30,20184,keyring-vpn-research-vpc184-tun2,isakmp-vpn-research-vpc184-tun2,ipsec-prop-vpn-research-vpc184-tun2,ipsec-vpn-research-vpc184-tun2,170.140.77.184,169.254.254.220/30,169.254.255.248/30,PROD",
            "185,10.66.112.0/23,10185,keyring-vpn-research-vpc185-tun1,isakmp-vpn-research-vpc185-tun1,ipsec-prop-vpn-research-vpc185-tun1,ipsec-vpn-research-vpc185-tun1,170.140.76.185,169.254.250.224/30,169.254.251.248/30,20185,keyring-vpn-research-vpc185-tun2,isakmp-vpn-research-vpc185-tun2,ipsec-prop-vpn-research-vpc185-tun2,ipsec-vpn-research-vpc185-tun2,170.140.77.185,169.254.254.224/30,169.254.255.248/30,PROD",
            "186,10.66.114.0/23,10186,keyring-vpn-research-vpc186-tun1,isakmp-vpn-research-vpc186-tun1,ipsec-prop-vpn-research-vpc186-tun1,ipsec-vpn-research-vpc186-tun1,170.140.76.186,169.254.250.228/30,169.254.251.248/30,20186,keyring-vpn-research-vpc186-tun2,isakmp-vpn-research-vpc186-tun2,ipsec-prop-vpn-research-vpc186-tun2,ipsec-vpn-research-vpc186-tun2,170.140.77.186,169.254.254.228/30,169.254.255.248/30,PROD",
            "187,10.66.116.0/23,10187,keyring-vpn-research-vpc187-tun1,isakmp-vpn-research-vpc187-tun1,ipsec-prop-vpn-research-vpc187-tun1,ipsec-vpn-research-vpc187-tun1,170.140.76.187,169.254.250.232/30,169.254.251.248/30,20187,keyring-vpn-research-vpc187-tun2,isakmp-vpn-research-vpc187-tun2,ipsec-prop-vpn-research-vpc187-tun2,ipsec-vpn-research-vpc187-tun2,170.140.77.187,169.254.254.232/30,169.254.255.248/30,PROD",
            "188,10.66.118.0/23,10188,keyring-vpn-research-vpc188-tun1,isakmp-vpn-research-vpc188-tun1,ipsec-prop-vpn-research-vpc188-tun1,ipsec-vpn-research-vpc188-tun1,170.140.76.188,169.254.250.236/30,169.254.251.248/30,20188,keyring-vpn-research-vpc188-tun2,isakmp-vpn-research-vpc188-tun2,ipsec-prop-vpn-research-vpc188-tun2,ipsec-vpn-research-vpc188-tun2,170.140.77.188,169.254.254.236/30,169.254.255.248/30,PROD",
            "189,10.66.120.0/23,10189,keyring-vpn-research-vpc189-tun1,isakmp-vpn-research-vpc189-tun1,ipsec-prop-vpn-research-vpc189-tun1,ipsec-vpn-research-vpc189-tun1,170.140.76.189,169.254.250.240/30,169.254.251.248/30,20189,keyring-vpn-research-vpc189-tun2,isakmp-vpn-research-vpc189-tun2,ipsec-prop-vpn-research-vpc189-tun2,ipsec-vpn-research-vpc189-tun2,170.140.77.189,169.254.254.240/30,169.254.255.248/30,PROD",
            "190,10.66.122.0/23,10190,keyring-vpn-research-vpc190-tun1,isakmp-vpn-research-vpc190-tun1,ipsec-prop-vpn-research-vpc190-tun1,ipsec-vpn-research-vpc190-tun1,170.140.76.190,169.254.250.244/30,169.254.251.248/30,20190,keyring-vpn-research-vpc190-tun2,isakmp-vpn-research-vpc190-tun2,ipsec-prop-vpn-research-vpc190-tun2,ipsec-vpn-research-vpc190-tun2,170.140.77.190,169.254.254.244/30,169.254.255.248/30,PROD",
            "191,10.66.124.0/23,10191,keyring-vpn-research-vpc191-tun1,isakmp-vpn-research-vpc191-tun1,ipsec-prop-vpn-research-vpc191-tun1,ipsec-vpn-research-vpc191-tun1,170.140.76.191,169.254.250.248/30,169.254.251.248/30,20191,keyring-vpn-research-vpc191-tun2,isakmp-vpn-research-vpc191-tun2,ipsec-prop-vpn-research-vpc191-tun2,ipsec-vpn-research-vpc191-tun2,170.140.77.191,169.254.254.248/30,169.254.255.248/30,PROD",
            "192,10.66.126.0/23,10192,keyring-vpn-research-vpc192-tun1,isakmp-vpn-research-vpc192-tun1,ipsec-prop-vpn-research-vpc192-tun1,ipsec-vpn-research-vpc192-tun1,170.140.76.192,169.254.250.252/30,169.254.251.248/30,20192,keyring-vpn-research-vpc192-tun2,isakmp-vpn-research-vpc192-tun2,ipsec-prop-vpn-research-vpc192-tun2,ipsec-vpn-research-vpc192-tun2,170.140.77.192,169.254.254.252/30,169.254.255.248/30,PROD",
            "193,10.66.128.0/23,10193,keyring-vpn-research-vpc193-tun1,isakmp-vpn-research-vpc193-tun1,ipsec-prop-vpn-research-vpc193-tun1,ipsec-vpn-research-vpc193-tun1,170.140.76.193,169.254.251.0/30,169.254.251.248/30,20193,keyring-vpn-research-vpc193-tun2,isakmp-vpn-research-vpc193-tun2,ipsec-prop-vpn-research-vpc193-tun2,ipsec-vpn-research-vpc193-tun2,170.140.77.193,169.254.255.0/30,169.254.255.248/30,PROD",
            "194,10.66.130.0/23,10194,keyring-vpn-research-vpc194-tun1,isakmp-vpn-research-vpc194-tun1,ipsec-prop-vpn-research-vpc194-tun1,ipsec-vpn-research-vpc194-tun1,170.140.76.194,169.254.251.4/30,169.254.251.248/30,20194,keyring-vpn-research-vpc194-tun2,isakmp-vpn-research-vpc194-tun2,ipsec-prop-vpn-research-vpc194-tun2,ipsec-vpn-research-vpc194-tun2,170.140.77.194,169.254.255.4/30,169.254.255.248/30,PROD",
            "195,10.66.132.0/23,10195,keyring-vpn-research-vpc195-tun1,isakmp-vpn-research-vpc195-tun1,ipsec-prop-vpn-research-vpc195-tun1,ipsec-vpn-research-vpc195-tun1,170.140.76.195,169.254.251.8/30,169.254.251.248/30,20195,keyring-vpn-research-vpc195-tun2,isakmp-vpn-research-vpc195-tun2,ipsec-prop-vpn-research-vpc195-tun2,ipsec-vpn-research-vpc195-tun2,170.140.77.195,169.254.255.8/30,169.254.255.248/30,PROD",
            "196,10.66.134.0/23,10196,keyring-vpn-research-vpc196-tun1,isakmp-vpn-research-vpc196-tun1,ipsec-prop-vpn-research-vpc196-tun1,ipsec-vpn-research-vpc196-tun1,170.140.76.196,169.254.251.12/30,169.254.251.248/30,20196,keyring-vpn-research-vpc196-tun2,isakmp-vpn-research-vpc196-tun2,ipsec-prop-vpn-research-vpc196-tun2,ipsec-vpn-research-vpc196-tun2,170.140.77.196,169.254.255.12/30,169.254.255.248/30,PROD",
            "197,10.66.136.0/23,10197,keyring-vpn-research-vpc197-tun1,isakmp-vpn-research-vpc197-tun1,ipsec-prop-vpn-research-vpc197-tun1,ipsec-vpn-research-vpc197-tun1,170.140.76.197,169.254.251.16/30,169.254.251.248/30,20197,keyring-vpn-research-vpc197-tun2,isakmp-vpn-research-vpc197-tun2,ipsec-prop-vpn-research-vpc197-tun2,ipsec-vpn-research-vpc197-tun2,170.140.77.197,169.254.255.16/30,169.254.255.248/30,PROD",
            "198,10.66.138.0/23,10198,keyring-vpn-research-vpc198-tun1,isakmp-vpn-research-vpc198-tun1,ipsec-prop-vpn-research-vpc198-tun1,ipsec-vpn-research-vpc198-tun1,170.140.76.198,169.254.251.20/30,169.254.251.248/30,20198,keyring-vpn-research-vpc198-tun2,isakmp-vpn-research-vpc198-tun2,ipsec-prop-vpn-research-vpc198-tun2,ipsec-vpn-research-vpc198-tun2,170.140.77.198,169.254.255.20/30,169.254.255.248/30,PROD",
            "199,10.66.140.0/23,10199,keyring-vpn-research-vpc199-tun1,isakmp-vpn-research-vpc199-tun1,ipsec-prop-vpn-research-vpc199-tun1,ipsec-vpn-research-vpc199-tun1,170.140.76.199,169.254.251.24/30,169.254.251.248/30,20199,keyring-vpn-research-vpc199-tun2,isakmp-vpn-research-vpc199-tun2,ipsec-prop-vpn-research-vpc199-tun2,ipsec-vpn-research-vpc199-tun2,170.140.77.199,169.254.255.24/30,169.254.255.248/30,PROD",
            "200,10.66.142.0/23,10200,keyring-vpn-research-vpc200-tun1,isakmp-vpn-research-vpc200-tun1,ipsec-prop-vpn-research-vpc200-tun1,ipsec-vpn-research-vpc200-tun1,170.140.76.200,169.254.251.28/30,169.254.251.248/30,20200,keyring-vpn-research-vpc200-tun2,isakmp-vpn-research-vpc200-tun2,ipsec-prop-vpn-research-vpc200-tun2,ipsec-vpn-research-vpc200-tun2,170.140.77.200,169.254.255.28/30,169.254.255.248/30,PROD"
    };


}
