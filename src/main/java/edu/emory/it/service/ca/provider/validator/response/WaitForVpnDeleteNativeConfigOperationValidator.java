package edu.emory.it.service.ca.provider.validator.response;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import org.apache.logging.log4j.Logger;

public class WaitForVpnDeleteNativeConfigOperationValidator implements WaitForVpnOperationValidator {

    private final Logger logger;
    private final String tunnelNumber;

    public WaitForVpnDeleteNativeConfigOperationValidator(Logger logger, String tunnelNumber) {
        this.logger = logger;
        this.tunnelNumber = tunnelNumber;
    }

    /**
     * Note that there is state set in the native configuration that is not captured by the VpnConnection command
     * that we *could* validate, e.g. bgp description (router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/description)
     * which is set to the vpc id when assigned, and 'AVAILABLE' when deleted.
     *
     * v1.1 note:  The move to template-driven command hydration could be used to add validation here. The hydrated
     *             template, raw response and token map could be used for validation.
     * @param vpnResponseValidatorCommand
     * @return
     */
    @Override
    public boolean validate(VpnResponseValidatorCommand vpnResponseValidatorCommand) {

        VpnDeleteNativeConfigValidatorCommand vpnDeleteNativeConfigValidatorCommand = (VpnDeleteNativeConfigValidatorCommand) vpnResponseValidatorCommand;

        VpnConnection vpnConnectionFromRouter = vpnDeleteNativeConfigValidatorCommand.getVpnConnection();

        if (vpnConnectionFromRouter != null) {
            logger.warn(getLogtag() + "vpn connection from router exists - delete is still in progress");
        }

        return vpnConnectionFromRouter == null;
    }

    private String getLogtag() {
        return "[CiscoAsr" + tunnelNumber + "][" + this.getClass().getSimpleName() + "] ";
    }


}
