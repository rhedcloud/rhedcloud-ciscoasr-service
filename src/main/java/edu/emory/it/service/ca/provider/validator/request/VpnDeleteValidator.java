package edu.emory.it.service.ca.provider.validator.request;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;

public class VpnDeleteValidator extends VpnRequestValidatorCommand implements VpnRequestValidator {

    @Override
    public void validate() throws ProviderException
    {
        VpnConnection vpnConnection = super.getVpnConnection();

        StringBuilder builder = new StringBuilder();

        String vpcId = vpnConnection.getVpcId();
        String vpnId = vpnConnection.getVpnId();

        validateVpcId(vpcId, builder);

        validateVpnId(vpnId, builder);

        if (builder.length() > 0) {
            throw new ProviderException("FATAL: Invalid vpn connection\n" + builder.toString());
        }

    }

}
