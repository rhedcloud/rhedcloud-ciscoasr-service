package edu.emory.it.service.ca.provider.mapper._1662;

import edu.emory.it.service.ca.provider.mapper.AbstractVpnMapper;
import edu.emory.it.service.ca.provider.mapper.VpnMapper;

public class _1662GcpVpnMapper extends AbstractVpnMapper implements VpnMapper {
    // All default behavior in AbstractStaticNatMapper is 1662GCP
}
