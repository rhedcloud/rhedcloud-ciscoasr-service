package edu.emory.it.service.ca.provider.mapper;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import org.openeai.config.AppConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public abstract class AbstractStaticNatMapper implements StaticNatMapper {

    protected AppConfig appConfig;

    protected String createStaticNatTemplate;

    protected String deleteStaticNatTemplate;

    protected String queryStaticNatTemplate;

    private String xpathLocalIp = "ip/nat/inside/source/static/nat-static-transport-list/local-ip";
    private String xpathGlobalIp = "ip/nat/inside/source/static/nat-static-transport-list/global-ip";
    private String xpathNativeTransportList = "native/ip/nat/inside/source/static/nat-static-transport-list";
    private String xpathTransportList = "ip/nat/inside/source/static/nat-static-transport-list";

    public void init(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    public void initializeTemplates(Properties templates) {

        createStaticNatTemplate = templates.getProperty("createStaticNatTemplate");
        deleteStaticNatTemplate = templates.getProperty("deleteStaticNatTemplate");
        queryStaticNatTemplate = templates.getProperty("queryStaticNatTemplate");

        StringBuilder builder = new StringBuilder();
        if (createStaticNatTemplate == null) {
            builder.append(" createStaticNatTemplate");
        }
        if (deleteStaticNatTemplate == null) {
            builder.append(" deleteStaticNatTemplate");
        }
        if (queryStaticNatTemplate == null) {
            builder.append(" queryStaticNatTemplate");
        }

        if (builder.length() > 0) {
            throw new RuntimeException("Following required static nat templates are missing from configuration:" + builder.toString());
        }
    }

    public String getCreateStaticNatTemplate() {
        return createStaticNatTemplate;
    }

    public String getDeleteStaticNatTemplate() {
        return deleteStaticNatTemplate;
    }

    public String getQueryStaticNatTemplate() {
        return queryStaticNatTemplate;
    }

    public Element buildCreateConfig(String staticNatConfigTemplateAsXml, String globalIpAddress, String localIpAddress) throws ProviderException {

        Element nativeFromXml;

        try {
            XMLParser xmlParser = new XMLParser();

            nativeFromXml = xmlParser.parse(staticNatConfigTemplateAsXml);

            nativeFromXml.setValue(xpathLocalIp(), localIpAddress);
            nativeFromXml.setValue(xpathGlobalIp(), globalIpAddress);
            nativeFromXml.markCreate(xpathTransportList());

        } catch (JNCException e) {
            String errMsg = "Exception while attempting to build create config element during static nat add";
            throw new ProviderException(errMsg, e);
        }

        return nativeFromXml;
    }

    public Element buildDeleteConfig(String staticNatConfigTemplate, String globalIpAddress, String localIpAddress) throws ProviderException {

        Element nativeFromXml;

        try {
            XMLParser xmlParser = new XMLParser();

            nativeFromXml = xmlParser.parse(staticNatConfigTemplate);

            nativeFromXml.setValue(xpathLocalIp(), localIpAddress);
            nativeFromXml.setValue(xpathGlobalIp(), globalIpAddress);
            nativeFromXml.markDelete(xpathTransportList());

        } catch (JNCException e) {
            String errMsg = "Exception while attempting to build create config element during static nat add";
            throw new ProviderException(errMsg, e);
        }

        return nativeFromXml;
    }

    public Element buildQueryConfig(String staticNatConfigTemplate, String globalIpAddress) throws ProviderException {

        Element nativeFromXml;

        try {
            XMLParser xmlParser = new XMLParser();

            nativeFromXml = xmlParser.parse(staticNatConfigTemplate);

            nativeFromXml.setValue(xpathGlobalIp(), globalIpAddress);
            nativeFromXml.setValue(xpathLocalIp(), "");

        } catch (JNCException e) {
            String errMsg = "Exception while attempting to build create config element during static nat add";
            throw new ProviderException(errMsg, e);
        }

        return nativeFromXml;
    }

    public String getExistingLocalIp(NodeSet config) throws ProviderException {
        String configuredLocalIpAddress;
        try {
            Element existingElement = config.get(0);
            Element existingLocalIp = existingElement.getFirst(xpathLocalIp());
            configuredLocalIpAddress = existingLocalIp.getValue().toString();
        } catch (Exception e) {
            throw new ProviderException("Failed to extract local-ip from config", e);
        }

        return configuredLocalIpAddress;
    }

    /**
     * Build xpath string for YANG version specific static nat filter using list of ip masks.
     * @param staticNatIpMasks - list of ip masks used to build xpath filter
     * @return - xpath for given ip masks
     */
    public String getStaticIpMasksFilter(List<String> staticNatIpMasks) {
        StringBuilder builder = new StringBuilder(xpathNativeTransportList() + "[");
        staticNatIpMasks.forEach(m -> {
//            infoBuilder.append("\n    " + m);
            if (builder.indexOf("starts-with") != -1) {
                builder.append(" or ");
            }
            builder.append("starts-with(global-ip, '" + m + "')");
        });
        builder.append("]");

        return builder.toString();
    }

    /**
     * Mine the static nat entries from the given results (NodeSet).
     *
     * Note that the "xpathTransportList()" is unique per YANG version.
     *
     * @param staticNatNodeSet - results from a "get" of the router state for a set of ip masks
     * @param errorBuilder - errors are written into this StringBuilder for caller to log
     * @return - List of StaticNat objects mined out of NodeSet
     * @throws ProviderException - any unhandled or config exceptions thrown are wrapped and thrown
     */
    public List<StaticNat> getStaticNatTransportList(NodeSet staticNatNodeSet, StringBuilder errorBuilder) throws ProviderException {
        List<StaticNat> staticNats = new ArrayList<>();

        try {
            StaticNat staticNat = AppConfigUtil.getStaticNatFromConfiguration(appConfig);

            String xpath = xpathTransportList();
            NodeSet result = staticNatNodeSet.get(xpath);
            result.forEach(sn -> {
                try {
                    StaticNat aStaticNat = (StaticNat) staticNat.clone();
                    aStaticNat.setPublicIp((String) sn.getFirst("global-ip").value);
                    aStaticNat.setPrivateIp((String) sn.getFirst("local-ip").value);
                    staticNats.add(aStaticNat);
                } catch (Exception e) {
                    errorBuilder.append(sn.toXMLString() + "\n");
                }
            });
        } catch (ProviderException e) {
            throw new ProviderException("Failed to get StaticNat from app config: " + e.getMessage());
        } catch (Exception e) {
            throw new ProviderException("Failed to get static nat transport list", e);
        }
        return staticNats;
    }

    // ####################### xpath accessors #################################

    @Override
    public String xpathLocalIp() {
        return xpathLocalIp;
    }

    @Override
    public String xpathGlobalIp() {
        return xpathGlobalIp;
    }

    @Override
    public String xpathNativeTransportList() {
        return xpathNativeTransportList;
    }

    @Override
    public String xpathTransportList() {
        return xpathTransportList;
    }
}
