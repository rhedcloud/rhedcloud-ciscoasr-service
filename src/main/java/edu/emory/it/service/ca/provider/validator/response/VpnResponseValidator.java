package edu.emory.it.service.ca.provider.validator.response;

public interface VpnResponseValidator {

    public boolean expectResult();

}
