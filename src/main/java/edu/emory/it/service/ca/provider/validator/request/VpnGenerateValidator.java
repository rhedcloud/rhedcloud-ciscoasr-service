package edu.emory.it.service.ca.provider.validator.request;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile;
import edu.emory.moa.objects.resources.v1_0.RemoteVpnConnectionInfo;
import edu.emory.moa.objects.resources.v1_0.RemoteVpnTunnel;
import edu.emory.moa.objects.resources.v1_0.TunnelProfile;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;

import java.util.List;

public class VpnGenerateValidator extends VpnRequestValidatorCommand implements VpnRequestValidator {

    @Override
    public void validate() throws ProviderException {
        StringBuilder builder = new StringBuilder();

        VpnConnectionRequisition vpnConnectionRequisition = super.getVpnConnectionRequisition();

        // Best we can do:  Use tunnelId to check consistency of remaining values.
        String vpcId = vpnConnectionRequisition.getOwnerId();
        String vpnId = vpnConnectionRequisition.getVpnConnectionProfile().getVpnConnectionProfileId();

        validateVpcId(vpcId, builder);

        validateVpnId(vpnId, builder);

        List<RemoteVpnConnectionInfo> remoteVpnConnectionInfoList = vpnConnectionRequisition.getRemoteVpnConnectionInfo();
        VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();
        List<TunnelProfile> tunnelProfileList = vpnConnectionProfile.getTunnelProfile();

        // This should always be a list of 1 - router specific info split by sender.
        if (remoteVpnConnectionInfoList.size() != 1) {
            builder.append("FATAL: No remote vpn connection info in requisition\n");
        }
        if (tunnelProfileList.size() == 0) {
            builder.append("FATAL: No tunnel profiles or remote vpn connection infos in requisition\n");
        }
        if (tunnelProfileList.size() > 0){
            for (int n = 0; n < tunnelProfileList.size(); n++) {
                TunnelProfile tunnelProfile = tunnelProfileList.get(n);
                if (tunnelProfile == null) {
                    builder.append("FATAL: No tunnel profile in vpn connection requisition for tunnel index: " + n + "\n");
                }
            }
        }
        if (remoteVpnConnectionInfoList.size() > 0) {
            for (int n = 0; n < remoteVpnConnectionInfoList.size(); n++) {
                RemoteVpnConnectionInfo remoteVpnConnectionInfo = remoteVpnConnectionInfoList.get(n);
                List<RemoteVpnTunnel> remoteVpnTunnels = remoteVpnConnectionInfo.getRemoteVpnTunnel();
                if (remoteVpnTunnels == null || remoteVpnTunnels.size() == 0) {
                    builder.append("FATAL: No remote vpn tunnels in remote vpn connection info for tunnel index: " + n + "\n");
                }
            }
        }


        if (builder.length() > 0) {
            throw new ProviderException("FATAL: Invalid vpn connection requisition\n" + builder.toString());
        }
    }

}
