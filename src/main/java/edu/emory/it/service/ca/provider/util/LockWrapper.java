package edu.emory.it.service.ca.provider.util;

import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.utils.lock.Key;
import org.openeai.utils.lock.Lock;
import org.openeai.utils.lock.LockAlreadySetException;
import org.openeai.utils.lock.LockException;

public class LockWrapper {

    private static String LOGTAG;
    private static final Logger logger = OpenEaiObject.logger;

    private final Lock lock;
    private Key key;
    private Integer obtainLockMaxWaitTime;
    private Integer obtainLockRetrySleepInterval;

    public LockWrapper() {
        lock = null;
        key = null;
        LOGTAG = "[LockWrapper] ";
    }

    public LockWrapper(Lock commandLock, Integer obtainLockMaxWaitTime, Integer obtainLockRetrySleepInterval, String logtag) {
        lock = commandLock;
        key = null;
        this.obtainLockMaxWaitTime = obtainLockMaxWaitTime;
        this.obtainLockRetrySleepInterval = obtainLockRetrySleepInterval;
        LOGTAG = logtag.trim() + "[LockWrapper] ";
    }

    public void acquireLock() throws LockException {
        int totalTime = 0;
        do {
            try {
                key = lock.set();
                if (key != null) {
                    break;
                } else {
                    totalTime += sleep();
                }
            } catch (LockAlreadySetException e) {
                if (totalTime < obtainLockMaxWaitTime) {
                    String warnMsg = "Unable to acquire command lock - in use. Retrying after " + getTimeComment(totalTime) + "...";
                    logger.warn(getLogtag() + warnMsg);
                    totalTime += sleep();
                } else {
                    String errMsg = "Unable to acquire command lock after " + getTimeComment(obtainLockMaxWaitTime) + ". Another application"
                            + " may be archiving sessions at this time. Cannot run the network ops command "
                            + "at this time.";
                    logger.info(getLogtag() + errMsg);
                    throw new LockException(errMsg, e);
                }
            } catch (LockException e) {
                // An error occurred setting the lock. Log it and throw an exception.
                String errMsg = "An error ocurred setting the network ops command lock. The " +
                        "exception is: " + e.getMessage();
                logger.fatal(getLogtag() + errMsg);
                throw new LockException(errMsg, e);
            }
        } while (key == null && totalTime <= obtainLockMaxWaitTime);

        if (key != null) {
            logger.info(getLogtag() + "lock acquired. key: " + key.getValue());
        } else if (key == null || totalTime > obtainLockMaxWaitTime) {
            throw new LockException("Failed to acquire lock");
        }
    }

    public void release() throws LockException {
        if (lock != null && lock.isSet()) {
            logger.info(getLogtag() + "releasing lock...");
            if (key != null) {
                lock.release(key);
                logger.info(getLogtag() + "lock " + key.getValue() + " released successfully");
            } else {
                logger.error(getLogtag() + "Lock not acquired - skipping release");
            }
        } else if (lock == null) {
            logger.warn(getLogtag() + "lock is null");
        } else if (!lock.isSet()) {
            logger.warn(getLogtag() + "lock is not set");
        }
    }

    public Lock getLock() {
        return lock;
    }

    public Key getKey() {
        return key;
    }

    private int sleep() throws LockException {
        try {
            Thread.sleep(obtainLockRetrySleepInterval);
        } catch (InterruptedException e) {
            String errMsg = "Command lock retry sleep interrupted";
            logger.error(getLogtag() + errMsg, e);
            throw new LockException(errMsg, e);
        }
        return obtainLockRetrySleepInterval;
    }

    private String getTimeComment(int timeInMillis) {
        String timeComment;

        // Milliseconds per hour: 60 * 60 * 1000
        long timeInHours = timeInMillis / 3600000;
        long timeInMinutes = timeInMillis / 60000 % 60;
        long timeInSeconds = timeInMillis / 1000 % 60;
        long millisecondRemainder = timeInMillis % 1000;

        String hourPart = "";
        String minutePart = "";
        String secondPart = "";
        String millisecondPart = "";

        if (timeInHours > 0) {
            hourPart = timeInHours + (timeInHours == 1 ? " hour" : " hours");
        }
        if (timeInMinutes > 0) {
            minutePart = timeInMinutes + (timeInMinutes == 1 ? " minute" : " minutes");
        }
        if (timeInSeconds > 0) {
            secondPart = timeInSeconds + (timeInSeconds == 1 ? " second" : " seconds");
        }
        if (millisecondRemainder > 0) {
            millisecondPart = millisecondRemainder + (millisecondRemainder == 1 ? " millisecond" : " milliseconds");
        }

        timeComment = hourPart;
        timeComment += (!(timeComment.isEmpty() || minutePart.isEmpty()) ? ", " : "") + minutePart;
        timeComment += (!(timeComment.isEmpty() || secondPart.isEmpty()) ? ", " : "") + secondPart;
        timeComment += (!(timeComment.isEmpty() || millisecondPart.isEmpty()) ? ", " : "") + millisecondPart;
        timeComment = timeComment.replaceFirst("[\\s\\,]+$", "");
        return timeComment;
    }

    private String getLogtag() {
        return LOGTAG;
    }
}
