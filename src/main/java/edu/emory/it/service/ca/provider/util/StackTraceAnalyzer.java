package edu.emory.it.service.ca.provider.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class StackTraceAnalyzer {

    public boolean isErrorPresent(String errorString, Exception e) {

        ByteArrayOutputStream os = new  ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        e.printStackTrace(ps);
        Collection<String> lineCollection = Arrays.asList(os.toString().split("\n"));
        Long count = lineCollection.stream()
                .filter(l -> l.contains(errorString))
                .collect(Collectors.counting());

        return count > 0;
    }
    public boolean isErrorPresent(String[] errorStrings, Exception e) {

        ByteArrayOutputStream os = new  ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        e.printStackTrace(ps);
        Collection<String> lineCollection = Arrays.asList(os.toString().split("\n"));
        Long count = lineCollection.stream()
                .filter(l -> {
                    boolean found = false;
                    for (int n =0; n < errorStrings.length; n++) {
                        if (l.contains(errorStrings[n])) {
                            found = true;
                            break;
                        }
                    }
                    return found;
                })
                .collect(Collectors.counting());

        return count > 0;
    }
    public boolean isErrorPresentIgnoreCase(String errorString, Exception e) {

        ByteArrayOutputStream os = new  ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        e.printStackTrace(ps);
        Collection<String> lineCollection = Arrays.asList(os.toString().split("\n"));
        final String errorStringLowerCase = errorString.toLowerCase();
        Long count = lineCollection.stream()
                .filter(l -> l.toLowerCase().contains(errorStringLowerCase))
                .collect(Collectors.counting());

        return count > 0;
    }

    public static String getStackTrace(Throwable t) {
        ByteArrayOutputStream os = new  ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        t.printStackTrace(ps);
        return t.toString();
    }

}
