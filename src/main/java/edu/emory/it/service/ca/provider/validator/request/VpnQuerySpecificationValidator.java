package edu.emory.it.service.ca.provider.validator.request;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;

public class VpnQuerySpecificationValidator extends VpnRequestValidatorCommand implements VpnRequestValidator {

    @Override
    public void validate() throws ProviderException {

        StringBuilder builder = new StringBuilder();

        VpnConnectionQuerySpecification vpnConnectionQuerySpecification = super.getVpnConnectionQuerySpecification();

//        String vpcId = vpnConnectionQuerySpecification.getVpcId();
        String vpnId = vpnConnectionQuerySpecification.getVpnId();

        // validateVpcId(vpcId, builder);

        validateVpnId(vpnId, builder);

        if (builder.length() > 0) {
            throw new ProviderException("FATAL: Invalid vpn connection query specification\n" + builder.toString());
        }
    }
}
