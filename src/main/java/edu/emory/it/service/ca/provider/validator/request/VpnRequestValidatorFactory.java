package edu.emory.it.service.ca.provider.validator.request;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.openeai.moa.XmlEnterpriseObject;

public class VpnRequestValidatorFactory {

    public static VpnRequestValidator create(XmlEnterpriseObject request) throws ProviderException {

        VpnRequestValidator command;
        switch (request.getClass().getSimpleName()) {
            case "VpnConnectionQuerySpecification":
                command = new VpnQuerySpecificationValidator();
                ((VpnQuerySpecificationValidator) command).setVpnConnectionQuerySpecification((VpnConnectionQuerySpecification) request);
                break;
            case "VpnConnection":
                command = new VpnDeleteValidator();
                ((VpnDeleteValidator)command).setVpnConnection((VpnConnection)request);
                break;
            case "VpnConnectionRequisition":
                command = new VpnGenerateValidator();
                ((VpnGenerateValidator)command).setVpnConnectionRequisition((VpnConnectionRequisition)request);
                break;
            default:
                throw new ProviderException("Invalid request class: " + request.getClass().getSimpleName());
        }

        return command;
    }
}
