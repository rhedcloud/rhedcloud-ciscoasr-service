package edu.emory.it.service.ca.provider.util;

public abstract class Dump {

    private final String indentAmount = "    ";
    protected String indent = "";

    protected void increaseIndent() {
        indent += indentAmount;
    }

    protected void decreaseIndent() {
        int amount = indentAmount.length();
        if (amount < indent.length()) {
            indent = indent.substring(amount);
        } else {
            indent = "";
        }
    }
    protected void dumpHeader(String header, StringBuilder builder) {

        String template = "####################################################################################################";
        template = template.substring(indent.length());
        int templateLength = template.length() ;

        header = " " + header + " ";
        int headerPlusSpacesLength = header.length();
        if (headerPlusSpacesLength > templateLength) {
            builder.append(indent + header + "\n");
        } else {
            int halfLength = ((templateLength / 2 + templateLength % 2) - (headerPlusSpacesLength / 2 + headerPlusSpacesLength % 2)) - indent.length() / 2;
            byte[] bytes = template.getBytes();
            int templateIndex = halfLength;
            for (int n = 0; n < header.length(); n++) {
                bytes[templateIndex++] = (byte)header.charAt(n);
            }

            builder.append(indent + new String(bytes) + "\n");
        }
    }
    protected void dump(String value, StringBuilder builder) {
        builder.append(indent + value + "\n");
    }

    protected void dumpNoValues(String message, StringBuilder builder) {
        builder.append(indent + ">>>>>> NO " + message + " VALUES <<<<<<\n");
    }


}
