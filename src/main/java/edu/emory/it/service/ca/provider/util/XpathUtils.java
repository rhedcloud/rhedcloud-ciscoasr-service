package edu.emory.it.service.ca.provider.util;

import com.tailf.jnc.NetconfSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XpathUtils {

    public XpathUtils() {
    }

    /**
     * Build an or-ed text comparison clause with all possible IP address values for the range passed in.  The
     * ipAddressRange value passed in is "extended" ip address format allowing for octet ranges.  Only the 2 least
     * significant octets are supported. The format of the ipAddressRange value is:
     *
     *    x.x.n1-m1.n2-m2
     *
     * where n1 < m1 and n2 < m3, and {n1, m1, n2, m3} in the set of {0..255}
     *
     * This can potentially produce 64K elements in the clause, for the following range:
     *
     *    x.x.0-255.0-255
     *
     * @param ipAddressRange - ip range value to process
     * @return
     * @throws com.sun.javaws.exceptions.InvalidArgumentException - for invalid ranges or ranges in the 2 most
     *                                                              significant octets
     */
    public String buildFilterClause(String ipAddressRange) {
        StringBuilder builder = new StringBuilder();

        // TODO: Create an xpath filter with or'd together ip addresses from expandIpRange() call.
        List<String> ipAddresses = expandIpRanges(ipAddressRange);
        // etc...

        return builder.toString();
    }

    private List<String> expandIpRanges(String ipAddressRange) {
        List<String> ipAddresses = new ArrayList<>();

        final Pattern ipRangePattern = Pattern.compile("((\\d+\\-\\d+)|(\\d+))\\.((\\d+\\-\\d+)|(\\d+))\\.((\\d+\\-\\d+)|(\\d+))\\.((\\d+\\-\\d+)|(\\d+))");
        final Matcher ipRangeMatcher = ipRangePattern.matcher(ipAddressRange);

        String prefix;
        Map<Integer, String> ranges = new HashMap();

        if (ipRangeMatcher.matches()) {

            for (int n = 1; n <= ipRangeMatcher.groupCount(); n++) {
                System.out.println("Group: " + n + " - value: " + ipRangeMatcher.group(n));
            }
            prefix = ipRangeMatcher.group(1) + "." + ipRangeMatcher.group(2) + ".";
            ranges.put(1, ipRangeMatcher.group(1));
            ranges.put(2, ipRangeMatcher.group(4));
            ranges.put(3, ipRangeMatcher.group(7));
            ranges.put(4, ipRangeMatcher.group(10));

        } else {
            throw new IllegalArgumentException("Invalid ip address range: " + ipAddressRange);
        }

        ipAddresses = buildRange(ranges.get(3), ranges.get(4), prefix);

        return ipAddresses;
    }

    private List<String> buildRange(String ipRange3, String ipRange4, String ipPrefix) {
        List<String> ipAddresses = new ArrayList<>();
        String[] rangeParts;

        OctetRange[] ranges = {
                new OctetRange(1),
                new OctetRange(2),
                new OctetRange(3),
                new OctetRange(4)
        };


        OctetRange octetRange3 = new OctetRange(3);
        OctetRange octetRange4 = new OctetRange(4);

        StringBuilder errors = new StringBuilder();

        rangeParts = ipRange3.split("\\-");

        if (rangeParts.length == 2) {

            octetRange3.start = Integer.valueOf(rangeParts[0]);
            octetRange3.end = Integer.valueOf(rangeParts[1]);

            errors.append(octetRange3.validate());
        } else {
            octetRange3.start = Integer.valueOf(ipRange3);
            octetRange3.end = Integer.valueOf(ipRange3);
        }

        rangeParts = ipRange4.split("\\-");
        if (rangeParts.length == 2) {

            octetRange4.start = Integer.valueOf(rangeParts[0]);
            octetRange4.end = Integer.valueOf(rangeParts[1]);

            errors.append(octetRange4.validate());

        } else {
            octetRange4.start = Integer.valueOf(ipRange4);
            octetRange4.end = Integer.valueOf(ipRange4);
        }

        if (errors.length() > 0) {
            throw new IllegalArgumentException("Invalid range values:\n" + errors.toString());
        }

        if (!ipPrefix.endsWith(".")) {
            ipPrefix += ".";
        }

        for (Integer n = octetRange3.start; n <= octetRange3.end; n++) {
            for (Integer m = octetRange4.start; m <= octetRange4.end; m++) {
                String ipAddress = ipPrefix + n + "." + m;
                ipAddresses.add(ipAddress);
            }
        }
        return ipAddresses;
    }

    private class OctetRange {

        final Integer octetNumber;
        Integer start = 0;
        Integer end = 255;

        public OctetRange(Integer octetNumber){
            this.octetNumber = octetNumber;
        }

        String validate() {
            StringBuilder errors = new StringBuilder();
            if (start > 255) {
                errors.append("\n    Range " + octetNumber + " start value exceeds 255");
            }
            if (end > 255) {
                errors.append("\n    Range " + octetNumber + " end value exceeds 255");
            }
            if (start >= end) {
                errors.append("\n    Range " + octetNumber + " start value is greater than or equal to end value");
            }

            return errors.toString();
        }
    }

    public static void main(String[] args) {
        XpathUtils utils = new XpathUtils();

        List<String> ipAddresses = utils.expandIpRanges("1.2.100-245.13");

        System.out.println("Number of ip addresses: " + ipAddresses.size());
    }
}
