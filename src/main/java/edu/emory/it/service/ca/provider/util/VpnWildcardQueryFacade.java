package edu.emory.it.service.ca.provider.util;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.mapper.VpnMapper;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VpnWildcardQueryFacade {

    private static final Logger logger = OpenEaiObject.logger;

    private final NetconfSession session;
    private final VpnMapper mapper;
    private final String logtag;

    public VpnWildcardQueryFacade(NetconfSession session, VpnMapper mapper, String logtag) {
        this.session = session;
        this.mapper = mapper;
        this.logtag = logtag;
    }

    public List<VpnConnection> performWildcardQuery(Map<String, Element> resultMap, AppConfig appConfig, String routerNumber) throws ProviderException {
        long totalQueryTime;

        NodeSet configuredTunnels;
        ElementUtils elementUtils = new ElementUtils();

        List<VpnConnection> vpnConnections = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        long time = startTime;

        // This is the query that blew up on the PROD router.  We're
        // "native/interface/Tunnel[starts-with(name, '${routerNumber}')]/../Tunnel[starts-with(description, '${tunnelPrefix}')]";
        // "native/interface/Tunnel/description[text()[contains(.,'vpc-')]]/..";

        String addTemplate = mapper.getTemplate("addTemplate");
        Element addFromXml;
        try {
            XMLParser xmlParser = new XMLParser();
            addFromXml = xmlParser.parse(addTemplate);
        } catch (JNCException e) {
            throw new ProviderException(getLogtag() + "Failed to parse add template", e);
        }
        String profilePrefix = getPrefix(addFromXml, mapper.xpathInterfaceTunnelTunnelProtectionIpsecProfile());

        String activeTunnelFilter = mapper.xpathActiveTunnelFilter().replace("${prefix}", profilePrefix);

//        activeTunnelFilter = "native/interface/Tunnel/description[text()[not(contains(., '(AVAILABLE)'))]]/..";
        try {
//            configuredTunnels = getWithFilter(activeTunnelFilter, session);
            configuredTunnels = rpcGetWithFilter(activeTunnelFilter, session);

        } catch (Exception e) {
            String errMsg = "Failed to query router " + routerNumber + " for native interfaces";
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg, e);
        }

        try {
            //##############################################################################
            // Okay, now we have a list of all active tunnels (native/interface/Tunnel)
            // Now, here are the mappings from tunnels to other native sections, bgp state and interfaces-state.
            // This is the pivot point for wildcard queries.
            //##############################################################################
            if (configuredTunnels == null || configuredTunnels.size() == 0) {
                logger.info(getLogtag() + "*** NO ACTIVE TUNNELS FOUND ON " + routerNumber + " ***");
            } else {
                configuredTunnels = configuredTunnels.get(mapper.xpathIntefaceTunnel());
                List<Element> activeTunnels = new ArrayList<>(configuredTunnels);

                // Gather as much info as possible from each configured tunnel for pivot to each section.
                for (Element tunnel : activeTunnels) {
                    try {

                        StringBuilder errorBuilder = new StringBuilder();

                        String interfaceTunnelName = ElementUtils.safeGetValue(tunnel, "name");
                        String interfaceTunnelDescription = elementUtils.safeGetValue(tunnel, "description");
                        String msg = "========> active tunnel:";
                        msg += "\n    path:" + tunnel.tagpath().toString();
                        msg += "\n    name:" + interfaceTunnelName;
                        msg += "\n    desc:" + interfaceTunnelDescription;

//                        logger.info(msg);

                        // TODO: Local tunnels? Probably passed in.
                        Integer localTunnelNumberZeroBased = 0;
                        String tunnelSuffix = RouterUtil.getTunnelSuffix(routerNumber, localTunnelNumberZeroBased);
                        String tunnelName = interfaceTunnelName;
                        Integer vpnId = mapper.getVpnIdFromTunnelName(tunnelName);
                        String paddedVpnId = mapper.buildPaddedVpnId(vpnId);
                        String vpcId = mapper.getVpcIdFromTunnelDescription(interfaceTunnelDescription);

                        Map<String, String> valuesMap = new HashMap<>();

                        // We're only interested in tunnel protection profile (see add template), so we only need to
                        // provide padded vpnId and tunnel number.
                        valuesMap.put(mapper.getSubstitutionToken("paddedVpnId"), paddedVpnId);
                        valuesMap.put(mapper.getSubstitutionToken("tunnelNumber"), routerNumber);
                        valuesMap.put(mapper.getSubstitutionToken("tunnelSuffix"), tunnelSuffix);

                        // pathValueMap will also be hydrated my the call to hydrateTemplate - any substitutions that
                        // occur using the valuesMap will be "recorded" in the pathValueMap, with the path to each template
                        // associated with the hydrated values.  It's a more direct way of getting hydrated values.
                        Map<String, String> pathValueMap = new HashMap<>();
                        Element hydratedAddTemplate = mapper.hydrateTemplate(valuesMap, "queryTemplate", pathValueMap);

                        // Values for crypto keyring, ipsec profile and ipsec transform-set are composite values
                        // and must be obtained from the hydrated template.
                        String cryptoKeyringNameHydrated = elementUtils.safeGetValue(hydratedAddTemplate, mapper.xpathNativeCryptoKeyringName());
                        String cryptoIpsecProfileHydrated = elementUtils.safeGetValue(hydratedAddTemplate, mapper.xpathNativeCryptoIpsecProfileName());
                        String cryptoIpsecTransformsetTagHydrated = elementUtils.safeGetValue(hydratedAddTemplate, mapper.xpathNativeCryptoIpsecTransformsetTag());

                        Map<String, String> hydratedValuesMap = new HashMap<>();
                        hydratedValuesMap.put(mapper.xpathNativeCryptoKeyringName(), cryptoKeyringNameHydrated);
                        hydratedValuesMap.put(mapper.xpathNativeCryptoIpsecProfileName(), cryptoIpsecProfileHydrated);
                        hydratedValuesMap.put(mapper.xpathNativeCryptoIpsecTransformsetTag(), cryptoIpsecTransformsetTagHydrated);

                        validateHydratedValues(hydratedValuesMap);

                        //###############################################################################################################################
                        // Override CSP specific pivot queries with generic.
                        //###############################################################################################################################
//                        String cryptoSectionDiscriminator = "-vpc${VpnId-Padded}-tun${TunnelNumber}";
//                        cryptoSectionDiscriminator = cryptoSectionDiscriminator.replace("${VpnId-Padded}", paddedVpnId).replace("${TunnelNumber}", tunnelSuffix);
//
//                        cryptoIpsecProfileHydrated = cryptoSectionDiscriminator;
//                        cryptoKeyringNameHydrated = cryptoSectionDiscriminator;
//                        cryptoIpsecTransformsetTagHydrated = cryptoSectionDiscriminator;
                        //###############################################################################################################################

                        // Pivot point queries - a query filter for each active section required to build a VpnConnection.
                        String activeNativeCryptoKeyringFilter = mapper.xpathActiveNativeCryptoKeyringFilter(cryptoKeyringNameHydrated);
                        String activeBgpStateFilter = mapper.xpathActiveBgpStateFilter(vpcId);
                        String activeNativeInterfaceFilter = mapper.xpathActiveNativeInterfaceFilter(interfaceTunnelName);
                        String activeIsakmpProfileFilter = mapper.xpathActiveIsakmpProfileFilter(vpcId);
                        String activeIpsecProfileFilter = mapper.xpathActiveIpsecProfileFilter(cryptoIpsecProfileHydrated);
                        String activeInterfacesStateFilter = mapper.xpathActiveInterfacesStateFilter(interfaceTunnelName);
                        String activeIpsecTransformsetFilter = mapper.xpathActiveIpsecTransformsetFilter(cryptoIpsecTransformsetTagHydrated);

                        //###############################################################################################################################
                        // Override CSP specific pivot queries with generic.
                        //###############################################################################################################################
//                        activeNativeCryptoKeyringFilter = "native/crypto/keyring/name[text()[contains(., '${Value}')]]/..".replace("${Value}", cryptoSectionDiscriminator);
//                        activeIpsecProfileFilter = "native/crypto/ipsec/profile/name[text()[contains(., '${Value}')]]/..".replace("${Value}", cryptoSectionDiscriminator);
//                        activeIpsecTransformsetFilter = "native/crypto/ipsec/transform-set/tag[text()[contains(., '${Value}')]]/..".replace("${Value}", cryptoSectionDiscriminator);

                        // If neighbor id is needed, use this filter:
                        // String activeRouterBgpDesciptionFilter = mapper.xpathActiveRouterBgpDesciptionFilter(vpcId);

                        NodeSet configuredNatives = null;
                        try {
                            configuredNatives = rpcGetWithFilter(activeNativeCryptoKeyringFilter, session);

                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for native interfaces";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredNatives != null && configuredNatives.size() == 1) {
                            Element cryptoElement = configuredNatives.get(0);
                            String xpath = mapper.xpathCryptoKeyring();
                            resultMap.put(xpath, cryptoElement);
                        } else {
                            errorBuilder.append("\n   Missing crypto keyring section for vpc id: " + vpcId);
                        }

                        NodeSet configuredNativeInterface = null;
                        try {
                            configuredNativeInterface = rpcGetWithFilter(activeNativeInterfaceFilter, session);

                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for active isakmp profile";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredNativeInterface != null && configuredNativeInterface.size() == 1) {
                            Element nativeInterfaceElement = configuredNativeInterface.get(0);
                            String xpath = mapper.xpathNativeInterface();
                            resultMap.put(xpath, nativeInterfaceElement);

                        } else  {
                            errorBuilder.append("\n   Missing native interface section for tunnel id: " + interfaceTunnelName);
                        }

                        NodeSet configuredBgpStateData = null;
                        try {
                            configuredBgpStateData = rpcGetWithFilter(activeBgpStateFilter, session);
                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for bgp state data";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredBgpStateData != null && configuredBgpStateData.size() == 1) {
                            Element bgpStateDataElement = configuredBgpStateData.get(0);
                            String xpath = mapper.xpathBgpStateData();
                            resultMap.put(xpath, bgpStateDataElement);
                        } else {
                            errorBuilder.append("\n   Missing bgp state data section for vpc id: " + vpcId);
                        }

                        NodeSet configuredCryptoIsakmpProfile = null;
                        try {
                            configuredCryptoIsakmpProfile = rpcGetWithFilter(activeIsakmpProfileFilter, session);

                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for active isakmp profile";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredCryptoIsakmpProfile != null && configuredCryptoIsakmpProfile.size() == 1) {
                            Element cryptoIsakmpProfileElement = configuredCryptoIsakmpProfile.get(0);
                            String xpath = mapper.xpathCryptoIsakmpProfile();
                            resultMap.put(xpath, cryptoIsakmpProfileElement);
                        } else  {
                            errorBuilder.append("\n   Missing crypto isakmp profile section for vpc id: " + vpcId);
                        }

                        NodeSet configuredCryptoIpsecProfile = null;
                        try {
                            configuredCryptoIpsecProfile = rpcGetWithFilter(activeIpsecProfileFilter, session);

                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for active ipsec profile";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredCryptoIpsecProfile != null && configuredCryptoIpsecProfile.size() == 1) {
                            Element cryptoIpsecProfileElement = configuredCryptoIpsecProfile.get(0);
                            String xpath = mapper.xpathCryptoIpsecProfile();
                            resultMap.put(xpath, cryptoIpsecProfileElement);
                        } else  {
                            errorBuilder.append("\n   Missing crypto ipsec profile section for vpc id: " + vpcId);
                        }

                        NodeSet configuredInterfacesState = null;
                        try {
                            configuredInterfacesState = rpcGetWithFilter(activeInterfacesStateFilter, session);

                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for active ipsec profile";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredInterfacesState != null || configuredInterfacesState.size() == 1) {
                            Element interfacesStateElement = configuredInterfacesState.get(0);
                            String xpath = mapper.xpathInterfacesState();
                            resultMap.put(xpath, interfacesStateElement);
                        } else {
                            errorBuilder.append("\n   Missing interfaces state section for vpc id: " + vpcId);
                        }

                        NodeSet configuredIpsecTransformset = null;
                        try {
                            configuredIpsecTransformset = rpcGetWithFilter(activeIpsecTransformsetFilter, session);

                        } catch (Exception e) {
                            String errMsg = "Failed to query router " + routerNumber + " for active ipsec profile";
                            logger.error(getLogtag() + errMsg, e);
                        }

                        if (configuredIpsecTransformset != null && configuredIpsecTransformset.size() == 1) {
                            Element cryptoIpsecTransformsetElement = configuredIpsecTransformset.get(0);
                            String xpath = mapper.xpathCryptoIpsecTransformset();
                            resultMap.put(xpath, cryptoIpsecTransformsetElement);
                        } else  {
                            errorBuilder.append("\n   Missing crypto ipsec transform-set section for vpc id: " + vpcId);
                        }

                        if (errorBuilder.length() > 0) {
                            logger.error(getLogtag() + "Incomplete configuration while assembling vpn connection config/state for tunnel Id: " + tunnelName + errorBuilder.toString());
                            continue;
                        }

                        VpnConnection vpnConnection = AppConfigUtil.getVpnConnectionFromConfiguration(appConfig);
                        vpnConnection = mapper.hydrateVpnConnection(vpnConnection, resultMap);

                        vpnConnection.setVpcId(vpcId);
                        vpnConnection.setVpnId(vpnId.toString());

                        vpnConnections.add(vpnConnection);


                    } catch (Exception e) {
                        logger.error(getLogtag() + "Failed to get configured tunnel context values", e);
                        // Continue on to the next...
                    }
                }

            }

            int activeTunnelCount = vpnConnections.size();
            logger.info(getLogtag() + vpnConnections.size() + " active tunnel" + (activeTunnelCount == 1 ? "" : "s") + " found on router " + routerNumber);

            totalQueryTime = System.currentTimeMillis() - time;
            logger.info(getLogtag() + "Total wildcard query time: " + totalQueryTime + " ms");

        } catch (JNCException e) {
            String msg = "Failed wildcard query: error while creating element";
            logger.error(getLogtag() + msg, e);
            throw new ProviderException(msg, e);
        }

        return vpnConnections;
    }

    /**
     * Get the prefix value, the part that is constant for this YANG version and CSP, from the add template for the
     * following element:
     *
     *      interface/Tunnel/tunnel/protection/ipsec/profile
     *
     * The prefix will be unique for the current YANG version and CSP, and will filter out tag/description elements
     * and their containing sections from the results of the wildcard query. This is brittle with respect to new
     * template structures for new YANG/CSP combinations.
     *
     * @return profile prefix for interface tunnel protection element
     * @throws ProviderException - For parsing errors due to malformed template content.
     */
    private String getTunnelProtectionProfilePrefix() throws ProviderException {
        String addTemplate = mapper.getTemplate("addTemplate");
        XMLParser xmlParser;

        Element addFromXml;
        String profile;

        try {
            xmlParser = new XMLParser();
            addFromXml = xmlParser.parse(addTemplate);
            profile = ElementUtils.safeGetValue(addFromXml, mapper.xpathInterfaceTunnelTunnelProtectionIpsecProfile());
        } catch (JNCException e) {
            throw new ProviderException(getLogtag() + "Failed to parse add template", e);
        }

        int index = profile.indexOf("${");
        if (index == -1) {
            throw new ProviderException("Failed to find tunnel protection profile prefix in add template!");
        }
        String profilePrefix = profile.substring(0, index);

        return profilePrefix;

    }
    private String getPrefix(Element template, String xpath) throws ProviderException {
        String templateValue;

        try {
            templateValue = ElementUtils.safeGetValue(template, xpath);
        } catch (JNCException e) {
            throw new ProviderException(getLogtag() + "Failed to parse add template", e);
        }

        int index = templateValue.indexOf("${");
        if (index == -1) {
            throw new ProviderException("Failed to find prefix in template for xpath: " + xpath);
        }
        String profilePrefix = templateValue.substring(0, index);

        return profilePrefix;

    }

    private void validateHydratedValues(Map<String, String> hydratedValuesMap) throws ProviderException {

        StringBuilder errors = new StringBuilder();

        hydratedValuesMap.entrySet().forEach(es -> {
            if (es.getValue().equals("[unknown]")) {
                if (errors.length() == 0) {
                    errors.append("Hydrated value(s) not found in query template - check for structural errors for following xpaths:");
                }
                errors.append("    \n" + es.getKey());
            }
        });

        if (errors.length() > 0) {
            throw new ProviderException(errors.toString());
        }
    }

    private NodeSet getWithFilter(String filter, NetconfSession session) throws JNCException, IOException {
        NodeSet result;

        // RPC calls can be implemented here (see history).
        result = session.get(filter);

        return result;
    }

    Integer messageId = 0;

    private NodeSet rpcGetWithFilter(String filter, NetconfSession session) throws JNCException, IOException {

        NodeSet result;

        final String rpcGetWithXpathTemplate = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"${messageId}\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" nc:select=\"${xpathFilter}\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";


        String rpc = rpcGetWithXpathTemplate.replace("${xpathFilter}", filter).replace("${messageId}", (++messageId).toString());
        Element element = session.rpc(rpc);
        result = element.getFirst("data").getChildren();

        return result;
    }

    private String getLogtag() {
        return logtag;
    }
}
