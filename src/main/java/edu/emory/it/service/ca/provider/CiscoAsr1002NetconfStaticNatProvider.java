package edu.emory.it.service.ca.provider;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.mapper.MapperFactory;
import edu.emory.it.service.ca.provider.mapper.StaticNatMapper;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.RouterUtil;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.moa.XmlEnterpriseObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Static Nat provider for performing requests.
 *
 * There are specific templates used for each YANG version + Cloud Service Provider combination, as it turns out some
 * of these combined values have unique YANG template requirements.
 *
 * Add/Delete requests perform retries when specific types of router errors occur - those categorized as "settling"
 * errors.  All other error categories result in operation abort.
 *
 */
public class CiscoAsr1002NetconfStaticNatProvider extends CiscoAsr1002Provider implements StaticNatProvider, CiscoAsrProvider{

    private static final Logger logger = OpenEaiObject.logger;
    private static final String PROVIDER_PROPERTIES = "caStaticNatProviderProperties";
    private static final String PROP_STATIC_NAT_IP_MASK_ROOT = "staticNatIpMask";

    protected StaticNatMapper mapper = null;

    protected List<String> staticNatIpMasks;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {


        logger.info(getLogtag() + "- Initializing");

        if (aConfig != null) {
            appConfig = aConfig;

            setProviderProperties(this, appConfig, PROVIDER_PROPERTIES);
            refreshLocalVariables();

            try {
                ciscoAsrSyncCommandProducerPool = (ProducerPool) appConfig.getObject("SyncSaveConfigCommandProducer");
            } catch (Exception e) {
                String errMsg = "No 'SyncSaveConfigCommandProducer' PubSubProducer found in AppConfig.";
                logger.fatal(getLogtag() + errMsg);
                throw new ProviderException(errMsg);
            }

            console(getLogtag() + " initialized.");
        } else {
            String errMsg = "No deployment descriptor provided!";
            logger.fatal(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
    }

    /**
     * Main entry point for StaticNatQuerySpecification query request.
     *
     * If the query spec contains a public IP value, a standard query is performed with a configured number of retries.
     *
     * If the query spec does not contain a public IP value, a wildcard query is performed.
     *
     * @param staticNatQuery - query specification used to perform query request
     * @return - List of static nat entries found
     * @throws ProviderException - thrown on any failure to perform query request
     */
    public List<StaticNat> query(StaticNatQuerySpecification staticNatQuery) throws ProviderException {
        List<StaticNat> staticNats;

        try {
            if (!StringUtils.isEmpty(staticNatQuery.getPublicIp())) {
                staticNats = new ArrayList<>();
                StaticNat staticNat = queryWithRetries(staticNatQuery);
                if (staticNat != null) {
                    staticNats.add(staticNat);
                }
            } else {
                staticNats = queryWithWildcards(staticNatQuery);
            }

            // If wait for response is needed, add here.

        } catch (Exception e){
            String errMsg = "Exception while querying static nat configuration for global ip " + staticNatQuery.getPublicIp();
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return staticNats;
    }

    /**
     * Main entry point for static nat add request
     *
     * Add static nat to router NAT table
     *
     * @param staticNat - static nat object to use for add request
     * @throws ProviderException - thrown on any failure to perform add request
     */
    public void add(StaticNat staticNat) throws ProviderException {

        // Used if wait for response needed.
        NetconfSession session = null;

        try {

            session = openNetconfSession();
            session.lock(NetconfSession.RUNNING);

            addWithRetries(staticNat, session);

            PubSubProducer pubSubProducer = null;
            try {
                pubSubProducer = (PubSubProducer) ciscoAsrSyncCommandProducerPool.getProducer();
                staticNat.createSync(pubSubProducer);
            } catch (Exception e) {
                String errMsg = "Failed to publish static nat create sync message";
                logger.error(getLogtag() + errMsg, e);
            } finally {
                ciscoAsrSyncCommandProducerPool.releaseProducer(pubSubProducer);
            }

        } catch (Exception e) {
            String errMsg = "Exception while attempting to add static nat netconf config";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } finally {
            long startTime = System.currentTimeMillis();
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
            routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
        }

    }

    /**
     * Main entry point for static nat delete request.
     *
     * Delete static nat from router NAT table.
     *
     * @param staticNat - static nat object used to perform delete
     * @throws ProviderException - thrown on any failure to perform delete request
     */
    public void delete(StaticNat staticNat) throws ProviderException {

        // Used if wait for response is needed.
        NetconfSession session = null;

        try {

            session = openNetconfSession();
            session.lock(NetconfSession.RUNNING);

            deleteWithRetries(staticNat, session);

            PubSubProducer pubSubProducer = null;
            try {
                pubSubProducer = (PubSubProducer) ciscoAsrSyncCommandProducerPool.getProducer();
                staticNat.deleteSync("Delete", pubSubProducer);
            } catch (Exception e) {
                String errMsg = "Failed to publish static nat delete sync message for public ip: " + staticNat.getPublicIp();
                logger.error(getLogtag() + errMsg, e);
            } finally {
                ciscoAsrSyncCommandProducerPool.releaseProducer(pubSubProducer);
            }

        } catch (Exception e) {
            String errMsg = "Exception while attempting to static nat provision delete";
            logger.error(getLogtag() + errMsg, e);

            throw new ProviderException(errMsg, e);
        } finally {
            long startTime = System.currentTimeMillis();
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
            routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
        }

    }

    @Override
    protected void refreshLocalVariables() throws ProviderException {

        super.refreshLocalVariables();


        Properties properties = getProperties();
        StringBuilder builder = new StringBuilder();

        // TODO: staticNatIpFilters -> IpAddressRange
        staticNatIpMasks = new ArrayList<>();
        for (Integer n = 1; n <= properties.size(); n++) {
            String mask = properties.getProperty(PROP_STATIC_NAT_IP_MASK_ROOT + n);
            if (StringUtils.isNotEmpty(mask)) {
                staticNatIpMasks.add(mask);
            }
        }

        if (staticNatIpMasks.isEmpty()) {
            builder.append("No static nat ip masks found in configuration");
        }

        if (builder.length() > 0) {
            String errMsg = "The following Static Nat Provider properties not found in app config:\n" + builder.toString();
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }

    }

    /********************************** Implementation **************************************/

    /**
     * Create a mapper instance based on YANG version and CloudServiceProvider:
     *   1) YANG version is obtained directly from the router using SSH2 and examining YANG capability revisions.
     *   2) CSP is determined TBD. It SHOULD be a discriminator field directly on the request object, but it might end
     *      up being field overloading.
     *
     * The combination of the YANG version number and the CSP is used as a parameter to the Mapping factory method.
     *
     * @param request - A query, generate or delete request used to obtain the CSP
     * @throws ProviderException
     */
    protected void createMapper(XmlEnterpriseObject request, NetconfSession session) throws ProviderException {

        logger.info(getLogtag() + "Creating mapper for " + request.getClass().getSimpleName() + " request object...");

        String routerVersion = super.getRouterVersion(session);
        // For initial multiversioning support, mapper type is comprised solely of router version.
        String mapperType = routerVersion;

        try {
            mapper = MapperFactory.createStaticNatMapper(mapperType);
        } catch (Exception e) {
            String errMsg = "Failed to create mapper from YANG version: " + routerVersion;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        // Load configuration - at this point, it's just the mapper specific templates.
        try {
            Properties mapperProps = appConfig.getProperties(routerVersion + "_Properties");
            mapper.initializeTemplates(mapperProps);
            mapper.init(appConfig);

        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Failed to load mapper configuration from YANG version: " + routerVersion;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        logger.info(getLogtag() + "Mapper type: " + mapperType);
    }

    private StaticNat queryWithRetries(StaticNatQuerySpecification staticNatQuery) throws ProviderException {
        String globalIpAsString = staticNatQuery.getPublicIp();
        StaticNat staticNat = null;

        int retryCount = operationErrorRetryCount;
        long startTime;

        NetconfSession session = null;
        mapper = null;

        String configuredLocalIpAddress;

        Exception lastRetryError = null;

        while (retryCount-- > 0) {
            try {

                startTime = System.currentTimeMillis();
                session = openNetconfSession();

                if (mapper == null) {
                    createMapper(staticNatQuery, session);
                }

                Element nativeFromXml = mapper.buildQueryConfig(mapper.getQueryStaticNatTemplate(), globalIpAsString);

                logger.info(getLogtag() + "Querying static nat config from device " + providerDevice.getName() + ": " + nativeFromXml.toXMLString());

                NodeSet existingStaticNatConfig = session.getConfig(nativeFromXml);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                if (existingStaticNatConfig.size() == 1) {
                    configuredLocalIpAddress = mapper.getExistingLocalIp(existingStaticNatConfig);

                    staticNat = AppConfigUtil.getStaticNatFromConfiguration(appConfig);
                    staticNat.setPublicIp(globalIpAsString);
                    staticNat.setPrivateIp(configuredLocalIpAddress);
                } else if (existingStaticNatConfig.size() > 1) {
                    // This should *NOT* be possible, however...
                    String errMsg = "Multiple static nat entries mapped to the same global IP address " + globalIpAsString;
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg);
                }
                logger.info(getLogtag() + "Successful static nat provision query after attempt #" + (operationErrorRetryCount - retryCount));
                break;

            } catch (Exception e) {
                String errMsg = "Exception while static nat provision query for global ip " + globalIpAsString +  " - retry #" + (operationErrorRetryCount - retryCount) + "...";;
                logger.error(getLogtag() + errMsg, e);

                if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    errMsg = "Router settling error while attempting to static nat provision query - retry #" + (operationErrorRetryCount - retryCount) + "...";
                    logger.error(getLogtag() + errMsg, e);

                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    throw new ProviderException("Error while attempting to static nat provision query", e);
                }

            } finally {
                startTime = System.currentTimeMillis();
                closeNetconfSession(session);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }

        if (retryCount < 0) {
            throw new ProviderException("Failed to static nat provision query after " + buildTriesString(), lastRetryError);
        }

        return staticNat;
    }

    /**
     * Use an xpath expression to filter by configured static nat masks.
     *
     * No retries here as this is a monolithic query used by reconciler - failure is probably due to traffic, and
     * reconciler is periodic.
     *
     * @return List of StaticNat object retrieved from router.
     *
     */
    private List<StaticNat> queryWithWildcards(StaticNatQuerySpecification staticNatQuery) throws ProviderException {
        List<StaticNat> staticNats = new ArrayList<>();

        NetconfSession session = null;
        String xpathFilter = null;
        try {

            session = openNetconfSession();
            session.lock(NetconfSession.RUNNING);

            createMapper(staticNatQuery, session);

            xpathFilter = mapper.getStaticIpMasksFilter(staticNatIpMasks);

            NodeSet staticNatNodeSet = session.get(xpathFilter);

            StringBuilder errorBuilder = new StringBuilder();

            staticNats = mapper.getStaticNatTransportList(staticNatNodeSet, errorBuilder);

            if (errorBuilder.length() > 0) {
                logger.error(getLogtag() + "Failed to convert the following result nodes to StaticNat objects:\n" + errorBuilder.toString());
            }

        } catch (Exception e) {
            String errMsg = "Failed wildcard static nat query";
            if (StringUtils.isNotEmpty(xpathFilter)) {
                errMsg += " with xpath filter: " + xpathFilter;;
            }
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

        return staticNats;
    }

    private void addWithRetries(StaticNat staticNat, NetconfSession session) throws ProviderException {
        String localIpAsString = staticNat.getPrivateIp();
        String globalIpAsString = staticNat.getPublicIp();

        int retryCount = operationErrorRetryCount;

        Exception lastRetryError = null;
        while (retryCount-- > 0) {
            try {
                if (mapper == null) {
                    createMapper(staticNat, session);
                }

                Element nativeFromXml = mapper.buildCreateConfig(mapper.getCreateStaticNatTemplate(), globalIpAsString, localIpAsString);

                logger.info(getLogtag() + "Adding static nat config from device " + providerDevice.getName() + ": " + nativeFromXml.toXMLString());
                long startTime = System.currentTimeMillis();
                session.editConfig(nativeFromXml);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                logger.info(getLogtag() + "Successful static nat provision add after attempt #" + (operationErrorRetryCount - retryCount));
                break;

            } catch (Exception e) {
                if (isRouterDataExistsError(e)) {
                    throw new ProviderException("Attempting to static nat provision add with existing data: " + staticNat.getPublicIp(), e);
                } else if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    String errMsg = "Router settling error while attempting to static nat provision query - retry #" + (operationErrorRetryCount - retryCount) + "...";
                    logger.error(getLogtag() + errMsg, e);

                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    throw new ProviderException("Error while attempting to static nat provision query", e);
                }
            } finally {
                long startTime = System.currentTimeMillis();
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }

        if (retryCount < 0) {
            throw new ProviderException("Failed to static nat provision add after " + operationErrorRetryCount + buildTriesString(), lastRetryError);
        }

    }

    private void deleteWithRetries(StaticNat staticNat, NetconfSession session) throws ProviderException {
        String localIpAsString = staticNat.getPrivateIp();
        String globalIpAsString = staticNat.getPublicIp();

        int retryCount = operationErrorRetryCount;
        long startTime;

        mapper = null;

        Exception lastRetryError = null;
        while (retryCount-- > 0) {
            try {

                if (mapper == null) {
                    createMapper(staticNat, session);
                }

                Element nativeFromXml = mapper.buildDeleteConfig(mapper.getDeleteStaticNatTemplate(), globalIpAsString, localIpAsString);

                logger.info(getLogtag() + "Deleting static nat config from device " + providerDevice.getName() + ": " + nativeFromXml.toXMLString());

                startTime = System.currentTimeMillis();
                session.editConfig(nativeFromXml);
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;

                logger.info(getLogtag() + "Successful static nat provision delete after attempt #" + (operationErrorRetryCount - retryCount));
                break;

            } catch (Exception e) {
                if (isRouterSettlingError(e)) {
                    lastRetryError = e;
                    operationSleep(operationErrorRetryWaitInMilliseconds);
                } else {
                    String errMsg = "Error while attempting static nat provision add - public ip: " + staticNat.getPublicIp();
                    logger.error(getLogtag() + errMsg);
                    throw new ProviderException(errMsg, e);
                }
            } finally {
                startTime = System.currentTimeMillis();
                routerExecutionTimeInMilliseconds += System.currentTimeMillis() - startTime;
            }
        }

        if (retryCount < 0) {
            throw new ProviderException("Failed to static nat provision delete after " + buildTriesString(), lastRetryError);
        }
    }



}
