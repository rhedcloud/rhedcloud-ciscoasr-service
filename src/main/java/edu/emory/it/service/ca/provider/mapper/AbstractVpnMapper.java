package edu.emory.it.service.ca.provider.mapper;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.ElementUtils;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.BgpPrefixes;
import edu.emory.moa.objects.resources.v1_0.BgpState;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecTransformSet;
import edu.emory.moa.objects.resources.v1_0.CryptoIsakmpProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoKeyring;
import edu.emory.moa.objects.resources.v1_0.LocalAddress;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;
import edu.emory.moa.objects.resources.v1_0.TunnelInterface;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseFieldException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract mapper that contains all xpath and xpath segments used to access both the router templates and xpath
 * returned from the router as responses to requests.
 *
 * The accessors to these values are protected so that they can be overwritten by concrete mapper classes as needed.
 *
 * The convention used for the xpath getters is an "xpath" prefix for readability.
 *
 * One last note:  YANG 1662 is the reference version against which all other versions specialize as necessary. The
 * xpath variable/method naming convention is based on 1662 xpath segments and are overridden in subclasses, but retain
 * the xpath name. When a YANG version arrives where this is no longer practical, other methods will be overridden, e.g.
 * hydrateVpnConnection.
 */
abstract  public class AbstractVpnMapper implements VpnMapper {

    /*
    //                String bgpConfigsFilter = "native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor[starts-with(id, '" + ipRange + "')]";

    // This is a change from original 1662 to the "tweaked" 1662 that manages bgp dynamically to enable version switching on the routers.
    // 1662 AWS has the ipv4-unicast segment, 1662 GCP DOES NOT

    1662 AWS/GCP: ipv4-unicast NOT present in native response

    1693 AWS/GCP: ipv4-unicast present in native response

 added  native/router/bgp/address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor[starts-with(id, '" + ipRange + "')]";
 +  router/bgp/address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor
 +  bgp-state-data/neighbors/neighbor[starts-with(neighbor-id, '" + ipRange + "')]";

//                NodeSet allConfigs = getWithFilter("native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor", session);

 added  native/crypto/ipsec/transform-set/tag
 added  crypto/isakmp/profile
 added  crypto/ipsec/transform-set
 added  crypto/ipsec/profile


     */
    private static final Logger logger = OpenEaiObject.logger;
    private final String LOGTAG = "[AbstractMapper] ";

    // Native sections
    private String _native = "native";
    private String nativeKeyring = "native/keyring";
    private String nativeInterface = "native/interface";
    private String nativeIsakmpProfile = "native/isakmp/profile";
    private String nativeIpsecTransformset = "native/ipsec/transform-set";
    private String nativeIpsecProfile = "native/ipsec/profile";
    private String nativeCryptoIpsecProfileName = "native/crypto/ipsec/profile/name";
    private String nativeCryptoIpsecTransformsetTag = "native/crypto/ipsec/transform-set/tag";
    private String nativeCryptoIsakmpProfileName = "native/crypto/isakmp/profile/name";
    private String nativeCryptoKeyringName = "native/crypto/keyring/name";
    private String nativeInterfaceTunnel = "native/interface/Tunnel";
    private String nativeInterfaceTunnelName = "native/interface/Tunnel/name";
    private String nativeInterfaceTunnelNameWithValue = "native/interface/Tunnel/name[text() = '${tunnelName}']/..";

    private String nativeRouterBgpId = "native/router/bgp/id";


    private String bgpStateData = "bgp-state-data";
    private String bgpstatedataNeighborsNeighborNeighborid = "bgp-state-data/neighbors/neighbor/neighbor-id";

    private String interfacesState = "interfaces-state";
    private String interfacesStateInterfaceName = "interfaces-state/interface/name";

    private String cryptoKeyringPresharedkeyAddressIpv4Key = "crypto/keyring/pre-shared-key/address/ipv4/key";

    // Crypto sections
    private String cryptoKeyring = "crypto/keyring";
    private String cryptoKeyringName = "crypto/keyring/name";
    private String cryptoKeyringDescription = "crypto/keyring/description";
    private String cryptoKeyringVrf = "crypto/keyring/vrf";
    private String cryptoKeyringPresharedkeyAddressIpv4Unencrytkey = "crypto/keyring/pre-shared-key/address/ipv4/unencryt-key";
    private String cryptoKeyringPresharedkeyAddressIpv4Ipv4addr = "crypto/keyring/pre-shared-key/address/ipv4/ipv4-addr";
    private String cryptoIsakmpProfile = "crypto/isakmp/profile";
    private String cryptoIsakmpProfileVrf = "crypto/isakmp/profile/vrf";
    private String cryptoIsakmpProfileDescription = "crypto/isakmp/profile/description";
    private String cryptoIsakmpProfileName = "crypto/isakmp/profile/name";
    private String cryptoIsakmpProfileMatchIdentityIpv4addressAddress = "crypto/isakmp/profile/match/identity/ipv4-address/address";
    private String cryptoIsakmpProfileMatchIdentityIpv4addressMask = "crypto/isakmp/profile/match/identity/ipv4-address/mask";
    private String cryptoIsakmpProfileMatchIdentityIpv4addressVrf = "crypto/isakmp/profile/match/identity/ipv4-address/vrf";
    private String cryptoIpsecProfileDescription = "crypto/ipsec/profile/description";
    private String cryptoIpsecProfile = "crypto/ipsec/profile";
    private String cryptoIpsecProfileName = "crypto/ipsec/profile/name";
    private String cryptoIpsecProfileSetPfsGroup = "crypto/ipsec/profile/set/pfs/group";
    private String cryptoIpsecProfileSetTransformset = "crypto/ipsec/profile/set/transform-set";
    private String cryptoIpsecTransformset = "crypto/ipsec/transform-set";
    private String cryptoIpsecTransformsetTag = "crypto/ipsec/transform-set/tag";
    private String cryptoIpsecTransformsetKeybit = "crypto/ipsec/transform-set/key-bit";
    private String cryptoIpsecTransformsetEsphmac = "crypto/ipsec/transform-set/esp-hmac";
    private String cryptoIpsecTransformsetModeTunnel = "crypto/ipsec/transform-set/mode/tunnel";

    // Interface xpath

    private String interfaceTunnel = "interface/Tunnel";
    private String interfaceAdminstatus = "interface/admin-status";
    private String interfaceOperstatus = "interface/oper-status";
    private String interfaceTunnelName = "interface/Tunnel/name";
    private String interfaceTunnelDescription = "interface/Tunnel/description";
    private String interfaceTunnelVrfForwarding = "interface/Tunnel/vrf/forwarding";
    private String interfaceTunnelIpAddressPrimaryAddress = "interface/Tunnel/ip/address/primary/address";
    private String interfaceTunnelIpAddressPrimaryMask = "interface/Tunnel/ip/address/primary/mask";
    private String interfaceTunnelIpTcpAdjustmss = "interface/Tunnel/ip/tcp/adjust-mss";
    private String interfaceTunnelTunnelSource = "interface/Tunnel/tunnel/source";
    private String interfaceTunnelTunnelDestination = "interface/Tunnel/tunnel/destination";
    private String interfaceTunnelTunnelIpvirtualReassembly = "interface/Tunnel/tunnel/ipVirtualReassembly";
    private String interfaceTunnelTunnelProtectionIpsecProfile = "interface/Tunnel/tunnel/protection/ipsec/profile";
    private String interfaceTunnelShutdown = "interface/Tunnel/shutdown";
    private String routerBgpAddressfamilyWithvrfIpv4VrfNeighborId = "router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/id";
    private String routerBgpAddressfamilyWithvrfIpv4VrfNeighborDescription = "router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/description";

    private String neighborsNeighborDescription = "neighbors/neighbor/description";
    private String neighborsNeighborSessionstate = "neighbors/neighbor/session-state";
    private String neighborsNeighborUptime = "neighbors/neighbor/up-time";
    private String neighborsNeighborNeighborid = "neighbors/neighbor/neighbor-id";
    private String neighborsNeighborPrefixActivitySentCurrentPrefixes = "neighbors/neighbor/prefix-activity/sent/current-prefixes";
    private String neighborsNeighborPrefixActivityReceivedCurrentPrefixes = "neighbors/neighbor/prefix-activity/received/current-prefixes";

    // Relative sub-paths
    private String ipAddressPrimaryAddress = "ip/address/primary/address";

    // Wildcard queries

    // Pivot point queries - an query filter for each active section required to build a VpnConnection.
    String activeTunnelFilter = "native/interface/Tunnel/tunnel/protection/ipsec/profile[text()[contains(.,'${prefix}')]]/../../../..";
    String activeNativeCryptoKeyringFilter = "native/crypto/keyring/name[text()[contains(., '${Value}')]]/..";
    String activeBgpStateFilter = "bgp-state-data/neighbors/neighbor/description[text() = '${Value}']/..";
    String activeNativeInterfaceFilter = "native/interface/Tunnel/name[text() = '${TunnelId}']/..";
    String activeIsakmpProfileFilter = "native/crypto/isakmp/profile/description[text() = '${VpcId}']/..";
    String activeIpsecProfileFilter = "native/crypto/ipsec/profile/name[text() = '${Value}']/..";
    String activeInterfacesStateFilter = "interfaces-state/interface/name[text() = 'Tunnel${TunnelId}']/..";
    String activeIpsecTransformsetFilter = "native/crypto/ipsec/transform-set/tag[text() = '${Value}']/..";
    String activeRouterBgpDesciptionFilter = "native/router/bgp/address-family/with-vrf/ipv4/vrf/ipv4-unicast/neighbor/description[text() = '${Value}']/..";

    protected String tunnelNumber;
    protected AppConfig appConfig;

    protected String createTunnelTemplate;

    protected String shutdownTunnelTemplate;

    protected String deleteTunnelTemplate;

    protected String queryTunnelTemplate;

    private final Boolean RFC_6536 = Boolean.FALSE;

    public void init(AppConfig appConfig, String tunnelNumber) {
        this.appConfig = appConfig;
        this.tunnelNumber = tunnelNumber;
    }

    public void initializeTemplates(Properties templates) {
        createTunnelTemplate = templates.getProperty("createTunnelTemplate");
        shutdownTunnelTemplate = templates.getProperty("shutdownTunnelTemplate");
        deleteTunnelTemplate = templates.getProperty("deleteTunnelTemplate");
        queryTunnelTemplate = templates.getProperty("queryTunnelTemplate");

        StringBuilder builder = new StringBuilder();
        if (createTunnelTemplate == null) {
            builder.append(" createTunnelTemplate");
        }
        if (shutdownTunnelTemplate == null) {
            builder.append(" shutdownTunnelTemplate");
        }
        if (deleteTunnelTemplate == null) {
            builder.append(" deleteTunnelTemplate");
        }
        if (queryTunnelTemplate == null) {
            builder.append(" queryTunnelTemplate");
        }

        if (builder.length() > 0) {
            throw new RuntimeException("Following required templates missing from configuration:" + builder.toString());
        }
    }

    // Return a substitution token for a given value id.  This decouples the provider from any knowledge of what's
    // inside the templates. This is also a binding contract between configured templates and this service.
    public String getSubstitutionToken(String valueId) throws ProviderException {
        final Map<String, String> substitutionTokenMap = new ConcurrentHashMap<>();

        substitutionTokenMap.put("vpcId", "${VpcId}");
        substitutionTokenMap.put("vpnId", "${VpnId}");

        substitutionTokenMap.put("bgpNeighborId", "${BgpNeighborId}");
        substitutionTokenMap.put("tunnelNumber", "${TunnelNumber}");
        substitutionTokenMap.put("tunnelId", "${TunnelId}");
        substitutionTokenMap.put("paddedVpnId", "${VpnId-Padded}");
        substitutionTokenMap.put("tunnelSuffix", "${TunnelSuffix}");
        substitutionTokenMap.put("localTunnelNumberZeroBased", "${LocalTunnelNumberZeroBased}");
        substitutionTokenMap.put("presharedKey", "${PresharedKey}");
        substitutionTokenMap.put("remoteVpnIpAddress", "${RemoteVpnIpAddress}");
        substitutionTokenMap.put("localVpnIpAddress", "${LocalVpnIpAddress}");
        substitutionTokenMap.put("bgpAsn", "${BgpAsn}");

        String token = substitutionTokenMap.get(valueId);
        if (token == null) {
            throw new ProviderException("No token found for value id: " + valueId);
        }
        return token;
    }

    public String getTemplate(String templateName) throws ProviderException {
        String template;

        switch (templateName) {
            case "queryTemplate":
                template = queryTunnelTemplate;
                break;
            case "addTemplate":
                template = createTunnelTemplate;
                break;
            case "deleteTemplate":
                template = deleteTunnelTemplate;
                break;
            case "shutdownTemplate":
                template = shutdownTunnelTemplate;
                break;
            default:
                throw new ProviderException("Unknown template type: " + templateName);
        }

        return template;
    }

    public Element hydrateTemplate(Map<String, String> valuesMap, String templateId, Map<String, String> pathValueMap) throws ProviderException {

        Element queryFromXml;
        try {

            String template = getTemplate(templateId);

            XMLParser xmlParser = new XMLParser();

            queryFromXml = xmlParser.parse(template);

            Map<String, List<String>> elementToTokensMap = buildElementToTokensMap(queryFromXml);

            Set<String> uniqueTokens = new HashSet<>();

            for (Map.Entry<String, List<String>> entrySet : elementToTokensMap.entrySet()) {
                String xpath = entrySet.getKey();
                List<String> tokens = entrySet.getValue();

                String value = queryFromXml.getValue(xpath).toString();
                for (String t : tokens) {
                    uniqueTokens.add(t);

                    String replacementValue = valuesMap.get(t);
                    if (replacementValue != null) {
                        value = value.replace(t, replacementValue);
                        queryFromXml.setValue(xpath, value);
                    } else {
                        value = "";
                    }
                }
                pathValueMap.put(xpath, value);

            }

        } catch (JNCException e) {
            throw new ProviderException("Failed to access query template value with xpath", e);
        }

        return queryFromXml;
    }

    /**
     * This is MRC (Model - Router - Controller) mapping thee router results into the domain object returned (VpnConnection
     * MOA). This is default behavior, i.e., mapping router version 1662 into the MOA.
     *
     * @param vpnConnection - MOA returned to caller
     * @param resultMap - A map containing all of the result YANG model elements for current YANG version.  Can be
     *                    used to override structural changes from reference version 1662.
     * @return VpnConnection - null signals the caller that the mapping failed.
     * @throws JNCException
     * @throws EnterpriseFieldException
     * @throws ProviderException
     */
    public VpnConnection hydrateVpnConnection(VpnConnection vpnConnection, Map<String, Element> resultMap) throws JNCException, EnterpriseFieldException, ProviderException {

        // Emory MOAs are intolerant to having fields set to empty/null, so we have to grunt through each value.
        String value;
        // Id used to map from RHEDcloud network to Clowd Service Provider.  A better Id would be "CLOUD"
        // In any case, it's constant for all sites.
        final String vrfValue = "AWS";

        Element interfacesState = resultMap.get(xpathInterfacesState());
        Element nativeInterface = resultMap.get(xpathNativeInterface());
        Element bgpStateData = resultMap.get(xpathBgpStateData());
        Element cryptoKeyringConfig = resultMap.get(xpathCryptoKeyring());
        Element cryptoIsakmpProfileConfig = resultMap.get(xpathCryptoIsakmpProfile());
        Element cryptoIpsecTransformSetConfig = resultMap.get(xpathCryptoIpsecTransformset());
        Element cryptoIpsecProfileConfig = resultMap.get(xpathCryptoIpsecProfile());

        CryptoKeyring cryptoKeyring = null;

        LocalAddress localAddress = AppConfigUtil.getLocalAddressProfileFromConfiguration(appConfig);
        if (cryptoKeyringConfig != null) {
            cryptoKeyring = AppConfigUtil.getCryptoKeyringFromConfiguration(appConfig);
            value = ElementUtils.safeGetValue(cryptoKeyringConfig, xpathCryptoKeyringName());
            if (StringUtils.isNotEmpty(value)) {
                cryptoKeyring.setName(value);
            }
            value = ElementUtils.safeGetValue(cryptoKeyringConfig, xpathCryptoKeyringDescription());
            if (StringUtils.isNotEmpty(value)) {
                cryptoKeyring.setDescription(value);
            }
            value = ElementUtils.safeGetValue(cryptoKeyringConfig, xpathCryptoKeyringPresharedkeyAddressIpv4Unencrytkey());
            if (StringUtils.isNotEmpty(value)) {
                cryptoKeyring.setPresharedKey(value);
            }

            //#########################################################################################################
            // INFO: Polymorphic between YANG versions
            //#########################################################################################################
            String localIpAddress = ElementUtils.safeGetValue(cryptoKeyringConfig, xpathCryptoKeyringPresharedkeyAddressIpv4Ipv4addr());
            if (localIpAddress != null) {
                localAddress.setIpAddress(localIpAddress);
                if (vrfValue != null && !vrfValue.isEmpty()) {
                    localAddress.setVirtualRouteForwarding(vrfValue);
                }
                cryptoKeyring.setLocalAddress(localAddress);
            } else {
                localAddress = null;
            }
        }

        CryptoIsakmpProfile cryptoIsakmpProfile = null;
        if (cryptoIsakmpProfileConfig != null) {
            cryptoIsakmpProfile = AppConfigUtil.getCryptoIsakmpProfileFromConfiguration(appConfig);
            value = ElementUtils.safeGetValue(cryptoIsakmpProfileConfig, xpathCryptoIsakmpProfileName());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIsakmpProfile.setName(value);
            }
            value = ElementUtils.safeGetValue(cryptoIsakmpProfileConfig, xpathCryptoIsakmpProfileDescription());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIsakmpProfile.setDescription(value);
            }
            value = ElementUtils.safeGetValue(cryptoIsakmpProfileConfig, xpathCryptoIsakmpProfileVrf());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIsakmpProfile.setVirtualRouteForwarding(value);
            }
            if (cryptoKeyring != null) {
                cryptoIsakmpProfile.setCryptoKeyring(cryptoKeyring);
            }

            if (localAddress != null) {
                if (vrfValue != null && !vrfValue.isEmpty()) {
                    localAddress.setVirtualRouteForwarding(vrfValue);
                }
                cryptoIsakmpProfile.setLocalAddress(localAddress);
            }
            if (vrfValue != null && !vrfValue.isEmpty()) {
                cryptoIsakmpProfile.setVirtualRouteForwarding(vrfValue);
            }
            MatchIdentity matchIdentity = AppConfigUtil.getMatchIdentityFromConfiguration(appConfig);
            value = ElementUtils.safeGetValue(cryptoIsakmpProfileConfig, xpathCryptoIsakmpProfileMatchIdentityIpv4addressAddress());
            if (StringUtils.isNotEmpty(value)) {
                matchIdentity.setIpAddress(value);
            }
            value = ElementUtils.safeGetValue(cryptoIsakmpProfileConfig, xpathCryptoIsakmpProfileMatchIdentityIpv4addressMask());
            if (StringUtils.isNotEmpty(value)) {
                matchIdentity.setNetmask(value);
            }
            value = ElementUtils.safeGetValue(cryptoIsakmpProfileConfig, xpathCryptoIsakmpProfileMatchIdentityIpv4addressVrf());
            if (StringUtils.isNotEmpty(value)) {
                matchIdentity.setVirtualRouteForwarding(value);
            }
            cryptoIsakmpProfile.setMatchIdentity(matchIdentity);

        }

        CryptoIpsecTransformSet cryptoIpsecTransformSet = null;
        if (cryptoIpsecTransformSetConfig != null) {
            cryptoIpsecTransformSet = AppConfigUtil.getCryptoIpsecTransformSetFromConfiguration(appConfig);
            value = ElementUtils.safeGetValue(cryptoIpsecTransformSetConfig, xpathCryptoIpsecTransformsetTag());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecTransformSet.setName(value);
            }
            value = ElementUtils.safeGetValue(cryptoIpsecTransformSetConfig, xpathCryptoIpsecTransformsetKeybit());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecTransformSet.setBits(value);
            }
            value = ElementUtils.safeGetValue(cryptoIpsecTransformSetConfig, xpathCryptoIpsecTransformsetEsphmac());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecTransformSet.setCipher(value);
            }
            value = ElementUtils.safeGetElementName(cryptoIpsecTransformSetConfig, xpathCryptoIpsecTransformsetModeTunnel());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecTransformSet.setMode(value);
            }
        }

        CryptoIpsecProfile cryptoIpsecProfile = null;
        if (cryptoIpsecProfileConfig != null) {
            cryptoIpsecProfile = AppConfigUtil.getCryptoIpsecProfileFromConfiguration(appConfig);
            value = ElementUtils.safeGetValue(cryptoIpsecProfileConfig, xpathCryptoIpsecProfileName());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecProfile.setName(value);
            }
            value = ElementUtils.safeGetValue(cryptoIpsecProfileConfig, xpathCryptoIpsecProfileDescription());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecProfile.setDescription(value);
            }
            cryptoIpsecProfile.setCryptoIpsecTransformSet(cryptoIpsecTransformSet);
            value = ElementUtils.safeGetValue(cryptoIpsecProfileConfig, xpathCryptoIpsecProfileSetPfsGroup());
            if (StringUtils.isNotEmpty(value)) {
                cryptoIpsecProfile.setPerfectForwardSecrecy(value);
            }
        }

        TunnelInterface tunnelInterface = null;
        if (nativeInterface != null && cryptoIpsecTransformSetConfig != null) {
            tunnelInterface = AppConfigUtil.getTunnelInterfaceFromConfiguration(appConfig);
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelName());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setName(value);
            }
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelDescription());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setDescription(value);
            }
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelVrfForwarding());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setVirtualRouteForwarding(value);
            }
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelIpAddressPrimaryAddress());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setIpAddress(value);
            }
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelIpAddressPrimaryMask());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setNetmask(value);
            }
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelIpTcpAdjustmss());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setTcpMaximumSegmentSize(value);
            }
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelTunnelSource());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setTunnelSource(value);
            }
            value = ElementUtils.safeGetElementName(cryptoIpsecTransformSetConfig, xpathCryptoIpsecTransformsetModeTunnel());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setTunnelMode(value);
            }
            //#########################################################################################################
            // TODO: polymorphic - 1693 is now destination/ipaddress-or-host
            //#########################################################################################################
            value = ElementUtils.safeGetValue(nativeInterface, xpathInterfaceTunnelTunnelDestination());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setTunnelDestination(value);
            }
            if (StringUtils.isNotEmpty(vrfValue)) {
                tunnelInterface.setIpVirtualReassembly(vrfValue);
            }
            tunnelInterface.setCryptoIpsecProfile(cryptoIpsecProfile);
            BgpState bgpState = AppConfigUtil.getBgpStateFromConfiguration(appConfig);
            if (bgpStateData != null) {
                value = ElementUtils.safeGetValue(bgpStateData, xpathNeighborsNeighborSessionstate());
                if (StringUtils.isNotEmpty(value)) {
                    bgpState.setStatus(value);
                }
                value = ElementUtils.safeGetValue(bgpStateData, xpathNeighborsNeighborUptime());
                if (StringUtils.isNotEmpty(value)) {
                    bgpState.setUptime(value);
                }
                // Note that while the router return structure has changed the BgpStateData structure has not.
                // We can continue to mine the neighborid the same way.
                value = ElementUtils.safeGetValue(bgpStateData, xpathNeighborsNeighborNeighborid());
                if (StringUtils.isNotEmpty(value)) {
                    bgpState.setNeighborId(value);
                }
                tunnelInterface.setBgpState(bgpState);
            }

            BgpPrefixes bgpPrefixes = AppConfigUtil.getBgpPrefixesFromConfiguration(appConfig);
            if (bgpPrefixes != null) {
                value = ElementUtils.safeGetValue(bgpStateData, xpathNeighborsNeighborPrefixActivitySentCurrentPrefixes());
                if (StringUtils.isNotEmpty(value)) {
                    bgpPrefixes.setSent(value);
                }
                value = ElementUtils.safeGetValue(bgpStateData, xpathNeighborsNeighborPrefixActivityReceivedCurrentPrefixes());
                if (StringUtils.isNotEmpty(value)) {
                    bgpPrefixes.setReceived(value);
                }
                tunnelInterface.setBgpPrefixes(bgpPrefixes);
            }
        }
        if (interfacesState != null && tunnelInterface != null) {
            value = ElementUtils.safeGetValue(interfacesState, xpathInterfaceAdminstatus());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setAdministrativeState(value);
            }
            value = ElementUtils.safeGetValue(interfacesState, xpathInterfaceOperstatus());
            if (StringUtils.isNotEmpty(value)) {
                tunnelInterface.setOperationalStatus(value);
            }
        }

        if (cryptoKeyring != null
                && cryptoIsakmpProfile != null
                && cryptoIpsecTransformSet != null
                && cryptoIpsecProfile != null
                && tunnelInterface != null
                && bgpStateData != null) {

            vpnConnection.setCryptoKeyring(cryptoKeyring);
            vpnConnection.setCryptoIsakmpProfile(cryptoIsakmpProfile);
            vpnConnection.setCryptoIpsecTransformSet(cryptoIpsecTransformSet);
            vpnConnection.setCryptoIpsecProfile(cryptoIpsecProfile);
            vpnConnection.addTunnelInterface(tunnelInterface);
        } else {
            vpnConnection = null;
        }

        return vpnConnection;
    }

    @Override
    public void validatePathToValuesMap(Map<String, String> pathToValuesMap) throws ProviderException {

        StringBuilder errors = new StringBuilder();

        for (Map.Entry<String, String> es : pathToValuesMap.entrySet()) {
            if (es.getValue().equals("")) {
                if (errors.length() == 0) {
                    errors.append("Paths missing values: ");
                }
                errors.append("\n    " + es.getKey());
            }
        }

        if (errors.length() > 0) {
            String msg = "Path to values map error: " + errors.toString();
            logger.error(getLogtag() + msg);
            throw new ProviderException(msg);
        }
    }

    /**
     * VpnId is defined as the last three digits of the tunnel name. So validate with regex, get last three digits and
     * convert to Integer.
     *
     * @param tunnelName - Tunnel name as String from which vpn id is extracted
     * @return Integer value of vpn id.
     */
    public Integer getVpnIdFromTunnelName(String tunnelName) {
        Integer vpnId;

        Pattern pattern = Pattern.compile("[\\d]{5}");
        Matcher matcher = pattern.matcher(tunnelName);

        if (matcher.matches()) {
            vpnId = Integer.valueOf(tunnelName.substring(2));
        } else {
            throw new IllegalArgumentException("Tunnel name is not 5 digits");
        }

        return vpnId;
    }

    public String getVpcIdFromTunnelDescription(String tunnelDescription) {
        String vpcId = null;

        Pattern pattern = Pattern.compile(".+\\(([^\\)]+)\\)");
        Matcher matcher = pattern.matcher(tunnelDescription);

        if (matcher.matches()) {
            vpcId = matcher.group(1);
        } else {
            throw new IllegalArgumentException("Tunnel description is malformed: " + tunnelDescription);
        }
        return vpcId;
    }

    public String buildPaddedVpnId(Integer vpnId) throws ProviderException {
        String paddedVpnId;
        try {
            paddedVpnId = String.format("%03d", vpnId);
        } catch (NumberFormatException e) {
            String msg = "Vpn ID is not a positive integer: " + vpnId;
            logger.error(getLogtag() + msg);
            throw new ProviderException(msg);
        }
        return paddedVpnId;
    }

    public String buildQueryTunnelId(Integer routerNumber, Integer localTunnelNumberZeroBased, Integer vpnId) throws ProviderException {
        String tunnelId;

        try {
            String paddedVpnId = buildPaddedVpnId(vpnId);
            Element nativeFromXml;
            XMLParser xmlParser = new XMLParser();
            nativeFromXml = xmlParser.parse(queryTunnelTemplate);
            tunnelId = ElementUtils.safeGetValue(nativeFromXml, xpathNativeInterfaceTunnelName());
            //${RouterNumber}${LocalTunnelNumberZeroBased}${VpnId-Padded}
            if (tunnelId == null
                    || !tunnelId.contains("${TunnelNumber}")
                    || !tunnelId.contains("${LocalTunnelNumberZeroBased}")
                    || !tunnelId.contains("${VpnId-Padded}")) {
                throw new IllegalArgumentException("Query tunnel template does not contain properly formatted tunnel Id: " + tunnelId);
            }
            tunnelId = tunnelId.replace("${TunnelNumber}", routerNumber.toString())
                    .replace("${LocalTunnelNumberZeroBased}", localTunnelNumberZeroBased.toString())
                    .replace("${VpnId-Padded}", paddedVpnId);
        } catch (JNCException e) {
            throw new ProviderException("Failed to parse create template", e);
        } catch (IllegalArgumentException e) {
            throw new ProviderException("Bad query template", e);
        }

        return tunnelId;
    }

    private Map<String, List<String>> buildElementToTokensMap(Element nodes) throws ProviderException {

        Map<String, List<String>> xpathToTokensMap = new ConcurrentHashMap<>();
        NodeSet childNodes = nodes.getChildren();

        recurseTemplateToBuildElementToTokensMap(xpathToTokensMap, childNodes);

        Set<String> uniqueTokens = new HashSet<>();

        for (Map.Entry<String, List<String>> es : xpathToTokensMap.entrySet()) {
            List<String> tokens = es.getValue();

            tokens.forEach(t -> {
                uniqueTokens.add(t);
            });

        }

        return xpathToTokensMap;
    }

    private void recurseTemplateToBuildElementToTokensMap(Map<String, List<String>> xpathToTokensMap, NodeSet nodes) {
        nodes.forEach(element -> {
            NodeSet children = element.getChildren();

            if (children != null && children.size() > 0) {
                recurseTemplateToBuildElementToTokensMap(xpathToTokensMap, children);
            }
            Object valueObject = element.getValue();
            if (valueObject != null) {
                String xpath = element.tagpath().toString();
                String rootNodeName = element.getRootElement().name + "/";
                xpath = xpath.replace(rootNodeName, "");
                String value = valueObject.toString();
//                logger.info(getLogtag() + element.tagpath().toString() + " -> " + value);
                final Pattern pattern = Pattern.compile("\\$\\{.+?\\}");
                final Matcher matcher = pattern.matcher(value);
                List<String> tokens = new ArrayList<>();
                while (matcher.find()) {
                    tokens.add(matcher.group());
                }
                if (tokens.size() != 0) {
                    if (xpathToTokensMap.get(xpath) == null) {
                        xpathToTokensMap.put(xpath, tokens);
                    }
                }
            }
        });


    }
    /**
     * This method is used by vpnId queries to establish all elements that contain a description element.  These
     * are used by the matching algorithm to determine if the result of the query matches the requested vpcId.
     *
     * This default implementation is used by YANG version 1662, and can be overridden by other versions.
     *
     * @param nativeResult - NodeSet containing all crypto elements returned from a query
     * @param nativeTunnelResult - NodeSet containing the native/interface/tunnel result
     * @param bgpStateDataResult - NodeSet containing the bgp-state-data result
     * @return
     */
    public Map<String, Element> buildDescriptionMap(NodeSet nativeResult, NodeSet nativeTunnelResult, NodeSet bgpStateDataResult) {
        Map<String, Element> descriptionMap = new HashMap<>();

        descriptionMap.put(xpathCryptoIpsecProfileDescription(), nativeResult.get(0));
        descriptionMap.put(xpathInterfaceTunnelDescription(), nativeTunnelResult.get(0));
        descriptionMap.put(xpathCryptoIsakmpProfileDescription(), nativeResult.get(0));
        descriptionMap.put(xpathCryptoKeyringDescription(), nativeResult.get(0));
        descriptionMap.put(xpathRouterBgpAddressfamilyWithvrfIpv4VrfNeighborDescription(), nativeResult.get(0));

        // bgp-state-data is not present when verifying deletes for some YANG versions.
        // It's just easier to check for no elements.
        if (bgpStateDataResult.size() > 0) {
            descriptionMap.put(xpathNeighborsNeighborDescription(), bgpStateDataResult.get(0));
        }

        return descriptionMap;
    }


    /**
     * This routine handles polymorphic update behavior between and within concrete mappers. State updates are
     * pre-loaded into maps that are used to set JNC Element objects. Different map suites are handled by using this
     * utility method that safely sets Element state.
     *
     * @param element - JNC Element to update
     * @param xpath - xpath to the desired child element within "element"
     * @param value - value, which might be null, to set on element
     * @throws JNCException
     */
    protected void setValue(Element element, String xpath, String value) throws JNCException {
        if (value != null) {
            element.setValue(xpath, value);
        }
    }

    String replaceMe(String original, String value) {

        final Pattern pattern = Pattern.compile("\\$\\{.+\\}");
        final Matcher matcher = pattern.matcher(original);

        if (matcher.find()) {
            String sentinel = matcher.group(0);
            value = original.replace(sentinel, value);
        }

        return value;
    }

    String replaceMe(String original, String sentinel, String value) {

        return original.replace(sentinel, value);
    }

    //#################################### xpath accessors #########################################
    
    @Override
    public String xpathNative() {
        return _native;
    }

    @Override
    public String xpathNativeInterface() {
        return nativeInterface;
    }

    @Override
    public String xpathNativeCryptoKeyringName() {
        return nativeCryptoKeyringName;
    }

    @Override
    public String xpathNativeCryptoIpsecProfileName() {
        return nativeCryptoIpsecProfileName;
    }

    @Override
    public String xpathNativeCryptoIpsecTransformsetTag() {
        return nativeCryptoIpsecTransformsetTag;
    }

    @Override
    public String xpathNativeInterfaceTunnelName() {
        return nativeInterfaceTunnelName;
    }

    @Override
    public String xpathNativeRouterBgpId() {
        return nativeRouterBgpId;
    }

    @Override
    public String xpathBgpStateData() {
        return bgpStateData;
    }

    @Override
    public String xpathInterfacesState() {
        return interfacesState;
    }

    @Override
    public String xpathCryptoKeyring() {
        return cryptoKeyring;
    }

    @Override
    public String xpathCryptoKeyringName() {
        return cryptoKeyringName;
    }

    @Override
    public String xpathCryptoKeyringPresharedkeyAddressIpv4Unencrytkey() {
        return cryptoKeyringPresharedkeyAddressIpv4Unencrytkey;
    }

    @Override
    public String xpathCryptoKeyringPresharedkeyAddressIpv4Ipv4addr() {
        return cryptoKeyringPresharedkeyAddressIpv4Ipv4addr;
    }

    @Override
    public String xpathCryptoIsakmpProfile() {
        return cryptoIsakmpProfile;
    }

    @Override
    public String xpathCryptoIsakmpProfileName() {
        return cryptoIsakmpProfileName;
    }

    @Override
    public String xpathCryptoIsakmpProfileVrf() {
        return cryptoIsakmpProfileVrf;
    }

    @Override
    public String xpathCryptoIpsecTransformset() {
        return cryptoIpsecTransformset;
    }

    @Override
    public String xpathCryptoIpsecTransformsetTag() {
        return cryptoIpsecTransformsetTag;
    }

    @Override
    public String xpathCryptoIpsecTransformsetKeybit() {
        return cryptoIpsecTransformsetKeybit;
    }

    @Override
    public String xpathCryptoIpsecTransformsetEsphmac() {
        return cryptoIpsecTransformsetEsphmac;
    }

    @Override
    public String xpathCryptoIpsecTransformsetModeTunnel() {
        return cryptoIpsecTransformsetModeTunnel;
    }

    @Override
    public String xpathCryptoIpsecProfile() {
        return cryptoIpsecProfile;
    }

    @Override
    public String xpathCryptoIpsecProfileName() {
        return cryptoIpsecProfileName;
    }

    @Override
    public String xpathCryptoIpsecProfileSetPfsGroup() {
        return cryptoIpsecProfileSetPfsGroup;
    }

    @Override
    public String xpathCryptoIpsecProfileDescription() {
        return cryptoIpsecProfileDescription;
    }

    @Override
    public String xpathInterfaceTunnelIpAddressPrimaryAddress() {
        return interfaceTunnelIpAddressPrimaryAddress;
    }

    @Override
    public String xpathInterfaceTunnelShutdown() {
        return interfaceTunnelShutdown;
    }

    @Override
    public String xpathIntefaceTunnel() {
        return interfaceTunnel;
    }

    @Override
    public String xpathInterfaceAdminstatus() {
        return interfaceAdminstatus;
    }

    @Override
    public String xpathInterfaceOperstatus() {
        return interfaceOperstatus;
    }

    @Override
    public String xpathInterfaceTunnelName() {
        return interfaceTunnelName;
    }

    @Override
    public String xpathInterfaceTunnelDescription() {
        return interfaceTunnelDescription;
    }

    @Override
    public String xpathInterfaceTunnelVrfForwarding() {
        return interfaceTunnelVrfForwarding;
    }

    @Override
    public String xpathInterfaceTunnelIpAddressPrimaryMask() {
        return interfaceTunnelIpAddressPrimaryMask;
    }

    @Override
    public String xpathInterfaceTunnelIpTcpAdjustmss() {
        return interfaceTunnelIpTcpAdjustmss;
    }

    @Override
    public String xpathInterfaceTunnelTunnelSource() {
        return interfaceTunnelTunnelSource;
    }

    @Override
    public String xpathInterfaceTunnelTunnelDestination() {
        return interfaceTunnelTunnelDestination;
    }

    @Override
    public String xpathInterfaceTunnelTunnelProtectionIpsecProfile() {
        return interfaceTunnelTunnelProtectionIpsecProfile;
    }

    @Override
    public String xpathCryptoIsakmpProfileDescription() {
        return cryptoIsakmpProfileDescription;
    }

    @Override
    public String xpathCryptoIsakmpProfileMatchIdentityIpv4addressAddress() {
        return cryptoIsakmpProfileMatchIdentityIpv4addressAddress;
    }

    @Override
    public String xpathCryptoIsakmpProfileMatchIdentityIpv4addressMask() {
        return cryptoIsakmpProfileMatchIdentityIpv4addressMask;
    }

    @Override
    public String xpathCryptoIsakmpProfileMatchIdentityIpv4addressVrf() {
        return cryptoIsakmpProfileMatchIdentityIpv4addressVrf;
    }

    @Override
    public String xpathCryptoKeyringDescription() {
        return cryptoKeyringDescription;
    }

    @Override
    public String xpathRouterBgpAddressfamilyWithvrfIpv4VrfNeighborDescription() {
        return routerBgpAddressfamilyWithvrfIpv4VrfNeighborDescription;
    }

    @Override
    public String xpathNeighborsNeighborDescription() {
        return neighborsNeighborDescription;
    }

    @Override
    public String xpathNeighborsNeighborSessionstate() {
        return neighborsNeighborSessionstate;
    }

    @Override
    public String xpathNeighborsNeighborUptime() {
        return neighborsNeighborUptime;
    }

    @Override
    public String xpathNeighborsNeighborNeighborid() {
        return neighborsNeighborNeighborid;
    }

    @Override
    public String xpathNeighborsNeighborPrefixActivitySentCurrentPrefixes() {
        return neighborsNeighborPrefixActivitySentCurrentPrefixes;
    }

    @Override
    public String xpathNeighborsNeighborPrefixActivityReceivedCurrentPrefixes() {
        return neighborsNeighborPrefixActivityReceivedCurrentPrefixes;
    }

    @Override
    public String xpathNativeInterfaceTunnelNameWithValue(String tunnelName) {
        return nativeInterfaceTunnelNameWithValue.replace("${tunnelName}", tunnelName);
    }

    // Wildcard queries

    @Override
    public String xpathActiveTunnelFilter() {
        return activeTunnelFilter;
    }

    @Override
    public String xpathActiveNativeCryptoKeyringFilter(String value) {
        return activeNativeCryptoKeyringFilter.replace("${Value}", value);
    }

    @Override
    public String xpathActiveBgpStateFilter(String value) {
        return activeBgpStateFilter.replace("${Value}", value);
    }

    @Override
    public String xpathActiveNativeInterfaceFilter(String tunnelId) {
        return activeNativeInterfaceFilter.replace("${TunnelId}", tunnelId);
    }

    @Override
    public String xpathActiveIsakmpProfileFilter(String vpcId) {
        return activeIsakmpProfileFilter.replace("${VpcId}", vpcId);
    }

    @Override
    public String xpathActiveIpsecProfileFilter(String value) {
        return activeIpsecProfileFilter.replace("${Value}", value);
    }

    @Override
    public String xpathActiveInterfacesStateFilter(String tunnelId) {
        return activeInterfacesStateFilter.replace("${TunnelId}", tunnelId);
    }

    @Override
    public String xpathActiveIpsecTransformsetFilter(String value) {
        return activeIpsecTransformsetFilter.replace("${Value}", value);
    }

    private String getLogtag() {
        return LOGTAG;
    }
}
