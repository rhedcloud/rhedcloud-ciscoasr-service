package edu.emory.it.service.ca.provider.mapper;

import com.tailf.jnc.Element;
import com.tailf.jnc.NodeSet;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import org.openeai.config.AppConfig;

import java.util.List;
import java.util.Properties;

public interface StaticNatMapper {

    void init(AppConfig appConfig);

    void initializeTemplates(Properties templates);

    String getExistingLocalIp(NodeSet template) throws ProviderException;

    String getStaticIpMasksFilter(List<String> staticIpMasks);

    List<StaticNat> getStaticNatTransportList(NodeSet staticNatNodeSet, StringBuilder errorBuilder) throws ProviderException;

    String getCreateStaticNatTemplate();

    String getDeleteStaticNatTemplate();

    String getQueryStaticNatTemplate();

    Element buildCreateConfig(String staticNatConfigTemplate, String globalIpAddress, String localIpAddress) throws ProviderException;

    Element buildDeleteConfig(String staticNatConfigTemplate, String globalIpAddress, String localIpAddress) throws ProviderException;

    Element buildQueryConfig(String staticNatConfigTemplate, String globalIpAddress) throws ProviderException;

    String xpathLocalIp();

    String xpathGlobalIp();

    String xpathNativeTransportList();

    String xpathTransportList();

}
