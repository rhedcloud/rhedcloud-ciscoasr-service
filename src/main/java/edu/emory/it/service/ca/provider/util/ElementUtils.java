package edu.emory.it.service.ca.provider.util;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NodeSet;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;

public class ElementUtils {

    private static final Logger logger = OpenEaiObject.logger;
    private static String LOGTAG = "[ElementUtils] ";

    /**
     * Safely retrieve element child node value referenced by xpath expression.
     *
     * @param element
     * @param xpath
     * @return Child node value if found, else null.
     * @throws JNCException
     */
    public static String safeGetValue(Element element, String xpath) throws JNCException {

        String value = "[unknown]";

        if (element != null) {
            String fullXpath = element.name + "/" + xpath;
            // TODO: OK?
            Element valueElement = element.getFirst(xpath);

            if (valueElement != null && valueElement.getValue() != null) {
                value = (String) valueElement.getValue();
            }

            if (value == null) {
                String returnType = "config";
                if (element.name.contains("state")) {
                    returnType = "state";
                }
                logger.warn(getLogtag() + fullXpath + " value not found in returned " + returnType);
            }
        }
        return value;

    }

    /**
     * Safely retrieve element child node value referenced by xpath expression.
     *
     * @param element
     * @param xpath
     * @return Child node value if found, else null.
     * @throws JNCException
     */
    public static String safeGetElementName(Element element, String xpath) throws JNCException {

        String elementName = "[unknown]";

        if (element != null) {
            String fullXpath = element.name + "/" + xpath;
            // TODO: OK?
            Element targetElement = element.getFirst(xpath);

            if (targetElement != null) {
                elementName = targetElement.name;
            }

            if (elementName == null) {
                String returnType = "config";
                if (element.name.contains("state")) {
                    returnType = "state";
                }
                logger.warn(getLogtag() + fullXpath + " element not found in returned " + returnType);
            }
        }
        return elementName;

    }

    public Element createElement(Element content) throws JNCException {
        Element newElement = null;

        Deque hierarchyStack = new ArrayDeque();

        Element parent = content;
        do {
            parent = parent.getParent();
            if (parent != null) {
                hierarchyStack.push(parent);
            }
        } while (parent != null);

        Element leafElement = null;
        do {
            Element element = (Element)hierarchyStack.pop();

            if ("rpc-reply".equalsIgnoreCase(element.name)) {
                Element peek = (Element)hierarchyStack.peek();
                if (peek != null && "data".equalsIgnoreCase(peek.name)) {
                    hierarchyStack.pop();
                    continue;
                }
            }

            Element nextElement = Element.create(element.getContextPrefixMap(), element.name);

            if (newElement == null) {
                newElement = nextElement;
                leafElement = newElement;
            } else {
                leafElement.addChild(nextElement);
                leafElement = nextElement;
            }
            if (hierarchyStack.isEmpty()) {
                leafElement.addChild(content);
                break;
            }
        } while (true);


        return newElement;
    }

    public void buildElementToValuesMap(Map<String, String> elementValuesMap, NodeSet nodes) {

        nodes.forEach(element -> {
            NodeSet children = element.getChildren();

            if (children != null && children.size() > 0) {
                buildElementToValuesMap(elementValuesMap, children);
            }
            Object valueObject = element.getValue();
            if (valueObject != null) {
                String xpath = element.tagpath().toString();
                String rootNodeName = element.getRootElement().name + "/";
                xpath = xpath.replace(rootNodeName, "");
                String value = valueObject.toString();
                elementValuesMap.put(xpath, value);
            }
        });


    }


    private static String getLogtag() {
        return LOGTAG;
    }

}
