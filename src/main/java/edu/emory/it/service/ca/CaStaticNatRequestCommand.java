package edu.emory.it.service.ca;

import com.openii.openeai.commands.MessageMetaData;
import edu.emory.it.service.ca.provider.CiscoAsrProvider;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.StaticNatProvider;
import edu.emory.it.service.ca.provider.util.LockWrapper;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.utils.lock.LockException;

import java.util.List;

public class CaStaticNatRequestCommand extends CaRequestCommand implements RequestCommand {

    private static final String PROVIDER_CLASS_NAME = "caStaticNatProviderClassName";

    private StaticNatProvider provider;

    public CaStaticNatRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        try {

            logger.info(getLogtag() + "Initializing");

            this.provider = initializeProvider();
            super.ciscoAsrProvider = (CiscoAsrProvider)this.provider;

        } catch (Exception e) {
            String errMessage = getLogtag() + "Error initializing provider";
            logger.error(getLogtag() + errMessage, e);
            throw new InstantiationException(errMessage + ": " + e.getMessage());
        }
    }

    @Override
    String handleQuery(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        StaticNatQuerySpecification querySpec = (StaticNatQuerySpecification) retrieveAndBuildObject("Query Data",
                mmd.getQueryObject(), mmd.getQueryObjectName(), (Element) mmd.getQueryObjects().get(0));

        // let the configured provider handle the query
        try {

            List<StaticNat> staticNat = provider.query(querySpec);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            if (staticNat == null || staticNat.isEmpty()) {
                logger.info(getLogtag() + " NULL query response ... returning empty reply");

                // remove the contents of the DataArea element from the primed Provide-Reply document
                // to give an empty reply
                replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());
            } else {
                logger.info(getLogtag() + " Adding static nat " + mmd.getMessageObject() + " object to the Provide-Reply document.");

                addXmlEnterpriseObjectToReplyDoc(mmd, replyDoc, staticNat);
            }

            // return the populated Provide-Reply document...
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        } catch (ProviderException e) {
            String errDescription = getLogtag() + "Exception occurred querying the '" + mmd.getMessageObject() + "' object.";
            logger.error(getLogtag() + errDescription, e);
            // TODO: errNumber
            return createErrorResponseReply(mmd, "application", "LDSS-1001", errDescription, e);
        } catch (EnterpriseLayoutException e) {
            // TODO: errNumber
            String errDescription = getLogtag() + "Exception occurred creating the '" + mmd.getMessageObject()
                    + "' object(s) from the data passed back from the provider.";
            logger.error(getLogtag() + errDescription, e);
            return createErrorResponseReply(mmd, "application", "LDSS-1002", errDescription, e);
        } catch (Exception e) {
            // TODO: errNumber
            String errDescription = getLogtag() + "Exception occurred creating the '" + mmd.getMessageObject()
                    + "' object(s) from the data passed back from the provider.";
            logger.error(getLogtag() + errDescription, e);
            return createErrorResponseReply(mmd, "application", "LDSS-1002", errDescription, e);
        }
    }

    @Override
    String handleCreate(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        StaticNat staticNat = (StaticNat) retrieveAndBuildObject("Create Data",
                mmd.getMessageObject(), mmd.getMessageObjectName(), mmd.getData());

        LockWrapper lockWrapper = acquireCommandLock();

        try {
            provider.add(staticNat);
            publishSync(mmd.getMessageObject(), mmd.getMessageAction(), staticNat, null);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        } catch (ProviderException e) {
            String errDescription = "Exception occurred creating the '" + mmd.getMessageObject() + "' object.";
            // TODO: errNumber
            return createErrorResponseReply(mmd, "application", "LDSS-1003", errDescription, e);
        } finally {
            try {
                lockWrapper.release();
                logger.info(getLogtag() + "Successfully released lock for create command");
            } catch (LockException e) {
                String errMsg = "Failed to release lock after completing handle create command: " + " - " + e.getMessage();
                logger.error(getLogtag() + errMsg);
            }
        }
    }

    @Override
    String handleDelete(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        StaticNat staticNat = (StaticNat) retrieveAndBuildObject("Delete Data",
                mmd.getMessageObject(), mmd.getMessageObjectName(), mmd.getData());

        LockWrapper lockWrapper = acquireCommandLock();
        try {
            provider.delete(staticNat);
            publishSync(mmd.getMessageObject(), mmd.getMessageAction(), staticNat, null);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        } catch (ProviderException e) {
            String errDescription = getLogtag() + "Exception occurred deleting the '" + mmd.getMessageObject() + "' object.";
            logger.error(getLogtag() + errDescription, e);
            return createErrorResponseReply(mmd, "application", "LDSS-1005", errDescription, e);
        } finally {
            try {
                lockWrapper.release();
                logger.info(getLogtag() + "Successfully released lock for delete command");
            } catch (LockException e) {
                String errMsg = "Failed to release lock after completing handle delete command: " + " - " + e.getMessage();
                logger.error(getLogtag() + errMsg);
            }
        }
    }

    @Override
    String handleGenerate(MessageMetaData mmd) throws CommandException {
        return handleUnsupportedAction(mmd);
    }

    @Override
    String handleUpdate(MessageMetaData mmd) throws CommandException {
        return handleUnsupportedAction(mmd);
    }

    @Override
    protected String getSupportedCommands() {
        return String.join(", ", QUERY_ACTION, CREATE_ACTION, DELETE_ACTION);
    }

    @Override
    String getRequiredMessageObject() { return "StaticNat"; }

    private StaticNatProvider initializeProvider() throws InstantiationException {
        String providerClassName = getProperties().getProperty(PROVIDER_CLASS_NAME);
        if (providerClassName == null || providerClassName.equals("")) {
            String errMsg = getLogtag() + "No " + PROVIDER_CLASS_NAME + " property specified. Can't continue.";
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            logger.info(getLogtag() + "Getting provider class for name: " + providerClassName);
            Class<?> providerClass = Class.forName(providerClassName);
            StaticNatProvider providerInstance = (StaticNatProvider) providerClass.newInstance();
            providerInstance.init(getAppConfig());
            logger.info(getLogtag() + "Initialization complete.");

            return providerInstance;
        } catch (ClassNotFoundException e) {
            String errMsg = getLogtag() + "Class named " + providerClassName + "not found on the classpath.  The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        } catch (IllegalAccessException e) {
            String errMsg = getLogtag() + "An error occurred getting a class for name: " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        } catch (ProviderException e) {
            String errMsg = getLogtag() + "An error occurred initializing the provider " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(getLogtag() + errMsg);
            throw new InstantiationException(errMsg);
        }
    }

}
