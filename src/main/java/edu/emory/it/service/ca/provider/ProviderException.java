package edu.emory.it.service.ca.provider;

public class ProviderException extends Exception {

    static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public ProviderException() {
        super();
    }

    /**
     * Message constructor.
     */
    public ProviderException(String msg) {
        super(msg);
    }

    /**
     * Throwable constructor.
     */
    public ProviderException(String msg, Throwable e) {
        super(msg, e);
    }
}
