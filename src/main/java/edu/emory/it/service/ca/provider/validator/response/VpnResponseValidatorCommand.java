package edu.emory.it.service.ca.provider.validator.response;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;

public abstract class VpnResponseValidatorCommand implements VpnResponseValidator {
    private String vpcId;
    private String vpnId;
    private VpnConnection vpnConnection;

    abstract public boolean expectResult();

    public String getVpcId() {
        return vpcId;
    }

    public void setVpcId(String vpcId) {
        this.vpcId = vpcId;
    }

    public String getVpnId() {
        return vpnId;
    }

    public void setVpnId(String vpnId) {
        this.vpnId = vpnId;
    }

    public VpnConnection getVpnConnection() {
        return vpnConnection;
    }

    public void setVpnConnection(VpnConnection vpnConnection) {
        this.vpnConnection = vpnConnection;
    }

}
