package edu.emory.it.service.ca.provider;

import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class ExampleStaticNatProvider extends ExampleProvider implements StaticNatProvider, CiscoAsrProvider{

    private static final Logger logger = OpenEaiObject.logger;
    private final Map<String, StaticNat> nativeModule = new ConcurrentHashMap<>();
    private String exampleProviderBanner =
            "########################### XXXXXXXXXXXXXXXX ############################\n" +
            "#                                                                       #\n" +
            "#        ######   #   #      #     #     #  ####   #       ######       #\n" +
            "#        #         # #      # #    ##   ##  #   #  #       #            #\n" +
            "#        #####      #      #####   # # # #  ####   #       #####        #\n" +
            "#        #         # #    #     #  #  #  #  #      #       #            #\n" +
            "#        ######   #   #  #       # #     #  #      ######  ######       #\n" +
            "#                                                                       #\n" +
            "#                       S T A T I C     N A T                           #\n" +
            "#                                                                       #\n" +
            "#        ####   ####     ##    #     #  #  ####    ######  ####         #\n" +
            "#        #   #  #   #   #  #   #     #  #  #   #   #       #   #        #\n" +
            "#        ####   ####   #    #   #   #   #  #   #   #####   ####         #\n" +
            "#        #      #   #   #  #     # #    #  #   #   #       #   #        #\n" +
            "#        #      #   #    ##       #     #  ####    ######  #   #        #\n" +
            "#                                                                       #\n" +
            "########################### XXXXXXXXXXXXXXXX ############################\n";
    @Override
    public void init(AppConfig appConfig) throws ProviderException {

        if (appConfig != null) {
            super.appConfig = appConfig;
            logger.info(getLogtag() + "init with AppConfig");
            try {
                Properties generalProperties  = appConfig.getProperties("GeneralProperties");
                routerNumber = (String)generalProperties.get("routerNumber");

                displayBanner(exampleProviderBanner);
            } catch (EnterpriseConfigurationObjectException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void add(StaticNat staticNat) throws ProviderException {
        logger.info(getLogtag() + "Add:  public ip: " + staticNat.getPublicIp() + " private ip: " + staticNat.getPrivateIp());

        //fail("2", "Test exception creating static nat");

        // Cause a JMSTimeoutException on the request - rollback will fail.
//        timeoutCondition.trigger("2", 60000);

        if (nativeModule.containsKey(staticNat.getPublicIp())) {
            String errMsg = "Native module already contains public ip " + staticNat.getPublicIp();
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
        nativeModule.put(staticNat.getPublicIp(), staticNat);

        // Cause a JMSTimeoutException on the request - rollback will be successful.
//        timeoutCondition.trigger("2", 60000);

        logger.info(getLogtag() + "Added StaticNat: public ip: " + staticNat.getPublicIp() + " - private ip: " + staticNat.getPrivateIp());

        routerExecutionTimeInMilliseconds = getExecutionTime();
    }

    @Override
    public void delete(StaticNat staticNat) throws ProviderException {
        logger.info(getLogtag() + "Delete:  public ip: " + staticNat.getPublicIp() + " private ip: " + staticNat.getPrivateIp());

        // Cause a JMSTimeoutException on the request - rollback will fail.
//        timeoutCondition.trigger("1", 60000);

        if (!nativeModule.containsKey(staticNat.getPublicIp())) {
            String errMsg = "Native module does not contain public ip " + staticNat.getPublicIp();
            logger.error(getLogtag() + errMsg);
            throw new ProviderException(errMsg);
        }
        nativeModule.remove(staticNat.getPublicIp());

        // Cause a JMSTimeoutException on the request - rollback will be successful.
//        timeoutCondition.trigger("1", 60000);

        logger.info(getLogtag() + "Removed StaticNat: public ip: " + staticNat.getPublicIp() + " - private ip: " + staticNat.getPrivateIp());
        routerExecutionTimeInMilliseconds = getExecutionTime();
    }

    @Override
    public List<StaticNat> query(StaticNatQuerySpecification staticNatQuerySpec) throws ProviderException {

        List<StaticNat> staticNats = null;
        logger.info(getLogtag() + "Query: public ip: " + staticNatQuerySpec.getPublicIp());

        // Cause a JMSTimeoutException on the request - causes rollback on confirmation steps.
//        timeoutCondition.trigger("2", 60000);

        // Cause rollback of confirm steps.
//        fail("2", "Test exception querying static nat");

        StaticNat existingStaticNat = nativeModule.get(staticNatQuerySpec.getPublicIp());
        routerExecutionTimeInMilliseconds = getExecutionTime();

        if (existingStaticNat != null) {
            staticNats = new ArrayList<>();
            logger.info(getLogtag() + "Query - found StaticNat: public ip: " + existingStaticNat.getPublicIp() + " - private ip: " + existingStaticNat.getPrivateIp());
            staticNats.add(existingStaticNat);
        } else {
            logger.info(getLogtag() + "Query - no StaticNat found for public ip: " + staticNatQuerySpec.getPublicIp());
        }
        return staticNats;
    }

    private long getExecutionTime() {
        Random r = new Random();
        return r.nextInt((999 - 1) + 1) + 1;
    }
}
