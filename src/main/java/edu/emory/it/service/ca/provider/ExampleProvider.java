package edu.emory.it.service.ca.provider;

import org.openeai.config.AppConfig;

import java.util.Arrays;

public class ExampleProvider extends CiscoAsr1002Provider {

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
    }

    protected ErrorCondition errorCondition = (a, b, c) -> { if (a > b && (c == null || c.equals(this.routerNumber))) return true; else return false; };
    protected ErrorCondition errorConditionFailOnCount = (a, b, c) -> { if (a == b && (c == null || c.equals(this.routerNumber))) return true; else return false; };
    protected ErrorCondition errorConditionFailAfterCount = (a, b, c) -> { if (a > b && (c == null || c.equals(this.routerNumber))) return true; else return false; };
    protected ErrorRangeCondition errorRangeCondition = (a, b, c, d) -> { if (a >= b && a <= c && (d == null || d.equals(this.routerNumber))) return true; else return false; };

    protected void displayBanner(String banner) {
        String serviceName = "CiscoAsr" + routerNumber + "Service";
        System.out.println(banner.replace("XXXXXXXXXXXXXXXX", serviceName));
    };

    // Note that the configurable property producerRequestTimeoutInterval must be set to a smaller value in NetworkOpsService for this to cause request timeouts. 10 seconds works well.
    protected TimeoutCondition timeoutCondition = (a, b) -> {
        if (a.equals(this.routerNumber)) {
            try {
                Thread.sleep(b);
            } catch (InterruptedException e) {
                throw new RuntimeException("timeoutCondition sleep interrupted", e);
            }
        }
    };

    protected void fail(String tunnelNumber, String message) throws ProviderException {
        if (tunnelNumber.equals(this.routerNumber)) {
            throw new ProviderException(message + " on router " + this.routerNumber);
        }
    }

    protected void fail(String[] tunnelNumber, String message) throws ProviderException {

        if (Arrays.asList(tunnelNumber).contains(this.routerNumber)) {
            throw new ProviderException(message + " on router " + this.routerNumber);
        }
    }

    protected void fail(ErrorCondition errorCondition, int counter, int max, String message) throws ProviderException {
        if (errorCondition.trigger(counter, max, null)) {
            throw new ProviderException(message + " on router " + this.routerNumber);
        }
    }

    protected void fail(ErrorCondition errorCondition, int counter, int max, String tunnelNumber, String message) throws ProviderException {
        if (errorCondition.trigger(counter, max, tunnelNumber)) {
            throw new ProviderException(message + " on router " + this.routerNumber);
        }
    }

    protected void fail(ErrorRangeCondition errorCondition, int counter, int min, int max, String tunnelNumber, String message) throws ProviderException {
        if (errorCondition.trigger(counter, min, max, tunnelNumber)) {
            throw new ProviderException(message + " on router " + this.routerNumber);
        }
    }

    public interface ErrorCondition {
        boolean trigger(int count, int max, String tunnelNumber);
    }

    public interface ErrorRangeCondition {
        boolean trigger(int count, int min, int max, String tunnelNumber);
    }

    public interface TimeoutCondition {
        void trigger(String tunnelNumber, Integer sleepInterval);
    }
}
