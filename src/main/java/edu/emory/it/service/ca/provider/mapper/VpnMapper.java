package edu.emory.it.service.ca.provider.mapper;

import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NodeSet;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseFieldException;

import java.util.Map;
import java.util.Properties;

public interface VpnMapper {


    void init(AppConfig appConfig, String tunnelNumber);
    void initializeTemplates(Properties templates);

    Element hydrateTemplate(Map<String, String> queryMap, String templateId, Map<String, String> pathValueMap) throws ProviderException;

    VpnConnection hydrateVpnConnection(VpnConnection vpnConnection, Map<String, Element> resultMap) throws JNCException, EnterpriseFieldException, ProviderException;

    String getTemplate(String templateName)  throws ProviderException;
    Integer getVpnIdFromTunnelName(String tunnelName);
    String getVpcIdFromTunnelDescription(String tunnelDescription);
    String getSubstitutionToken(String valueId) throws ProviderException;
    String buildQueryTunnelId(Integer routerNumber, Integer tunnelNumber, Integer vpnId) throws ProviderException;
    String buildPaddedVpnId(Integer vpnId) throws ProviderException;
    Map<String, Element> buildDescriptionMap(NodeSet nativeResult, NodeSet nativeTunnelResult, NodeSet bgpStateDataResult);
    void validatePathToValuesMap(Map<String, String> pathToValuesMap) throws ProviderException;

    String xpathNative();
    String xpathNativeInterface();
    String xpathNativeCryptoKeyringName();
    String xpathNativeCryptoIpsecProfileName();
    String xpathNativeCryptoIpsecTransformsetTag();
    String xpathNativeInterfaceTunnelNameWithValue(String tunnelName);


    String xpathNativeRouterBgpId();
    String xpathBgpStateData();

    String xpathInterfacesState();

    String xpathCryptoKeyring();
    String xpathCryptoKeyringName();
    String xpathCryptoKeyringPresharedkeyAddressIpv4Unencrytkey();
    String xpathCryptoKeyringPresharedkeyAddressIpv4Ipv4addr();
    String xpathCryptoIsakmpProfile();
    String xpathCryptoIsakmpProfileName();
    String xpathCryptoIsakmpProfileVrf();
    String xpathCryptoIsakmpProfileMatchIdentityIpv4addressAddress();
    String xpathCryptoIsakmpProfileMatchIdentityIpv4addressMask();
    String xpathCryptoIsakmpProfileMatchIdentityIpv4addressVrf();
    String xpathCryptoIpsecProfileName();
    String xpathCryptoIpsecProfileSetPfsGroup();
    String xpathCryptoIpsecTransformset();
    String xpathCryptoIpsecTransformsetTag();
    String xpathCryptoIpsecTransformsetKeybit();
    String xpathCryptoIpsecTransformsetEsphmac();
    String xpathCryptoIpsecTransformsetModeTunnel();

    String xpathCryptoIpsecProfile();

    String xpathNativeInterfaceTunnelName();

    String xpathCryptoIpsecProfileDescription();

    String xpathIntefaceTunnel();
    String xpathInterfaceAdminstatus();
    String xpathInterfaceOperstatus();
    String xpathInterfaceTunnelName();
    String xpathInterfaceTunnelIpAddressPrimaryAddress();
    String xpathInterfaceTunnelDescription();
    String xpathInterfaceTunnelShutdown();
    String xpathInterfaceTunnelVrfForwarding();
    String xpathInterfaceTunnelIpAddressPrimaryMask();
    String xpathInterfaceTunnelIpTcpAdjustmss();
    String xpathInterfaceTunnelTunnelSource();
    String xpathInterfaceTunnelTunnelDestination();
    String xpathInterfaceTunnelTunnelProtectionIpsecProfile();

    String xpathCryptoIsakmpProfileDescription();
    String xpathCryptoKeyringDescription();
    String xpathRouterBgpAddressfamilyWithvrfIpv4VrfNeighborDescription();

    String xpathNeighborsNeighborDescription();
    String xpathNeighborsNeighborSessionstate();
    String xpathNeighborsNeighborUptime();
    String xpathNeighborsNeighborNeighborid();
    String xpathNeighborsNeighborPrefixActivitySentCurrentPrefixes();
    String xpathNeighborsNeighborPrefixActivityReceivedCurrentPrefixes();

    String xpathActiveTunnelFilter();
    String xpathActiveNativeCryptoKeyringFilter(String vpcId);
    String xpathActiveBgpStateFilter(String vpcId);
    String xpathActiveNativeInterfaceFilter(String tunnelId);
    String xpathActiveIsakmpProfileFilter(String vpcId);
    String xpathActiveIpsecProfileFilter(String vpcId);
    String xpathActiveInterfacesStateFilter(String tunnelId);
    String xpathActiveIpsecTransformsetFilter(String value);


}
