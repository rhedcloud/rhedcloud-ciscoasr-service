package edu.emory.it.service.ca.tailf.sample.log;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile;
import edu.emory.moa.objects.resources.v1_0.TunnelProfile;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;

import java.util.List;

public class VpnConnectionRequisitionDump extends Dump {

    final private VpnConnectionRequisition vpnConnectionRequisition;
    private int profileNumber = 1;

    public VpnConnectionRequisitionDump(VpnConnectionRequisition vpnConnectionRequisition) {
        this.vpnConnectionRequisition = vpnConnectionRequisition;
    }

    public void dump() {
        VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.getVpnConnectionProfile();

        dumpHeader("VpnConnectionRequisition values");
        dump("Owner Id: " + vpnConnectionRequisition.getOwnerId());
        // TODO: From RemoteVpnConnectionInfo
//        dump("Remove Vpn Ip Address: " + vpnConnectionRequisition.getRemoteVpnIpAddress());
//        dump("Preshared Key: " + vpnConnectionRequisition.getPresharedKey());

        increaseIndent();

        dumpHeader("VpnConnectionProfile values");

        dump("Vpn Connection Profile Id: " + vpnConnectionProfile.getVpnConnectionProfileId());
        dump("Vpc Network: " + vpnConnectionProfile.getVpcNetwork());
        List<TunnelProfile> tunnelProfileList = vpnConnectionProfile.getTunnelProfile();
        if (tunnelProfileList != null) {
            increaseIndent();
            dumpHeader("Tunnel Profiles");
            dump("Tunnel Profile count: " + tunnelProfileList.size());
            increaseIndent();
            tunnelProfileList.forEach(profile -> {
                dumpHeader("Tunnel Profile " + getProfileNumber());
                increaseIndent();
                dump("Tunnel Id: " + profile.getTunnelId());
                dump("Crypto Keyring Name: " + profile.getCryptoKeyringName());
                dump("Isakmp Profile Name: " + profile.getIsakampProfileName());
                dump("Ipsec TransformSet Name: " + profile.getIpsecTransformSetName());
                dump("Ipsec Profile Name: " + profile.getIpsecProfileName());
                dump("Tunnel Description: " + profile.getTunnelDescription());
                dump("Customer Gateway Ip: " + profile.getCustomerGatewayIp());
                dump("Vpn Inside Ip Cidr 1: " + profile.getVpnInsideIpCidr1());
                dump("Vpn Inside Ip Cidr 2: " + profile.getVpnInsideIpCidr2());
                decreaseIndent();

            });
        }
    }

    private String getProfileNumber() {
        return new Integer(profileNumber++).toString();
    }

}
