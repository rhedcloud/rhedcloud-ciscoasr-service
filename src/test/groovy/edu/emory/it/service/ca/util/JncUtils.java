package edu.emory.it.service.ca.util;

import com.tailf.jnc.*;
import org.junit.Test;

public class JncUtils {

    @Test
    public void buildErrorReplyNodeSet() {

        String error = "<rpc-error xmlns=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
                "    <error-type>application</error-type>\n" +
                "    <error-tag>resource-denied</error-tag>\n" +
                "    <error-severity>error</error-severity>\n" +
                "    <nc:error-path xmlns:cisco-ia=\"http://cisco.com/yang/cisco-ia\" xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
                "      /nc:rpc/cisco-ia:save-config\n" +
                "    </nc:error-path>\n" +
                "    <error-message unknown:lang=\"en\">resource denied: Sync is in progress</error-message>\n" +
                "    <error-info>\n" +
                "      <bad-element>save-config</bad-element>\n" +
                "    </error-info>\n" +
                "  </rpc-error>";

        try {


            Prefix[] prefixes = new Prefix[3];

            prefixes[0] = new Prefix("", "urn:ietf:params:xml:ns:netconf:base:1.0");
            prefixes[1] = new Prefix("nc", "urn:ietf:params:xml:ns:netconf:base:1.0");
            prefixes[2] = new Prefix("unknown", "urn:ietf:params:xml:ns:netconf:base:1.0");
            PrefixMap prefixMap = new PrefixMap(prefixes);

            Element rpcError = Element.create(prefixMap, "rpc-error");
            NodeSet errorReplyNodeSet = new NodeSet(rpcError);

            rpcError.createChild("error-type", "application");
            rpcError.createChild("error-tag", "resource-denied");
            rpcError.createChild("error-severity", "error");
            rpcError.createChild("error-path", "/nc:rpc/cisco-ia:save-config");

            Element errorMessage = rpcError.createChild("error-message", "resource denied: Sync is in progress");
            Attribute errorMessageAttribute = new Attribute("unknown:lang", "en");
            errorMessage.addAttr(errorMessageAttribute);

            Element errorInfo = rpcError.createChild("errorInfo");
            errorInfo.createChild("bad-element", "save-config");

            System.out.println(errorReplyNodeSet.toXMLString());

            errorMessage = errorReplyNodeSet.getFirst("error-message");

            if (errorMessage != null) {
                System.out.println("Error message: " + errorMessage.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Test
    public void buildSuccessReplyNodeSet() {

        String success = "<result xmlns=\"http://cisco.com/yang/cisco-ia\" xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">Save running-config successful</result>";

        try {


            Prefix[] prefixes = new Prefix[2];

            prefixes[0] = new Prefix("", "http://cisco.com/yang/cisco-ia");
            prefixes[1] = new Prefix("nc", "urn:ietf:params:xml:ns:netconf:base:1.0");
            PrefixMap prefixMap = new PrefixMap(prefixes);

            Element result = Element.create(prefixMap, "result");
            result.setValue("Save running-config successful");

            NodeSet successReplyNodeSet = new NodeSet(result);

            System.out.println(successReplyNodeSet.toXMLString());

            Element errorMessage = successReplyNodeSet.getFirst("error-message");

            if (errorMessage != null) {
                System.out.println("Error message: " + errorMessage.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // Success:
    // <result xmlns="http://cisco.com/yang/cisco-ia" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">Save running-config successful</result>

}
