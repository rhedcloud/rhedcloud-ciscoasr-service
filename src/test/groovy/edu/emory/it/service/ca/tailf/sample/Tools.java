package edu.emory.it.service.ca.tailf.sample;

import com.tailf.jnc.Capabilities;
import com.tailf.jnc.Element;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.NodeSet;
import com.tailf.jnc.XMLParser;
import edu.emory.it.service.ca.provider.CiscoAsr1002NetconfVpnConnectionProvider;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.IPV4Address;
import edu.emory.it.service.ca.provider.util.RouterUtil;
import edu.emory.it.service.ca.provider.util._1662AwsCiscoAsrSpreadsheet;
import edu.emory.it.service.ca.provider.validator.VpnConnectionOperationContext;
import edu.emory.it.service.ca.util.SpreadSheetWrapper;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import org.junit.Before;
import org.junit.Test;
import org.openeai.config.AppConfig;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.w3c.dom.Document;
import sun.net.util.IPAddressUtil;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class Tools extends BaseTools {

    private static final String QUERY_RESULT_MAP_NATIVE_KEY = "native";
    private static final String QUERY_RESULT_MAP_NATIVE_KEYRING_KEY = "native/keyring";
    private static final String QUERY_RESULT_MAP_NATIVE_ISAKMP_PROFILE_KEY = "native/isakmp/profile";
    private static final String QUERY_RESULT_MAP_NATIVE_IPSEC_TRANSFORM_SET_KEY = "native/ipsec/transform-set";
    private static final String QUERY_RESULT_MAP_NATIVE_IPSEC_PROFILE_KEY = "native/ipsec/profile";

    private static final String QUERY_RESULT_MAP_NATIVE_INTERFACE = "nativeInterface";
    private static final String QUERY_RESULT_MAP_BGP_STATE_DATA_KEY = "bgp-state-data";
    private static final String QUERY_RESULT_MAP_BGP_CONFIG_DATA_KEY = "bgp-config";
    private static final String QUERY_RESULT_MAP_INTERFACES_STATE_KEY = "interfaces-state";


    private AppConfig appConfig;
    @Override
    protected String getLogtag() { return "[Netconf Tools] ";}

    @Before
    public void init() {
//        TestInitialContextFactoryBuilder.setInitialContextFactoryBuilder(new File(jndiFile));
        appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
        try {
            getRouterCredentials(appConfig);
        } catch (ProviderException e) {
            logger.error("Failed to get router credentials", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * With netconf session:
     * <interfaces-state xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
     *
     * With rpc call:
     * <interfaces-state xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
     */
    @Test
    public void testInterfacesStateWildcardQuery() {
        String tunnelNumber = "2";
        Router router;
        _1662AwsCiscoAsrSpreadsheet spreadsheet = new _1662AwsCiscoAsrSpreadsheet();

        if ("1".equals(tunnelNumber)) {
            router = Router.ROUTER1;
        } else if ("2".equals(tunnelNumber)) {
            router = Router.ROUTER2;
        } else {
            throw new RuntimeException("Invalid tunnel number: " + tunnelNumber);
        }

        String activeTunnelFilter = "native/interface/Tunnel[starts-with(name, '" + tunnelNumber + "')]/../Tunnel[starts-with(description, '" + spreadsheet.getTunnelDescriptionPrefix() + "')]";
        String keyringSectionsFilter = "native/crypto/keyring[starts-with(name, 'keyring-vpn-research-vpc')]";
        String isakmpProfileSectionsFilter = "native/crypto/isakmp/profile[starts-with(name, 'isakmp-vpn-research-vpc')]";
//        String interfacesStateFilter = "interfaces-state/interface[starts-with(name, 'Tunnel" + tunnelNumber + "')]";
        String interfacesStateFilter = "interfaces-state/interface[type='ianaift:tunnel' and number(substring-after(name, 'Tunnel')) > number(10000)]";
//        String interfacesStateFilter = "interfaces-state/interface[contains(name, 'Tunnel" + tunnelNumber + "')]";
        String typeFilter = "interfaces-state/interface[starts-with(type, 'ianaift:t')]";

        // <type xmlns:ianaift="urn:ietf:params:xml:ns:yang:iana-if-type">ianaift:tunnel</type>

        NetconfSession session;

        NodeSet configuredTunnels;
        try {
            session = openNetconfSession(router);
            useRpcCall = true;
            configuredTunnels = getWithFilter(interfacesStateFilter, session);
//            configuredTunnels = getWithFilter(typeFilter, session);
//            configuredTunnels = getWithFilter(activeTunnelFilter, session);
//            configuredTunnels = getWithFilter(keyringSectionsFilter, session);
//            configuredTunnels = getWithFilter(keyringSectionsFilter, session);

        } catch (Exception e) {
            String errMsg = "Failed to query router " + tunnelNumber + " for native interfaces";
            logger.error(getLogtag() + errMsg);
            throw new RuntimeException(errMsg, e);
        }

        List<Element> interfaces_state;
        try {

            String xml = configuredTunnels.toXMLString();
            configuredTunnels = configuredTunnels.get("interface");

            interfaces_state = configuredTunnels.stream()
                    .filter(t -> {
                        try {
                            return t.getFirst("name").value.toString().startsWith("Tunnel" + tunnelNumber);
                        } catch (Exception e) {
                            return false;
                        }
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException("Failed to get interfaces-state nodes", e);
        }

        interfaces_state.forEach(is -> {
            try {
                String val = is.getFirst("name").value.toString();
                logger.info("Val: " + val);
            } catch (Exception e) {
                throw new RuntimeException("Failed to get first interface", e);
            }
        });
    }

    @Test
    public void testMultisessionCapability() {
        Router router = Router.ROUTER1;

        NetconfSession session1 = null;
        NetconfSession session2 = null;
        try {
            session1 = openNetconfSession(router);
            session2 = openNetconfSession(router);
            logger.info("Two sessions opened.");

            String vpcId = "vpc-0cff8a62f60a197cf";
            NodeSet result = session2.get("native/interface/Tunnel/description[text()[contains(.,'" + vpcId + "')]]/.. " +
                    "| native//description[text() = '" + vpcId + "']/.. " +
                    "| bgp-state-data/neighbors/neighbor/description[text() = '" + vpcId + "']/..");

            if (result != null) {
                logger.info("vpn info: " + result.toXMLString());
            } else {
                logger.info("No vpn info returned for vpcId: " + vpcId);
            }

        } catch (Exception e) {
            logger.error("Failed to open two sessions: " + e.getMessage());
        } finally {
            closeNetconfSession(session1);
            closeNetconfSession(session2);
        }

    }


    @Test
    public void testMultipleSessions() {
        Router router = Router.ROUTER1;

        NetconfSession session1 = null;
        NetconfSession session2 = null;

        try {
            session1 = openNetconfSession(router);
            session2 = openNetconfSession(router);

            NodeSet staticNats1 = session1.get("native/ip/nat/inside/source/static");
            logger.info("staticNats1:\n" + staticNats1.toXMLString());
            NodeSet staticNats2 = session2.get("native/ip/nat/inside/source/static");
            logger.info("staticNats2:\n" + staticNats2.toXMLString());

        } catch (Exception e) {
            logger.error("Failed to open sessions", e);
        } finally {
            closeNetconfSession(session1);
            closeNetconfSession(session2);
        }
    }

    @Test
    public void getStaticNatsOnBothRouters() {

        getStaticNats(Router.ROUTER1);

        getStaticNats(Router.ROUTER2);
    }

    public void getStaticNats(Router router) {
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            logger.info("********** " + router.getName() + " ***********");
            NodeSet staticNats = session.get("native/ip/nat/inside/source/static");

            staticNats = session.get("native/ip/nat/inside/source/static/nat-static-transport-list[starts-with(global-ip, '170.140.248.') or starts-with(global-ip, '170.140.249.')] ");
//            staticNats = session.get("native/ip/nat/inside/source/static/nat-static-transport-list[starts-with(global-ip, '1')] ");


            NodeSet result = staticNats.get("ip/nat/inside/source/static/nat-static-transport-list");
            Map<String, String> resultMap = new LinkedHashMap<>();
            result.forEach(sn -> {
                logger.info(sn.toXMLString());
                try {
                    resultMap.put((String)sn.getFirst("global-ip").value, (String)sn.getFirst("local-ip").value);
                } catch (JNCException e) {
                    logger.error("Fail", e);
                }
            });
//            logger.info(staticNats.toXMLString());

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

    }

    @Test
    public void getActiveTunnelsOnBothRouters() {
        List<Integer> tunnelNumbers;

        Router router = Router.BOTH;

        // When tracking deletes - set to false.
        boolean checkForMissingTunnels = false;
        List<Integer> activeTunnels;

        if (router.equals(Router.ROUTER1) || router.equals(Router.BOTH)) {
            tunnelNumbers = getActiveTunnels(Router.ROUTER1);

            if (checkForMissingTunnels) {
                activeTunnels = checkContinuity(tunnelNumbers, 10001);

                if (activeTunnels.size() > 0) {
                    logger.error("Missing tunnel numbers on router 1:");
                    activeTunnels.forEach(tunnelNumber -> {
                        logger.error("   " + tunnelNumber);
                    });
                }
            }
        }

        tunnelNumbers = getActiveTunnels(Router.ROUTER2);

        if (router.equals(Router.ROUTER1) || router.equals(Router.BOTH)) {
            if (checkForMissingTunnels) {
                activeTunnels = checkContinuity(tunnelNumbers, 20001);

                if (activeTunnels.size() > 0) {
                    logger.error("Missing tunnel numbers on router 2:");
                    activeTunnels.forEach(tunnelNumber -> {
                        logger.error("   " + tunnelNumber);
                    });
                }
            }
        }

        logger.info("\nDone.\n");
    }

    /*
<rpc message-id="101" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <get>
    <filter>
      <netconf-state xmlns="urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring">
        <capabilities/>
      </netconf-state>
    </filter>
  </get>
</rpc>
     */
    @Test
    public void testGetCapabilities() {
        NetconfSession session = null;
        final String rpcGetWithXpathTemplate = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"1\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter><netconf-state xmlns=\"urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring\">\n" +
                "        <capabilities/>\n" +
                "      </netconf-state></nc:filter>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        final String getCapabilities = "<netconf-state xmlns=\"urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring\">\n" +
                "        <capabilities/>\n" +
                "      </netconf-state>";
        try {
            session = openNetconfSession(Router.ROUTER1);

            String rpcCmd = rpcGetWithXpathTemplate;

            logger.info(rpcCmd);

            Element result = session.rpc(rpcCmd);

            logger.info("Capabilities: " + result.toXMLString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            closeNetconfSession(session);
        }
    }


    public List<Integer> getActiveTunnels(Router router) {
        List<Integer> tunnelNumbers = new ArrayList<>();
        _1662AwsCiscoAsrSpreadsheet spreadsheet = new _1662AwsCiscoAsrSpreadsheet();

        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
//            session.lock(NetconfSession.RUNNING);


            NodeSet configuredTunnels = session.get("native/interface/Tunnel[starts-with(name, '" + router.getTunnelNumber() + "')]/../Tunnel[starts-with(description, '" + spreadsheet.getTunnelDescriptionPrefix() + "')]");

            configuredTunnels = configuredTunnels.get("interface/Tunnel");
            List<Element> activeTunnels = configuredTunnels.stream()
                    .filter(t -> {
                        try {
                            return !t.getFirst("description").value.toString().contains("AVAILABLE");
                        } catch (Exception e) {
                            return false;
                        }
                    })
                    .collect(Collectors.toList());

            if (activeTunnels.size() == 0) {
                logger.info("\n*** NO ACTIVE TUNNELS FOUND ON ROUTER " + router.getTunnelNumber() + " ***");
            } else {
                logger.info("\n" + activeTunnels.size() + " active tunnels found on router " + router.getTunnelNumber() + ":");
                activeTunnels.forEach(at -> {
                    try {
                        String tunnelNumber = (String) at.getFirst("name").value;
                        String description = (String) at.getFirst("description").value;

                        logger.info("Tunnel name: " + tunnelNumber + " - description: " + description);

                        tunnelNumbers.add(Integer.parseInt(tunnelNumber));
                    } catch (JNCException e) {
                        logger.error("Failed to get 'name' element", e);
                    }
                });
            }
            //logger.info(configuredTunnels.toXMLString());


        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
//            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
        return tunnelNumbers;
    }

    private Long totalTotalQueryTime = 0L;

    @Test
    public void testGetActiveTunnelsByWildcard() {

        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

        try {
            provider.init(appConfig);
        } catch (ProviderException e) {
            logger.error(getLogtag() + "Failed to init provider", e);
            throw new RuntimeException("Failed to init provider", e);
        }

        router = Router.ROUTER2;


//        List<VpnConnection> vpnConnections = getActiveTunnelsByWildcard(provider, router, Boolean.FALSE);
        List<VpnConnection> vpnConnections = null;

        useRpcCall = Boolean.FALSE;

        int iterations = 1;
        for (int n = 1; n <= iterations; n++) {
            logger.info("Iteration #" + n);
            Long totalQueryTime = 0L;
            try {
                VpnConnectionQuerySpecification vpnConnectionQuerySpecification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
                vpnConnections = provider.queryByWildcard(vpnConnectionQuerySpecification);
            } catch (ProviderException e) {
                logger.error("Failed to query by wildcard", e);
                break;
            }
            totalTotalQueryTime += totalQueryTime;
        }

        logger.info("Average total query time: " + totalTotalQueryTime / iterations);

        if (vpnConnections == null || vpnConnections.size() == 0) {
            logger.info(getLogtag() + "No vpn connections found for wildcard query");
        } else {

            logger.info(getLogtag() + vpnConnections.size() + " active tunnels found for wildcard query");

            int lastVpnConnectionIndex = vpnConnections.size() - 1;

            VpnConnection vpnConnection = vpnConnections.get(lastVpnConnectionIndex);

            try {
                logger.info(getLogtag() + "Sample vpn connection:\n" + vpnConnection.toXmlString());
            } catch (XmlEnterpriseObjectException e) {
                logger.error("Failed to format as XML for last vpn connection at index: " + labRemoteVpnIpAddress);
            }
        }
    }

    private Boolean useRpcCall = false;
    static Long messageId = 0L;

    private NodeSet getWithFilter(String filter, NetconfSession session) throws JNCException, IOException {
        NodeSet result;

        // xmlns:nc="urn:ietf:params:xml:ns:yang:ietf-interfaces"

        final String rpcGetWithXpathTemplate = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"  nc:message-id=\"[----messageId----]\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" nc:select=\"[----xpathFilter----]\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        final String rpcGetWithXpathTemplateType = "<nc:rpc xmlns=\"urn:ietf:params:xml:ns:yang:ietf-interfaces\" xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"  nc:message-id=\"[----messageId----]\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" xmlns:ianaift=\"urn:ietf:params:xml:ns:yang:iana-if-type\" nc:select=\"[----xpathFilter----]\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        final String rpcGetWithXpathTemplateSave = "<nc:rpc xmlns=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:nc=\"urn:ietf:params:xml:ns:yang:ietf-interfaces\" nc:message-id=\"[----messageId----]\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" xmlns:nc=\"urn:ietf:params:xml:ns:yang:ietf-interfaces\" nc:select=\"[----xpathFilter----]\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        if (useRpcCall) {

            Boolean useRegex = Boolean.TRUE;

            String rootNodeName;

            // Regex is about 500 ms slower than character crawl...
            if (useRegex) {
                final Pattern pattern = Pattern.compile("^([\\w\\-\\_]+)[/\\[]{0,1}.*");
                Matcher matcher = pattern.matcher(filter);

                if (matcher.matches()) {
                    rootNodeName = matcher.group(1);
                } else {
                    throw new IllegalArgumentException("No root node found in filter: " + filter);
                }

            } else {
                int segmentIndex = filter.indexOf('/');
                int expressionIndex = filter.indexOf('[');
                if (segmentIndex > 0) {
                    if (expressionIndex > 0 && expressionIndex < segmentIndex) {
                        rootNodeName = filter.substring(0, expressionIndex);
                    } else {
                        rootNodeName = filter.substring(0, segmentIndex);
                    }
                } else if (segmentIndex == -1 && expressionIndex > 0){
                    rootNodeName = filter.substring(0, expressionIndex);
                } else if (filter.length() > 0){
                    rootNodeName = filter;
                } else {
                    throw new IllegalArgumentException("No root node found in filter: " + filter);
                }
            }
            String rpc = rpcGetWithXpathTemplate.replace("[----xpathFilter----]", filter).replace("[----messageId----]", (++messageId).toString());
            logger.info("RPC call:\n" + rpc);
            Element element = session.rpc(rpc);
            String xmlResult = element.toXMLString();
            logger.info("rpc result: \n" + xmlResult);
            result = element.get("data/" + rootNodeName);
        } else {
            result = session.get(filter);
            String xmlResult = result.toXMLString();
            logger.info("get result: " + xmlResult);
        }

        return result;
    }

    @Test
    public void testSendRpc() {

        NetconfSession session = null;
        _1662AwsCiscoAsrSpreadsheet spreadsheet = new _1662AwsCiscoAsrSpreadsheet();

        String rpcCommand = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"6\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" nc:select=\"interfaces-state/interface[starts-with(name, 'Tunnel1')]\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        String rpcCommand2 = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"6\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" nc:select=\"native/interface/Tunnel[starts-with(name, '10')]/../Tunnel[starts-with(description, '" + spreadsheet.getTunnelDescriptionPrefix() + "')]\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";


        String rpcCommand3 = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"6\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" nc:select=\"native/interface/Tunnel[contains(name, '^[12][12][0-9][0-9][0-9]$')]/../Tunnel[not (contains(description, 'AVAILABLE'))]\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        Map<String, String> spreadSheetValues = spreadsheet.buildSpreadsheetValues("vpc-testnum2", 1, "1", VpnConnectionOperationContext.DELETE_REQUEST);

        logger.info(getLogtag() + "tunnelDescription: " + spreadSheetValues.get("tunnelDescription"));
        // ^[12][12][0-9][0-9][0-9]$
        try{
            session = openNetconfSession();

            long startTime = System.currentTimeMillis();

            Element results = session.rpc(rpcCommand2);

            NodeSet tunnels = results.get("data/native/interface/Tunnel");

            List<Element> activeTunnels = tunnels.stream()
                    .filter(t -> {
                        try {
                            return !t.getFirst("description").value.toString().contains("AVAILABLE");
                        } catch (Exception e) {
                            return false;
                        }
                    })
                    .collect(Collectors.toList());

            long totalTime = (System.currentTimeMillis() - startTime);


            logger.info(getLogtag() + tunnels.size() + " total rpc results, with " + activeTunnels.size() + " active tunnels in " + totalTime + " milliseconds:\n" + results.toXMLString());

        } catch (Exception e) {
            logger.error(getLogtag() + "Failed to send rpc", e);
        } finally {
            closeNetconfSession(session);
        }

    }

    @Test
    public void recursiveScanTest() {
        String xmlString = queryTunnelTemplate;

        Element nodes;
        try {
            nodes = new XMLParser().parse(xmlString);
        } catch (JNCException e) {
            throw new RuntimeException("Failed to parse xmlString", e);
        }

        Map<String, List<String>> elementTokensMap = new ConcurrentHashMap<>();
        NodeSet childNodes = nodes.getChildren();

        buildElementToTokensMap(elementTokensMap, childNodes);

        Set<String> uniqueTokens = new HashSet<>();
        elementTokensMap.entrySet().forEach(es -> {
            String xpath = es.getKey();
            List<String> tokens = es.getValue();

            try {
                Object value = nodes.getValue(xpath);
                System.out.println(xpath + " -> " + value);
            } catch (JNCException e) {
                throw new RuntimeException("Failed to get node value for xpath: " + xpath, e);
            }
            tokens.forEach(t -> {
                System.out.println("    " + t);
                uniqueTokens.add(t);
            });

        });

        System.out.println("########################## Unique Tokens ################################");

        uniqueTokens.forEach(ut -> {
            System.out.println(ut);
        });
    }

    private void buildElementToTokensMap(Map<String, List<String>> elementTokensMap, NodeSet nodes) {

        nodes.forEach(element -> {
            NodeSet children = element.getChildren();

            if (children != null && children.size() > 0) {
                buildElementToTokensMap(elementTokensMap, children);
            }
            Object valueObject = element.getValue();
            if (valueObject != null) {
                String xpath = element.tagpath().toString();
                String rootNodeName = element.getRootElement().name + "/";
                xpath = xpath.replace(rootNodeName, "");
                String value = valueObject.toString();
                System.out.println(element.tagpath().toString() + " -> " + value);
                final Pattern pattern = Pattern.compile("\\$\\{.+?\\}");
                final Matcher matcher = pattern.matcher(value);
                List<String> tokens = new ArrayList<>();
                while (matcher.find()) {
                    tokens.add(matcher.group());
                }
                if (tokens.size() != 0) {
                    if (elementTokensMap.get(xpath) == null) {
                        elementTokensMap.put(xpath, tokens);
                    }
                }
            }
        });


    }

    @Test
    public void recursiveScanForValuesTest() {
        String xmlString = createTunnelTemplate;

        Element nodes;
        try {
            nodes = new XMLParser().parse(xmlString);
        } catch (JNCException e) {
            throw new RuntimeException("Failed to parse xmlString", e);
        }

        Map<String, String> elementValuesMap = new ConcurrentHashMap<>();
        NodeSet childNodes = nodes.getChildren();

        buildElementToValuesMap(elementValuesMap, childNodes);


//        System.out.println("########################## Element -> Value ################################");
//
//        elementValuesMap.forEach((k,v) -> {
//            System.out.println(k + " -> " + v);
//        });

        // Scan for ${BgpNeighborId}
        elementValuesMap.forEach((k,v) -> {

            if (k.startsWith("router") && v.equals("${BgpNeighborId}")) {
                System.out.println("Path to neighborid in router: " + k);
            }
            System.out.println(k + " -> " + v);
        });


    }

    private void buildElementToValuesMap(Map<String, String> elementValuesMap, NodeSet nodes) {

        nodes.forEach(element -> {
            NodeSet children = element.getChildren();

            if (children != null && children.size() > 0) {
                buildElementToValuesMap(elementValuesMap, children);
            }
            Object valueObject = element.getValue();
            if (valueObject != null) {
                String xpath = element.tagpath().toString();
//                String rootNodeName = element.getRootElement().name + "/";
//                xpath = xpath.replace(rootNodeName, "");
                String value = valueObject.toString();
                elementValuesMap.put(xpath, value);
            }
        });


    }

    @Test
    public void dumpByteArray() {

        // To convert debug byte array dump (each line will look like this: 0 = 60) into the following:

        // 1) Use following regex:
        //       find:    ^\d+ \= (\d+)
        //       replace: $1,

        // 2) Place results between curly braces:
        //
        //  byte[] b = {
        //  };

        // 3) Discard everything after, including first two ']' (93) :

        byte[] b = {
                60,
                110,
                99,
                58,
                114,
                112,
                99,
                32,
                120,
                109,
                108,
                110,
                115,
                58,
                110,
                99,
                61,
                34,
                117,
                114,
                110,
                58,
                105,
                101,
                116,
                102,
                58,
                112,
                97,
                114,
                97,
                109,
                115,
                58,
                120,
                109,
                108,
                58,
                110,
                115,
                58,
                110,
                101,
                116,
                99,
                111,
                110,
                102,
                58,
                98,
                97,
                115,
                101,
                58,
                49,
                46,
                48,
                34,
                32,
                110,
                99,
                58,
                109,
                101,
                115,
                115,
                97,
                103,
                101,
                45,
                105,
                100,
                61,
                34,
                54,
                34,
                62,
                60,
                110,
                99,
                58,
                103,
                101,
                116,
                62,
                13,
                10,
                60,
                110,
                99,
                58,
                102,
                105,
                108,
                116,
                101,
                114,
                32,
                110,
                99,
                58,
                116,
                121,
                112,
                101,
                61,
                34,
                120,
                112,
                97,
                116,
                104,
                34,
                32,
                110,
                99,
                58,
                115,
                101,
                108,
                101,
                99,
                116,
                61,
                34,
                105,
                110,
                116,
                101,
                114,
                102,
                97,
                99,
                101,
                115,
                45,
                115,
                116,
                97,
                116,
                101,
                47,
                105,
                110,
                116,
                101,
                114,
                102,
                97,
                99,
                101,
                91,
                115,
                116,
                97,
                114,
                116,
                115,
                45,
                119,
                105,
                116,
                104,
                40,
                110,
                97,
                109,
                101,
                44,
                32,
                39,
                84,
                117,
                110,
                110,
                101,
                108,
                49,
                39,
                41,
                93,
                34,
                47,
                62,
                13,
                10,
                60,
                47,
                110,
                99,
                58,
                103,
                101,
                116,
                62,
                13,
                10,
                60,
                47,
                110,
                99,
                58,
                114,
                112,
                99,
                62,
//                93,
//                93,
//                62,
//                93,
//                93,
//                62,
//                10,
//                60,
//                47,
//                110,
//                99,
//                58,
//                114,
//                112,
//                99,
//                62,
//                93,
//                93,
//                62,
//                93,
//                93,
//                62,
//                114,
//                112,
//                99,
//                62,
//                93,
//                93,
//                62,
//                93,
//                93,
//                62,
//                13,
//                10,
//                60,
//                47,
//                110,
//                99,
//                58,
//                114,
//                112,
//                99,
//                62,
//                93,
//                93,
//                62,
//                93,
//                93,
//                62,
        };
        String rpcCall = new String(b);


        // Produces this:

        /*
            <nc:rpc xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0" nc:message-id="6">
                <nc:get>
                    <nc:filter nc:type="xpath" nc:select="interfaces-state/interface[starts-with(name, 'Tunnel1')]"/>
                </nc:get>
            </nc:rpc>
         */
        logger.info(rpcCall);

    }

    private void dumpConfiguration(NodeSet configuredTunnels,
                                   NodeSet keyringSections,
                                   NodeSet isakmpProfileSections,
                                   NodeSet ipsecTransformSetSections,
                                   NodeSet ipsecProfileSections,
                                   NodeSet interfacesState,
                                   NodeSet bgpConfigs,
                                   NodeSet bgpStateDatas) throws JNCException {
        Element test;

        test = configuredTunnels.get(0);
        test = createElement(test);
        logger.info(test.toXMLString());

        test = keyringSections.get("crypto/keyring").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());


        test = isakmpProfileSections.get("crypto/isakmp/profile").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());


        test = ipsecTransformSetSections.get("crypto/ipsec/transform-set").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());

        test = ipsecProfileSections.get("crypto/ipsec/profile").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());

        test = interfacesState.get("interface").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());

        test = bgpConfigs.get("router/bgp/address-family/with-vrf/ipv4/vrf/neighbor").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());

        test = bgpStateDatas.get("neighbors/neighbor").get(0);
        test = createElement(test);
        logger.info(test.toXMLString());

    }

    private Element createElement(Element content) throws JNCException {
        Element newElement = null;

        Deque hierarchyStack = new ArrayDeque();

        Element parent = content;
        do {
            parent = parent.getParent();
            if (parent != null) {
                hierarchyStack.push(parent);
            }
        } while (parent != null);

        Element leafElement = null;
        do {
            Element element = (Element)hierarchyStack.pop();

            if ("rpc-reply".equalsIgnoreCase(element.name)) {
                Element peek = (Element)hierarchyStack.peek();
                if (peek != null && "data".equalsIgnoreCase(peek.name)) {
                    hierarchyStack.pop();
                    continue;
                }
            }

            Element nextElement = Element.create(element.getContextPrefixMap(), element.name);

            if (newElement == null) {
                newElement = nextElement;
                leafElement = newElement;
            } else {
                leafElement.addChild(nextElement);
                leafElement = nextElement;
            }
            if (hierarchyStack.isEmpty()) {
                leafElement.addChild(content);
                break;
            }
        } while (true);


        return newElement;
    }

    private Element createElementOrig(Element content) throws JNCException {
        Element newElement = null;

        Deque hierarchyStack = new ArrayDeque();

        Element parent = content;
        do {
            parent = parent.getParent();
            if (parent != null) {
                hierarchyStack.push(parent);
            }
        } while (parent != null);

        Element leafElement = null;
        do {
            Element element = (Element)hierarchyStack.pop();


            Element nextElement = Element.create(element.getContextPrefixMap(), element.name);

            if (newElement == null) {
                newElement = nextElement;
            } else {
                NodeSet children = newElement.getChildren();
                if (children != null) {
                    while (children != null) {
                        leafElement = children.get(0);
                        children = leafElement.getChildren();
                    }
                } else {
                    leafElement = newElement;
                }
                leafElement.addChild(nextElement);
            }
            if (hierarchyStack.isEmpty() && leafElement != null) {
                leafElement.addChild(content);
                break;
            }
        } while (true);

        return newElement;
    }

    @Test
    public void testGetIpRangeForAddresses() {
        List<String> ipList = new ArrayList<>();
        ipList.add("169.254.248.2");
        ipList.add("169.254.248.6");
        ipList.add("169.254.248.10");
        ipList.add("169.254.248.14");
        ipList.add("169.254.248.18");
        ipList.add("169.254.248.22");
        ipList.add("169.254.248.26");
        ipList.add("169.254.248.30");
        ipList.add("169.254.248.34");
        ipList.add("169.254.248.38");
        ipList.add("169.254.248.46");
        ipList.add("169.254.248.50");
        ipList.add("169.254.248.54");
        ipList.add("169.254.248.58");
        ipList.add("169.254.248.62");
        ipList.add("169.254.248.66");
        ipList.add("169.254.248.74");
        ipList.add("169.254.248.82");
        ipList.add("169.254.248.90");
        ipList.add("169.254.248.94");
        ipList.add("169.254.248.98");
        ipList.add("169.254.248.102");
        ipList.add("169.254.248.106");
        ipList.add("169.254.248.110");
        ipList.add("169.254.248.114");
        ipList.add("169.254.248.118");
        ipList.add("169.254.248.122");
        ipList.add("169.254.248.126");
        ipList.add("169.254.248.142");
        ipList.add("169.254.248.146");
        ipList.add("169.254.248.150");
        ipList.add("169.254.248.154");
        ipList.add("169.254.248.158");

        String ipRange = getIpRangeForAddresses(ipList);

        if (ipRange != null) {
            logger.info("ipRange: " + ipRange);
        } else {
            logger.error("No ipRange returned!");
        }
    }

    private String getIpRangeForAddresses(List<String> ipAddresses) {
        Integer[] ipRange = null;

        for (String ip: ipAddresses) {
            Integer[] octets = IPV4Address.getOctets(ip);
            if (ipRange == null) {
                ipRange = new Integer[4];
                ipRange[0] = octets[0];
                ipRange[1] = octets[1];
                ipRange[2] = octets[2];
                ipRange[3] = octets[3];
                continue;
            }

            if (ipRange[3] != null && !ipRange[3].equals(octets[3])) {
                ipRange[3] = null;
            }
            if (ipRange[2] != null && !ipRange[2].equals(octets[2])) {
                ipRange[2] = null;
            }
            if (ipRange[1] != null && !ipRange[1].equals(octets[1])) {
                ipRange[1] = null;
            }
            if (ipRange[0] != null && !ipRange[0].equals(octets[0])) {
                ipRange[0] = null;
            }
        }

        String resultIpRange = null;

        if (ipRange[3] != null) {
            resultIpRange = ipRange[3] + ".";
            if (ipRange[2] != null) {
                resultIpRange += ipRange[2] + ".";
                if (ipRange[1] != null) {
                    resultIpRange += ipRange[1] + ".";
                    if (ipRange[0] != null) {
                        resultIpRange += ipRange[0];
                    }
                }
            }
        }
        return resultIpRange;
    }


    @Test
    public void testGetProfileIds() {
        List<String> profileIds = getProfileIds(Router.ROUTER1);
    }

    /**
     * Validation added by Jimmy K. from IT:
     *
     * One thing I forgot to mention is that there will be other non-AWS related tunnel interfaces that show up in your list as a result of an open query.  You'll need to ignore these and only look at the [12]0XXX interfaces.
     *
     * On 1/8/19 3:16 PM, Steve Brodeur wrote:
     * Couple of clarification questions:
     *
     * 1) To check if destination is configured, the tunnel/destination value should exist and be valid ip address
     * Correct
     * 2) "protected by an ipsec profile"  means that the protection/ipsec/profile node exists and is not empty, or do we want to validate the contents, e.g., profile is of the form ipsec-vpn-research-vpcnnn-tunx and use the "nnn" for the profile id
     * I think validating the contents is better.  That way we know exactly what profile the tunnel interface is locked to.
     *
     * Here is Tunnel config from router:
     *
     *     <Tunnel>
     *       <name>10042</name>
     *       <description>AWS Research VPC042 Tunnel1 (vpc-09dd9159e99addda8)</description>
     *       <vrf>
     *         <forwarding>AWS</forwarding>
     *       </vrf>
     *       <ip>
     *         <address>
     *           <primary>
     *             <address>169.254.248.166</address>
     *             <mask>255.255.255.252</mask>
     *           </primary>
     *         </address>
     *         <tcp>
     *           <adjust-mss>1387</adjust-mss>
     *         </tcp>
     *       </ip>
     *       <tunnel xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel">
     *         <source>170.140.76.42</source>
     *         <destination>71.21.209.225</destination>      <!-- Must exist and be valid IP Address -->
     *         <mode>
     *           <ipsec>
     *             <ipv4/>          </ipsec>
     *         </mode>
     *         <protection>
     *           <ipsec xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto">
     *             <profile>ipsec-vpn-research-vpc042-tun1</profile>   <!-- Must exist and be proper format: profile is 'nnn' - vpcnnn -->
     *           </ipsec>
     *         </protection>
     *         <vrf>AWS</vrf>
     *       </tunnel>
     *     </Tunnel>
     *
     * @param router
     * @return
     */
    public List<String> getProfileIds(Router router) {
        List<String> profileIds = null;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            NodeSet result = session.get("native/interface/Tunnel/description[text()[not (contains(.,'AVAILABLE'))]]/..");
//                    "| bgp-state-data/neighbors/neighbor/description[text()[not (text() = 'AVAILABLE')]]/..");
            if (result.size() != 0) {

                Map<String, String> profileMap = new HashMap<>();

                String xmlString = result.toXMLString();
                NodeSet tunnels = new XMLParser().parse(xmlString).get("interface/Tunnel");
                profileIds = tunnels.stream()
                        .map(tunnel -> {
                            try {
                                String tunnelName = (String) tunnel.getFirst("name").value;
                                String description = (String) tunnel.getFirst("description").value;
                                String vpcId = null;
                                Pattern pattern = Pattern.compile("\\((^\\))");
                                Matcher matcher = pattern.matcher(description);
                                if (matcher.find()) {
                                    vpcId = matcher.group(1);

                                }

                                logger.info("Tunnel Name: " + tunnelName + " Tunnel Description: " + description);

                                if (!tunnelName.matches("[\\d]{5}")) {
                                    throw new Exception("Tunnel Name is not a number");
                                }
                                Integer profileIdAsInteger = Integer.valueOf(tunnelName.substring(2));
                                return profileIdAsInteger.toString();
                            } catch (Exception e) {
                                logger.error("Bad tunnel name: " + e.getMessage());
                                return null;
                            }
                        })
                        .filter(tunnelName -> {
                            return tunnelName != null;
                        })
                        .collect(toList());

                logger.info("################ " + router.getName() + " ################");
                profileIds.forEach(name -> logger.info(name));

            }
        } catch (JNCException e) {

        } catch (IOException e) {

        } finally {
            closeNetconfSession(session);
        }

        return profileIds;
    }

    @Test
    public void testGetProfileMap() {
        Map<String, String> profileIdMap = getProfileMap(Router.ROUTER1);
        logger.info("################ " + Router.ROUTER1.getName() + " ################");
        profileIdMap.forEach((profileId, vpcId) -> logger.info("Profile Id: " + profileId + "  ->  VpcId: " + vpcId));


    }

    public Map<String, String> getProfileMap(Router router) {
        Map<String, String> profileIdMap = null;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            NodeSet result = session.get("native/interface/Tunnel/description[text()[not (contains(.,'AVAILABLE'))]]/..");
//                    "| bgp-state-data/neighbors/neighbor/description[text()[not (text() = 'AVAILABLE')]]/..");
            if (result.size() != 0) {

                String xmlString = result.toXMLString();
                NodeSet tunnels = new XMLParser().parse(xmlString).get("interface/Tunnel");

                tunnels = tunnels.stream()
                        .filter(this::validateTunnel)
                        .collect(Collectors.toCollection(NodeSet::new));

                profileIdMap = tunnels.stream()
                        .collect(Collectors.toMap(this::getProfileId, this::getVpcId));

                profileIdMap = profileIdMap.entrySet().stream()
                      .filter(entry -> !"".equals(entry.getKey()) && !"".equals(entry.getValue()))
                      .collect(Collectors.toMap(
                                 Map.Entry::getKey,
                                 Map.Entry::getValue
                      ));
            }
        } catch (JNCException e) {

        } catch (IOException e) {

        } finally {
            closeNetconfSession(session);
        }

        return profileIdMap;
    }

    /**
     * Tunnel validation requires two subnodes exist and are valid:
     *
     * tunnel/destination
     * tunnel/protection/ipsec/profile
     *
     * If either are not present, we'll slip on a null pointer and return ""
     *
     * If both are present, we'll validate:
     *    1) destination is a valid ip address
     *    2) profile contains a well-known prefix "ipsec-vpn-research"
     *
     * @param tunnel
     * @return
     */
    private boolean validateTunnel(Element tunnel) {

        boolean isValid = false;
        String tunnelName = null;
        try {
            tunnelName = (String) tunnel.getFirst("name").value;
            String tunnelDestination = (String)tunnel.getFirst("tunnel").getFirst("destination").value;
            String tunnelProfile = (String)tunnel.getFirst("tunnel").getFirst("protection").getFirst("ipsec").getFirst("profile").value;

            if (IPAddressUtil.isIPv4LiteralAddress(tunnelDestination) && tunnelProfile.startsWith("ipsec-vpn-research")) {
                isValid = true;
            }
        } catch (Exception e) {
            logger.error("Failed to validate tunnel " + (tunnelName != null ? tunnelName : "<unknown>"));
        }
        return isValid;
    }

    private String getProfileId(Element tunnel) {
        try {
            String tunnelName = (String) tunnel.getFirst("name").value;
            if (!tunnelName.matches("[\\d]{5}")) {
                throw new Exception("Tunnel Name is not a number");
            }
            Integer profileIdAsInteger = Integer.valueOf(tunnelName.substring(2));
            String profileId =  profileIdAsInteger.toString();
            return profileId;
        } catch (Exception e) {
            logger.error("Bad tunnel name: " + e.getMessage());
            return "";
        }
    }

    private String getVpcId(Element tunnel) {
        try {
            String description = (String) tunnel.getFirst("description").value;
            String vpcId = "";
            Pattern pattern = Pattern.compile("\\((.+?)\\)");
            Matcher matcher = pattern.matcher(description);
            if (matcher.find()) {
                vpcId = matcher.group(1);
            }
            return vpcId;
        } catch (Exception e) {
            logger.error("Bad tunnel description: " + e.getMessage());
            return "";
        }
    }

    public List<TunnelInformation> getActtiveTunnelInformation(Router router) {
        List<TunnelInformation> tunnelInformationList = new ArrayList<>();

        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
//            session.lock(NetconfSession.RUNNING);

            NodeSet configuredTunnels = session.get("native/interface/Tunnel/description[text()[not (contains(.,'AVAILABLE'))]]/..");

            if (configuredTunnels.size() == 0) {
                logger.info("\n*** NO ACTIVE TUNNELS FOUND ON " + router.getTunnelNumber() + " ***");
            } else {
                String xmlString = configuredTunnels.toXMLString();
                logger.info(prettyPrintXmlString(xmlString));
                XMLParser parser = new XMLParser();
                Element xml = parser.parse(xmlString);
                NodeSet nodes = xml.get("interface/Tunnel");

                if (nodes.size() > 0) {
                    logger.info(nodes.size() + " active tunnels found on router " + router.getTunnelNumber());

                    nodes.forEach(tunnel -> {
                        try {
                            String tunnelNumber = (String)tunnel.getFirst("name").value;
                            String description = (String)tunnel.getFirst("description").value;
                            TunnelInformation tunnelInformation = hydrateTunnelInformation(tunnelNumber, description);

                            tunnelInformationList.add(tunnelInformation);
                        } catch (JNCException e) {
                            logger.error("Failed to get 'name' element", e);
                        }
                    });
                }
                //logger.info(configuredTunnels.toXMLString());
            }

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
//            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

        return tunnelInformationList;
    }

    /*
    <Tunnel>
      <name>10042</name>
      <description>AWS Research VPC042 Tunnel1 (vpc-09dd9159e99addda8)</description>
      <vrf>
        <forwarding>AWS</forwarding>
      </vrf>
      <ip>
        <address>
          <primary>
            <address>169.254.248.166</address>
            <mask>255.255.255.252</mask>
          </primary>
        </address>
        <tcp>
          <adjust-mss>1387</adjust-mss>
        </tcp>
      </ip>
      <tunnel xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel">
        <source>170.140.76.42</source>
        <destination>71.21.209.225</destination>
        <mode>
          <ipsec>
            <ipv4/>          </ipsec>
        </mode>
        <protection>
          <ipsec xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto">
            <profile>ipsec-vpn-research-vpc042-tun1</profile>
          </ipsec>
        </protection>
        <vrf>AWS</vrf>
      </tunnel>
    </Tunnel>

     */
    @Test
    public void testHydreateTunnelInformation() {

        String tunnelNumber = "10003";

        String description = "AWS Research VPC001 Tunnel1 (vpc-0fada93db5c59020c)";
        TunnelInformation tunnelInformation = hydrateTunnelInformation(tunnelNumber, description);

    }

    private TunnelInformation hydrateTunnelInformation(String tunnelNumber, String description) throws NumberFormatException {

        // Should be 5 digits long.
        if (tunnelNumber.length() != 5) {
            throw new RuntimeException("Invalid tunnel number: " + tunnelNumber);
        }

        String tunnel;
        String profileId;

        Pattern pattern = Pattern.compile("(\\d{1})[0]+(\\d{1,3})");
        Matcher matcher = pattern.matcher(tunnelNumber);
        if (matcher.matches()) {
            tunnel = matcher.group(1);
            profileId = matcher.group(2);
        } else {
            throw new RuntimeException("Invalid tunnel number - value: " + tunnelNumber);
        }

        Integer profileIdAsInt = Integer.valueOf(profileId);

        String vpcId;
        pattern = Pattern.compile("^[\\(]+\\((^[\\)]+)");
        matcher = pattern.matcher(description);
        if (matcher.find()) {
            vpcId = matcher.group(1);
        } else {
            throw new RuntimeException("Invalid description: " + description);
        }

        TunnelInformation tunnelInformation = new TunnelInformation(tunnel, vpcId, profileIdAsInt);
        return tunnelInformation;
    }


    /**
     * Utility routine to lock session on specified router for CiscoAsrService testing.
     * Debug and sit on logger line.
     */
    @Test
    public void blockSessionAccess() {

        Router router = Router.ROUTER1;

        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            logger.info("Session locked.");

        } catch (Exception e) {

        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
        }

    }

    @Test
    public void testGetAvailableTunnelByProfileId() {
        getAvailableTunnelByProfileId(2);;
    }

    /**
     * Check native configuration for available tunnel by profile id:
     *
     * 1) Check native/interface/tunnel configuration to assure that tunnel has not been assigned. The description
     *    should contain (AVAILABLE) instead of an assigned vpc id.
     * 2) Check native/router/bgp configuration for NO assigned description, which is a vpc id. The configuration is
     *    located by using the calculated neighbor id, which is derived from the router's assigned inside cidr value.
     *
     * @param profileId - Profile id used to locate configuration.
     *
     * @throws  RuntimeException if tunnel is not available.
     */
    public void getAvailableTunnelByProfileId(Integer profileId) {
        Router router = Router.ROUTER1;

        // Setup profile id and Tunnel description.
        // 3 digits - left zero padded.
        String profileIdPadded = String.format("%03d", profileId);

        String description = "AWS Research VPC" + profileIdPadded + " Tunnel1 (AVAILABLE)";

        SpreadSheetWrapper spreadSheetWrapper = new SpreadSheetWrapper();
        Map<String, Map<String, String>> spreadsheetMap = spreadSheetWrapper.getSpreadsheetMap();
        Map<String, String> spreadsheetLine = spreadsheetMap.get(profileId.toString());

        String insideCidrRange;

        // Inside CIDR range by router.
        switch (router) {
            case ROUTER1:
                insideCidrRange = spreadsheetLine.get("VpnInsideIpCidr_1A");
                break;
            case ROUTER2:
                insideCidrRange = spreadsheetLine.get("VpnInsideIpCidr_2A");
                break;
            default:
                throw new RuntimeException("Invalid router: " + router.getName());
        }

        // Neighbor id: inside cidr range base ip address plus 1.
        String neighborId = IPV4Address.adjustAddressByOffset(insideCidrRange, 1);

        NetconfSession session = null;

        try {

            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            StringBuilder errors = new StringBuilder();
            NodeSet availableTunnel = session.get("native/interface/Tunnel/description[text()='" + description + "']/..");

            if (availableTunnel.size() == 0) {
                errors.append(" - No available tunnel in native/interface/Tunnel configuration for profile id: " + profileId);
            }
            NodeSet availableBgpState = session.get("native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/id[text()='" + neighborId + "']/..");

            NodeSet bgpNeighborDescription = availableBgpState.get("router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/description");

            String bgpNeighborDescriptionValue = null;

            if (bgpNeighborDescription.size() > 0) {
                bgpNeighborDescriptionValue = (String)bgpNeighborDescription.first().value;
            }
            if (bgpNeighborDescriptionValue != null) {
                if (!"AVAILABLE".equals(bgpNeighborDescriptionValue)) {
                    errors.append("\n - Neighbor description is assigned in native/router/bgp configuration: " + bgpNeighborDescriptionValue);
                }
            }

            if (errors.length() > 0) {
                throw new RuntimeException("Tunnel is not available for profile id: " + profileId + "\n\n" + errors.toString() + "\n");
            }
            logger.info("Tunnel is available for profile id " + profileId);

            logger.info("\nBgpState: " + availableBgpState.toXMLString());
            logger.info("Interface: " + availableTunnel.toXMLString());

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

    }
    @Test
    public void getAvailableTunnels() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            NodeSet shutdownTunnels = session.get("native/interface/Tunnel/shutdown[text()='']/..");
            NodeSet availableTunnels = session.get("native/interface/Tunnel/description[text()[contains(.,'AVAILABLE')]]/..");
            NodeSet configuredTunnels = session.get("native/interface/Tunnel/description[text()[contains(.,'AVAILABLE')]]/.. and native/interface/Tunnel/shutdown[text()='']/..");

            logger.info(configuredTunnels.toXMLString());

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    @Test
    public void getBrokenTunnels() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
//            session.lock(NetconfSession.RUNNING);

            NodeSet configuredTunnels = session.get("native/interface/Tunnel/description[text()[not (contains(.,'AVAILABLE'))]]/..");
            NodeSet shutdownTunnels = session.get("native/interface/Tunnel/shutdown/..");

            if (configuredTunnels.size() > 0) {
                NodeSet configuredTunnelsWithShutdownElement = configuredTunnels.get("interface/Tunnel/shutdown");

                if (configuredTunnelsWithShutdownElement.size() > 0) {
                    logger.error(configuredTunnelsWithShutdownElement.size() + " broken tunnels found");
                    logger.error(configuredTunnelsWithShutdownElement.toXMLString());
                } else {
                    logger.info("No broken tunnels found");
                }
            } else {
                logger.info("No configured tunnels found");
            }

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
//            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    @Test
    public void getBrokenNativeConfig() {

        Router router = Router.ROUTER1;
        String tunnelNumberAsString;
        Integer profileId = 1;
        String profileIdAsString = profileId.toString();

        tunnelNumberAsString = router.getTunnelNumber();

        logger.info("Checking for broken native config on router " + router.getTunnelNumber());
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            SpreadSheetWrapper spreadSheetWrapper = new SpreadSheetWrapper();
            Map<String, Map<String, String>> spreadsheetMap = spreadSheetWrapper.getSpreadsheetMap();
            String paddedVpcNumberAsString = String.format("%03d", profileId);
            Map<String, String> spreadsheetLine = spreadsheetMap.get(profileIdAsString);

            // Keep track of EXISTING native crypto config xpath - if not all present, they need repair.
            List<String> brokenXpaths = new ArrayList<>();
            for (int n = 0; n < allNativeCryptoConfigElements.length; n++) {
                String elementXpath = allNativeCryptoConfigElements[n];
                String value;
                NodeSet result = null;
                switch (elementXpath) {
                    case "crypto/keyring":
                        value = spreadsheetLine.get("CRYPTO_KEYRING_" + tunnelNumberAsString);
                        result = session.get("native/" + elementXpath + "/name[text() = '" + value + "']/..");
                        break;
                    case "crypto/isakmp/profile":
                        value = spreadsheetLine.get("ISAKMP_PROFILE_" + tunnelNumberAsString);
                        result = session.get("native/" + elementXpath + "/name[text() = '" + value + "']/..");
                        break;
                    case "crypto/ipsec/profile":
                        value = spreadsheetLine.get("IPSEC_PROFILE_" + tunnelNumberAsString);
                        result = session.get("native/" + elementXpath + "/name[text() = '" + value + "']/..");
                        break;
                    case "crypto/ipsec/transform-set":
                        value = spreadsheetLine.get("IPSEC_TRANSFORM_SET_" + tunnelNumberAsString);
                        result = session.get("native/" + elementXpath + "/tag[text() = '" + value + "']/..");
                        break;

                }
                if (result.size() == 1) {
                    logger.info("Found: " + elementXpath);
                    brokenXpaths.add(elementXpath);
                } else {
                    logger.info("Not found: " + elementXpath);
                }
            }

            if (brokenXpaths.size() == allNativeCryptoConfigElements.length) {
                logger.info("\nNative crypto config is healthy.");
            } else {
                logger.info("found: " + brokenXpaths.size() + " - repair required");
                repairNativeModuleConfig(brokenXpaths, session);
            }

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    @Test
    public void queryTunnelInterface() {
        Router router = Router.ROUTER2;
        Integer tunnelNumberAsInteger = 1;
        Integer vpcNumber = 2;

        NetconfSession session;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            String tunnelId = router.getTunnelNumber() + String.format("%04d", vpcNumber);
            NodeSet result = session.get("native/interface/Tunnel/name[text() = '" + tunnelId + "']/..");

            Element tunnelInterfaceElement = null;

            if (result != null && result.size() > 0) {
                tunnelInterfaceElement = result.get(0);
            }

            if (tunnelInterfaceElement != null) {
                logger.info(tunnelInterfaceElement.toXMLString());
                String tunnelName = (String)tunnelInterfaceElement.getFirst("interface/Tunnel/name").getValue();
                String description = (String)tunnelInterfaceElement.getFirst("interface/Tunnel/description").getValue();
                boolean shutdownExists = tunnelInterfaceElement.getFirst("interface/Tunnel/shutdown") != null;

                Pattern pattern = Pattern.compile("\\((.+?)\\)");
                Matcher matcher = pattern.matcher(description);
                if (matcher.find()) {
                    String vpcId = matcher.group(1);
                    if (vpcId.equalsIgnoreCase("AVAILABLE")) {
                        if (shutdownExists) {
                            logger.info("Valid tunnel interface - AVAILABLE: tunnelId [" + tunnelName + "]");
                        } else {
                            logger.error("ERROR: Invalid tunnel interface - AVAILABLE but not shutdown!");
                        }
                    } else {
                        if (!shutdownExists) {
                            logger.info("Valid tunnel interface - CONFIGURED: configured for vpcId: [" + vpcId + "] - tunnelId: [" + tunnelName + "]");
                        } else {
                            logger.error("ERROR: Invalid tunnel interface - vpcId exists (" + vpcId + ") but shutdown!");
                        }
                    }
                }

            } else {
                logger.info("No tunnel config for tunnel Id: " + tunnelId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void findBgpStateByVpcId() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            String vpcId = "vpc-0643ec83b393d4e05";

            NodeSet result = session.get("bgp-state-data/neighbors/neighbor/description[text() = '" + vpcId + "']/..");

            Element bgpStateData = result.get(0);

            if (bgpStateData != null) {

                logger.info("neighbor id: " + bgpStateData.getFirst("neighbors/neighbor/neighbor-id").getValue());
                logger.info(bgpStateData.toXMLString());


            }

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }


    @Test
    public void findBgpStateByTunnelPrimaryIpAddress() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            String tunnelPrimaryIpAddress = "169.254.248.6";

            String neighborId = IPV4Address.adjustAddressByOffset(tunnelPrimaryIpAddress, -1);

            NodeSet result = session.get("bgp-state-data/neighbors/neighbor/neighbor-id[text() = '" + neighborId + "']/..");

            Element bgpStateData = result.get(0);

            if (bgpStateData != null) {

                logger.info("result name: " + bgpStateData.name);
                logger.info("neighbor id: " + bgpStateData.getFirst("neighbors/neighbor/neighbor-id").getValue());
                logger.info(bgpStateData.toXMLString());


            }

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    private static final String QUERY_RESULT_MAP_TRANSFORM_SET_KEY = "transform-set";

    @Test
    public void queryByTemplate() {
        Element queryFromXml;

        Router router = Router.ROUTER1;
        NetconfSession session = null;
        _1662AwsCiscoAsrSpreadsheet spreadsheet = new _1662AwsCiscoAsrSpreadsheet();

        try {
            session = openNetconfSession(router);

            Map<String, Element> resultMap = new HashMap<>();

            String vpcId = "vpc-querytest";
            String vpnId = "101";
            Integer vpnIdAsInteger = Integer.valueOf(vpnId);
            String tunnelNumberAsString = "1";
            String tunnelName;

            Map<String, String> spreadsheetValues = spreadsheet.buildSpreadsheetValues(vpcId, vpnIdAsInteger, tunnelNumberAsString, VpnConnectionOperationContext.QUERY_REQUEST);

            tunnelName = spreadsheetValues.get("tunnelId");

            NodeSet tunnel = session.get("native/interface/Tunnel/name[text() = '" + tunnelName + "']/..");

            String primaryIp = (String)tunnel.getFirst("interface/Tunnel/ip/address/primary/address").getValue();
            String neighborId = IPV4Address.adjustAddressByOffset(primaryIp, -1);

            XMLParser xmlParser = new XMLParser();

            queryFromXml = xmlParser.parse(queryTunnelTemplate);

            queryFromXml.setValue("native/crypto/ipsec/profile/name", spreadsheetValues.get("ipsecProfileName"));
            queryFromXml.setValue("native/crypto/ipsec/transform-set/tag", spreadsheetValues.get("ipsecTransformSetName"));
            queryFromXml.setValue("native/crypto/isakmp/profile/name", spreadsheetValues.get("isakmpProfileName"));
            queryFromXml.setValue("native/crypto/keyring/name", spreadsheetValues.get("cryptoKeyringName"));
            queryFromXml.setValue("native/router/bgp/address-family/with-vrf/ipv4/neighbor/id", neighborId);
            queryFromXml.setValue("bgp-state-data/neighbors/neighbor/neighbor-id", neighborId);
            queryFromXml.setValue("interfaces-state/interface/name", "Tunnel" + tunnelName);

            logger.info("query: \n" + queryFromXml.toXMLString());

            NodeSet nativeResult = session.get(queryFromXml.getFirst("native"));
            NodeSet bgpStateDataResult = session.get(queryFromXml.getFirst("bgp-state-data"));
            NodeSet interfacesStateResult = session.get(queryFromXml.getFirst("interfaces-state"));


            if (nativeResult.size() == 1 && bgpStateDataResult.size() == 1 && interfacesStateResult.size() == 1) {
                logger.info("native result: \n" + nativeResult.toXMLString());
                logger.info("bgp-state-data result: \n" + bgpStateDataResult.toXMLString());
                logger.info("interfaces-state result: \n" + interfacesStateResult.toXMLString());
                Map<String, Element> descriptionMap = new HashMap<>();
                descriptionMap.put("crypto/ipsec/profile/description", nativeResult.get(0));
                descriptionMap.put("crypto/isakmp/profile/description", nativeResult.get(0));
                descriptionMap.put("crypto/keyring/description", nativeResult.get(0));
                descriptionMap.put("neighbors/neighbor/description", bgpStateDataResult.get(0));


                if (matchVpcIdInResponse(descriptionMap, vpcId)) {
                    resultMap.put(QUERY_RESULT_MAP_NATIVE_KEY, nativeResult.get(0));
                    resultMap.put(QUERY_RESULT_MAP_TRANSFORM_SET_KEY, nativeResult.getFirst("crypto/ipsec/transform-set"));
                    resultMap.put(QUERY_RESULT_MAP_BGP_STATE_DATA_KEY, bgpStateDataResult.get(0));
                    resultMap.put(QUERY_RESULT_MAP_INTERFACES_STATE_KEY, interfacesStateResult.get(0));
                } else {
                    logger.info("no vpn connection found for vpc id: " + vpcId + " - vpn id: " + vpnId);
                }

            }
            logger.info("done");

        } catch (Exception e) {
            throw new RuntimeException("Failed to query by template", e);
        } finally {
            closeNetconfSession(session);
        }
    }

    private boolean matchVpcIdInResponse(Map<String, Element> descriptionMap, String vpcId) {
        boolean isMatch = true;

        for (Map.Entry<String, Element> entry : descriptionMap.entrySet()) {
            try {
                Element element = entry.getValue().getFirst(entry.getKey());
                String description = element != null ? (String) element.getValue() : null;
                if (description == null || !description.contains(vpcId)) {
                    isMatch = false;
                    logger.warn(entry.getKey() + " does not match vpcId " + vpcId + ": " + description);
                }
            } catch (JNCException e) {
                String errMsg = "Failed to get description for " + entry.getKey();
                logger.error(getLogtag() + errMsg);
                isMatch = false;
            }
        };
        return isMatch;
    }

    @Test
    public void findAllMissingFiles() throws IOException {
        String file = "doc/tmp.txt";
        Path path = FileSystems.getDefault().getPath(file);

        Files.lines(path).forEach(line -> {
            if (line.contains("java.io.FileNotFoundException")) {
                logger.info(line.replace("java.io.FileNotFoundException: E:\\Workspaces\\Surge\\emory-ciscoasr-service\\deploy\\build-test\\", ""));
            }
        });




    }


    @Test
    public void testFindBgpStateNeighborId() {
        String neighborId = findBgpStateNeighborId("vpc-testnum101");

        logger.info("NeighborId: " + neighborId);

    }

    @Test
    public void testAdjustIpAddressByOffset() {
//        String ipAddress = "169.254.248.38";
        String ipAddress = "254.255.255.255";

        ipAddress = IPV4Address.adjustAddressByOffset(ipAddress, -256);

//        assert "0.255.255.254".equals(ipAddress);

        logger.info("Adjusted IP Address: " + ipAddress);
    }

    private String findBgpStateNeighborId(String vpcId) {

        String bgpStateNeighborId = null;

        NetconfSession session = null;
        try {
            session = openNetconfSession();
            session.lock(NetconfSession.RUNNING);

            NodeSet result = session.get("bgp-state-data/neighbors/neighbor/description[text() = '" + vpcId + "']/..");

            if (result.size() == 1) {
                Element bgpStateData = result.get(0);

                if (bgpStateData != null) {
                    Element neighborIdElement = bgpStateData.getFirst("neighbors/neighbor/neighbor-id");
                    if (neighborIdElement != null) {
                        bgpStateNeighborId = (String)neighborIdElement.getValue();
                    }
                }
            }
            if (bgpStateNeighborId == null || !isValidIpAddress(bgpStateNeighborId)) {
                String errMsg = (bgpStateNeighborId == null ? "bgp-state-data neighbor-id not found for vpcId: " + vpcId
                        : "Invalid gp-state-data neighbor-id: " + bgpStateNeighborId + " for vpcId: " + vpcId);
                logger.error(getLogtag() + errMsg);
                throw new RuntimeException(errMsg);
            }

        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            String errMsg = "Failed to find bgp-state-data neighbor-id";
            logger.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }

        return bgpStateNeighborId;
    }


    @Test
    public void testIsValidIpAddress() {
        String ipAddress = "1.0.3.255";

        if (isValidIpAddress(ipAddress)) {
            logger.info("Valid ip address: " + ipAddress);
        } else {
            logger.error("Invalid ip address: " + ipAddress);
        }
    }

    private boolean isValidIpAddress(String ipAddress) {
        boolean valid = true;

        try {
            Pattern p = Pattern.compile("(\\d+?)\\.(\\d+?)\\.(\\d+?)\\.(\\d+?)");
            Matcher m = p.matcher(ipAddress.trim());
            if (m.matches()) {
                for (int n = 0; n < m.groupCount(); n++) {
                    int octet = Integer.parseInt(m.group(n + 1));
                    if (octet > 255) {
                        logger.error(getLogtag() + "Invalid IP address: " + ipAddress);
                        valid = false;
                        break;
                    }
                }
            } else {
                logger.error(getLogtag() + "Invalid IP address: " + ipAddress);
                valid = false;
            }
        } catch (Exception e) {
            logger.error( getLogtag() + "Invalid IP address: " + ipAddress, e);
            valid = false;
        }

        return valid;
    }

    @Test
    public void fixTunnelInterface() {
        Router router = Router.ROUTER2;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            String vpcNumber = "001";
            String vpcId = "vpc-testnum5";

            XMLParser parser = new XMLParser();
            Element shutdownElement = parser.parse(this.tunnelShutdownConfigTemplate);

            shutdownElement.getFirst("interface/Tunnel/name").setValue(router.getTunnelNumber() + "0" + vpcNumber);
            Element descriptionElement = shutdownElement.getFirst("interface/Tunnel/description");
            String description = (String)descriptionElement.getValue();

            descriptionElement.setValue(description.replace("###", vpcNumber).replace("$$$", vpcId));

            logger.info("Fixing tunnel interface with this config:" + shutdownElement.toXMLString());

            session.editConfig(shutdownElement);

            logger.info("Success.");

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    @Test
    public void saveConfig() {

        Router router = Router.ROUTER1;
        String saveConfigXml = "<save-config xmlns=\"http://cisco.com/yang/cisco-ia\"/>";
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);

            XMLParser xmlParser = new XMLParser();
            Element saveConfigElement = xmlParser.parse(saveConfigXml);

            NodeSet reply = session.callRpc(saveConfigElement);
            logger.info(getLogtag() + "Saved config on device" + router.getName() + ":" + reply.toXMLString());
            /**
             * <rpc-error xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
             *   <error-type>application</error-type>
             *   <error-tag>resource-denied</error-tag>
             *   <error-severity>error</error-severity>
             *   <nc:error-path xmlns:cisco-ia="http://cisco.com/yang/cisco-ia" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
             *     /nc:rpc/cisco-ia:save-config
             *   </nc:error-path>
             *   <error-message unknown:lang="en">resource denied: Sync is in progress</error-message>
             *   <error-info>
             *     <bad-element>save-config</bad-element>
             *   </error-info>
             * </rpc-error>
             */
            Element errorMessage = reply.getFirst("error-message");
            if (errorMessage != null) {
                throw new RuntimeException((String)errorMessage.getValue());
            }
        } catch (Exception e) {
            String errMsg = "Failed to save config on device " + router.getName() + ": ";
            logger.error(getLogtag() + errMsg, e);
            throw new RuntimeException(errMsg, e);
        } finally {
            closeNetconfSession(session);
        }

    }

//    @Test
//    public void testRepairNativeModuleConfig() {
//
//        List<String> brokenXpathList = new ArrayList<>();
//        // Comment out elements not needed for fix.
//        String[] nativeConfigElements = {
////                "crypto/keyring",
//                "crypto/isakmp/profile",
////                "crypto/ipsec/profile",
////                "crypto/ipsec/transform-set",
//        };
//
//        for (int n = 0; n < nativeConfigElements.length; n++) {
//            brokenXpathList.add(nativeConfigElements[n]);
//        }
//
//        repairNativeModuleConfig(brokenXpathList);
//    }

    private List<Integer> checkContinuity(List<Integer> integers, Integer start) {
        List<Integer> missingIntegers = new ArrayList<>();
        Integer index = start;

        for (Integer i: integers) {
            if (!index.equals(i)) {
                while (!index.equals(i)) {
                    missingIntegers.add(index++);
                }
            } else {
                index++;
            }
        }
        return missingIntegers;
    }

//    private String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?>\n" +
//            "<hello><capabilities>\n" +
//            "     <capability>urn:ietf:params:netconf:base:1.0</capability>\n" +
//            "      <capability>urn:ietf:params:netconf:capability:writeable-running:1.0</capability>\n" +
//            "       <capability>urn:ietf:params:netconf:capability:rollback-on-error:1.0</capability>\n" +
//            "        <capability>urn:ietf:params:netconf:capability:startup:1.0</capability>\n" +
//            "         <capability>urn:ietf:params:netconf:capability:url:1.0</capability>\n" +
//            "        <capability>urn:cisco:params:netconf:capability:pi-data-model:1.0</capability>\n" +
//            "       <capability>urn:cisco:params:netconf:capability:notification:1.0</capability>\n" +
//            "   </capabilities>\n" +
//            "</hello>]]>]]>\n" +
//            " ";

//    private String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?>\n" +
//            "<hello><capabilities>\n" +
//            "        <capability>urn:ietf:params:netconf:capability:startup:1.0</capability>\n" +
//            "   </capabilities>\n" +
//            "</hello>]]>]]>\n" +
//            " ";
//    private String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?>\n" +
//            "<hello><capabilities>\n" +
//            "        <capability>urn:ietf:params:netconf:capability:candidate:1.0</capability>\n" +
//            "   </capabilities>\n" +
//            "</hello>]]>]]>\n" +
//            " ";

//    private String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?>\n" +
//            "<hello><capabilities>\n" +
//            "   </capabilities>\n" +
//            "</hello>]]>]]>\n" +
//            " ";

    private String helloRpc = "<?xml version=\"1.0\" encoding=\\\"UTF-8\\\"?>\n" +
            "<hello/>]]>]]n";

//    private String helloRpc = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"6\">\n" +
//        "<nc:hello><nc:capabilities>\n" +
//        "     <nc:capability>urn:ietf:params:netconf:base:1.0</nc:capability>\n" +
//        "      <nc:capability>urn:ietf:params:netconf:capability:writeable-running:1.0</nc:capability>\n" +
//        "       <nc:capability>urn:ietf:params:netconf:capability:rollback-on-error:1.0</nc:capability>\n" +
//        "        <nc:capability>urn:ietf:params:netconf:capability:startup:1.0</nc:capability>\n" +
//        "         <nc:capability>urn:ietf:params:netconf:capability:url:1.0</nc:capability>\n" +
//        "        <nc:capability>urn:cisco:params:netconf:capability:pi-data-model:1.0</nc:capability>\n" +
//        "       <nc:capability>urn:cisco:params:netconf:capability:notification:1.0</nc:capability>\n" +
//        "   </nc:capabilities>\n" +
//        "</nc:hello>>\n" +
//        "</nc:rpc>";
//
//    private String helloRpc = "<hello><capabilities>\n" +
//        "     <capability>>urn:ietf:params:netconf:base:1.0</capability>>\n" +
//        "      <capability>>urn:ietf:params:netconf:capability:writeable-running:1.0</capability>>\n" +
//        "       <capability>>urn:ietf:params:netconf:capability:rollback-on-error:1.0</capability>>\n" +
//        "        <capability>>urn:ietf:params:netconf:capability:startup:1.0</capability>>\n" +
//        "         <capability>>urn:ietf:params:netconf:capability:url:1.0</capability>>\n" +
//        "        <capability>>urn:cisco:params:netconf:capability:pi-data-model:1.0</capability>>\n" +
//        "       <capability>>urn:cisco:params:netconf:capability:notification:1.0</capability>>\n" +
//        "   </capabilities>\n" +
//        "</hello>>\n";

    private String runningConfig = "<rpc xmlns=\"urn:ietf:params:xml:ns:netconf:base:1.0\" message-id=\"101\">\n" +
            "       <get-config>\n" +
            "         <source>\n" +
            "           <running/>\n" +
            "         </source>\n" +
            "       </get-config>\n" +
            "     </rpc>\n";


    private final Map<String, Map<String, String>> routerVersionMap = new HashMap<>();

    @Test
    public void testGetRouterVersion() {
        Router router = Router.ROUTER1;

        String routerVersion = null;

        // Setup router versionMap - configuration?
        Map<String, String> _1662RevisionMap = new HashMap<>();
        Map<String, String> _1694RevisionMap = new HashMap<>();

        _1662RevisionMap.put("tunnel", "2017-07-11");
        _1662RevisionMap.put("crypto", "2017-05-10");
        _1662RevisionMap.put("native", "2017-08-30");
        _1662RevisionMap.put("bgp", "2017-04-28");

        _1694RevisionMap.put("tunnel", "2017-08-28");
        _1694RevisionMap.put("crypto", "2019-04-25");
        _1694RevisionMap.put("native", "2018-07-27");
        _1694RevisionMap.put("bgp", "2019-01-09");

        routerVersionMap.put("1662", _1662RevisionMap);
        routerVersionMap.put("1694", _1694RevisionMap);

        Map<String, String> routerHostAndPortMap = getRouterHostAndPort(router);

        String routerHostName = routerHostAndPortMap.get("routerHost");
        Integer routerPort = Integer.valueOf(routerHostAndPortMap.get("routerPort"));

        RouterUtil routerUtil = new RouterUtil(routerHostName, routerPort, netconfAdminUsername, netconfAdminPassword);

        NetconfSession session = null;

        try {
            session = openNetconfSession(router);
            routerVersion = routerUtil.getRouterVersion(routerVersionMap, session);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get router version", e);
        } finally {
            closeNetconfSession(session);
        }


        if (routerVersion != null) {
            logger.info("Router version: " + routerVersion);
        } else {
            logger.error("Unsupported router version");
        }
    }

    private final String getRouterCapabilitiesRpc = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"1\">\n" +
            "    <nc:get>\n" +
            "        <nc:filter><netconf-state xmlns=\"urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring\">\n" +
            "            <capabilities/>\n" +
            "        </netconf-state></nc:filter>\n" +
            "    </nc:get>\n" +
            "</nc:rpc>\n";


    private String sendHelloCommandToRouter(NetconfSession session) throws IOException, JNCException {

        Element capabilitiesElement = session.rpc(getRouterCapabilitiesRpc);

        capabilitiesElement = capabilitiesElement.getFirst("data/netconf-state/capabilities");

        String capabilities = capabilitiesElement.getChildren().stream()
                .map(c -> c.getValue().toString())
                .collect(Collectors.joining("\n"));

        return capabilities;
    }

    public Map<String, String> getCapabilityRevisionsMap(String capabilities) {
        Map<String, String> capabilityRevisionsMap = new HashMap<>();

        StringBuilder builder = new StringBuilder();
        List<Pattern> patterns = new ArrayList<>();

        // Patterns could be configured.
        patterns.add(Pattern.compile("Cisco-IOS-XE-(tunnel).+?module=Cisco-IOS-XE-tunnel.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));
        patterns.add(Pattern.compile("Cisco-IOS-XE-(crypto).+?module=Cisco-IOS-XE-crypto.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));
        patterns.add(Pattern.compile("Cisco-IOS-XE-(native).+?module=Cisco-IOS-XE-native.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));
        patterns.add(Pattern.compile("Cisco-IOS-XE-(bgp).+?module=Cisco-IOS-XE-bgp.+?revision=(\\d\\d\\d\\d-\\d\\d-\\d\\d)"));

        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(capabilities);

            if (matcher.find()) {
                capabilityRevisionsMap.put(matcher.group(1), matcher.group(2));
            } else {
                builder.append("\n    No capability found for pattern: " + pattern.pattern());
            }
        }


        if (builder.length() > 0) {
            throw new RuntimeException("Failed to find required capabilities:" + builder.toString());
        }
        return capabilityRevisionsMap;
    }


    @Test
    public void listCapabilities() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;

        try {
            session = openNetconfSession(router);
//            Capabilities capabilities = session.getCapabilities();
//
//            printCapabilities(capabilities);

            Element result = session.rpc(helloRpc);

            if (result != null) {
                logger.info("result returned");
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            closeNetconfSession(session);
        }

    }

    private void printCapabilities(Capabilities capabilities) {

        boolean capability;

        logger.info("=================== Capabilities ===================");
        capability = capabilities.hasWritableRunning(); {
            System.out.print("writable");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasCandidate(); {
            System.out.print("candidate data store");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasConfirmedCommit(); {
            System.out.print("confirmed commit");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasRollbackOnError(); {
            System.out.print("rollback on error");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasValidate(); {
            System.out.print("validate");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasStartup(); {
            System.out.print("startup");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasNotification(); {
            System.out.print("notification");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasInterleave(); {
            System.out.print("interleave");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasUrl(); {
            System.out.print("url");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasXPath(); {
            System.out.print("xPath");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasPartialLock(); {
            System.out.print("partial lock");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasActions(); {
            System.out.print("actions");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasTransactions(); {
            System.out.print("transactions");
            logger.info(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasWithDefaults(); {
            System.out.print("with defaults");
            logger.info(capability ? " enabled" : " disabled");
        }

        logger.info("=================== /Capabilities ===================");

    }

    private void repairNativeModuleConfig(List<String> nativeConfigElementToFixList, NetconfSession session) {

        // ###################### SETUP ########################

        // Setup these values.
        Integer profileId = 1;
        String profileIdAsString = profileId.toString();
        String tunnelNumberAsString = "1";

        // ###################### SETUP ########################

        try {

            SpreadSheetWrapper spreadSheetWrapper = new SpreadSheetWrapper();
            XMLParser xmlParser = new XMLParser();
            Element nativeConfig = xmlParser.parse(deleteNativeCryptoConfigTemplate);

            for (int n = 0; n < allNativeCryptoConfigElements.length; n++) {
                String xpath = allNativeCryptoConfigElements[n];
                if (!nativeConfigElementToFixList.contains(xpath)) {
                    nativeConfig.delete(xpath);
                }
            }

            Map<String, Map<String,String>> spreadsheetMap = spreadSheetWrapper.getSpreadsheetMap();
            String paddedVpcNumberAsString = String.format("%03d", profileId);
            Map<String, String> spreadsheetLine = spreadsheetMap.get(profileIdAsString);

            nativeConfigElementToFixList.forEach(elementXPath -> {
                try {
                    Element deleteCryptoConfigElement = nativeConfig.getFirst(elementXPath);
                    String fix;
                    switch (elementXPath) {
                        case "crypto/keyring":
                            fix = spreadsheetLine.get("CRYPTO_KEYRING_" + tunnelNumberAsString);
                            deleteCryptoConfigElement.getFirst("name").setValue(fix);
                            deleteCryptoConfigElement.getFirst("pre-shared-key/address/ipv4/ipv4-addr").setValue(labRemoteVpnIpAddress);
                            deleteCryptoConfigElement.getFirst("pre-shared-key/address/ipv4/unencryt-key").setValue(buildLabUnencryptKey(profileId));
                            break;
                        case "crypto/isakmp/profile":
                            fix = spreadsheetLine.get("ISAKMP_PROFILE_" + tunnelNumberAsString);
                            deleteCryptoConfigElement.getFirst("name").setValue(fix);
                            deleteCryptoConfigElement.getFirst("match/identity/ipv4-address/address").setValue(labRemoteVpnIpAddress);
                            break;
                        case "crypto/ipsec/profile":
                            fix = spreadsheetLine.get("IPSEC_PROFILE_" + tunnelNumberAsString);
                            deleteCryptoConfigElement.getFirst("name").setValue(fix);
                            break;
                        case "crypto/ipsec/transform-set":
                            fix = spreadsheetLine.get("IPSEC_TRANSFORM_SET_" + tunnelNumberAsString);
                            deleteCryptoConfigElement.getFirst("tag").setValue(fix);
                            break;

                    }
                } catch (Exception e) {
                    logger.error("Failed to configure");
                    throw new RuntimeException(e);
                }
            });

            logger.info(nativeConfig.toXMLString());

            session.editConfig(nativeConfig);

        } catch (Exception e) {
            logger.error("Failed ", e);
        }
    }


    @Test
    public void template() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            logger.info("Do something here...");

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    public String prettyPrintXmlString(String xmlString) throws Exception {
        String pretty = "";

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("http://xml.org/sax/features/namespaces", false);
        dbf.setFeature("http://xml.org/sax/features/validation", false);
        dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new ByteArrayInputStream(xmlString.getBytes()));
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        pretty = result.getWriter().toString();
        return pretty;
    }

    private String buildLabUnencryptKey(Integer vpcNumber) {
        String unencryptKey;

        unencryptKey = String.format("test%03d", vpcNumber);

        return unencryptKey;
    }

    //########################################## Config Templates ####################################################//


    final private String tunnelShutdownConfigTemplate = "<native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\"\n" +
            "        xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "        xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "        xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
            "  <interface>\n" +
            "    <Tunnel>\n" +
            "      <name>10025</name>" +
            "      <description>AWS Research VPC### Tunnel1 ($$$)</description>\n" +
            "      <shutdown nc:operation=\"remove\"/>\n" +
            "    </Tunnel>\n" +
            "  </interface>\n" +
            "</native>\n";

    final private String deleteNativeCryptoConfigTemplate = "<native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\"\n" +
            "        xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "        xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "        xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "        xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\">\n" +
            "  <crypto>\n" +
            "    <ios-crypto:keyring>\n" +
            "      <ios-crypto:name>keyring-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>\n" +
            "      <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "      <ios-crypto:description nc:operation=\"remove\"></ios-crypto:description>\n" +
            "      <ios-crypto:pre-shared-key>\n" +
            "        <ios-crypto:address>\n" +
            "          <ios-crypto:ipv4 nc:operation=\"remove\">\n" +
            "            <ios-crypto:ipv4-addr>FIXME_RemoteVpnIpAddress</ios-crypto:ipv4-addr>\n" +
            "            <ios-crypto:key/>\n" +
            "            <ios-crypto:unencryt-key>FIXME_PresharedKey</ios-crypto:unencryt-key>\n" +
            "          </ios-crypto:ipv4>\n" +
            "        </ios-crypto:address>\n" +
            "      </ios-crypto:pre-shared-key>\n" +
            "    </ios-crypto:keyring>\n" +
            "    <ios-crypto:isakmp>\n" +
            "      <ios-crypto:profile>\n" +
            "        <ios-crypto:name>isakmp-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>\n" +
            "        <ios-crypto:description nc:operation=\"remove\"></ios-crypto:description>\n" +
            "        <ios-crypto:match>\n" +
            "          <ios-crypto:identity>\n" +
            "            <ios-crypto:ipv4-address nc:operation=\"remove\">\n" +
            "              <ios-crypto:address>FIXME_RemoteVpnIpAddress</ios-crypto:address>\n" +
            "              <ios-crypto:mask>255.255.255.255</ios-crypto:mask>\n" +
            "              <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "            </ios-crypto:ipv4-address>\n" +
            "          </ios-crypto:identity>\n" +
            "        </ios-crypto:match>\n" +
            "      </ios-crypto:profile>\n" +
            "    </ios-crypto:isakmp>\n" +
            "    <ios-crypto:ipsec>\n" +
            "      <ios-crypto:transform-set nc:operation=\"remove\">\n" +
            "        <ios-crypto:tag>ipsec-prop-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:tag>\n" +
            "      </ios-crypto:transform-set>\n" +
            "      <ios-crypto:profile nc:operation=\"remove\">\n" +
            "        <ios-crypto:name>ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum</ios-crypto:name>\n" +
            "      </ios-crypto:profile>\n" +
            "    </ios-crypto:ipsec>\n" +
            "  </crypto>\n" +
            "</native>\n";

    final String[] allNativeCryptoConfigElements = {
            "crypto/keyring",
            "crypto/isakmp/profile",
            "crypto/ipsec/profile",
            "crypto/ipsec/transform-set",
    };

    final private String queryTunnelTemplate = " <filter>\n" +
            "<native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
            "              <crypto>\n" +
            "                <ios-crypto:ipsec>\n" +
            "                  <ios-crypto:profile>\n" +
            "                    <ios-crypto:name>ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                  </ios-crypto:profile>\n" +
            "                  <ios-crypto:transform-set>\n" +
            "                    <ios-crypto:tag>ipsec-prop-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:tag>\n" +
            "                  </ios-crypto:transform-set>\n" +
            "                </ios-crypto:ipsec>\n" +
            "                <ios-crypto:isakmp>\n" +
            "                  <ios-crypto:profile>\n" +
            "                    <ios-crypto:name>isakmp-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                  </ios-crypto:profile>\n" +
            "                </ios-crypto:isakmp>\n" +
            "                <ios-crypto:keyring>\n" +
            "                  <ios-crypto:name>keyring-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                </ios-crypto:keyring>\n" +
            "              </crypto>\n" +
            "              <interface>\n" +
            "                <Tunnel>\n" +
            "                  <!-- This must be a composite substitution - query request does not have tunnelId ! -->\n" +
            "                  <name>${RouterNumber}${TunnelNumberZeroBased}${VpnId-Padded}</name>\n" +
            "                </Tunnel>\n" +
            "              </interface>\n" +
            "              <router>\n" +
            "                <ios-bgp:bgp>\n" +
            "                  <ios-bgp:id>3512</ios-bgp:id>\n" +
            "                  <ios-bgp:address-family>\n" +
            "                    <ios-bgp:with-vrf>\n" +
            "                      <ios-bgp:ipv4>\n" +
            "                        <ios-bgp:af-name>unicast</ios-bgp:af-name>\n" +
            "                        <ios-bgp:vrf>\n" +
            "                          <ios-bgp:name>AWS</ios-bgp:name>\n" +
            "                          <ios-bgp:neighbor>\n" +
            "                            <ios-bgp:id>${BgpNeighborId}</ios-bgp:id>\n" +
            "                          </ios-bgp:neighbor>\n" +
            "                        </ios-bgp:vrf>\n" +
            "                      </ios-bgp:ipv4>\n" +
            "                    </ios-bgp:with-vrf>\n" +
            "                  </ios-bgp:address-family>\n" +
            "                </ios-bgp:bgp>\n" +
            "              </router>\n" +
            "            </native>\n" +
            "            <bgp-state-data xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp-oper\">\n" +
            "              <neighbors>\n" +
            "                <neighbor>\n" +
            "                  <afi-safi>vpnv4-unicast</afi-safi>\n" +
            "                  <vrf-name>AWS</vrf-name>\n" +
            "                  <neighbor-id>${BgpNeighborId}</neighbor-id>\n" +
            "                  <description/>\n" +
            "                  <up-time/>\n" +
            "                  <installed-prefixes/>\n" +
            "                  <session-state/>\n" +
            "                  <prefix-activity>\n" +
            "                    <sent>\n" +
            "                      <current-prefixes/>\n" +
            "                    </sent>\n" +
            "                    <received>\n" +
            "                      <current-prefixes/>\n" +
            "                    </received>\n" +
            "                  </prefix-activity>\n" +
            "                </neighbor>\n" +
            "              </neighbors>\n" +
            "            </bgp-state-data>\n" +
            "            <interfaces-state xmlns=\"urn:ietf:params:xml:ns:yang:ietf-interfaces\">\n" +
            "                <interface>\n" +
            "                  <name>Tunnel${TunnelId}</name>\n" +
            "                  <admin-status/>\n" +
            "                  <oper-status/>\n" +
            "                </interface>\n" +
            "            </interfaces-state>\n" +
            "        </filter>\n";

    //################################################################################################################//

    String createTunnelTemplate = "<native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
            "              <crypto>\n" +
            "                <ios-crypto:keyring>\n" +
            "                  <ios-crypto:name>keyring-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                  <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "                  <ios-crypto:description>${VpcId}</ios-crypto:description>\n" +
            "                  <ios-crypto:local-address>\n" +
            "                    <ios-crypto:bind-ip-address>\n" +
            "                      <ios-crypto:ip-address>${LocalVpnIpAddress}</ios-crypto:ip-address>\n" +
            "                      <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "                    </ios-crypto:bind-ip-address>\n" +
            "                  </ios-crypto:local-address>\n" +
            "                  <ios-crypto:pre-shared-key>\n" +
            "                    <ios-crypto:address>\n" +
            "                      <ios-crypto:ipv4>\n" +
            "                        <ios-crypto:ipv4-addr>${RemoteVpnIpAddress}</ios-crypto:ipv4-addr>\n" +
            "                        <ios-crypto:key/>\n" +
            "                        <ios-crypto:unencryt-key>${PresharedKey}</ios-crypto:unencryt-key>\n" +
            "                      </ios-crypto:ipv4>\n" +
            "                    </ios-crypto:address>\n" +
            "                  </ios-crypto:pre-shared-key>\n" +
            "                </ios-crypto:keyring>\n" +
            "                <ios-crypto:isakmp>\n" +
            "                  <ios-crypto:profile>\n" +
            "                    <ios-crypto:name>isakmp-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                    <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "                    <ios-crypto:description>${VpcId}</ios-crypto:description>\n" +
            "                    <ios-crypto:keyring>\n" +
            "                      <ios-crypto:name>keyring-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                    </ios-crypto:keyring>\n" +
            "                    <ios-crypto:local-address>\n" +
            "                      <ios-crypto:bind-ip-address>\n" +
            "                        <ios-crypto:ip-address>${LocalVpnIpAddress}</ios-crypto:ip-address>\n" +
            "                        <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "                      </ios-crypto:bind-ip-address>\n" +
            "                    </ios-crypto:local-address>\n" +
            "                    <ios-crypto:match>\n" +
            "                      <ios-crypto:identity>\n" +
            "                        <ios-crypto:address>\n" +
            "                          <ios-crypto:ip>${RemoteVpnIpAddress}</ios-crypto:ip>\n" +
            "                          <ios-crypto:mask>255.255.255.255</ios-crypto:mask>\n" +
            "                          <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "                        </ios-crypto:address>\n" +
            "                      </ios-crypto:identity>\n" +
            "                    </ios-crypto:match>\n" +
            "                  </ios-crypto:profile>\n" +
            "                </ios-crypto:isakmp>\n" +
            "                <ios-crypto:ipsec>\n" +
            "                  <ios-crypto:transform-set>\n" +
            "                    <ios-crypto:tag>ipsec-prop-vpn-GCP-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:tag>\n" +
            "                    <ios-crypto:esp>esp-aes</ios-crypto:esp>\n" +
            "                    <ios-crypto:esp-hmac>esp-sha-hmac</ios-crypto:esp-hmac>\n" +
            "                    <ios-crypto:mode>\n" +
            "                      <ios-crypto:tunnel/>\n" +
            "                    </ios-crypto:mode>\n" +
            "                  </ios-crypto:transform-set>\n" +
            "                  <ios-crypto:profile>\n" +
            "                    <ios-crypto:name>ipsec-vpn-GCP-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                    <ios-crypto:description>${VpcId}</ios-crypto:description>\n" +
            "                    <ios-crypto:set>\n" +
            "                      <ios-crypto:pfs>\n" +
            "                        <ios-crypto:group>group2</ios-crypto:group>\n" +
            "                      </ios-crypto:pfs>\n" +
            "                      <ios-crypto:transform-set>ipsec-prop-vpn-GCP-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:transform-set>\n" +
            "                      <ios-crypto:security-association>\n" +
            "                        <ios-crypto:lifetime>\n" +
            "                          <ios-crypto:seconds>10800</ios-crypto:seconds>\n" +
            "                          <ios-crypto:kilobytes>disable</ios-crypto:kilobytes>\n" +
            "                        </ios-crypto:lifetime>\n" +
            "                      </ios-crypto:security-association>\n" +
            "                    </ios-crypto:set>\n" +
            "                  </ios-crypto:profile>\n" +
            "                </ios-crypto:ipsec>\n" +
            "              </crypto>\n" +
            "              <interface>\n" +
            "                <Tunnel>\n" +
            "                  <name>${TunnelId}</name>\n" +
            "                  <shutdown xc:operation=\"remove\"/>\n" +
            "                  <description>GCP Research VPC${VpnId-Padded} Tunnel${TunnelNumber} (${VpcId})</description>\n" +
            "                  <ios-tun:tunnel>\n" +
            "                    <ios-tun:destination>\n" +
            "                      <ios-tun:ipaddress-or-host>${RemoteVpnIpAddress}</ios-tun:ipaddress-or-host>\n" +
            "                    </ios-tun:destination>\n" +
            "                    <ios-tun:protection>\n" +
            "                      <ios-crypto:ipsec>\n" +
            "                        <ios-crypto:profile>ipsec-vpn-GCP-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:profile>\n" +
            "                      </ios-crypto:ipsec>\n" +
            "                    </ios-tun:protection>\n" +
            "                  </ios-tun:tunnel>\n" +
            "                  <ip>\n" +
            "                    <mtu>1400</mtu>\n" +
            "                    <tcp>\n" +
            "                      <adjust-mss>1360</adjust-mss>\n" +
            "                    </tcp>\n" +
            "                  </ip>\n" +
            "                </Tunnel>\n" +
            "              </interface>\n" +
            "              <router>\n" +
            "                <ios-bgp:bgp>\n" +
            "                  <ios-bgp:id>3512</ios-bgp:id>\n" +
            "                  <ios-bgp:address-family>\n" +
            "                    <ios-bgp:with-vrf>\n" +
            "                      <ios-bgp:ipv4>\n" +
            "                        <ios-bgp:af-name>unicast</ios-bgp:af-name>\n" +
            "                        <ios-bgp:vrf>\n" +
            "                          <ios-bgp:name>AWS</ios-bgp:name>\n" +
            "                          <ios-bgp:ipv4-unicast>\n" +
            "                            <ios-bgp:neighbor>\n" +
            "                              <ios-bgp:id>${BgpNeighborId}</ios-bgp:id>\n" +
            "                              <ios-bgp:peer-group>\n" +
            "                                <ios-bgp:peer-group-name>GCP_RESEARCH_VPCs</ios-bgp:peer-group-name>\n" +
            "                              </ios-bgp:peer-group>\n" +
            "                              <ios-bgp:description>${VpcId}</ios-bgp:description>\n" +
            "                            </ios-bgp:neighbor>\n" +
            "                          </ios-bgp:ipv4-unicast>\n" +
            "                        </ios-bgp:vrf>\n" +
            "                      </ios-bgp:ipv4>\n" +
            "                    </ios-bgp:with-vrf>\n" +
            "                  </ios-bgp:address-family>\n" +
            "                </ios-bgp:bgp>\n" +
            "              </router>\n" +
            "            </native>\n";

    String deleteTunnelTemplate = "<native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
            "    <router>\n" +
            "        <ios-bgp:bgp>\n" +
            "            <ios-bgp:id>3512</ios-bgp:id>\n" +
            "            <ios-bgp:address-family>\n" +
            "                <ios-bgp:with-vrf>\n" +
            "                    <ios-bgp:ipv4>\n" +
            "                        <ios-bgp:af-name>unicast</ios-bgp:af-name>\n" +
            "                        <ios-bgp:vrf>\n" +
            "                            <ios-bgp:name>AWS</ios-bgp:name>\n" +
            "                            <ios-bgp:ipv4-unicast>\n" +
            "                                <ios-bgp:neighbor xc:operation=\"remove\">\n" +
            "                                    <ios-bgp:id>${BgpNeighborId}</ios-bgp:id>\n" +
            "                                </ios-bgp:neighbor>\n" +
            "                            </ios-bgp:ipv4-unicast>\n" +
            "                        </ios-bgp:vrf>\n" +
            "                    </ios-bgp:ipv4>\n" +
            "                </ios-bgp:with-vrf>\n" +
            "            </ios-bgp:address-family>\n" +
            "        </ios-bgp:bgp>\n" +
            "    </router>\n" +
            "    <interface>\n" +
            "        <Tunnel>\n" +
            "            <name>${TunnelId}</name>\n" +
            "            <description>AWS Research VPC${VpnId-Padded} Tunnel${TunnelNumber} (AVAILABLE)</description>\n" +
            "            <ios-tun:tunnel>\n" +
            "                <ios-tun:destination>\n" +
            "                    <ios-tun:ipaddress-or-host xc:operation=\"remove\"></ios-tun:ipaddress-or-host>\n" +
            "                </ios-tun:destination>\n" +
            "                <ios-tun:protection>\n" +
            "                    <ios-crypto:ipsec>\n" +
            "                        <ios-crypto:profile xc:operation=\"remove\">ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:profile>\n" +
            "                    </ios-crypto:ipsec>\n" +
            "                </ios-tun:protection>\n" +
            "            </ios-tun:tunnel>\n" +
            "        </Tunnel>\n" +
            "    </interface>\n" +
            "    <crypto>\n" +
            "        <ios-crypto:keyring>\n" +
            "            <ios-crypto:name>keyring-vpn-research-vpc${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:name>\n" +
            "            <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "            <ios-crypto:description xc:operation=\"remove\"></ios-crypto:description>\n" +
            "            <ios-crypto:pre-shared-key>\n" +
            "                <ios-crypto:address>\n" +
            "                    <ios-crypto:ipv4 xc:operation=\"remove\">\n" +
            "                        <ios-crypto:ipv4-addr>${RemoteVpnIpAddress}</ios-crypto:ipv4-addr>\n" +
            "                        <ios-crypto:key/>\n" +
            "                        <ios-crypto:unencryt-key>${PresharedKey}</ios-crypto:unencryt-key>\n" +
            "                    </ios-crypto:ipv4>\n" +
            "                </ios-crypto:address>\n" +
            "            </ios-crypto:pre-shared-key>\n" +
            "        </ios-crypto:keyring>\n" +
            "        <ios-crypto:isakmp>\n" +
            "            <ios-crypto:profile>\n" +
            "                <ios-crypto:name>isakmp-vpn-research-vpc${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:name>\n" +
            "                <ios-crypto:description xc:operation=\"remove\"></ios-crypto:description>\n" +
            "                <ios-crypto:match>\n" +
            "                    <ios-crypto:identity>\n" +
            "                        <ios-crypto:address xc:operation=\"remove\">\n" +
            "                            <ios-crypto:ip>${RemoteVpnIpAddress}</ios-crypto:ip>\n" +
            "                            <ios-crypto:mask>255.255.255.255</ios-crypto:mask>\n" +
            "                            <ios-crypto:vrf>AWS</ios-crypto:vrf>\n" +
            "                        </ios-crypto:address>\n" +
            "                    </ios-crypto:identity>\n" +
            "                </ios-crypto:match>\n" +
            "            </ios-crypto:profile>\n" +
            "        </ios-crypto:isakmp>\n" +
            "        <ios-crypto:ipsec>\n" +
            "            <ios-crypto:transform-set xc:operation=\"remove\">\n" +
            "                <ios-crypto:tag>ipsec-prop-vpn-research-vpc${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:tag>\n" +
            "            </ios-crypto:transform-set>\n" +
            "            <ios-crypto:profile xc:operation=\"remove\">\n" +
            "                <ios-crypto:name>ipsec-vpn-research-vpc${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:name>\n" +
            "            </ios-crypto:profile>\n" +
            "        </ios-crypto:ipsec>\n" +
            "    </crypto>\n" +
            "</native>\n";

    String _1662AddNativeResults = "    <native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\" xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
            "        <interface>\n" +
            "            <Tunnel>\n" +
            "                <name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                      xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                      xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                      xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                      xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">20104\n" +
            "                </name>\n" +
            "                <description>AWS Research VPC104 Tunnel2 (vpc-666666666666666)</description>\n" +
            "                <vrf>\n" +
            "                    <forwarding>AWS</forwarding>\n" +
            "                </vrf>\n" +
            "                <ip>\n" +
            "                    <address>\n" +
            "                        <primary>\n" +
            "                            <address>169.254.253.158</address>\n" +
            "                            <mask>255.255.255.252</mask>\n" +
            "                        </primary>\n" +
            "                    </address>\n" +
            "                    <tcp>\n" +
            "                        <adjust-mss>1360</adjust-mss>\n" +
            "                    </tcp>\n" +
            "                    <mtu>1400</mtu>\n" +
            "                </ip>\n" +
            "                <tunnel xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\">\n" +
            "                    <source>170.140.77.104</source>\n" +
            "                    <destination>72.21.209.225</destination>\n" +
            "                    <mode>\n" +
            "                        <ipsec>\n" +
            "                            <ipv4/>\n" +
            "                        </ipsec>\n" +
            "                    </mode>\n" +
            "                    <protection>\n" +
            "                        <ipsec xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "                            <profile>ipsec-vpn-GCP-vpc104-tun2</profile>\n" +
            "                        </ipsec>\n" +
            "                    </protection>\n" +
            "                    <vrf>AWS</vrf>\n" +
            "                </tunnel>\n" +
            "            </Tunnel>\n" +
            "        </interface>\n" +
            "        <crypto>\n" +
            "            <ipsec xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "                <profile>\n" +
            "                    <ios-crypto:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                     xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                     xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                     xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                     xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">ipsec-vpn-GCP-vpc104-tun2\n" +
            "                    </ios-crypto:name>\n" +
            "                    <description>vpc-666666666666666</description>\n" +
            "                    <set>\n" +
            "                        <pfs>\n" +
            "                            <group>group2</group>\n" +
            "                        </pfs>\n" +
            "                        <transform-set>ipsec-prop-vpn-GCP-vpc104-tun2</transform-set>\n" +
            "                        <security-association>\n" +
            "                            <lifetime>\n" +
            "                                <kilobytes>disable</kilobytes>\n" +
            "                                <seconds>10800</seconds>\n" +
            "                            </lifetime>\n" +
            "                        </security-association>\n" +
            "                    </set>\n" +
            "                </profile>\n" +
            "                <transform-set>\n" +
            "                    <ios-crypto:tag xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                    xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                    xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                    xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                    xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">ipsec-prop-vpn-GCP-vpc104-tun2\n" +
            "                    </ios-crypto:tag>\n" +
            "                    <esp>esp-aes</esp>\n" +
            "                    <esp-hmac>esp-sha-hmac</esp-hmac>\n" +
            "                    <mode>\n" +
            "                        <tunnel/>\n" +
            "                    </mode>\n" +
            "                </transform-set>\n" +
            "            </ipsec>\n" +
            "            <isakmp xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "                <profile>\n" +
            "                    <ios-crypto:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                     xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                     xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                     xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                     xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">isakmp-vpn-research-vpc104-tun2\n" +
            "                    </ios-crypto:name>\n" +
            "                    <description>vpc-666666666666666</description>\n" +
            "                    <keyring>keyring-vpn-research-vpc104-tun2</keyring>\n" +
            "                    <match>\n" +
            "                        <identity>\n" +
            "                            <ipv4-address>\n" +
            "                                <address>72.21.209.225</address>\n" +
            "                                <mask>255.255.255.255</mask>\n" +
            "                                <vrf>AWS</vrf>\n" +
            "                            </ipv4-address>\n" +
            "                        </identity>\n" +
            "                    </match>\n" +
            "                    <vrf>AWS</vrf>\n" +
            "                </profile>\n" +
            "            </isakmp>\n" +
            "            <keyring xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "                <ios-crypto:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                 xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                 xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                 xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                 xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">keyring-vpn-research-vpc104-tun2\n" +
            "                </ios-crypto:name>\n" +
            "                <vrf>AWS</vrf>\n" +
            "                <description>vpc-666666666666666</description>\n" +
            "                <pre-shared-key>\n" +
            "                    <address>\n" +
            "                        <ipv4>\n" +
            "                            <ipv4-addr>72.21.209.225</ipv4-addr>\n" +
            "                            <key/>\n" +
            "                            <unencryt-key>test104</unencryt-key>\n" +
            "                        </ipv4>\n" +
            "                    </address>\n" +
            "                </pre-shared-key>\n" +
            "            </keyring>\n" +
            "        </crypto>\n" +
            "        <router>\n" +
            "            <bgp xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\">\n" +
            "                <ios-bgp:id xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                            xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                            xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                            xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                            xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">3512\n" +
            "                </ios-bgp:id>\n" +
            "                <address-family>\n" +
            "                    <with-vrf>\n" +
            "                        <ipv4>\n" +
            "                            <ios-bgp:af-name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                             xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                             xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                             xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                             xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">unicast\n" +
            "                            </ios-bgp:af-name>\n" +
            "                            <vrf>\n" +
            "                                <ios-bgp:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                              xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                              xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                              xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                              xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">AWS\n" +
            "                                </ios-bgp:name>\n" +
            "                                <neighbor>\n" +
            "                                    <ios-bgp:id xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\"\n" +
            "                                                xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\"\n" +
            "                                                xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\"\n" +
            "                                                xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\"\n" +
            "                                                xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">169.254.253.157\n" +
            "                                    </ios-bgp:id>\n" +
            "                                    <peer-group>\n" +
            "                                        <peer-group-name>GCP_RESEARCH_VPCs</peer-group-name>\n" +
            "                                    </peer-group>\n" +
            "                                    <description>vpc-666666666666666</description>\n" +
            "                                </neighbor>\n" +
            "                            </vrf>\n" +
            "                        </ipv4>\n" +
            "                    </with-vrf>\n" +
            "                </address-family>\n" +
            "            </bgp>\n" +
            "        </router>\n" +
            "    </native>\n";

    String _1662GCPQueryNativeResult = "<native xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-native\" xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n" +
            "  <interface>\n" +
            "    <Tunnel>\n" +
            "      <name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">20104</name>\n" +
            "      <description>AWS Research VPC104 Tunnel2 (vpc-666666666666666)</description>\n" +
            "      <vrf>\n" +
            "        <forwarding>AWS</forwarding>\n" +
            "      </vrf>\n" +
            "      <ip>\n" +
            "        <address>\n" +
            "          <primary>\n" +
            "            <address>169.254.253.158</address>\n" +
            "            <mask>255.255.255.252</mask>\n" +
            "          </primary>\n" +
            "        </address>\n" +
            "        <tcp>\n" +
            "          <adjust-mss>1360</adjust-mss>\n" +
            "        </tcp>\n" +
            "        <mtu>1400</mtu>\n" +
            "      </ip>\n" +
            "      <tunnel xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\">\n" +
            "        <source>170.140.77.104</source>\n" +
            "        <destination>72.21.209.225</destination>\n" +
            "        <mode>\n" +
            "          <ipsec>\n" +
            "            <ipv4/>          </ipsec>\n" +
            "        </mode>\n" +
            "        <protection>\n" +
            "          <ipsec xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "            <profile>ipsec-vpn-GCP-vpc104-tun2</profile>\n" +
            "          </ipsec>\n" +
            "        </protection>\n" +
            "        <vrf>AWS</vrf>\n" +
            "      </tunnel>\n" +
            "    </Tunnel>\n" +
            "  </interface>\n" +
            "  <crypto>\n" +
            "    <ipsec xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "      <profile>\n" +
            "        <ios-crypto:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">ipsec-vpn-GCP-vpc104-tun2</ios-crypto:name>\n" +
            "        <description>vpc-666666666666666</description>\n" +
            "        <set>\n" +
            "          <pfs>\n" +
            "            <group>group2</group>\n" +
            "          </pfs>\n" +
            "          <transform-set>ipsec-prop-vpn-GCP-vpc104-tun2</transform-set>\n" +
            "          <security-association>\n" +
            "            <lifetime>\n" +
            "              <kilobytes>disable</kilobytes>\n" +
            "              <seconds>10800</seconds>\n" +
            "            </lifetime>\n" +
            "          </security-association>\n" +
            "        </set>\n" +
            "      </profile>\n" +
            "      <transform-set>\n" +
            "        <ios-crypto:tag xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">ipsec-prop-vpn-GCP-vpc104-tun2</ios-crypto:tag>\n" +
            "        <esp>esp-aes</esp>\n" +
            "        <esp-hmac>esp-sha-hmac</esp-hmac>\n" +
            "        <mode>\n" +
            "          <tunnel/>        </mode>\n" +
            "      </transform-set>\n" +
            "    </ipsec>\n" +
            "    <isakmp xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "      <profile>\n" +
            "        <ios-crypto:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">isakmp-vpn-research-vpc104-tun2</ios-crypto:name>\n" +
            "        <description>vpc-666666666666666</description>\n" +
            "        <keyring>keyring-vpn-research-vpc104-tun2</keyring>\n" +
            "        <match>\n" +
            "          <identity>\n" +
            "            <ipv4-address>\n" +
            "              <address>72.21.209.225</address>\n" +
            "              <mask>255.255.255.255</mask>\n" +
            "              <vrf>AWS</vrf>\n" +
            "            </ipv4-address>\n" +
            "          </identity>\n" +
            "        </match>\n" +
            "        <vrf>AWS</vrf>\n" +
            "      </profile>\n" +
            "    </isakmp>\n" +
            "    <keyring xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\">\n" +
            "      <ios-crypto:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">keyring-vpn-research-vpc104-tun2</ios-crypto:name>\n" +
            "      <vrf>AWS</vrf>\n" +
            "      <description>vpc-666666666666666</description>\n" +
            "      <pre-shared-key>\n" +
            "        <address>\n" +
            "          <ipv4>\n" +
            "            <ipv4-addr>72.21.209.225</ipv4-addr>\n" +
            "            <key/>            <unencryt-key>test104</unencryt-key>\n" +
            "          </ipv4>\n" +
            "        </address>\n" +
            "      </pre-shared-key>\n" +
            "    </keyring>\n" +
            "  </crypto>\n" +
            "  <router>\n" +
            "    <bgp xmlns=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\">\n" +
            "      <ios-bgp:id xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">3512</ios-bgp:id>\n" +
            "      <address-family>\n" +
            "        <with-vrf>\n" +
            "          <ipv4>\n" +
            "            <ios-bgp:af-name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">unicast</ios-bgp:af-name>\n" +
            "            <vrf>\n" +
            "              <ios-bgp:name xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">AWS</ios-bgp:name>\n" +
            "              <neighbor>\n" +
            "                <ios-bgp:id xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" xmlns:ios-crypto=\"http://cisco.com/ns/yang/Cisco-IOS-XE-crypto\" xmlns:ios-tun=\"http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel\" xmlns:ios-bgp=\"http://cisco.com/ns/yang/Cisco-IOS-XE-bgp\" xmlns:xc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">169.254.253.157</ios-bgp:id>\n" +
            "                <peer-group>\n" +
            "                  <peer-group-name>GCP_RESEARCH_VPCs</peer-group-name>\n" +
            "                </peer-group>\n" +
            "                <activate/>                <description>vpc-666666666666666</description>\n" +
            "              </neighbor>\n" +
            "            </vrf>\n" +
            "          </ipv4>\n" +
            "        </with-vrf>\n" +
            "      </address-family>\n" +
            "    </bgp>\n" +
            "  </router>\n" +
            "</native>\n";

    public class TunnelInformation {
        final public String tunnelNumber;
        final public String vpcId;
        final public Integer profileId;

        public TunnelInformation(String tunnelNumber, String vpcId, Integer profileId) {
            this.tunnelNumber = tunnelNumber;
            this.vpcId = vpcId;
            this.profileId = profileId;
        }
    }

}
