package edu.emory.it.service.ca.provider;

import org.junit.Before;
import org.junit.Test;

public class ExampleProviderTest {

    Logger logger = new Logger();
    MyExampleProvider myExampleProvider = new MyExampleProvider();

    @Before
    public void init() {
        myExampleProvider = new MyExampleProvider();
    }

    @Test
    public void testFailWithErrorCondition() {



        try {
            myExampleProvider.fail(myExampleProvider.errorCondition, 3, 2, "My test fail");
            logger.error("Unexpected success");
            assert false;
        } catch (ProviderException e) {
            logger.info("Expected ProviderException caught");
            assert true;
        }
    }

    @Test
    public void testFailWithTunnelNumbers() {

        myExampleProvider.routerNumber = "1";

        String[] tunnelNumbers = {};
        try {
            myExampleProvider.fail(tunnelNumbers, "Test exception for generate");
            logger.info("Expected lack of ProviderException");
            assert true;
        } catch (ProviderException e) {
            logger.error("Unexpected ProviderException");
            assert false;
        }

        String[] tunnelNumbers2 = {"1", "2"};

        try {
            myExampleProvider.fail(tunnelNumbers2, "Test exception for generate");
            logger.error("Unexpected lack of ProviderException");
            assert false;
        } catch (ProviderException e) {
            logger.info("Expected ProviderException");
            assert true;
        }


    }
    public class MyExampleProvider extends ExampleProvider {


    }

    public class Logger {
        public void info(String message) {
            System.out.println(message);
        }

        public void error(String error) {
            System.err.println(error);
        }
    }
}
