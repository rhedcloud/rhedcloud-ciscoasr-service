package edu.emory.it.service.ca.provider

import org.jdom.Document
import org.jdom.Element
import org.junit.runner.RunWith
import org.openeai.layouts.EnterpriseLayoutException
import org.openeai.layouts.EnterpriseLayoutManager
import org.openeai.moa.XmlEnterpriseObject
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate
import org.spockframework.runtime.Sputnik
import spock.lang.Specification

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(Sputnik.class)
@PrepareForTest([ExampleProvider.class])
class ExampleProviderSpec extends Specification {

    def "test Fail With Error Condition" () {

        given:
        ExampleProvider exampleProvider = Mock(ExampleProvider)

        when:
        exampleProvider.fail(exampleProvider.errorCondition, 3, 2, "My test fail")

        then:
//        thrown(ProviderException.class)
        1 * exampleProvider.fail(_,_,_,_)

    }

    EnterpriseLayoutManager buildLayoutManager() {
        return new EnterpriseLayoutManager()  {
            @Override
            void init(String s, Document document) throws EnterpriseLayoutException {

            }

            @Override
            void buildObjectFromInput(Object o, XmlEnterpriseObject xmlEnterpriseObject) throws EnterpriseLayoutException {

            }

            @Override
            Object buildOutputFromObject(XmlEnterpriseObject xmlEnterpriseObject) throws EnterpriseLayoutException {
                return null
            }

            @Override
            Object buildOutputFromObject(XmlEnterpriseObject xmlEnterpriseObject, String s) throws EnterpriseLayoutException {
                return null
            }

            @Override
            void setLayoutManagerName(String s) {

            }

            @Override
            String getLayoutManagerName() {
                return null
            }

            @Override
            Element getLayoutRoot() {
                return null
            }

            @Override
            void setEnterpriseObjectsUri(String s) {

            }

            @Override
            String getEnterpriseObjectsUri() {
                return null
            }
        }
    }

}
