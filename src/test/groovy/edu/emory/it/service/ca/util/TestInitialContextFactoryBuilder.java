package edu.emory.it.service.ca.util;

import edu.emory.it.service.ca.Starter;
import org.apache.activemq.jndi.ActiveMQInitialContextFactory;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.InitialContextFactoryBuilder;
import javax.naming.spi.NamingManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;

public class TestInitialContextFactoryBuilder implements InitialContextFactoryBuilder {

    private Hashtable<?, ?> myEnvironment;

    public TestInitialContextFactoryBuilder(Hashtable<?,?> environment) {
        this.myEnvironment = environment;
    }

    @Override
    public InitialContextFactory createInitialContextFactory(Hashtable<?, ?> environment) throws NamingException {

        ActiveMQInitialContextFactory factory = new TestActiveMQInitialContextFactory(myEnvironment);
        return factory;
    }


    /**
     * Set initial context factory builder so we can override jndi properties for test suite.
     *
     *   Historical curiosity: When deployment descriptor ProviderUrl config did *NOT*
     *   match test jndi properties file setting for java.naming.provider.url (Context.PROVIDER_URL)
     *   VM Starter and broker mismatch occurred on our InitialContext. This manifested itself in
     *   a multitude of VM Starter and broker log messages, and the test app listening on wrong provider instance.
     *
     * @param jndiFile
     */
    public static void setInitialContextFactoryBuilder(File jndiFile) {

        try {
            InputStream inputStream = new FileInputStream(jndiFile);
            Properties properties = new Properties();
            properties.load(inputStream);

            properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");

            if (NamingManager.hasInitialContextFactoryBuilder()) {
                throw new IllegalStateException("ContextFactoryBuilder already exists!");
            }
            TestInitialContextFactoryBuilder ctxBuilder = new TestInitialContextFactoryBuilder(properties);
            NamingManager.setInitialContextFactoryBuilder(ctxBuilder);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to set initial context factory builder", e);
        }

    }

    private class TestActiveMQInitialContextFactory extends ActiveMQInitialContextFactory {

        private Hashtable<?, ?> myEnvironment;

        public TestActiveMQInitialContextFactory(Hashtable<?,?> environment) {

            myEnvironment = environment;
        }

        @SuppressWarnings("rawtypes")
        @Override
        public Context getInitialContext(Hashtable environment) throws NamingException {
            return super.getInitialContext(myEnvironment);
        }
    }

}
