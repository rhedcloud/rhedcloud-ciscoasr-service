package edu.emory.it.service.ca;

import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.util.XmlUtils;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProvisioning;
import org.junit.Test;
import org.openeai.config.*;
import org.openeai.utils.lock.DbLockConfig;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.List;

public class CommandTests {
    private static final String VPN_CONNECTION_PROVISIONING_CLASS_NAME = "vpnconnectionprovisioning.v1_0";
    private static final String VPN_CONNECTION_PROFILE_CLASS_NAME = "vpnconnectionprofile.v1_0";

    private static final String VPN_CONNECTION_QUERY_REQUEST_XML = "/message/releases/edu/emory/Network/VpnConnection/1.0/xml/Query-Request.xml";
    private static final String VPN_CONNECTION_GENERATE_REQUEST_XML = "/message/releases/edu/emory/Network/VpnConnection/1.0/xml/Generate-Request.xml";
    private static final String VPN_CONNECTION_DELETE_REQUEST_XML = "/message/releases/edu/emory/Network/VpnConnection/1.0/xml/Delete-Request.xml";
    private static final String STATIC_NAT_PROVISIONING_CREATE_REQUEST_XML = "/message/releases/edu/emory/Network/StaticNat/1.0/xml/Create-Request.xml";
    private static final String STATIC_NAT_PROVISIONING_QUERY_REQUEST_XML = "/message/releases/edu/emory/Network/StaticNat/1.0/xml/Query-Request.xml";
    private static final String SAVE_CONFIG_UPDATE_REQUEST_XML = "/message/releases/edu/emory/Network/ElasticIpAssignment/1.0/xml/Update-Request.xml";
    private final String testDirectory = "/deploy/build-test";


    private Logger logger = new Logger();

    private String tunnelNumber = "1";

    private String getLogtag() {
        return "[CiscoAsr" + tunnelNumber + "][" + this.getClass().getSimpleName() + "] ";
    }

    @Test
    public void testStaticNatCommandCreate() {

        AppConfig appConfig = createAppConfigFromConfigObjects("StaticNatRequestCommand");

        try {
            CommandConfig commandConfig = new CommandConfig();
            commandConfig.setAppConfig(appConfig);

            CaStaticNatRequestCommand command = new CaStaticNatRequestCommand(commandConfig);

            String cwd = System.getProperty("user.dir");
            FileSystem fileSystem = FileSystems.getDefault();
            Path createRequestDoc = fileSystem.getPath(cwd + STATIC_NAT_PROVISIONING_CREATE_REQUEST_XML);
            byte[] bytes = Files.readAllBytes(createRequestDoc);
            String requestDocAsString = new String(bytes);
            TextMessage message = new MyTextMessage();
            message.setText(requestDocAsString);

            Message response = command.execute(1, message);

            assert response != null;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    @Test
    public void testStaticNatCommandQuery() {

        AppConfig appConfig = createAppConfigFromConfigObjects("StaticNatRequestCommand");

        try {
            CommandConfig commandConfig = new CommandConfig();
            commandConfig.setAppConfig(appConfig);
            CaStaticNatRequestCommand command;

//            Class CaStaticNatRequestCommandClass = CaStaticNatRequestCommand.class;
//
//            Constructor<CaStaticNatRequestCommand> c = CaStaticNatRequestCommandClass.getConstructor(CommandConfig.class);
//
//
//            command = c.newInstance(commandConfig);
            command = new CaStaticNatRequestCommand(commandConfig);

            String cwd = System.getProperty("user.dir");
            FileSystem fileSystem = FileSystems.getDefault();
            Path createRequestDoc = fileSystem.getPath(cwd + STATIC_NAT_PROVISIONING_QUERY_REQUEST_XML);
            byte[] bytes = Files.readAllBytes(createRequestDoc);
            String requestDocAsString = new String(bytes);
            TextMessage message = new MyTextMessage();
            message.setText(requestDocAsString);

            Message response = command.execute(1, message);

            assert response != null;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    // VPN_CONNECTION_QUERY_REQUEST_XML
    @Test
    public void testVpnConnectionProvisioningCommandQuery() {

        AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");

        try {
            CommandConfig commandConfig = new CommandConfig();
            commandConfig.setAppConfig(appConfig);

            CaVpnConnectionRequestCommand command = new CaVpnConnectionRequestCommand(commandConfig);

            String cwd = System.getProperty("user.dir");
            FileSystem fileSystem = FileSystems.getDefault();
            Path createRequestDoc = fileSystem.getPath(cwd + VPN_CONNECTION_QUERY_REQUEST_XML);
            byte[] bytes = Files.readAllBytes(createRequestDoc);
            String requestDocAsString = new String(bytes);
            TextMessage message = new MyTextMessage();
            message.setText(requestDocAsString);

            Message response = command.execute(1, message);

            assert response != null;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }


    @Test
    public void testVpnConnectionProvisioningCommandGenerate() {

        AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");

        try {
            CommandConfig commandConfig = new CommandConfig();
            commandConfig.setAppConfig(appConfig);

            CaVpnConnectionRequestCommand command = new CaVpnConnectionRequestCommand(commandConfig);

            String cwd = System.getProperty("user.dir");
            FileSystem fileSystem = FileSystems.getDefault();
            Path createRequestDoc = fileSystem.getPath(cwd + VPN_CONNECTION_GENERATE_REQUEST_XML);
            byte[] bytes = Files.readAllBytes(createRequestDoc);
            String requestDocAsString = new String(bytes);
            TextMessage message = new MyTextMessage();
            message.setText(requestDocAsString);

            Message response = command.execute(1, message);

            assert response != null;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    @Test
    public void testVpnConnectionProvisioningCommandDelete() {

        AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");

        try {
            CommandConfig commandConfig = new CommandConfig();
            commandConfig.setAppConfig(appConfig);

            CaVpnConnectionRequestCommand command = new CaVpnConnectionRequestCommand(commandConfig);

            String cwd = System.getProperty("user.dir");
            FileSystem fileSystem = FileSystems.getDefault();
            Path createRequestDoc = fileSystem.getPath(cwd + VPN_CONNECTION_DELETE_REQUEST_XML);
            byte[] bytes = Files.readAllBytes(createRequestDoc);
            String requestDocAsString = new String(bytes);
            TextMessage message = new MyTextMessage();
            message.setText(requestDocAsString);

            Message response = command.execute(1, message);

            assert response != null;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    private AppConfig createAppConfigFromConfigObjects(String commandName) {
        String deploymentDescriptor = "configs/CiscoAsr1Service.xml";
        XmlUtils xmlUtils = new XmlUtils();

        String cwd = System.getProperty("user.dir");
        System.setProperty("user.dir", cwd + testDirectory);

        List<PropertyConfig> propertyConfigs = xmlUtils.getCommandPropertyConfigList(deploymentDescriptor, commandName);
        List<ProducerConfig> producerConfigs = xmlUtils.getCommandProducerConfigList(deploymentDescriptor, commandName);
        List<MessageObjectConfig> messageObjectConfigs = xmlUtils.getAllMessageObjectConfigs(deploymentDescriptor, commandName);
        List<DbLockConfig> dbLockConfigs = xmlUtils.getAllDbLockConfigs(deploymentDescriptor);
        addTestConfigurationObjects(messageObjectConfigs);


        AppConfigWrapper wrapper = new AppConfigWrapper(propertyConfigs, messageObjectConfigs, dbLockConfigs, producerConfigs);

        return wrapper;
    }

    private void addTestConfigurationObjects(List<MessageObjectConfig> messageObjectConfigs) {
        XmlUtils xmlUtils = new XmlUtils();
        MessageObjectConfig vpnConnectionProvisioningMessageObject = xmlUtils.getMessageObjectConfigFromString(vpnConnectionProvisioningXml);
        messageObjectConfigs.add(vpnConnectionProvisioningMessageObject);
        MessageObjectConfig vpnConnectionProfileMessageObject = xmlUtils.getMessageObjectConfigFromString(vpnConnectionProfileXml);
        messageObjectConfigs.add(vpnConnectionProfileMessageObject);
    }

    private VpnConnectionProvisioning getVpnConnectionProvisioningFromConfiguration(AppConfig appConfig) throws ProviderException {
        VpnConnectionProvisioning vpnConnectionProvisioning;

        try {
            vpnConnectionProvisioning = (VpnConnectionProvisioning)appConfig.getObject(VPN_CONNECTION_PROVISIONING_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection connection provisioning object from app config for class name " + VPN_CONNECTION_PROVISIONING_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return vpnConnectionProvisioning;
    }

    /**
     * Pull VpnConnectionProfile from app config.
     *
     * @param appConfig - Config from which object is pulled.
     * @return - VpnConnectionProfile instance from config.
     * @throws ProviderException
     *
     * @apiNote  - It turns out that a properly hydrated VpnConnectionProvisioning object will synthesize one of these
     *             correctly, however since I got it working, might as well keep it! See xml below.
     */
    private VpnConnectionProfile getVpnConnectionProfileFromConfiguration(AppConfig appConfig) throws ProviderException {
        VpnConnectionProfile vpnConnectionProfile;

        try {
            vpnConnectionProfile = (VpnConnectionProfile)appConfig.getObject(VPN_CONNECTION_PROFILE_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection connection profile object from app config for class name " + VPN_CONNECTION_PROFILE_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return vpnConnectionProfile;
    }

    /**
     * Synthesized VpnConnectionProvisioning MessageObjectConfig xml for test purposes only.
     *
     */
    private String vpnConnectionProvisioningXml = "<MessageObjectConfig name=\"VpnConnectionProvisioning.v1_0\" translationType=\"application\" xmlDocumentValidation=\"false\">\n" +
            "                                        <CommandName>VpnConnectionProvisioning.v1_0</CommandName>\n" +
            "                                        <ConfigClass>org.openeai.config.MessageObjectConfig</ConfigClass>\n" +
            "                                        <ObjectClass>edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProvisioning</ObjectClass>\n" +
            "                                        <EnterpriseObjectDocument\n" +
            "                                                docUri=\"eos/edu/emory/Network/1.0/VpnConnectionProvisioningEO.xml\"\n" +
            "                                                ignoreMissingFields=\"true\" ignoreValidation=\"false\"/>\n" +
            "                                        <LayoutManager>\n" +
            "                                            <InputLayout type=\"xml\">\n" +
            "                                                <ObjectClass>org.openeai.layouts.XmlLayout</ObjectClass>\n" +
            "                                            </InputLayout>\n" +
            "                                            <OutputLayout type=\"xml\">\n" +
            "                                                <ObjectClass>org.openeai.layouts.XmlLayout</ObjectClass>\n" +
            "                                            </OutputLayout>\n" +
            "                                        </LayoutManager>\n" +
            "                                    </MessageObjectConfig>";

    /**
     * Synthesized VpnConnectionProfile MessageObjectConfig xml for test purposes only.
     *
     * @apiNote  - It turns out that a properly hydrated VpnConnectionProvisioning object will synthesize one of these
     *             correctly, however since I got it working, might as well keep it!
     */
    private String vpnConnectionProfileXml = "<MessageObjectConfig name=\"VpnConnectionProfile.v1_0\" translationType=\"application\" xmlDocumentValidation=\"false\">\n" +
            "                                        <CommandName>VpnConnectionProfile.v1_0</CommandName>\n" +
            "                                        <ConfigClass>org.openeai.config.MessageObjectConfig</ConfigClass>\n" +
            "                                        <ObjectClass>edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile</ObjectClass>\n" +
            "                                        <EnterpriseObjectDocument\n" +
            "                                                docUri=\"eos/edu/emory/Network/1.0/VpnConnectionProfileEO.xml\"\n" +
            "                                                ignoreMissingFields=\"true\" ignoreValidation=\"false\"/>\n" +
            "                                        <LayoutManager>\n" +
            "                                            <InputLayout type=\"xml\">\n" +
            "                                                <ObjectClass>org.openeai.layouts.XmlLayout</ObjectClass>\n" +
            "                                            </InputLayout>\n" +
            "                                            <OutputLayout type=\"xml\">\n" +
            "                                                <ObjectClass>org.openeai.layouts.XmlLayout</ObjectClass>\n" +
            "                                            </OutputLayout>\n" +
            "                                        </LayoutManager>\n" +
            "                                    </MessageObjectConfig>";


    private class Logger {
        public void info(String message) {
            System.out.println(message);
        }

        public void error(String message) {
            System.out.println(message);
        }

        public void error(String message, Exception e) {
            System.out.println(message + "\n");
            e.printStackTrace();
        }
    }

    private class MyTextMessage implements TextMessage {

        private String text;

        @Override
        public void setText(String s) throws JMSException {
            this.text = s;
        }

        @Override
        public String getText() throws JMSException {
            return text;
        }

        @Override
        public String getJMSMessageID() throws JMSException {
            return null;
        }

        @Override
        public void setJMSMessageID(String s) throws JMSException {

        }

        @Override
        public long getJMSTimestamp() throws JMSException {
            return 0;
        }

        @Override
        public void setJMSTimestamp(long l) throws JMSException {

        }

        @Override
        public byte[] getJMSCorrelationIDAsBytes() throws JMSException {
            return new byte[0];
        }

        @Override
        public void setJMSCorrelationIDAsBytes(byte[] bytes) throws JMSException {

        }

        @Override
        public void setJMSCorrelationID(String s) throws JMSException {

        }

        @Override
        public String getJMSCorrelationID() throws JMSException {
            return null;
        }

        @Override
        public Destination getJMSReplyTo() throws JMSException {
            return null;
        }

        @Override
        public void setJMSReplyTo(Destination destination) throws JMSException {

        }

        @Override
        public Destination getJMSDestination() throws JMSException {
            return null;
        }

        @Override
        public void setJMSDestination(Destination destination) throws JMSException {

        }

        @Override
        public int getJMSDeliveryMode() throws JMSException {
            return 0;
        }

        @Override
        public void setJMSDeliveryMode(int i) throws JMSException {

        }

        @Override
        public boolean getJMSRedelivered() throws JMSException {
            return false;
        }

        @Override
        public void setJMSRedelivered(boolean b) throws JMSException {

        }

        @Override
        public String getJMSType() throws JMSException {
            return null;
        }

        @Override
        public void setJMSType(String s) throws JMSException {

        }

        @Override
        public long getJMSExpiration() throws JMSException {
            return 0;
        }

        @Override
        public void setJMSExpiration(long l) throws JMSException {

        }

        @Override
        public int getJMSPriority() throws JMSException {
            return 0;
        }

        @Override
        public void setJMSPriority(int i) throws JMSException {

        }

        @Override
        public void clearProperties() throws JMSException {

        }

        @Override
        public boolean propertyExists(String s) throws JMSException {
            return false;
        }

        @Override
        public boolean getBooleanProperty(String s) throws JMSException {
            return false;
        }

        @Override
        public byte getByteProperty(String s) throws JMSException {
            return 0;
        }

        @Override
        public short getShortProperty(String s) throws JMSException {
            return 0;
        }

        @Override
        public int getIntProperty(String s) throws JMSException {
            return 0;
        }

        @Override
        public long getLongProperty(String s) throws JMSException {
            return 0;
        }

        @Override
        public float getFloatProperty(String s) throws JMSException {
            return 0;
        }

        @Override
        public double getDoubleProperty(String s) throws JMSException {
            return 0;
        }

        @Override
        public String getStringProperty(String s) throws JMSException {
            return null;
        }

        @Override
        public Object getObjectProperty(String s) throws JMSException {
            return null;
        }

        @Override
        public Enumeration getPropertyNames() throws JMSException {
            return null;
        }

        @Override
        public void setBooleanProperty(String s, boolean b) throws JMSException {

        }

        @Override
        public void setByteProperty(String s, byte b) throws JMSException {

        }

        @Override
        public void setShortProperty(String s, short i) throws JMSException {

        }

        @Override
        public void setIntProperty(String s, int i) throws JMSException {

        }

        @Override
        public void setLongProperty(String s, long l) throws JMSException {

        }

        @Override
        public void setFloatProperty(String s, float v) throws JMSException {

        }

        @Override
        public void setDoubleProperty(String s, double v) throws JMSException {

        }

        @Override
        public void setStringProperty(String s, String s1) throws JMSException {

        }

        @Override
        public void setObjectProperty(String s, Object o) throws JMSException {

        }

        @Override
        public void acknowledge() throws JMSException {

        }

        @Override
        public void clearBody() throws JMSException {

        }
    }


}
