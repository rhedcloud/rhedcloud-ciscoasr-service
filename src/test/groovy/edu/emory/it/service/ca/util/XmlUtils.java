package edu.emory.it.service.ca.util;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.filter.ElementFilter;
import org.jdom.input.SAXBuilder;
import org.openeai.config.*;
import org.openeai.utils.lock.DbLockConfig;
import org.openeai.utils.sequence.DbSequenceConfig;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class XmlUtils {
    /**
     * Get all properties under parent element PropertyConfig, of the form:
     *
     *    <PropertyConfig name="propertyName" refresh="false">
     *
     * Child properties use the following sub-document structure:
     *
     *    <Property>
     *        <PropertyName>name</PropertyName>
     *        <PropertyValue>value</PropertyValue>
     *    </Property>
     *
     * @param xmlFile - path to xml properties file
     * @param propertyName - parent element name
     * @return
     */
    public Properties getProperties(final String xmlFile, final String propertyName) {

        Properties properties;

        try {
            properties = getProps(xmlFile, propertyName);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to get properties " + propertyName + " for file " + xmlFile, e);
        }

        return properties;
    }


    /**
     * Get all properties under all PropertyConfig parent elements, of the form:
     *
     *    <PropertyConfig ...>
     *
     * Child properties use the following sub-document structure:
     *
     *    <Property>
     *        <PropertyName>name</PropertyName>
     *        <PropertyValue>value</PropertyValue>
     *    </Property>
     *
     * @param xmlFile - path to xml properties file
     * @return
     */
    public Properties getAllProperties(final String xmlFile) {

        Properties properties;

        try {
            properties = getProps(xmlFile);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to get all properties for file " + xmlFile, e);
        }

        return properties;
    }


    public List<PropertyConfig> getCommandPropertyConfigList(final String xmlFile, String commandName) {
        List<PropertyConfig> propertyConfigs;

        try {
            propertyConfigs = getPropertyConfigList(xmlFile, commandName);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get property configs for file " + xmlFile + " for command name " + commandName, e);
        }
        return propertyConfigs;
    }

    public List<ProducerConfig> getCommandProducerConfigList(final String xmlFile, String commandName) {
        List<ProducerConfig> producerConfigs;

        try {
            producerConfigs = getProducerConfigList(xmlFile, commandName);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get producer configs for file " + xmlFile + " for command name " + commandName, e);
        }
        return producerConfigs;
    }

    public List<Element> getMessageObjectConfigsAsElement(final String xmlFile) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<MessageObjectConfig> propertyConfigs = new ArrayList<>();
        final List<Element> messageObjectConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();
        parseMessageObjectConfigElements(messageObjectConfigElements, rootElement);
        return messageObjectConfigElements;
    }

    public List<MessageObjectConfig> getAllMessageObjectConfigs(final String xmlFile, String commandName) {
        List<MessageObjectConfig> messageObjectConfigs;

        try {
            messageObjectConfigs = getMessageObjectConfigs(xmlFile, commandName);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get message object configs for file " + xmlFile + " for command name " + commandName, e);
        }
        return messageObjectConfigs;
    }

    public List<ProducerConfig> getAllProducerConfigs(final String xmlFile, String commandName) {
        List<ProducerConfig> producerConfigs;

        try {
            producerConfigs = getProducerConfigList(xmlFile, commandName);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get producer configs for file " + xmlFile + " for command name " + commandName, e);
        }
        return producerConfigs;
    }

    public List<ThreadPoolConfig> getAllThreadPoolConfigs(final String xmlFile) {
        List<ThreadPoolConfig> threadPoolConfigs;

        try {
            threadPoolConfigs = getThreadPoolConfigs(xmlFile);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get thread pool configs for file " + xmlFile, e);
        }
        return threadPoolConfigs;
    }

    public List<DbLockConfig> getAllDbLockConfigs(final String xmlFile) {
        List<DbLockConfig> dbLockConfigs;

        try {
            dbLockConfigs = getDbLockConfigs(xmlFile);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get db lock configs for file " + xmlFile, e);
        }
        return dbLockConfigs;
    }

    public List<DbSequenceConfig> getAllDbSequenceConfigs(final String xmlFile) {
        List<DbSequenceConfig> dbSequenceConfigs;

        try {
            dbSequenceConfigs = getDbSequenceConfigs(xmlFile);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get db sequence configs for file " + xmlFile, e);
        }
        return dbSequenceConfigs;
    }

    public MessageObjectConfig getMessageObjectConfigFromString(final String xml) {

        SAXBuilder builder = new SAXBuilder();
        final List<MessageObjectConfig> propertyConfigs = new ArrayList<>();
        final List<Element> messageObjectConfigElements = new ArrayList<>();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());

        Document document = null;
        try {
            document = builder.build(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("Failed to parse xml string: " + xml);
        }
        Element rootElement = document.getRootElement();
        parseMessageObjectConfigElements(messageObjectConfigElements, rootElement);
        final MessageObjectConfig messageObjectConfig;
        try {
            messageObjectConfig = new MessageObjectConfig(rootElement);
            propertyConfigs.add(messageObjectConfig);
        } catch (EnterpriseConfigurationObjectException e) {
            throw new RuntimeException("Failed to create PropertyConfig for element " + rootElement.getName(), e);
        }
        return messageObjectConfig;
    }

    @SuppressWarnings("unchecked")
    public Element getApplicationElement(final String xmlFile) {
        Element element = null;

        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(xmlFile);
            Element rootNode = document.getRootElement();
            List<Element> list = (List<Element>)rootNode.getChildren("Application");

            element = list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to get document element", e);
        }

        return element;
    }

    @SuppressWarnings("unchecked")
    public Element getPrimedDocElement(final String xmlFile) {
        Element element = null;

        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(xmlFile);
            element = document.getRootElement();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to get document element", e);
        }

        return element;
    }

    private Properties getProps(final String xmlFile, final String propertyName) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        final Properties properties = new Properties();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();
        parse(propertyName, properties, rootElement);

        return properties;
    }

    private Properties getProps(final String xmlFile) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        final Properties properties = new Properties();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();
        parse(null, properties, rootElement);

        return properties;
    }

    private List<PropertyConfig> getPropertyConfigList(final String xmlFile, String commandName) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<PropertyConfig> propertyConfigs = new ArrayList<>();
        final List<Element> propertyConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();

        Iterator iterator = rootElement.getDescendants(new ElementFilter("CommandName"));
        Element commandConfig = null;
        while (iterator.hasNext()) {
            Element e = (Element)iterator.next();
            if (e.getText().equalsIgnoreCase(commandName)) {
                System.out.println("Name: " + commandName);
                commandConfig = e.getParentElement();
                break;
            }
        }
        if (commandConfig == null) {
            throw new RuntimeException("Command config not found for command name: " + commandName);
        }


        parsePropertyConfigElements(propertyConfigElements, commandConfig);
        propertyConfigElements.forEach(element -> {
            try {
                PropertyConfig propertyConfig = new PropertyConfig(element);
                propertyConfigs.add(propertyConfig);
            } catch (EnterpriseConfigurationObjectException e) {
                throw new RuntimeException("Failed to create PropertyConfig for element " + element.getName(), e);
            }
        });
        return propertyConfigs;
    }

    private List<MessageObjectConfig> getMessageObjectConfigs(final String xmlFile, String commandName) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<MessageObjectConfig> propertyConfigs = new ArrayList<>();
        final List<Element> messageObjectConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();

        Iterator iterator = rootElement.getDescendants(new ElementFilter("CommandName"));
        Element commandConfig = null;
        while (iterator.hasNext()) {
            Element e = (Element)iterator.next();
            if (e.getText().equalsIgnoreCase(commandName)) {
                System.out.println("Name: " + commandName);
                commandConfig = e.getParentElement();
                break;
            }
        }
        if (commandConfig == null) {
            throw new RuntimeException("Command config not found for command name: " + commandName);
        }


        parseMessageObjectConfigElements(messageObjectConfigElements, rootElement);
        messageObjectConfigElements.forEach(element -> {
            try {
                final MessageObjectConfig messageObjectConfig = new MessageObjectConfig(element);
                Attribute nameAttribute = element.getAttribute("name");
                messageObjectConfig.setName(nameAttribute.getValue().toLowerCase());
                propertyConfigs.add(messageObjectConfig);
            } catch (EnterpriseConfigurationObjectException e) {
                throw new RuntimeException("Failed to create MessageObjectConfig for element " + element.getName(), e);
            }
        });
        return propertyConfigs;
    }

    private List<ProducerConfig> getProducerConfigList(final String xmlFile, String commandName) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<ProducerConfig> producerConfigs = new ArrayList<>();
        final List<Element> producerConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();

        Iterator iterator = rootElement.getDescendants(new ElementFilter("CommandName"));
        Element commandConfig = null;
        while (iterator.hasNext()) {
            Element e = (Element)iterator.next();
            if (e.getText().equalsIgnoreCase(commandName)) {
                System.out.println("Name: " + commandName);
                commandConfig = e.getParentElement();
                break;
            }
        }
        if (commandConfig == null) {
            throw new RuntimeException("Command config not found for command name: " + commandName);
        }

        String connectionFactoryName = null;
        String initialContextFactory = null;
        String providerUrl = null;
        String producerIdURL = null;
        String securityPrincipal = null;
        String securityCredentials = null;
        String startOnInitialization = null;
        iterator = commandConfig.getDescendants(new ElementFilter("ProducerConfigs"));
        while (iterator.hasNext()) {
            Element e = (Element)iterator.next();
            Element child = e.getChild("ConnectionFactoryName");
            connectionFactoryName = child.getText();
            child = e.getChild("InitialContextFactory");
            initialContextFactory = child.getText();
            child = e.getChild("ProviderURL");
            providerUrl = child.getText();
            child = e.getChild("ProducerIdURL");
            producerIdURL = child.getText();
            child = e.getChild("SecurityPrincipal");
            securityPrincipal = child.getText();
            child = e.getChild("SecurityCredentials");
            securityCredentials = child.getText();
            Attribute startOnInitAttr = e.getAttribute("startOnInitialization");
            if (startOnInitAttr != null) {
                startOnInitialization = startOnInitAttr.getValue();
            }
        }


        parseProducerConfigElements(producerConfigElements, commandConfig);
        for (Element element: producerConfigElements) {
            try {
                Attribute startOnInitializeAttr = new Attribute("startOnInitialization", startOnInitialization);
                element.getAttributes().add(startOnInitializeAttr);
                final ProducerConfig producerConfig = new ProducerConfig(element);
//                startOnInitializeAttr = element.getAttribute("startOnInitialization");
//                if (startOnInitializeAttr != null) {
//                    String startOnInitialize = startOnInitializeAttr.getValue();
//                    producerConfig.getProperties().setProperty("startOnInitialization", startOnInitialize);
//                } else {
//                    producerConfig.getProperties().setProperty("startOnInitialization", startOnInitialization);
//                }
                producerConfig.setName(producerConfig.getProperties().getProperty("name"));
                producerConfig.setAppName("edu.emory.NetworkOpsService");
                producerConfig.addProperty("ConnectionFactoryName", connectionFactoryName);
                producerConfig.addProperty("InitialContextFactory", initialContextFactory);
                producerConfig.addProperty("ProviderURL", providerUrl);
                producerConfig.addProperty("ProducerIdURL", producerIdURL);
                producerConfig.addProperty("SecurityPrincipal", securityPrincipal);
                producerConfig.addProperty("SecurityCredentials", securityCredentials);
                producerConfigs.add(producerConfig);
            } catch (EnterpriseConfigurationObjectException e) {
                throw new RuntimeException("Failed to create ProducerConfig for element " + element.getName(), e);
            }
        };
        return producerConfigs;
    }

    private List<ThreadPoolConfig> getThreadPoolConfigs(final String xmlFile) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<ThreadPoolConfig> threadPoolConfigs = new ArrayList<>();
        final List<Element> threadPoolConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();
        parseThreadPoolConfigElements(threadPoolConfigElements, rootElement);
        threadPoolConfigElements.forEach(element -> {
            try {
                final ThreadPoolConfig threadPoolConfig = new ThreadPoolConfig(element);
                threadPoolConfigs.add(threadPoolConfig);
            } catch (EnterpriseConfigurationObjectException e) {
                throw new RuntimeException("Failed to create ThreadPoolConfig for element " + element.getName(), e);
            }
        });
        return threadPoolConfigs;
    }

    private List<DbLockConfig> getDbLockConfigs(final String xmlFile) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<DbLockConfig> dbLockConfigs = new ArrayList<>();
        final List<Element> dbLockConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();
        parseDbLockConfigElements(dbLockConfigElements, rootElement);
        dbLockConfigElements.forEach(element -> {
            try {
                final DbLockConfig dbLockConfig = new DbLockConfig(element);
                dbLockConfigs.add(dbLockConfig);
            } catch (EnterpriseConfigurationObjectException e) {
                throw new RuntimeException("Failed to create DbLockConfig for element " + element.getName(), e);
            }
        });
        return dbLockConfigs;
    }

    private List<DbSequenceConfig> getDbSequenceConfigs(final String xmlFile) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        final List<DbSequenceConfig> dbSequenceConfigs = new ArrayList<>();
        final List<Element> dbSequenceConfigElements = new ArrayList<>();
        Document document = builder.build(xmlFile);
        Element rootElement = document.getRootElement();
        parseDbSequenceConfigElements(dbSequenceConfigElements, rootElement);
        dbSequenceConfigElements.forEach(element -> {
            try {
                final DbSequenceConfig dbSequenceConfig = new DbSequenceConfig(element);
                dbSequenceConfigs.add(dbSequenceConfig);
            } catch (EnterpriseConfigurationObjectException e) {
                throw new RuntimeException("Failed to create DbSequenceConfig for element " + element.getName(), e);
            }
        });
        return dbSequenceConfigs;
    }

    @SuppressWarnings("unchecked")
    private void parse(final String propertyName, final Properties properties, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("PropertyConfig")) {
                Attribute name = child.getAttribute("name");
                if (propertyName == null || name.getValue().equals(propertyName)) {
                    extractProperties(child, properties);
                }
            }
            parse(propertyName, properties, child);
        }
    }

    @SuppressWarnings("unchecked")
     private void parsePropertyConfigElements(final List<Element> propertyConfigElements, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("PropertyConfig")) {
                Attribute name = child.getAttribute("name");
                propertyConfigElements.add(child);
            }
            parsePropertyConfigElements(propertyConfigElements, child);
        }
    }

    @SuppressWarnings("unchecked")
    private void parseMessageObjectConfigElements(final List<Element> messageObjectConfigElements, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("MessageObjectConfig")) {
                Attribute name = child.getAttribute("name");
                messageObjectConfigElements.add(child);
            }
            parseMessageObjectConfigElements(messageObjectConfigElements, child);
        }
    }

    @SuppressWarnings("unchecked")
    private void parseProducerConfigElements(final List<Element> producerConfigElements, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("ProducerConfig")) {
                Attribute name = child.getAttribute("name");
                producerConfigElements.add(child);
            }
            parseProducerConfigElements(producerConfigElements, child);
        }
    }

    @SuppressWarnings("unchecked")
    private void parseThreadPoolConfigElements(final List<Element> threadPoolConfigElements, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("ThreadPoolConfig")) {
                Attribute name = child.getAttribute("name");
                threadPoolConfigElements.add(child);
            }
            parseThreadPoolConfigElements(threadPoolConfigElements, child);
        }
    }

    @SuppressWarnings("unchecked")
    private void parseDbLockConfigElements(final List<Element> dbLockConfigEelements, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("DbLockConfig")) {
                Attribute name = child.getAttribute("name");
                dbLockConfigEelements.add(child);
            }
            parseDbLockConfigElements(dbLockConfigEelements, child);
        }
    }

    @SuppressWarnings("unchecked")
    private void parseDbSequenceConfigElements(final List<Element> dbSequenceConfigElements, final Element e) {
        final List<Element> children = (List<Element>)e.getChildren();
        for (int n = 0; n < children.size(); n++) {
            final Element child = children.get(n);
            String nodeName = child.getName();
            if (nodeName.equals("DbSequenceConfig")) {
                Attribute name = child.getAttribute("name");
                dbSequenceConfigElements.add(child);
            }
            parseDbSequenceConfigElements(dbSequenceConfigElements, child);
        }
    }

    @SuppressWarnings("unchecked")
    private void extractProperties(Element element, Properties properties) {
        List<Element> propertyChildren = (List<Element>)element.getChildren("Property");

        for (int n = 0; n < propertyChildren.size(); n++) {
            Element childElement = propertyChildren.get(n);

            List<Element> propertyNameChildren = childElement.getChildren("PropertyName");
            Element propertyElement = propertyNameChildren.get(0);
            String nameValue = propertyElement.getText();
            propertyNameChildren = childElement.getChildren("PropertyValue");
            propertyElement = propertyNameChildren.get(0);
            String valueValue = propertyElement.getText();

            properties.put(nameValue, valueValue);
        }
    }


}
