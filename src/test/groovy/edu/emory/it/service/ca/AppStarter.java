package edu.emory.it.service.ca;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class AppStarter extends Starter {

    public static void main(String[] args) {

        AppStarter appStarter = new AppStarter();

        appStarter.doIt();
    }

    public void doIt() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        try {
            //getServiceNumber();
            setServiceNumber(1);

            getEnvironment(LOCAL);

            initializeApp();

            startApp();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
