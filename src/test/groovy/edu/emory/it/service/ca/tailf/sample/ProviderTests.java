package edu.emory.it.service.ca.tailf.sample;

import com.tailf.jnc.Element;
import com.tailf.jnc.NetconfSession;
import edu.emory.it.service.ca.provider.CiscoAsr1002NetconfStaticNatProvider;
import edu.emory.it.service.ca.provider.CiscoAsr1002NetconfVpnConnectionProvider;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.StaticNatProvider;
import edu.emory.it.service.ca.provider.util.AppConfigUtil;
import edu.emory.it.service.ca.provider.util.IPV4Address;
import edu.emory.it.service.ca.provider.util.StackTraceAnalyzer;
import edu.emory.it.service.ca.provider.validator.VpnConnectionOperationContext;
import edu.emory.it.service.ca.tailf.sample.log.Logger;
import edu.emory.it.service.ca.tailf.sample.log.VpnConnectionDump;
import edu.emory.it.service.ca.tailf.sample.log.VpnConnectionRequisitionDump;
import edu.emory.it.service.ca.util.SpreadSheetWrapper;
import edu.emory.it.service.ca.util.XmlUtils;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnectionProfile;
import edu.emory.moa.objects.resources.v1_0.BgpPrefixes;
import edu.emory.moa.objects.resources.v1_0.BgpState;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoIpsecTransformSet;
import edu.emory.moa.objects.resources.v1_0.CryptoIsakmpProfile;
import edu.emory.moa.objects.resources.v1_0.CryptoKeyring;
import edu.emory.moa.objects.resources.v1_0.LocalAddress;
import edu.emory.moa.objects.resources.v1_0.MatchIdentity;
import edu.emory.moa.objects.resources.v1_0.RemoteVpnConnectionInfo;
import edu.emory.moa.objects.resources.v1_0.RemoteVpnTunnel;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.TunnelInterface;
import edu.emory.moa.objects.resources.v1_0.TunnelProfile;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.VpnConnectionRequisition;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ProviderTests extends BaseTools {


    private Logger logger;

    private static final String VPN_CONNECTION_CLASS_NAME = "vpnconnection.v1_0";
    private static final String VPN_CONNECTION_REQUISITION_CLASS_NAME = "vpnconnectionrequisition.v1_0";
    private static final String VPN_CONNECTION_QUERY_SPECIFICATION_CLASS_NAME = "vpnconnectionqueryspecification.v1_0";
    private static final String VPN_CONNECTION_PROFILE_CLASS_NAME = "vpnconnectionprofile.v1_0";
    private static final String REMOTE_VPN_CONNECTION_INFO_CLASS_NAME = "remotevpnconnectioninfo.v1_0";
    private static final String REMOTE_VPN_TUNNEL_CLASS_NAME = "remotevpntunnel.v1_0";

    private static final String STATIC_NAT_CLASS_NAME = "staticnat.v1_0";
    private static final String STATIC_NAT_QUERY_SPECIFICATION_CLASS_NAME = "staticnatqueryspecification.v1_0";

    private final String spreadsheet = "/doc/routers/aws-ip-allocations_v3.csv";

    private SpreadSheetWrapper spreadSheetWrapper;

    Tools tools;

    @Before
    public void init() {
        logger = new Logger();
        tools = new Tools();
        spreadSheetWrapper = new SpreadSheetWrapper();

//        TestInitialContextFactoryBuilder.setInitialContextFactoryBuilder(new File(jndiFile));

    }

    @Override
    protected String getLogtag() { return "[" + this.getClass().getSimpleName() + "] "; }
    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ######################################## StaticNat tests #################################################### */
    /** ############################################################################################################# */
    /** ############################################################################################################# */

    String routerNumber = "1";

    @Test
    public void queryStaticNat() {
        StaticNatProvider provider = new CiscoAsr1002NetconfStaticNatProvider();

        boolean wildcardQuery = false;

        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("StaticNatRequestCommand", routerNumber);

            provider.init(appConfig);

            // This essentially mimics the Command object's use of the StaticNat MOA
            // Received from ESB - we simulate this...
            StaticNatQuerySpecification staticNatQuerySpec = AppConfigUtil.getStaticNatQuerySpecificationFromConfiguration(appConfig);

            String publicIp = "170.140.201.201";
            if (wildcardQuery) {
                staticNatQuerySpec.setPublicIp("");
            } else {
                staticNatQuerySpec.setPublicIp(publicIp);
            }

            ((CiscoAsr1002NetconfStaticNatProvider) provider).setRouterExecutionTimeInMilliseconds(0L);
            List<StaticNat> staticNats = provider.query(staticNatQuerySpec);

            if (wildcardQuery) {
                System.out.println(staticNats.isEmpty() ? "No static nats found for wildcard query" : staticNats.size() + " static nats found for wildcard query:\n##########################################################################");
                staticNats.forEach(staticNat -> {
                    System.out.println("##### Global address: " + staticNat.getPublicIp());
                    System.out.println("##### Local address:  " + staticNat.getPrivateIp());
                    System.out.println("##########################################################################");
                });
            } else {
                if (staticNats != null && staticNats.size() != 0 && staticNats.get(0).getPrivateIp() != null) {
                    StaticNat staticNat = staticNats.get(0);
                    System.out.println("############################# StaticNat found ############################");
                    System.out.println("##### Global address: " + staticNat.getPublicIp());
                    System.out.println("##### Local address:  " + staticNat.getPrivateIp());
                    System.out.println("##########################################################################");
                } else {
                    System.out.println("!!!!!!!!!!! No Static Nat configured for public ip " + publicIp);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Test
    public void addStaticNat() {
        StaticNatProvider provider = new CiscoAsr1002NetconfStaticNatProvider();

        /*
        <PublicIp>10.65.128.0</PublicIp>
        <PrivateIp>170.140.248.0</PrivateIp>
         */
        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("StaticNatRequestCommand", routerNumber);

            ((CiscoAsr1002NetconfStaticNatProvider) provider).setRouterExecutionTimeInMilliseconds(0);
            provider.init(appConfig);

            // This essentially mimics the Command object's use of the StaticNat MOA
            // Received from ESB - we simulate this...

            StaticNat staticNat = AppConfigUtil.getStaticNatFromConfiguration(appConfig);
            staticNat.setPublicIp("170.140.249.202");
            staticNat.setPrivateIp("10.22.128.101");

            // Now, command object will pass in StaticNat MOA
            provider.add(staticNat);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void deleteStaticNat() {
        StaticNatProvider provider = new CiscoAsr1002NetconfStaticNatProvider();

        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("StaticNatRequestCommand", routerNumber);

            ((CiscoAsr1002NetconfStaticNatProvider) provider).setRouterExecutionTimeInMilliseconds(0);
            provider.init(appConfig);

            // This essentially mimics the Command object's use of the StaticNat MOA
            // Received from ESB - we simulate this...

            /*
            <nat-static-transport-list>
              <local-ip>170.140.248.0</local-ip>
              <global-ip>10.65.128.0</global-ip>
              <vrf>NAT</vrf>
            </nat-static-transport-list>
             */
            StaticNat staticNat = AppConfigUtil.getStaticNatFromConfiguration(appConfig);
//            staticNat.setPublicIp("170.140.249.201");
//            staticNat.setPrivateIp("10.22.128.100");
            staticNat.setPublicIp("170.140.201.201");
            staticNat.setPrivateIp("10.22.128.77");

            // Now, command object will pass in StaticNat MOA
            provider.delete(staticNat);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ###################################### VpnProvisioning tests ################################################ */
    /** ############################################################################################################# */
    /** ############################################################################################################# */


    // Router 1 YANG 1693
    // AWS
    //         vpcId                            profileId
    //         vpc-04b5f86e39dbb4c38            5
    // GCP
    //         vpcId                            profileId

    // Router 2 YANG 1662
    // AWS
    //         vpcId                            profileId
    // GCP
    //         vpcId                            profileId

    String vpcId = "vpc-testnum50";
    Integer profileId = 50;

    @Test
    public void queryVpnProvisioning() {
        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();


        boolean wildcardQuery = false;

        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand", routerNumber);

            provider.init(appConfig);
            provider.setRouterExecutionTimeInMilliseconds(0);

            // Just to generate some logging.
            buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    routerNumber,
                    VpnConnectionOperationContext.QUERY_REQUEST,
                    false
            );

            VpnConnectionQuerySpecification vpnConnectionQuerySpecification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
            if (!wildcardQuery) {
                vpnConnectionQuerySpecification.setVpcId(vpcId);
                vpnConnectionQuerySpecification.setVpnId(profileId.toString());
            }

            List<VpnConnection> vpnConnections = provider.query(vpnConnectionQuerySpecification);

            if (wildcardQuery) {
                if (vpnConnections != null && !vpnConnections.isEmpty()) {
                    logger.info(vpnConnections.size() + " vpn connection found for wildcard query");
                } else {
                    logger.info("No vpn connections found for wildcard query");
                }
            } else if (vpnConnections != null && !vpnConnections.isEmpty()) {

                VpnConnection vpnConnection = vpnConnections.get(0);

                if (StringUtils.isEmpty(vpcId)) {
                    vpcId = vpnConnection.getVpcId();
                }
                logger.info("Vpn connection found for vpcId: " + vpcId);
                new VpnConnectionDump(vpnConnection).dump();
            } else {
                if (StringUtils.isEmpty(vpcId)) {
                    logger.info("No vpn connection found for profile id: " + profileId);
                } else {
                    logger.info("No vpn connection for vpcId: " + vpcId);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void generateVpnProvisioning() {

        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

        provider.setRouterExecutionTimeInMilliseconds(0L);

        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand", routerNumber);
            provider.init(appConfig);

            Properties properties = appConfig.getProperties("GeneralProperties");

            Map<String, String> addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    routerNumber,
                    VpnConnectionOperationContext.GENERATE_SEND,
                    false
                    );

            VpnConnectionRequisition vpnConnectionRequisition = buildVpnConnectionRequisitionObject(appConfig, addDeleteParams);


            logger.info("Vpn Connection Requisition ADD with:\n\n");

            new VpnConnectionRequisitionDump(vpnConnectionRequisition).dump();

            VpnConnection vpnConnection = provider.generate(vpnConnectionRequisition);

            new VpnConnectionDump(vpnConnection).dump();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void deleteVpnProvisioning() {
        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();
        AppConfig appConfig;

        provider.setRouterExecutionTimeInMilliseconds(0L);

        try {
            appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand", routerNumber);
            provider.init(appConfig);

        } catch (ProviderException e) {
            String errMsg = "Failed to create app config/init provider";
            logger.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }

        try {
            Map<String, String> addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    routerNumber,
                    VpnConnectionOperationContext.DELETE_REQUEST,
                    false
            );

            VpnConnection vpnConnection = buildVpnConnectionObject(appConfig, addDeleteParams, provider);

             new VpnConnectionDump(vpnConnection).dump();

            provider.delete(vpnConnection);
        } catch (Exception e) {
            logger.error("Failed delete", e);
        }

    }

    @Test
    public void queryVpnConnectionsWithWildcard() {
        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();
        AppConfig appConfig;

        provider.setRouterExecutionTimeInMilliseconds(0L);

        try {
            appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand", routerNumber);
            provider.init(appConfig);

        } catch (ProviderException e) {
            String errMsg = "Failed to create app config/init provider";
            logger.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }

        List<VpnConnection> vpnConnections;
        try {
            VpnConnectionQuerySpecification vpnConnectionQuerySpecification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
            vpnConnections = provider.query(vpnConnectionQuerySpecification);
        } catch (Exception e) {
            String errMsg = "Failed to query by wildcard";
            logger.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }

        if (vpnConnections == null || vpnConnections.size() == 0) {
            logger.info("No vpn connections returned.");
        } else {
            logger.info(vpnConnections.size() + " vpn connection(s) returned.");

            vpnConnections.forEach(vc -> {
                logger.info("===========================================================================================");
                new VpnConnectionDump(vc).dump();
            });
        }
        logger.info("Made it");
    }

    @Test
    public void generateVpnProvisioningForDifferentCsps() {

        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

        provider.setRouterExecutionTimeInMilliseconds(0L);

        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand", routerNumber);
            provider.init(appConfig);

            Map<String, String> addDeleteParams1 = buildAddDeleteParamsFromSpreadsheet(
                    "vpc-12345678",
                    130,
                    routerNumber,
                    VpnConnectionOperationContext.GENERATE_SEND,
                    false
            );

            VpnConnectionRequisition vpnConnectionRequisition1 = buildVpnConnectionRequisitionObject(appConfig, addDeleteParams1);

            logger.info("Vpn Connection Requisition ADD on Router1 with:\n\n");

            new VpnConnectionRequisitionDump(vpnConnectionRequisition1).dump();

            VpnConnection vpnConnection1 = provider.generate(vpnConnectionRequisition1);

            new VpnConnectionDump(vpnConnection1).dump();

            Map<String, String> addDeleteParams2 = buildAddDeleteParamsFromSpreadsheet(
                    "gcp:vpc-87654321",
                    131,
                    routerNumber,
                    VpnConnectionOperationContext.GENERATE_SEND,
                    false
            );

            VpnConnectionRequisition vpnConnectionRequisition2 = buildVpnConnectionRequisitionObject(appConfig, addDeleteParams2);

            logger.info("Vpn Connection Requisition ADD on Router 2 with:\n\n");

            new VpnConnectionRequisitionDump(vpnConnectionRequisition2).dump();

            VpnConnection vpnConnection2 = provider.generate(vpnConnectionRequisition2);

            new VpnConnectionDump(vpnConnection2).dump();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void queryVpnProvisioningForTwoCsps() {
        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

        String localVpcId = "vpc-0f2d21a671cffb9b4";
        Integer localProfileId = 6;

        try {
            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand", routerNumber);

            VpnConnectionQuerySpecification vpnConnectionQuerySpecification;
            List<VpnConnection> vpnConnections;

            provider.init(appConfig);
            provider.setRouterExecutionTimeInMilliseconds(0);

            // Just to generate some logging.
            buildAddDeleteParamsFromSpreadsheet(
                    localVpcId,
                    localProfileId,
                    routerNumber,
                    VpnConnectionOperationContext.QUERY_REQUEST,
                    false
            );

            vpnConnectionQuerySpecification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
            vpnConnectionQuerySpecification.setVpcId(localVpcId);
            vpnConnectionQuerySpecification.setVpnId(localProfileId.toString());

            vpnConnections = provider.query(vpnConnectionQuerySpecification);

            if (vpnConnections != null && !vpnConnections.isEmpty()) {

                VpnConnection vpnConnection = vpnConnections.get(0);

                logger.info("Vpn connection found for vpcId: " + localVpcId + " - profileId:" + localProfileId);
                new VpnConnectionDump(vpnConnection).dump();
            } else {
                logger.info("No vpn connection for vpcId: " + localVpcId + " - profileId:" + localProfileId);
            }

            localVpcId = "gcp:vpc-87654321";
            localProfileId = 131;
            // Just to generate some logging.
            buildAddDeleteParamsFromSpreadsheet(
                    localVpcId,
                    localProfileId,
                    routerNumber,
                    VpnConnectionOperationContext.QUERY_REQUEST,
                    false
            );

            vpnConnectionQuerySpecification = AppConfigUtil.getVpnConnectionQuerySpecificationFromConfiguration(appConfig);
            vpnConnectionQuerySpecification.setVpcId(localVpcId);
            vpnConnectionQuerySpecification.setVpnId(localProfileId.toString());

            vpnConnections = provider.query(vpnConnectionQuerySpecification);

            if (vpnConnections != null && !vpnConnections.isEmpty()) {

                VpnConnection vpnConnection = vpnConnections.get(0);

                logger.info("Vpn connection found for vpcId: " + localVpcId + " - profileId:" + localProfileId);
                new VpnConnectionDump(vpnConnection).dump();
            } else {
                logger.info("No vpn connection for vpcId: " + localVpcId + " - profileId:" + localProfileId);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void generateTestDocs() {

        try {
            CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
            provider.init(appConfig);

            String tunnelNumberAsString = "1";
            String vpcId = "vpc-testnum25";
            Integer profileId = 25;

            Map<String, XmlEnterpriseObject> testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

            logger.info("################# VpnConnectionRequisition ##################\n" + testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
            logger.info("############# VpnConnection generate/query response ###############\n" + testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
            logger.info("################ VpnConnection delete request ###############\n" + testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testGetCapabilities() {
        NetconfSession session = null;
        final String rpcGetWithXpathTemplate = "<nc:rpc xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\" nc:message-id=\"${messageId}\">\n" +
                "                <nc:get>\n" +
                "                    <nc:filter nc:type=\"xpath\" nc:select=\"${xpathFilter}\"/>\n" +
                "                </nc:get>\n" +
                "            </nc:rpc>";

        final String getCapabilities = "<netconf-state xmlns=\"urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring\">\n" +
                "        <capabilities/>\n" +
                "      </netconf-state>";
        try {

            session = openNetconfSession(Router.ROUTER1);

            String rpcCmd = rpcGetWithXpathTemplate.replace("${xpathFilter}", getCapabilities);

            Element result = session.rpc(rpcCmd);

            logger.info("Here.");
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            closeNetconfSession(session);
        }
    }

    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ##################################### Static Nat Test Generators ############################################ */
    /** ############################################################################################################# */
    /** ############################################################################################################# */

    @Test
    public void generateStaticNatLockStepTestSeries() {
//         170.140.248.0 - 170.140.248.255 (256 addresses)
//         170.140.249.0 - 170.140.249.255 (256 addresses)

        // Sample mapping (public -> private):
        // 10.65.128.159 170.140.248.11

        /*
        You can use 10.140.248.0 ---> 10.140.249.255 as your private addresses for your test suite.

                To make things easy, you could do

            10.140.248.0 : 170.140.248.0
        10.140.248.1 : 170.140.248.1
...
        10.140.249.255 : 170.140.249.255

        */

        /*
          <DataArea>
    <NewData>
      <StaticNat>
        <PublicIp>170.140.248.11</PublicIp>
        <PrivateIp>10.22.128.2</PrivateIp>
      </StaticNat>
    </NewData>
  </DataArea>
         */
        Integer seriesNumber = 1;
        Integer numberOfTestSeries = 1;
        Boolean localTests = true;

        // 0 - 255
        final String privateIpTunnelBase1 = "10.140.248.";
        final String privateIpTunnelBase2 = "10.140.249.";
        final String publicIpBase1 = "170.140.248.";
        final String publicIpBase2 = "170.140.249.";

        String publicIpBase = publicIpBase1;
        String privateIpTunnelBase = privateIpTunnelBase1;

        try {


            AppConfig appConfig = createAppConfigFromConfigObjects("StaticNat.v1_0");

            StringBuilder builder = new StringBuilder();
            Map<String, Object> parameterMap = new HashMap<>();

            StaticNat staticNat = getStaticNatFromConfiguration(appConfig);
            StaticNatQuerySpecification staticNatQuerySpecification = getStaticNatQuerySpecificationFromConfiguration(appConfig);

            Integer lastOctet = 0;
            for (Integer n = 0; (n <= 255 * 2) && seriesNumber <= numberOfTestSeries; n++) {

                String privateIpTunnel = privateIpTunnelBase + lastOctet;
                String publicIp = publicIpBase + lastOctet;

                parameterMap.put("staticNat", staticNat);
                parameterMap.put("staticNatQuerySpecification", staticNatQuerySpecification);
                parameterMap.put("publicIp", publicIp);
                parameterMap.put("privateIpTunnel", privateIpTunnel);
                parameterMap.put("seriesNumber", seriesNumber++);
                parameterMap.put("localTests", localTests);

                String testSeries = fillStaticNatTemplate(parameterMap);
                builder.append(testSeries);

                if (lastOctet++ > 255) {
                    lastOctet = 0;
                    publicIpBase = publicIpBase2;
                    privateIpTunnelBase = privateIpTunnelBase2;
                }
            }

            logger.info(builder.toString());

        } catch (Exception e) {
            logger.error("Error generating static nat test series", e);
        }

    }

    private String fillStaticNatTemplate(Map<String, Object> parameterMap) throws Exception {
        StringBuilder builder = new StringBuilder();

        String privateIpTunnel = (String)parameterMap.get("privateIpTunnel");
        String publicIp = (String)parameterMap.get("publicIp");
        StaticNatQuerySpecification staticNatQuerySpecification = (StaticNatQuerySpecification)parameterMap.get("staticNatQuerySpecification");
        StaticNat staticNat = (StaticNat)parameterMap.get("staticNat");
        Integer seriesNumber = (Integer)parameterMap.get("seriesNumber");
        Boolean localTests = (Boolean)parameterMap.get("localTests");


        staticNat.setPublicIp(publicIp);
        staticNat.setPrivateIp(privateIpTunnel);

        staticNatQuerySpecification.setPublicIp(publicIp);

        String staticNatQuerySpecificationXmlString = staticNatQuerySpecification.toXmlString();
        String staticNatXmlString = staticNat.toXmlString();
        String localTemplate = staticNatTestSeriesLockstepTemplate;

        localTemplate = localTemplate.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber++ + "\\\"");

        localTemplate = localTemplate.replace("[---- StaticNat1 ----]", staticNatXmlString);
        localTemplate = localTemplate.replace("[---- StaticNat2 ----]", staticNatXmlString);
        localTemplate = localTemplate.replace("[---- StaticNatQuerySpecification1 ----]", staticNatQuerySpecificationXmlString);
        localTemplate = localTemplate.replace("[---- StaticNatQuerySpecification2 ----]", staticNatQuerySpecificationXmlString);

        localTemplate = tools.prettyPrintXmlString(localTemplate).replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", "");

        if (localTests) {
            localTemplate = localTemplate.replaceAll("preExecutionSleepInterval\\=\"\\d+?\"", "");
        }


        builder.append(localTemplate);

        return builder.toString();
    }

    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ################################## VPN Connection Test Generators ########################################### */
    /** ############################################################################################################# */
    /** ############################################################################################################# */

    @Test
    public void generateVpnLockStepTestSeries() {

        int numberOfTestSeries = 1;
        Integer seriesNumber = 1;
        Integer profileId = 100;
        boolean localTests = true;
        boolean completeTest = true;

        try {

            // vpnTestSeriesLockstepTemplate or vpnTestSeriesByRouterTemplate
            String template = vpnTestSeriesLockstepTemplate;
            CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
            provider.init(appConfig);

            String vpcIdBase = "vpc-testnum";
            StringBuilder builder = new StringBuilder();

            if (completeTest) {
                builder.append(testSuiteHeader);
            }

            System.out.print("Generating tests ");

            for (Integer testNumber = 1; testNumber <= numberOfTestSeries; testNumber++) {

                System.out.write('.');
                System.out.flush();
                String vpcId = vpcIdBase + profileId;

                String tunnelNumberAsString = "1";

                Map<String, XmlEnterpriseObject> testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                String localTemplate = template.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber++ + "\\\"");

                localTemplate = localTemplate.replaceAll("<VpcId>vpc-testnum\\d+</VpcId>", "<VpcId>vpc-testnum" + profileId + "</VpcId>");
                localTemplate = localTemplate.replace("[----VpnId----]", profileId.toString());

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router1----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router1----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");

                tunnelNumberAsString = "2";

                testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router2----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router2----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");

                //<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                String prettyPrintedTemplate = tools.prettyPrintXmlString(localTemplate);

                if (localTests) {
                    prettyPrintedTemplate = prettyPrintedTemplate.replaceAll("preExecutionSleepInterval\\=\"\\d+?\"", "");
                }

                builder.append(prettyPrintedTemplate.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""));

                profileId++;
            }

            if (completeTest) {
                builder.append(testSuiteFooter);
            }

            String testSuite = builder.toString();
            testSuite = tools.prettyPrintXmlString(testSuite);

            String cwd = System.getProperty("user.dir").replace('\\', '/');
            String testFile = cwd + "/tests/TestSeries-" + numberOfTestSeries + ".xml";
            logger.info("\nWriting test suite " + numberOfTestSeries + " to file: " + testFile.substring(testFile.indexOf("build-test")));

            writeTestSuiteToFile(testSuite, testFile);


        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Test
    public void generateVpnLockStepCreateDeleteTestSeries() {

        int numberOfTestSeries = 1;
        Integer seriesNumber = 1;
        Integer profileId;
        boolean completeTest = true;

        try {

            String template;
            CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
            provider.init(appConfig);

            String vpcIdBase = "vpc-testnum";
            StringBuilder builder = new StringBuilder();

            System.out.print("Generating tests ");

            if (completeTest) {
                builder.append(testSuiteHeader);
            }

            // Generates
            profileId = 1;
            template = vpnTestSeriesLockstepCreateTemplate;
            for (Integer testNumber = 1; testNumber <= numberOfTestSeries; testNumber++) {

                System.out.write('.');
                System.out.flush();
                String vpcId = vpcIdBase + profileId;

                String tunnelNumberAsString = "1";

                Map<String, XmlEnterpriseObject> testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                String localTemplate = template.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber++ + "\\\"");

                localTemplate = localTemplate.replaceAll("<VpcId>vpc-testnum\\d+</VpcId>", "<VpcId>vpc-testnum" + profileId + "</VpcId>");
                localTemplate = localTemplate.replace("[----VpnId----]", profileId.toString());

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router1----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");

                tunnelNumberAsString = "2";

                testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router2----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");

                //<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                String prettyPrintedTemplate = tools.prettyPrintXmlString(localTemplate);

                builder.append(prettyPrintedTemplate.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""));

                profileId++;
            }

            // Deletes
            profileId = 1;
            template = vpnTestSeriesLockstepDeleteMinimumQueriesTemplate;
            for (Integer testNumber = 1; testNumber <= numberOfTestSeries; testNumber++) {

                System.out.write('.');
                System.out.flush();
                String vpcId = vpcIdBase + profileId;

                String tunnelNumberAsString = "1";

                Map<String, XmlEnterpriseObject> testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                String localTemplate = template.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber++ + "\\\"");

                localTemplate = localTemplate.replaceAll("<VpcId>vpc-testnum\\d+</VpcId>", "<VpcId>vpc-testnum" + profileId + "</VpcId>");
                localTemplate = localTemplate.replace("[----VpnId----]", profileId.toString());

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router1----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router1----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");

                tunnelNumberAsString = "2";

                testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router2----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router2----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");

                //<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                String prettyPrintedTemplate = tools.prettyPrintXmlString(localTemplate);

                builder.append(prettyPrintedTemplate.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""));

                profileId++;
            }

            if (completeTest) {
                builder.append(testSuiteFooter);
            }

            String testSuite = builder.toString();
            testSuite = tools.prettyPrintXmlString(testSuite);

            logger.info("\nWriting test suite " + numberOfTestSeries);
            String cwd = System.getProperty("user.dir");
            String testFile = cwd + "/tests/TestSeries-" + numberOfTestSeries + ".xml";

            writeTestSuiteToFile(testSuite, testFile);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Test
    public void generateVpnLockStepCreateOnlyTestSeries() {

        int numberOfTestSeries = 1;
        Integer seriesNumber = 1;
        Integer profileId = 50;
        boolean completeTest = true;

        try {

            String template;
            CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
            provider.init(appConfig);

            String vpcIdBase = "vpc-testnum";
            StringBuilder builder = new StringBuilder();

            System.out.print("Generating tests ");

            if (completeTest) {
                builder.append(testSuiteHeader);
            }

            // Generates
            template = vpnTestSeriesLockstepCreateTemplate;
            for (Integer testNumber = 1; testNumber <= numberOfTestSeries; testNumber++) {

                System.out.write('.');
                System.out.flush();
                String vpcId = vpcIdBase + profileId;

                String tunnelNumberAsString = "1";

                Map<String, XmlEnterpriseObject> testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                String localTemplate = template.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber++ + "\\\"");

                localTemplate = localTemplate.replaceAll("<VpcId>vpc-testnum\\d+</VpcId>", "<VpcId>vpc-testnum" + profileId + "</VpcId>");
                localTemplate = localTemplate.replace("[----VpnId----]", profileId.toString());

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router1----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");

                tunnelNumberAsString = "2";

                testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router2----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");

                //<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                String prettyPrintedTemplate = tools.prettyPrintXmlString(localTemplate);

                builder.append(prettyPrintedTemplate.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""));

                profileId++;
            }

            if (completeTest) {
                builder.append(testSuiteFooter);
            }

            String testSuite = builder.toString();
            testSuite = tools.prettyPrintXmlString(testSuite);

            logger.info("\nWriting test suite " + numberOfTestSeries);
            String cwd = System.getProperty("user.dir");
            String testFile = cwd + "/tests/TestSeries-" + numberOfTestSeries + ".xml";

            writeTestSuiteToFile(testSuite, testFile);


        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Test
    public void generateVpnLockStepDeleteOnlyTestSeries() {

        int numberOfTestSeries = 1;
        Integer seriesNumber = 1;
        Integer profileId = 50;
        boolean completeTest = true;

        try {

            String template;
            CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

            AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
            provider.init(appConfig);

            String vpcIdBase = "vpc-testnum";
            StringBuilder builder = new StringBuilder();

            System.out.print("Generating tests ");

            if (completeTest) {
                builder.append(testSuiteHeader);
            }

            // Deletes
            template = vpnTestSeriesLockstepDeleteMinimumQueriesTemplate;
            for (Integer testNumber = 1; testNumber <= numberOfTestSeries; testNumber++) {

                System.out.write('.');
                System.out.flush();
                String vpcId = vpcIdBase + profileId;

                String tunnelNumberAsString = "1";

                Map<String, XmlEnterpriseObject> testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                String localTemplate = template.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber++ + "\\\"");

                localTemplate = localTemplate.replaceAll("<VpcId>vpc-testnum\\d+</VpcId>", "<VpcId>vpc-testnum" + profileId + "</VpcId>");
                localTemplate = localTemplate.replace("[----VpnId----]", profileId.toString());

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router1----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router1----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router1----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");

                tunnelNumberAsString = "2";

                testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router2----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router2----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
                localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router2----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");

                //<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                String prettyPrintedTemplate = tools.prettyPrintXmlString(localTemplate);

                builder.append(prettyPrintedTemplate.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""));

                profileId++;
            }

            if (completeTest) {
                builder.append(testSuiteFooter);
            }

            String testSuite = builder.toString();
            testSuite = tools.prettyPrintXmlString(testSuite);

            logger.info("\nWriting test suite " + numberOfTestSeries);
            String cwd = System.getProperty("user.dir");
            String testFile = cwd + "/tests/TestSeries-" + numberOfTestSeries + ".xml";

            writeTestSuiteToFile(testSuite, testFile);



        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Test
    public void generateVpnRollingTestSeries() throws Exception {
        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();

        AppConfig appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");
        provider.init(appConfig);


        FileSystem fileSystem = FileSystems.getDefault();

        String cwd = System.getProperty("user.dir");
        Path rollingTestFilePath = fileSystem.getPath("tests/test.xml");

        if (Files.exists(rollingTestFilePath)) {
            Files.delete(rollingTestFilePath);
        }
        Files.createFile(rollingTestFilePath);
        Files.write(rollingTestFilePath, testSuiteHeader.getBytes());

        Integer seriesNumber = generateVpnRollingTestSeries(appConfig, provider,"1", 1, rollingTestFilePath);
        generateVpnRollingTestSeries(appConfig, provider, "2", seriesNumber, rollingTestFilePath);

        Files.write(rollingTestFilePath, testSuiteFooter.getBytes(), StandardOpenOption.APPEND);
    }

    private Integer generateVpnRollingTestSeries(AppConfig appConfig, CiscoAsr1002NetconfVpnConnectionProvider provider, String tunnelNumberAsString, Integer seriesNumber, Path testFile) {

        final Integer totalVpnSlots = 200;
        final Integer maxVpnCount = 25;
        Integer profileId = 1;
//        final Integer totalVpnSlots = 5;
//        final Integer maxVpnCount = 3;
//        Integer profileId = 1;
        boolean debug = true;

        StringBuilder builder = new StringBuilder();

        try {

            String createTemplate = vpnRollingTestSeriesPerRouterCreateTemplate;
            String deleteTemplate = vpnRollingTestSeriesPerRouterDeleteTemplate;

            Map<String, Object> parameterMap = new HashMap<>();

            String vpcIdBase = "vpc-testnum";

            for (Integer create = 1, delete = create - maxVpnCount;
                 create <= totalVpnSlots + maxVpnCount;
                 delete++, create++) {

                String vpcId;

                Integer runningTotalVpns = 0;
                Map<String, XmlEnterpriseObject> testDocs;

                parameterMap.put("tunnelNumberAsString", tunnelNumberAsString);

                String testSeries;

                if (delete > 0) {
                    if (debug) {
                        logger.info("Deleting " + delete + " (" + seriesNumber + ") ");
                        runningTotalVpns -= delete;
                    }

                    // This only works with profileId in the set: {1, 2, 3, ...}
                    vpcId = vpcIdBase + delete;
                    testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, delete);

                    parameterMap.put("seriesNumber", seriesNumber++);
                    parameterMap.put("template", deleteTemplate);
                    parameterMap.put("profileId", delete);

                    testSeries = fillRollingVpnConnectionTemplate(parameterMap, testDocs);
                    builder.append(testSeries);

                }
                if (create <= totalVpnSlots) {
                    if (debug) {
                        logger.info("Adding: " + create + " (" + seriesNumber + ")");
                        runningTotalVpns += create;
                    }

                    vpcId = vpcIdBase + profileId;
                    testDocs = buildTestDocs(appConfig, provider, tunnelNumberAsString, vpcId, profileId);

                    parameterMap.put("seriesNumber", seriesNumber++);
                    parameterMap.put("template", createTemplate);
                    parameterMap.put("profileId", profileId);

                    testSeries = fillRollingVpnConnectionTemplate(parameterMap, testDocs);
                    builder.append(testSeries);

                } else {
                    if (debug) {
                        runningTotalVpns += totalVpnSlots;
                    }
                }

                if (debug) {
                    logger.info(" Total vpns: " + runningTotalVpns);
                }

                Files.write(testFile, builder.toString().getBytes(), StandardOpenOption.APPEND);
                builder.delete(0, builder.length());
                profileId++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seriesNumber;
    }

    private String fillRollingVpnConnectionTemplate(Map<String, Object> parameterMap, Map<String, XmlEnterpriseObject> testDocs) throws Exception {

        String template = (String)parameterMap.get("template");
        Integer seriesNumber = (Integer)parameterMap.get("seriesNumber");
        String tunnelNumberAsString = (String)parameterMap.get("tunnelNumberAsString");
        Integer profileId = (Integer)parameterMap.get("profileId");
        String producerName = ("1".equals(tunnelNumberAsString) ? "producerName=\\\"TestSuiteApp1Producer\\\"" : "producerName=\\\"TestSuiteApp2Producer\\\"");

        String localTemplate = template.replaceFirst("<TestSeries number=\\\"\\d+\\\"", "<TestSeries number=\\\"" + seriesNumber + "\\\"");
        localTemplate = localTemplate.replaceAll("producerName=\\\"TestSuiteApp1Producer\\\"", producerName);
        localTemplate = localTemplate.replaceAll("<VpcId>vpc-testnum\\d+</VpcId>", "<VpcId>vpc-testnum" + profileId + "</VpcId>");
        localTemplate = localTemplate.replace("[----VpnId----]", profileId.toString());

        localTemplate = localTemplate.replace("[----VpnConnectionRequisition-Router----]", testDocs.get("vpnConnectionRequisition").toXmlString() + "\n");
        localTemplate = localTemplate.replace("[----VpnConnectionGenerateResponse-Router----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
        localTemplate = localTemplate.replace("[----VpnConnectionQueryRequest-Router----]", testDocs.get("vpnConnectionGenerateResponse").toXmlString() + "\n");
        localTemplate = localTemplate.replace("[----VpnConnectionDeleteRequest-Router----]", testDocs.get("vpnConnectionDeleteRequest").toXmlString() + "\n");
        localTemplate = localTemplate.replace("router n", "router " + tunnelNumberAsString);

        //<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        String prettyPrintedTemplate = tools.prettyPrintXmlString(localTemplate);
        prettyPrintedTemplate = prettyPrintedTemplate.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", "");


        return prettyPrintedTemplate;
    }

    private Map<String, XmlEnterpriseObject> buildTestDocs(AppConfig appConfig, CiscoAsr1002NetconfVpnConnectionProvider provider, String tunnelNumberAsString, String vpcId, Integer profileId) {
        Map<String, XmlEnterpriseObject> testDocs = new HashMap<>();

        try {

            Map<String, String> addDeleteParams;

            addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    tunnelNumberAsString,
                    VpnConnectionOperationContext.GENERATE_SEND,
                    false
            );
            testDocs.put("vpnConnectionRequisition", buildVpnConnectionRequisitionObject(appConfig, addDeleteParams));

            addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    tunnelNumberAsString,
                    VpnConnectionOperationContext.GENERATE_RESPONSE,
                    false
            );

            testDocs.put("vpnConnectionGenerateResponse", buildVpnConnectionObject(appConfig, addDeleteParams, provider));

            addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    tunnelNumberAsString,
                    VpnConnectionOperationContext.DELETE_REQUEST,
                    false
            );


            testDocs.put("vpnConnectionDeleteRequest", buildVpnConnectionObject(appConfig, addDeleteParams, provider));


        } catch (Exception e) {
            e.printStackTrace();
        }

        return testDocs;
    }

    @Test
    public void generateVpnSoapMessages() {

        routerNumber = "2";
        try {
            VpnSoapMessageGenerator vpnSoapMessageGenerator = new VpnSoapMessageGenerator();
            String tunnelNumberAsString = routerNumber;
            // 000000000000001
            String vpcId = "vpc-000000000000130";
            Integer profileId = 130;

            Map<String, String> addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    tunnelNumberAsString,
                    VpnConnectionOperationContext.QUERY_REQUEST,
                    false
            );

            logger.info("############### SOAP Query ##############\n" + vpnSoapMessageGenerator.buildQuerySpecificationMessage(addDeleteParams));

            addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    tunnelNumberAsString,
                    VpnConnectionOperationContext.GENERATE_SEND,
                    false
            );

            logger.info("############### SOAP Generate #############\n" + vpnSoapMessageGenerator.buildGenerateMessage(addDeleteParams));

            addDeleteParams = buildAddDeleteParamsFromSpreadsheet(
                    vpcId,
                    profileId,
                    tunnelNumberAsString,
                    VpnConnectionOperationContext.DELETE_REQUEST,
                    false
            );

            logger.info("############### SOAP Delete ###############\n" + vpnSoapMessageGenerator.buildDeleteMessage(addDeleteParams));

        } catch (Exception e) {
            logger.error("Failed to generate soap messages", e);
        }

    }

    @Test
    public void testGetAllProperties() {
        String deploymentDescriptor = "deploy/build-test/configs/NetworkOperationsService.xml";
        XmlUtils xmlUtils = new XmlUtils();

        Properties properties = xmlUtils.getAllProperties(deploymentDescriptor);

        System.out.println(properties.size() + " properties found");

    }

    private VpnConnectionQuerySpecification buildVpnConnectionQuerySpecificationObject(AppConfig appConfig, Map<String, String> addDeleteParams) throws EnterpriseFieldException, ProviderException {
        VpnConnectionQuerySpecification vpnConnectionQuerySpecification = getVpnConnectionQuerySpecificationFromConfiguration(appConfig);

        vpnConnectionQuerySpecification.setVpcId(addDeleteParams.get("vpcId"));

        // TODO:  vcpNumberAsString?
        vpnConnectionQuerySpecification.setVpnId(addDeleteParams.get("vpcId"));

        return vpnConnectionQuerySpecification;

    }
    private VpnConnectionRequisition buildVpnConnectionRequisitionObject(AppConfig appConfig, Map<String, String> addDeleteParams) throws EnterpriseFieldException, ProviderException {

        // Notes:
        // Remote VPN Address is AWS

        VpnConnectionRequisition vpnConnectionRequisition = getVpnConnectionRequisitionFromConfiguration(appConfig);
        vpnConnectionRequisition.setOwnerId(addDeleteParams.get("vpcId"));  // vpcId
        List<RemoteVpnConnectionInfo> remoteVpnConnectionInfoList = vpnConnectionRequisition.getRemoteVpnConnectionInfo();
        RemoteVpnConnectionInfo remoteVpnConnectionInfo = getRemoteVpnConnectionInfoFromConfiguration(appConfig);
        remoteVpnConnectionInfo.setRemoteVpnConnectionId(addDeleteParams.get("vpnId"));
        RemoteVpnTunnel remoteVpnTunnel = getRemoteVpnTunnelFromConfiguration(appConfig);
        remoteVpnTunnel.setLocalTunnelId(addDeleteParams.get("tunnelId"));
        remoteVpnTunnel.setPresharedKey(addDeleteParams.get("presharedKey"));
        remoteVpnTunnel.setRemoteVpnIpAddress(addDeleteParams.get("remoteVpnIpAddress"));
        remoteVpnTunnel.setVpnInsideIpCidr(addDeleteParams.get("insideCidrIpRange"));
        remoteVpnConnectionInfo.addRemoteVpnTunnel(remoteVpnTunnel);
        remoteVpnConnectionInfoList.add(remoteVpnConnectionInfo);

        VpnConnectionProfile vpnConnectionProfile = vpnConnectionRequisition.newVpnConnectionProfile();
        vpnConnectionProfile.setVpnConnectionProfileId(addDeleteParams.get("profileId"));
        vpnConnectionProfile.setVpcNetwork(addDeleteParams.get("vpcNetworkIpRange"));

        TunnelProfile tunnelProfile = vpnConnectionProfile.newTunnelProfile();
        tunnelProfile.setVpnInsideIpCidr1(addDeleteParams.get("insideCidrIpRange"));
        tunnelProfile.setVpnInsideIpCidr2(addDeleteParams.get("insideCidrIpRange"));
        tunnelProfile.setCryptoKeyringName(addDeleteParams.get("cryptoKeyringName"));
        tunnelProfile.setIsakampProfileName(addDeleteParams.get("isakmpProfileName"));
        tunnelProfile.setIpsecTransformSetName(addDeleteParams.get("ipsecTransformSetName"));
        tunnelProfile.setIpsecProfileName(addDeleteParams.get("ipsecProfileName"));
        tunnelProfile.setTunnelId(addDeleteParams.get("tunnelId"));
        tunnelProfile.setTunnelDescription(addDeleteParams.get("tunnelDescription"));
        tunnelProfile.setCustomerGatewayIp(addDeleteParams.get("customerGatewayIp"));

        vpnConnectionProfile.addTunnelProfile(tunnelProfile);

        vpnConnectionRequisition.setVpnConnectionProfile(vpnConnectionProfile);

        return vpnConnectionRequisition;
    }


    private VpnConnection buildVpnConnectionObject(AppConfig appConfig, Map<String, String> addDeleteParams, CiscoAsr1002NetconfVpnConnectionProvider provider) throws EnterpriseFieldException, ProviderException {

        // Notes:
        // 1) Remote VPN Address is AWS

        VpnConnection vpnConnection = AppConfigUtil.getVpnConnectionFromConfiguration(appConfig);

        CryptoKeyring cryptoKeyring = AppConfigUtil.getCryptoKeyringFromConfiguration(appConfig);
        CryptoIsakmpProfile cryptoIsakmpProfile = AppConfigUtil.getCryptoIsakmpProfileFromConfiguration(appConfig);
        CryptoIpsecTransformSet cryptoIpsecTransformSet  = AppConfigUtil.getCryptoIpsecTransformSetFromConfiguration(appConfig);
        CryptoIpsecProfile cryptoIpsecProfile = AppConfigUtil.getCryptoIpsecProfileFromConfiguration(appConfig);
        TunnelInterface tunnelInterface = AppConfigUtil.getTunnelInterfaceFromConfiguration(appConfig);
        LocalAddress localAddress = AppConfigUtil.getLocalAddressProfileFromConfiguration(appConfig);
        MatchIdentity matchIdentity = AppConfigUtil.getMatchIdentityFromConfiguration(appConfig);
        BgpState bgpState = AppConfigUtil.getBgpStateFromConfiguration(appConfig);
        BgpPrefixes bgpPrefixes = AppConfigUtil.getBgpPrefixesFromConfiguration(appConfig);

        cryptoKeyring.setLocalAddress(localAddress);
        cryptoIsakmpProfile.setLocalAddress(localAddress);
        vpnConnection.setCryptoKeyring(cryptoKeyring);
        vpnConnection.setCryptoIsakmpProfile(cryptoIsakmpProfile);
        vpnConnection.setCryptoIpsecTransformSet(cryptoIpsecTransformSet);
        vpnConnection.setCryptoIpsecProfile(cryptoIpsecProfile);
        vpnConnection.addTunnelInterface(tunnelInterface);

        String tunnelInterfaceIpAddress = addDeleteParams.get("tunnelIpAddress");

        vpnConnection.setVpcId(addDeleteParams.get("vpcId"));
        vpnConnection.setVpnId(addDeleteParams.get("vpnId"));

        localAddress.setIpAddress(addDeleteParams.get("remoteVpnIpAddress"));
        localAddress.setVirtualRouteForwarding("AWS");
        cryptoKeyring.setName(addDeleteParams.get("cryptoKeyringName"));
        cryptoKeyring.setDescription(addDeleteParams.get("vpcId"));
        cryptoKeyring.setPresharedKey(addDeleteParams.get("presharedKey"));
        cryptoIsakmpProfile.setName(addDeleteParams.get("isakmpProfileName"));
        cryptoIsakmpProfile.setDescription(addDeleteParams.get("vpcId"));
        cryptoIsakmpProfile.setVirtualRouteForwarding("AWS");
        cryptoIsakmpProfile.setCryptoKeyring(cryptoKeyring);
        cryptoIsakmpProfile.setMatchIdentity(matchIdentity);
        cryptoIpsecProfile.setPerfectForwardSecrecy("group2");
        cryptoIpsecProfile.setDescription(addDeleteParams.get("vpcId"));
        cryptoIpsecProfile.setCryptoIpsecTransformSet(cryptoIpsecTransformSet);
        cryptoIpsecTransformSet.setName(addDeleteParams.get("ipsecTransformSetName"));
        cryptoIpsecTransformSet.setMode("tunnel");
        cryptoIpsecTransformSet.setBits("256");
        cryptoIpsecTransformSet.setCipher("esp-sha256-hmac");
        cryptoIpsecProfile.setName(addDeleteParams.get("ipsecProfileName"));
        tunnelInterface.setName(addDeleteParams.get("tunnelId"));
        tunnelInterface.setDescription(addDeleteParams.get("tunnelDescription"));
        tunnelInterface.setTunnelSource(addDeleteParams.get("customerGatewayIp"));
        tunnelInterface.setTunnelDestination(addDeleteParams.get("remoteVpnIpAddress"));
        tunnelInterface.setIpAddress(tunnelInterfaceIpAddress);
        tunnelInterface.setVirtualRouteForwarding("AWS");
        tunnelInterface.setNetmask("255.255.255.252");
        tunnelInterface.setTcpMaximumSegmentSize("1387");
        tunnelInterface.setAdministrativeState("up");     // TODO: up/down for add or delete?
        tunnelInterface.setTunnelMode("tunnel");
        tunnelInterface.setIpVirtualReassembly("[unknown]");
        tunnelInterface.setOperationalStatus("up");
        tunnelInterface.setCryptoIpsecProfile(cryptoIpsecProfile);
        tunnelInterface.setBgpState(bgpState);
        tunnelInterface.setBgpPrefixes(bgpPrefixes);

        matchIdentity.setVirtualRouteForwarding("AWS");
        matchIdentity.setIpAddress(addDeleteParams.get("remoteVpnIpAddress"));
        matchIdentity.setNetmask("255.255.255.255");

        bgpState.setStatus("fsm-established");
        bgpState.setUptime("00:00:00");
        bgpState.setNeighborId(addDeleteParams.get("neighborId"));

        bgpPrefixes.setSent("0");
        bgpPrefixes.setReceived("0");

        return vpnConnection;
    }


    private VpnConnectionRequisition getVpnConnectionRequisitionFromConfiguration(AppConfig appConfig) throws ProviderException {
        VpnConnectionRequisition vpnConnectionRequisition;

        try {
            vpnConnectionRequisition = (VpnConnectionRequisition) appConfig.getObject(VPN_CONNECTION_REQUISITION_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection requisition object from app config for class name " + VPN_CONNECTION_REQUISITION_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return vpnConnectionRequisition;
    }

    private RemoteVpnConnectionInfo getRemoteVpnConnectionInfoFromConfiguration(AppConfig appConfig) throws ProviderException {
        RemoteVpnConnectionInfo remoteVpnConnectionInfo;

        try {
            remoteVpnConnectionInfo = (RemoteVpnConnectionInfo) appConfig.getObject(REMOTE_VPN_CONNECTION_INFO_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting remote vpn connection info object from app config for class name " + REMOTE_VPN_CONNECTION_INFO_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return remoteVpnConnectionInfo;
    }

    private RemoteVpnTunnel getRemoteVpnTunnelFromConfiguration(AppConfig appConfig) throws ProviderException {
        RemoteVpnTunnel remoteVpnTunnel;

        try {
            remoteVpnTunnel = (RemoteVpnTunnel) appConfig.getObject(REMOTE_VPN_TUNNEL_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection requisition object from app config for class name " + REMOTE_VPN_TUNNEL_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }


        return remoteVpnTunnel;
    }

    private StaticNatQuerySpecification getStaticNatQuerySpecificationFromConfiguration(AppConfig appConfig) throws ProviderException {
        StaticNatQuerySpecification staticNatQuerySpecification;

        try {
            staticNatQuerySpecification = (StaticNatQuerySpecification) appConfig.getObject(STATIC_NAT_QUERY_SPECIFICATION_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting static nat object from app config for class name " + STATIC_NAT_QUERY_SPECIFICATION_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return staticNatQuerySpecification;

    }

    private StaticNat getStaticNatFromConfiguration(AppConfig appConfig) throws ProviderException {

        StaticNat staticNat;

        try {
            staticNat = (StaticNat) appConfig.getObject(STATIC_NAT_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting static nat object from app config for class name " + STATIC_NAT_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return staticNat;
    }
    private VpnConnectionQuerySpecification getVpnConnectionQuerySpecificationFromConfiguration(AppConfig appConfig) throws ProviderException {
        VpnConnectionQuerySpecification vpnConnectionQuerySpecification;

        try {
            vpnConnectionQuerySpecification = (VpnConnectionQuerySpecification)appConfig.getObject(VPN_CONNECTION_QUERY_SPECIFICATION_CLASS_NAME);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Exception while getting vpn connection query specification object from app config for class name " + VPN_CONNECTION_QUERY_SPECIFICATION_CLASS_NAME;
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        return vpnConnectionQuerySpecification;
    }

    @Test
    public void testAppConfigCreation() {
        AppConfig appConfig;
        appConfig = createAppConfigFromConfigObjects("VpnConnectionRequestCommand");

        assert appConfig != null;
    }

    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ################################### Utility Routines used by tests ########################################## */
    /** ############################################################################################################# */
    /** ############################################################################################################# */

    private Map<String, String> buildAddDeleteParamsFromSpreadsheet(String vpcId, Integer profileId, String tunnelNumberAsString, VpnConnectionOperationContext operationContext, boolean verbose) {
        Map<String, String> addDeleteParams = new HashMap<>();

        try {
            final Map<String, Map<String, String>> spreadsheetMap = spreadSheetWrapper.getSpreadsheetMap();
            String paddedVpcNumberAsString = String.format("%03d", profileId);
            String profileIdAsString = profileId.toString();

            Map<String, String> lineMap = spreadsheetMap.get(profileIdAsString);
            if (lineMap == null) {
                throw new RuntimeException("Bad vpc number - no map entry!");
            }

            addDeleteParams.put("operation", operationContext.getOperation());
            String remoteVpnIpAddress = getLabRemoteVpnIpAddress();
            String testPresharedKey = buildLabUnencryptKey(Integer.valueOf(profileIdAsString));

            addDeleteParams.put("vpcId", vpcId);
            addDeleteParams.put("vpnId", profileIdAsString);
            addDeleteParams.put("profileId", profileIdAsString);
            addDeleteParams.put("paddedVpcNumberAsString", paddedVpcNumberAsString);
            addDeleteParams.put("vpcNetworkIpRange", lineMap.get("VpcCidr"));
            addDeleteParams.put("presharedKey", testPresharedKey);
            addDeleteParams.put("remoteVpnIpAddress", remoteVpnIpAddress);

            logger.info("========> presharedKey: " + testPresharedKey);
            logger.info("========> remoteVpnIpAddress: " + remoteVpnIpAddress);

            String insideCidrIpRange;
            String insideCidrIpRange2;
            String tunnelDescription;
            switch (tunnelNumberAsString) {
                case "1":
                    addDeleteParams.put("customerGatewayIp", lineMap.get("CustomerGatewayIpAddress_1"));
                    addDeleteParams.put("cryptoKeyringName", lineMap.get("CRYPTO_KEYRING_1"));
                    addDeleteParams.put("isakmpProfileName", lineMap.get("ISAKMP_PROFILE_1"));
                    addDeleteParams.put("ipsecTransformSetName", lineMap.get("IPSEC_TRANSFORM_SET_1"));
                    addDeleteParams.put("ipsecProfileName", lineMap.get("IPSEC_PROFILE_1"));
                    addDeleteParams.put("tunnelId", "1" + String.format("%04d", Integer.parseInt(lineMap.get("VpnConnectionProfileId"))));
                    tunnelDescription = lineMap.get("TUNNEL_DESCRIPTION_1");
                    insideCidrIpRange = lineMap.get("VpnInsideIpCidr_1A");
                    insideCidrIpRange2 = lineMap.get("VpnInsideIpCidr_1B");
                    break;
                case "2":
                    addDeleteParams.put("customerGatewayIp", lineMap.get("CustomerGatewayIpAddress_2"));
                    addDeleteParams.put("cryptoKeyringName", lineMap.get("CRYPTO_KEYRING_2"));
                    addDeleteParams.put("isakmpProfileName", lineMap.get("ISAKMP_PROFILE_2"));
                    addDeleteParams.put("ipsecTransformSetName", lineMap.get("IPSEC_TRANSFORM_SET_2"));
                    addDeleteParams.put("ipsecProfileName", lineMap.get("IPSEC_PROFILE_2"));
                    addDeleteParams.put("tunnelId", "2" + String.format("%04d", Integer.parseInt(lineMap.get("VpnConnectionProfileId"))));
                    tunnelDescription = lineMap.get("TUNNEL_DESCRIPTION_2");
                    insideCidrIpRange = lineMap.get("VpnInsideIpCidr_2A");
                    insideCidrIpRange2 = lineMap.get("VpnInsideIpCidr_2B");
                    break;
                default:
                    throw new RuntimeException("Bad tunnel number: " + tunnelNumberAsString);
            }

            addDeleteParams.put("insideCidrIpRange", insideCidrIpRange);
            addDeleteParams.put("insideCidrIpRange2", insideCidrIpRange2);
            addDeleteParams.put("tunnelIpAddress", IPV4Address.adjustAddressByOffset(insideCidrIpRange, 2));
            addDeleteParams.put("neighborId", IPV4Address.adjustAddressByOffset(insideCidrIpRange, 1));

            logger.info("========> insideCidrIpRange: " + insideCidrIpRange);
            logger.info("========> tunnelIpAddress: " + addDeleteParams.get("tunnelIpAddress"));
            logger.info("========> neighborId: " + addDeleteParams.get("neighborId"));

            switch (operationContext) {
                case QUERY_REQUEST:
                case GENERATE_SEND:
                case GENERATE_RESPONSE:
                    addDeleteParams.put("tunnelDescription", String.format("%s (%s)", tunnelDescription, vpcId));
                    break;
                case DELETE_REQUEST:
                    addDeleteParams.put("tunnelDescription", String.format("%s (AVAILABLE)", tunnelDescription));
                    break;
            }

            if (verbose) {
                dumpAddDeleteParams(addDeleteParams);
            }

        } catch (Exception e ) {
            logger.error("Failed to build Add/Delete params from spreadsheet", e);
        }

        return addDeleteParams;
    }

    private void dumpAddDeleteParams(Map<String, String> addDeleteParams) {
        logger.info("=================== Add/Delete Params  ====================");
        logger.info("vpcId: " + addDeleteParams.get("vpcId"));
        logger.info("profileId: " + addDeleteParams.get("profileId"));
        logger.info("paddedVpcNumberAsString: " + addDeleteParams.get("paddedVpcNumberAsString"));
        logger.info("vpcNetworkIpRange: " + addDeleteParams.get("vpcNetworkIpRange"));
        logger.info("customerGatewayIp: " + addDeleteParams.get("customerGatewayIp"));
        logger.info("presharedKey: " + addDeleteParams.get("presharedKey"));
        logger.info("insideCidrIpRange: " + addDeleteParams.get("insideCidrIpRange"));
        logger.info("tunnelId: " + addDeleteParams.get("tunnelId"));
        logger.info("remoteVpnIpAddress: " + addDeleteParams.get("remoteVpnIpAddress"));
        logger.info("cryptoKeyringName: " + addDeleteParams.get("cryptoKeyringName"));
        logger.info("isakmpProfileName: " + addDeleteParams.get("isakmpProfileName"));
        logger.info("ipsecTransformSetName: " + addDeleteParams.get("ipsecTransformSetName"));
        logger.info("ipsecProfileName: " + addDeleteParams.get("ipsecProfileName"));
        logger.info("tunnelDescription: " + addDeleteParams.get("tunnelDescription"));
        logger.info("remoteVpnIpAddress: " + addDeleteParams.get("remoteVpnIpAddress"));

    }

    private String buildLabUnencryptKey(Integer profileId) {
        String unencryptKey;

        unencryptKey = String.format("test%03d", profileId);

        return unencryptKey;
    }

    // Only allowable lab value: 72.21.209.225
    private String getLabRemoteVpnIpAddress() {
        return "72.21.209.225";
    }

    private void writeTestSuiteToFile(String testSuite, String testFile) {
        FileSystem fileSystem = FileSystems.getDefault();
        Path testFilePath = fileSystem.getPath(testFile);
        StandardOpenOption standardOpenOption;
        if (testFilePath.toFile().exists()) {
            standardOpenOption = StandardOpenOption.TRUNCATE_EXISTING;
        } else {
            standardOpenOption = StandardOpenOption.CREATE;
        }
        try {
            Files.write(testFilePath, testSuite.getBytes(), standardOpenOption);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void provisionVpnByBuildingNodes() {
//        setupProviderEnvironmentToLocal();
//        CiscoAsr1002NetconfVpnConnectionProvider provider = new CiscoAsr1002NetconfVpnConnectionProvider();
//
//        // Stuff that will be passed in from command
//        String vpcNumberAsString = "1";
//        String tunnelNumberAsString = "0";
//        String vrfAsString = "AWS";
//        String vpcIdAsString = "1";
//        String vpcVpnIp = "0.1.2.3";
//        String preSharedKeyAsString = "abcdef";
//
//
//        try {
//            provider.init(null);
//            NetconfSession session = provider.getNetconfRouter1Session();
//
//
//            Prefix[] prefixes = new Prefix[2];
//
//            prefixes[0] = new Prefix("", "http://cisco.com/ns/yang/Cisco-IOS-XE-native");
//            prefixes[1] = new Prefix("ios-crypto", "http://cisco.com/ns/yang/Cisco-IOS-XE-crypto");
//            PrefixMap prefixMap = new PrefixMap(prefixes);
//
//
//            Element _native = Element.create(prefixMap, "native");
//            Element crypto = _native.createChild("crypto");
//
//            //################################### crypto/keyring ##############################################
//            Element keyringKeyring = crypto.createChild("ios-crypto:keyring");
//            Element keyringName = keyringKeyring.createChild("ios-crypto:name");
//            keyringName.setValue("keyringName");
//            Element keyringVrf = keyringKeyring.createChild("ios-crypto:vrf");
//            keyringVrf.setValue(vrfAsString);
//            Element keyringDescription = keyringKeyring.createChild("ios-crypto:description");
//            keyringDescription.setValue(vpcIdAsString);
//
//            Element keyringPreSharedKey = keyringKeyring.createChild("ios-crypto:pre-shared-key");
//            Element address = keyringPreSharedKey.createChild("ios-crypto:address");
//
//            Element keyringIpv4 = address.createChild("ios-crypto:ipv4");
//            Element keyringIpv4Address = keyringIpv4.createChild("ios-crypto:ipv4-address");
//            keyringIpv4Address.setValue(vpcVpnIp);
//            Element keyringKey = keyringIpv4.createChild("ios-crypto:key");
//            keyringKey.markCreate();
//            Element keyringUnencrytKey = keyringIpv4.createChild("ios-crypto:unencryt-key"); // Yes, that's how it's spelled.
//            keyringUnencrytKey.setValue(preSharedKeyAsString);
//
//
//            //################################## crypto/isakmp #################################################
//            Element isakmpIsakmp = crypto.createChild("ios-crypto:isakmp");
//            Element isakmpProfile = isakmpIsakmp.createChild("ios-crypto:profile");
//
//            System.out.println(_native.toXMLString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//

    }

    private String staticNatTestSeriesLockstepTemplate =
            "<TestSeries number=\"1\" producerName=\"TestSuiteApp1Producer\">\n" +
            "    <Description>This series tests basic StaticNat CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "    <TestCase number=\"1\">\n" +
            "        <Description>This case is for StaticNat CRD (no Update).</Description>\n" +
            "        <TestStep number=\"1\">\n" +
            "            <Description>This step sends a Query-Request to router 1 and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "            <QueryRequest messageCategory=\"edu.emory.CiscoAsrService\" queryObjectName=\"StaticNatQuerySpecification\"\n" +
            "                          messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <QueryData>\n" +
            "                    [---- StaticNatQuerySpecification1 ----]\n" +
            "                </QueryData>\n" +
            "                <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                <ProvideData>\n" +
            "                    <!-- Shouldn't exist -->\n" +
            "                </ProvideData>\n" +
            "            </QueryRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"2\" producerName=\"TestSuiteApp2Producer\">\n" +
            "            <Description>This step sends a Query-Request to router 2 and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "            <QueryRequest messageCategory=\"edu.emory.CiscoAsrService\" queryObjectName=\"StaticNatQuerySpecification\"\n" +
            "                          messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <QueryData>\n" +
            "                    [---- StaticNatQuerySpecification2 ----]\n" +
            "                </QueryData>\n" +
            "                <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                <ProvideData>\n" +
            "                    <!-- Shouldn't exist -->\n" +
            "                </ProvideData>\n" +
            "            </QueryRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"3\">\n" +
            "            <Description>This step sends a Create-Request to router 1.</Description>\n" +
            "            <CreateRequest messageCategory=\"edu.emory.CiscoAsrService\"\n" +
            "                           messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <NewData>\n" +
            "                    [---- StaticNat1 ----]\n" +
            "                </NewData>\n" +
            "                <TestResult action=\"Create\" status=\"success\"/>\n" +
            "                <!--\n" +
            "                TODO: Return provisioned static nat?\n" +
            "                -->\n" +
            "            </CreateRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"4\" producerName=\"TestSuiteApp2Producer\">\n" +
            "            <Description>This step sends a Create-Request to router 2.</Description>\n" +
            "            <CreateRequest messageCategory=\"edu.emory.CiscoAsrService\"\n" +
            "                           messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <NewData>\n" +
            "                    [---- StaticNat2 ----]\n" +
            "                </NewData>\n" +
            "                <TestResult action=\"Create\" status=\"success\"/>\n" +
            "                <!--\n" +
            "                TODO: Return provisioned static nat?\n" +
            "                -->\n" +
            "            </CreateRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"5\">\n" +
            "            <Description>Send Query-Request again should return entry created on router 1 in step 2.</Description>\n" +
            "            <QueryRequest messageCategory=\"edu.emory.CiscoAsrService\" queryObjectName=\"StaticNatQuerySpecification\"\n" +
            "                          messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <QueryData>\n" +
            "                    [---- StaticNatQuerySpecification1 ----]\n" +
            "                </QueryData>\n" +
            "                <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                <ProvideData>\n" +
            "                    [---- StaticNat1 ----]\n" +
            "                </ProvideData>\n" +
            "            </QueryRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"6\" producerName=\"TestSuiteApp2Producer\">\n" +
            "            <Description>Send Query-Request again should return entry created on router 2 in step 2.</Description>\n" +
            "            <QueryRequest messageCategory=\"edu.emory.CiscoAsrService\" queryObjectName=\"StaticNatQuerySpecification\"\n" +
            "                          messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <QueryData>\n" +
            "                    [---- StaticNatQuerySpecification2 ----]\n" +
            "                </QueryData>\n" +
            "                <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                <ProvideData>\n" +
            "                    [---- StaticNat2 ----]\n" +
            "                </ProvideData>\n" +
            "            </QueryRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"7\">\n" +
            "            <Description>Send Delete-Request to router 1.</Description>\n" +
            "            <DeleteRequest messageCategory=\"edu.emory.CiscoAsrService\"\n" +
            "                           messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <DeleteData>\n" +
            "                    <DeleteAction type=\"Delete\"/>\n" +
            "                    [---- StaticNat1 ----]\n" +
            "                </DeleteData>\n" +
            "                <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "            </DeleteRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"8\" producerName=\"TestSuiteApp2Producer\">\n" +
            "            <Description>Send Delete-Request to router 2.</Description>\n" +
            "            <DeleteRequest messageCategory=\"edu.emory.CiscoAsrService\"\n" +
            "                           messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <DeleteData>\n" +
            "                    <DeleteAction type=\"Delete\"/>\n" +
            "                    [---- StaticNat2 ----]\n" +
            "                </DeleteData>\n" +
            "                <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "            </DeleteRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"9\">\n" +
            "            <Description>Send Query-Request to router 1 again should return no entry because it was deleted in step 6.</Description>\n" +
            "            <QueryRequest messageCategory=\"edu.emory.CiscoAsrService\" queryObjectName=\"StaticNatQuerySpecification\"\n" +
            "                          messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <QueryData>\n" +
            "                    [---- StaticNatQuerySpecification1 ----]\n" +
            "                </QueryData>\n" +
            "                <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                <ProvideData>\n" +
            "                    <!-- no results because OU doesn't exist any more -->\n" +
            "                </ProvideData>\n" +
            "            </QueryRequest>\n" +
            "        </TestStep>\n" +
            "        <TestStep number=\"10\" producerName=\"TestSuiteApp2Producer\">\n" +
            "            <Description>Send Query-Request to router 2 again should return no entry because it was deleted in step 6.</Description>\n" +
            "            <QueryRequest messageCategory=\"edu.emory.CiscoAsrService\" queryObjectName=\"StaticNatQuerySpecification\"\n" +
            "                          messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"StaticNat\">\n" +
            "                <QueryData>\n" +
            "                    [---- StaticNatQuerySpecification2 ----]\n" +
            "                </QueryData>\n" +
            "                <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                <ProvideData>\n" +
            "                    <!-- no results because OU doesn't exist any more -->\n" +
            "                </ProvideData>\n" +
            "            </QueryRequest>\n" +
            "        </TestStep>\n" +
            "    </TestCase>\n" +
            "</TestSeries>\n";

    private String vpnTestSeriesLockstepTemplate = "    <TestSeries number=\"2\" producerName=\"TestSuiteApp1Producer\">\n" +
            "        <Description>This series tests basic Vpn Connection CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "        <TestCase number=\"1\">\n" +
            "            <Description>This case is for VpnConnection Generate/Query/Delete/Query operations.</Description>\n" +
            "            <TestStep number=\"1\">\n" +
            "                <Description>This step sends a Query-Request to router 1 and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- no results because OU doesn't exist yet -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"2\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Query-Request to router 2 and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- no results because OU doesn't exist yet -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"3\" >\n" +
            "                <Description>This step sends a Generate-Request to router 1 and is expected to succeed and return ReponseData, namely a synthesized VpnConnection object.</Description>\n" +
            "                <GenerateRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\" generateObjectName=\"VpnConnectionRequisition\">\n" +
            "                    <GenerateData>\n" +
            "                        [----VpnConnectionRequisition-Router1----]" +
            "                    </GenerateData>\n" +
            "                    <TestResult action=\"Generate\" status=\"success\"/>\n" +
            "                    <ResponseData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionGenerateResponse-Router1----]" +
            "                    </ResponseData>\n" +
            "                </GenerateRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"4\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Generate-Request to router 2 and is expected to succeed and return ReponseData, namely a synthesized VpnConnection object.</Description>\n" +
            "                <GenerateRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\" generateObjectName=\"VpnConnectionRequisition\">\n" +
            "                    <GenerateData>\n" +
            "                        [----VpnConnectionRequisition-Router2----]" +
            "                    </GenerateData>\n" +
            "                    <TestResult action=\"Generate\" status=\"success\"/>\n" +
            "                    <ResponseData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionGenerateResponse-Router2----]" +
            "                    </ResponseData>\n" +
            "                </GenerateRequest>\n" +
            "            </TestStep>\n" +
            "            <!-- Wait 1 minute to allow router to \"settle\" after generate in prior step -->\n" +
            "            <TestStep number=\"5\">\n" +
            "                <Description>This step sends a Query-Request to router 1 and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router1----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"6\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Query-Request to router 2 and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router2----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <!-- Wait 30 sec. to allow router to \"settle\" after query in prior step -->\n" +
            "            <TestStep number=\"7\" >\n" +
            "                <Description>Send Delete-Request to router 1.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router1----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"8\"  producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>Send Delete-Request to router 2.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router2----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"9\">\n" +
            "                <Description>Send Query-Request to router 1 again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"10\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>Send Query-Request to router 2 again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "        </TestCase>\n" +
            "    </TestSeries>\n";

    private String vpnTestSeriesLockstepCreateTemplate = "    <TestSeries number=\"2\" producerName=\"TestSuiteApp1Producer\">\n" +
            "        <Description>This series tests basic Vpn Connection CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "        <TestCase number=\"1\">\n" +
            "            <Description>This case is for VpnConnection Generate/Query/Delete/Query operations.</Description>\n" +
            "            <TestStep number=\"1\">\n" +
            "                <Description>This step sends a Query-Request to router 1 and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- no results because OU doesn't exist yet -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"2\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Query-Request to router 2 and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- no results because OU doesn't exist yet -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"3\" >\n" +
            "                <Description>This step sends a Generate-Request to router 1 and is expected to succeed and return ReponseData, namely a synthesized VpnConnection object.</Description>\n" +
            "                <GenerateRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\" generateObjectName=\"VpnConnectionRequisition\">\n" +
            "                    <GenerateData>\n" +
            "                        [----VpnConnectionRequisition-Router1----]" +
            "                    </GenerateData>\n" +
            "                    <TestResult action=\"Generate\" status=\"success\"/>\n" +
            "                    <ResponseData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionGenerateResponse-Router1----]" +
            "                    </ResponseData>\n" +
            "                </GenerateRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"4\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Generate-Request to router 2 and is expected to succeed and return ReponseData, namely a synthesized VpnConnection object.</Description>\n" +
            "                <GenerateRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\" generateObjectName=\"VpnConnectionRequisition\">\n" +
            "                    <GenerateData>\n" +
            "                        [----VpnConnectionRequisition-Router2----]" +
            "                    </GenerateData>\n" +
            "                    <TestResult action=\"Generate\" status=\"success\"/>\n" +
            "                    <ResponseData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionGenerateResponse-Router2----]" +
            "                    </ResponseData>\n" +
            "                </GenerateRequest>\n" +
            "            </TestStep>\n" +
            "            <!-- Wait 1 minute to allow router to \"settle\" after generate in prior step -->\n" +
            "            <TestStep number=\"5\">\n" +
            "                <Description>This step sends a Query-Request to router 1 and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router1----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"6\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Query-Request to router 2 and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router2----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "        </TestCase>\n" +
            "    </TestSeries>\n";

    private String vpnTestSeriesLockstepDeleteTemplate = "    <TestSeries number=\"2\" producerName=\"TestSuiteApp1Producer\">\n" +
            "        <Description>This series tests basic Vpn Connection CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "        <TestCase number=\"1\">\n" +
            "            <TestStep number=\"1\">\n" +
            "                <Description>This step sends a Query-Request to router 1 and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router1----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"2\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>This step sends a Query-Request to router 2 and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router2----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <!-- Wait 30 sec. to allow router to \"settle\" after query in prior step -->\n" +
            "            <TestStep number=\"3\" >\n" +
            "                <Description>Send Delete-Request to router 1.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router1----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"4\"  producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>Send Delete-Request to router 2.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router2----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"5\">\n" +
            "                <Description>Send Query-Request to router 1 again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"6\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>Send Query-Request to router 2 again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "        </TestCase>\n" +
            "    </TestSeries>\n";

    private String vpnTestSeriesLockstepDeleteMinimumQueriesTemplate = "    <TestSeries number=\"2\" producerName=\"TestSuiteApp1Producer\">\n" +
            "        <Description>This series tests basic Vpn Connection CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "        <TestCase number=\"1\">\n" +
            "            <TestStep number=\"1\" >\n" +
            "                <Description>Send Delete-Request to router 1.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router1----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"2\"  producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>Send Delete-Request to router 2.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router2----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"3\">\n" +
            "                <Description>Send Query-Request to router 1 again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"4\" producerName=\"TestSuiteApp2Producer\">\n" +
            "                <Description>Send Query-Request to router 2 again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "        </TestCase>\n" +
            "    </TestSeries>\n";

    private String vpnRollingTestSeriesPerRouterCreateTemplate = "    <TestSeries number=\"2\" producerName=\"TestSuiteApp1Producer\">\n" +
            "        <Description>This series tests basic Vpn Connection CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "        <TestCase number=\"1\">\n" +
            "            <Description>This case is for VpnConnection Generate/Query/Delete/Query operations.</Description>\n" +
            "            <TestStep number=\"1\">\n" +
            "                <Description>This step sends a Query-Request to router n and is expected to succeed and return a (empty) Provide-Reply.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- no results because OU doesn't exist yet -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"2\" >\n" +
            "                <Description>This step sends a Generate-Request to router n and is expected to succeed and return ReponseData, namely a synthesized VpnConnection object.</Description>\n" +
            "                <GenerateRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\" generateObjectName=\"VpnConnectionRequisition\">\n" +
            "                    <GenerateData>\n" +
            "                        [----VpnConnectionRequisition-Router----]" +
            "                    </GenerateData>\n" +
            "                    <TestResult action=\"Generate\" status=\"success\"/>\n" +
            "                    <ResponseData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionGenerateResponse-Router----]" +
            "                    </ResponseData>\n" +
            "                </GenerateRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"3\">\n" +
            "                <Description>This step sends a Query-Request to router n and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "        </TestCase>\n" +
            "    </TestSeries>\n";

    private String vpnRollingTestSeriesPerRouterDeleteTemplate = "    <TestSeries number=\"2\" producerName=\"TestSuiteApp1Producer\">\n" +
            "        <Description>This series tests basic Vpn Connection CRD (no Update) operations for the CiscoAsrService.</Description>\n" +
            "        <TestCase number=\"1\">\n" +
            "            <TestStep number=\"1\">\n" +
            "                <Description>This step sends a Query-Request to router n and is expected to succeed and return a Provide-Reply containing the previously generated VpnConnection.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData xslUri=\"tests/TestSuite-CiscoAsrService.xslt\">\n" +
            "                        [----VpnConnectionQueryRequest-Router----]" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"2\">\n" +
            "                <Description>Send Delete-Request to router n.</Description>\n" +
            "                <DeleteRequest messageCategory=\"edu.emory.Network\"\n" +
            "                               messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <DeleteData>\n" +
            "                        <DeleteAction type=\"Delete\"/>\n" +
            "                            [----VpnConnectionDeleteRequest-Router----]" +
            "                    </DeleteData>\n" +
            "                    <TestResult action=\"Delete\" status=\"success\"/>\n" +
            "                </DeleteRequest>\n" +
            "            </TestStep>\n" +
            "            <TestStep number=\"3\">\n" +
            "                <Description>Send Query-Request to router n again should return no entry verifying delete in prior step.</Description>\n" +
            "                <QueryRequest messageCategory=\"edu.emory.Network\" queryObjectName=\"VpnConnectionQuerySpecification\"\n" +
            "                              messageRelease=\"1.0\" messagePriority=\"9\" messageObjectName=\"VpnConnection\">\n" +
            "                    <QueryData>\n" +
            "                        <VpnConnectionQuerySpecification>\n" +
            "                            <VpcId>vpc-testnum1</VpcId>\n" +
            "                            <VpnId>[----VpnId----]</VpnId>\n" +
            "                        </VpnConnectionQuerySpecification>\n" +
            "                    </QueryData>\n" +
            "                    <TestResult action=\"Query\" status=\"success\"/>\n" +
            "                    <ProvideData>\n" +
            "                        <!-- No results due to delete in prior step -->\n" +
            "                    </ProvideData>\n" +
            "                </QueryRequest>\n" +
            "            </TestStep>\n" +
            "        </TestCase>\n" +
            "    </TestSeries>\n";

    private class VpnSoapMessageGenerator {

        public String buildQuerySpecificationMessage(Map<String, String> addDeleteParams) throws Exception {
            String tunnelId = addDeleteParams.get("tunnelId");
            if (tunnelId.startsWith("2")) {
                vpnQuerySpecificationSoapTemplate = vpnQuerySpecificationSoapTemplate
                        .replace("CiscoAsr1Service", "CiscoAsr2Service")
                        .replace("ciscoasr1service", "ciscoasr2service");
            }

            Pattern pattern = Pattern.compile("(\\$\\{.+?\\})");
            Matcher matcher = pattern.matcher(vpnQuerySpecificationSoapTemplate);

            List<String> tokens = new ArrayList<>();
            while (matcher.find() ) {
                tokens.add(matcher.group(1));
            }

            for (String token : tokens) {
                switch (token) {
                    case "${VpcId}":
                        vpnQuerySpecificationSoapTemplate = vpnQuerySpecificationSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpcId"));
                        break;
                    case "${VpnId}":
                        vpnQuerySpecificationSoapTemplate = vpnQuerySpecificationSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpnId"));
                        break;
                }

            }

            return vpnQuerySpecificationSoapTemplate;

        }


        /*
template tokens:

0 = "vpnConnectionProfileId"
1 = "vpcNetwork"
2 = "tunnelId1"
3 = "cryptoKeyringName1"
4 = "isakampProfileName1"
5 = "ipsecTransformSetName1"
6 = "ipsecProfileName1"
7 = "tunnelDescription1"
8 = "customerGatewayIp1"
9 = "vpnInsideIpCidr11"
10 = "vpnInsideIpCidr21"
11 = "ownerId"
12 = "remoteVpnConnectionId"
13 = "localTunnelId1"
14 = "remoteVpnIpAddress1"
15 = "presharedKey1"
16 = "vpnInsideIpCidr1"

VpnConnectionRequisition:

m_vpnConnectionProfile = (Collection)
		m_vpnConnectionProfileId = "1"
		m_vpcNetwork = "10.65.0.0/23"
		m_tunnelProfile = {ArrayList@4064}  size = 1
m_ownerId = "vpc-000000000000000"
m_remoteVpnConnectionInfo
		m_remoteVpnConnectionId = "1"
		m_remoteVpnTunnel = {Collection)
			m_localTunnelId = "10001"
			m_remoteVpnIpAddress = "72.21.209.225"
			m_presharedKey = "test001"
			m_vpnInsideIpCidr = "169.254.248.0/30"


addDeleteParams:

"vpcNetworkIpRange" -> "10.65.0.0/23"
"ipsecTransformSetName" -> "ipsec-prop-vpn-research-vpc001-tun1"
"customerGatewayIp" -> "170.140.76.1"
"cryptoKeyringName" -> "keyring-vpn-research-vpc001-tun1"
"tunnelDescription" -> "AWS Research VPC001 Tunnel1 (vpc-000000000000000)"
"neighborId" -> "169.254.248.1"
"presharedKey" -> "test001"
"remoteVpnIpAddress" -> "72.21.209.225"
"isakmpProfileName" -> "isakmp-vpn-research-vpc001-tun1"
"profileId" -> "1"
"vpcId" -> "vpc-000000000000000"
"paddedVpcNumberAsString" -> "001"
"tunnelId" -> "10001"
"vpnId" -> "1"
"ipsecProfileName" -> "ipsec-vpn-research-vpc001-tun1"
"operation" -> "generate-send"
"tunnelIpAddress" -> "169.254.248.2"
"insideCidrIpRange" -> "169.254.248.0/30"

         */
        public String buildGenerateMessage(Map<String, String> addDeleteParams) throws Exception {


            String tunnelId = addDeleteParams.get("tunnelId");
            if (tunnelId.startsWith("2")) {
                vpnGenerateSoapTemplate = vpnGenerateSoapTemplate
                        .replace("CiscoAsr1Service", "CiscoAsr2Service")
                        .replace("ciscoasr1service", "ciscoasr2service");
            }

            Pattern pattern = Pattern.compile("\\$\\{(.+?)\\}");
            Matcher matcher = pattern.matcher(vpnGenerateSoapTemplate);

            List<String> tokens = new ArrayList<>();
            while (matcher.find() ) {
                String token = matcher.group(1);
//                if (token.length() > 0) {
//                    String firstChar = token.substring(0, 1);
//                    firstChar = firstChar.toLowerCase();
//                    token = firstChar + token.substring(1);
//                }
                tokens.add("${" + token + "}");
            }

            for (String token : tokens) {
                switch (token) {
                    case "${VpnConnectionProfileId}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "profileId"));
                        break;
                    case "${VpcNetwork}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpcNetworkIpRange"));
                        break;
                    case "${TunnelId1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "tunnelId"));
                        break;
                    case "${CryptoKeyringName1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "cryptoKeyringName"));
                        break;
                    case "${IsakampProfileName1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "isakmpProfileName"));
                        break;
                    case "${IpsecTransformSetName1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "ipsecTransformSetName"));
                        break;
                    case "${IpsecProfileName1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "ipsecProfileName"));
                        break;
                    case "${TunnelDescription1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "tunnelDescription"));
                        break;
                    case "${CustomerGatewayIp1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "customerGatewayIp"));
                        break;
                    case "${VpnInsideIpCidr11}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "insideCidrIpRange"));
                        break;
                    case "${VpnInsideIpCidr21}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "insideCidrIpRange2"));
                        break;
                    case "${OwnerId}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpcId"));
                        break;
                    case "${RemoteVpnConnectionId}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpnId"));
                        break;
                    case "${LocalTunnelId1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "tunnelId"));
                        break;
                    case "${RemoteVpnIpAddress1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "remoteVpnIpAddress"));
                        break;
                    case "${PresharedKey1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "presharedKey"));
                        break;
                    case "${VpnInsideIpCidr1}":
                        vpnGenerateSoapTemplate = vpnGenerateSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "insideCidrIpRange"));
                        break;
                }


            }
            return vpnGenerateSoapTemplate;
        }

        private String safeGetAddDeleteParamsValue(Map<String, String> addDeleteParams, String key) {
            String safeValue = "";

            if (addDeleteParams.get(key) != null) {
                safeValue = addDeleteParams.get(key);
            }
            return safeValue;
        }

        public String buildDeleteMessage(Map<String, String> addDeleteParams) throws Exception {

            // "neighborId" from addDeleteParams

            String tunnelId = addDeleteParams.get("tunnelId");
            if (tunnelId.startsWith("2")) {
                vpnDeleteSoapTemplate = vpnDeleteSoapTemplate
                        .replace("CiscoAsr1Service", "CiscoAsr2Service")
                        .replace("ciscoasr1service", "ciscoasr2service");
            }

            Pattern pattern = Pattern.compile("(\\$\\{.+?\\})");
            Matcher matcher = pattern.matcher(vpnDeleteSoapTemplate);

            List<String> tokens = new ArrayList<>();
            while (matcher.find() ) {
                tokens.add(matcher.group(1));
            }

            for (String token : tokens) {
                switch (token) {
                    case "${BgpNeighborId}":
                        vpnDeleteSoapTemplate = vpnDeleteSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "neighborId"));
                        break;
                    case "${TunnelId1}":
                        vpnDeleteSoapTemplate = vpnDeleteSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "tunnelId"));
                        break;
                    case "${VpcId}":
                        vpnDeleteSoapTemplate = vpnDeleteSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpcId"));
                        break;
                    case "${VpnId}":
                        vpnDeleteSoapTemplate = vpnDeleteSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "vpnId"));
                        break;
                    case "${RemoteVpnIpAddress1}":
                        vpnDeleteSoapTemplate = vpnDeleteSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "remoteVpnIpAddress"));
                        break;
                    case "${PresharedKey1}":
                        vpnDeleteSoapTemplate = vpnDeleteSoapTemplate.replace(token, safeGetAddDeleteParamsValue(addDeleteParams, "presharedKey"));
                        break;
                }

            }

            return vpnDeleteSoapTemplate;

        }

        private String vpnQuerySpecificationSoapTemplate = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cis=\"http://www.rhedcloud.org/CiscoAsr2Service/\" xmlns:cis1=\"http://www.rhedcloud.org/ciscoasr2service/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <cis:VpnConnectionQueryRequest>\n" +
                "         <cis1:VpnConnectionQuerySpecification>\n" +
                "            <cis1:VpnId>${VpnId}</cis1:VpnId>\n" +
                "            <cis1:VpcId>${VpcId}</cis1:VpcId>\n" +
                "         </cis1:VpnConnectionQuerySpecification>\n" +
                "      </cis:VpnConnectionQueryRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";

        private String vpnGenerateSoapTemplate = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cis=\"http://www.rhedcloud.org/CiscoAsr1Service/\" xmlns:cis1=\"http://www.rhedcloud.org/ciscoasr1service/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <cis:VpnConnectionGenerateRequest>\n" +
                "         <cis1:VpnConnectionRequisition>\n" +
                "            <cis1:VpnConnectionProfile>\n" +
                "               <cis1:VpnConnectionProfileId>${VpnConnectionProfileId}</cis1:VpnConnectionProfileId>\n" +
                "               <cis1:VpcNetwork>${VpcNetwork}</cis1:VpcNetwork>\n" +
                "               <!--1 or more repetitions:-->\n" +
                "               <cis1:TunnelProfile>\n" +
                "                  <cis1:TunnelId>${TunnelId1}</cis1:TunnelId>\n" +
                "                  <cis1:CryptoKeyringName>${CryptoKeyringName1}</cis1:CryptoKeyringName>\n" +
                "                  <cis1:IsakampProfileName>${IsakampProfileName1}</cis1:IsakampProfileName>\n" +
                "                  <cis1:IpsecTransformSetName>${IpsecTransformSetName1}</cis1:IpsecTransformSetName>\n" +
                "                  <cis1:IpsecProfileName>${IpsecProfileName1}</cis1:IpsecProfileName>\n" +
                "                  <cis1:TunnelDescription>${TunnelDescription1}</cis1:TunnelDescription>\n" +
                "                  <cis1:CustomerGatewayIp>${CustomerGatewayIp1}</cis1:CustomerGatewayIp>\n" +
                "                  <cis1:VpnInsideIpCidr1>${VpnInsideIpCidr11}</cis1:VpnInsideIpCidr1>\n" +
                "                  <cis1:VpnInsideIpCidr2>${VpnInsideIpCidr21}</cis1:VpnInsideIpCidr2>\n" +
                "               </cis1:TunnelProfile>\n" +
                "            </cis1:VpnConnectionProfile>\n" +
                "            <cis1:OwnerId>${OwnerId}</cis1:OwnerId>\n" +
                "            <!--1 or more repetitions:-->\n" +
                "            <cis1:RemoteVpnConnectionInfo>\n" +
                "               <cis1:RemoteVpnConnectionId>${RemoteVpnConnectionId}</cis1:RemoteVpnConnectionId>\n" +
                "               <!--1 or more repetitions:-->\n" +
                "               <cis1:RemoteVpnTunnel>\n" +
                "                  <cis1:LocalTunnelId>${LocalTunnelId1}</cis1:LocalTunnelId>\n" +
                "                  <cis1:RemoteVpnIpAddress>${RemoteVpnIpAddress1}</cis1:RemoteVpnIpAddress>\n" +
                "                  <cis1:PresharedKey>${PresharedKey1}</cis1:PresharedKey>\n" +
                "                  <cis1:VpnInsideIpCidr>${VpnInsideIpCidr1}</cis1:VpnInsideIpCidr>\n" +
                "               </cis1:RemoteVpnTunnel>\n" +
                "            </cis1:RemoteVpnConnectionInfo>\n" +
                "         </cis1:VpnConnectionRequisition>\n" +
                "      </cis:VpnConnectionGenerateRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";

        /*
        valuesMap.put(mapper.getSubstitutionToken("paddedVpnId"), vpnIdPadded);
        valuesMap.put(mapper.getSubstitutionToken("remoteVpnIpAddress"), remoteVpnIpAddress);
        valuesMap.put(mapper.getSubstitutionToken("bgpNeighborId"), neighborId);
        valuesMap.put(mapper.getSubstitutionToken("tunnelId"), tunnelId);
        valuesMap.put(mapper.getSubstitutionToken("tunnelNumber"), routerNumber);
        valuesMap.put(mapper.getSubstitutionToken("tunnelSuffix"), tunnelSuffix);
        valuesMap.put(mapper.getSubstitutionToken("presharedKey"), preSharedKey);
        valuesMap.put(mapper.getSubstitutionToken("bgpAsn"), bgpAsn);

        NOTE: To determined which field are ACTUALLY used, see delete() method in vpn connection provider.
         */
        private String vpnDeleteSoapTemplate = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cis=\"http://www.rhedcloud.org/CiscoAsr1Service/\" xmlns:cis1=\"http://www.rhedcloud.org/ciscoasr1service/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <cis:VpnConnectionRequest>\n" +
                "         <cis1:VpnConnection>\n" +
                "            <cis1:VpnId>${VpnId}</cis1:VpnId>\n" +
                "            <cis1:VpcId>${VpcId}</cis1:VpcId>\n" +
                "            <cis1:CryptoKeyring>\n" +
                "               <cis1:Name>NOT_USED</cis1:Name>\n" +
                "               <cis1:Description>NOT_USED</cis1:Description>\n" +
                "               <cis1:LocalAddress>\n" +
                "                  <cis1:IpAddress>NOT_USED</cis1:IpAddress>\n" +
                "                  <cis1:VirtualRouteForwarding>NOT_USED</cis1:VirtualRouteForwarding>\n" +
                "               </cis1:LocalAddress>\n" +
                "               <cis1:PresharedKey>${PresharedKey1}</cis1:PresharedKey>\n" +
                "            </cis1:CryptoKeyring>\n" +
                "            <cis1:CryptoIsakmpProfile>\n" +
                "               <cis1:Name>NOT_USED</cis1:Name>\n" +
                "               <cis1:Description>NOT_USED</cis1:Description>\n" +
                "               <cis1:VirtualRouteForwarding>NOT_USED</cis1:VirtualRouteForwarding>\n" +
                "               <cis1:CryptoKeyring>\n" +
                "                  <cis1:Name>NOT_USED</cis1:Name>\n" +
                "                  <cis1:Description>NOT_USED</cis1:Description>\n" +
                "                  <cis1:LocalAddress>\n" +
                "                     <cis1:IpAddress>NOT_USED</cis1:IpAddress>\n" +
                "                     <cis1:VirtualRouteForwarding>NOT_USED</cis1:VirtualRouteForwarding>\n" +
                "                  </cis1:LocalAddress>\n" +
                "                  <cis1:PresharedKey>${PresharedKey1}</cis1:PresharedKey>\n" +
                "               </cis1:CryptoKeyring>\n" +
                "               <cis1:MatchIdentity>\n" +
                "                  <cis1:IpAddress>NOT_USED</cis1:IpAddress>\n" +
                "                  <cis1:Netmask>NOT_USED</cis1:Netmask>\n" +
                "                  <cis1:VirtualRouteForwarding>NOT_USED</cis1:VirtualRouteForwarding>\n" +
                "               </cis1:MatchIdentity>\n" +
                "               <cis1:LocalAddress>\n" +
                "                  <cis1:IpAddress>NOT_USED</cis1:IpAddress>\n" +
                "                  <cis1:VirtualRouteForwarding>NOT_USED</cis1:VirtualRouteForwarding>\n" +
                "               </cis1:LocalAddress>\n" +
                "            </cis1:CryptoIsakmpProfile>\n" +
                "            <cis1:CryptoIpsecTransformSet>\n" +
                "               <cis1:Name>NOT_USED</cis1:Name>\n" +
                "               <cis1:Cipher>NOT_USED</cis1:Cipher>\n" +
                "               <cis1:Bits>NOT_USED</cis1:Bits>\n" +
                "               <cis1:Mode>NOT_USED</cis1:Mode>\n" +
                "            </cis1:CryptoIpsecTransformSet>\n" +
                "            <cis1:CryptoIpsecProfile>\n" +
                "               <cis1:Name>NOT_USED</cis1:Name>\n" +
                "               <cis1:Description>NOT_USED</cis1:Description>\n" +
                "               <cis1:CryptoIpsecTransformSet>\n" +
                "                  <cis1:Name>NOT_USED</cis1:Name>\n" +
                "                  <cis1:Cipher>NOT_USED</cis1:Cipher>\n" +
                "                  <cis1:Bits>NOT_USED</cis1:Bits>\n" +
                "                  <cis1:Mode>NOT_USED</cis1:Mode>\n" +
                "               </cis1:CryptoIpsecTransformSet>\n" +
                "               <cis1:PerfectForwardSecrecy>NOT_USED</cis1:PerfectForwardSecrecy>\n" +
                "            </cis1:CryptoIpsecProfile>\n" +
                "            <!--1 or more repetitions:-->\n" +
                "            <cis1:TunnelInterface>\n" +
                "               <cis1:Name>${TunnelId1}</cis1:Name>\n" +
                "               <cis1:Description>NOT_USED</cis1:Description>\n" +
                "               <cis1:VirtualRouteForwarding>NOT_USED</cis1:VirtualRouteForwarding>\n" +
                "               <cis1:IpAddress>NOT_USED</cis1:IpAddress>\n" +
                "               <cis1:Netmask>NOT_USED</cis1:Netmask>\n" +
                "               <cis1:TcpMaximumSegmentSize>NOT_USED</cis1:TcpMaximumSegmentSize>\n" +
                "               <cis1:AdministrativeState>NOT_USED</cis1:AdministrativeState>\n" +
                "               <cis1:TunnelSource>NOT_USED</cis1:TunnelSource>\n" +
                "               <cis1:TunnelMode>NOT_USED</cis1:TunnelMode>\n" +
                "               <cis1:TunnelDestination>${RemoteVpnIpAddress1}</cis1:TunnelDestination>\n" +
                "               <cis1:CryptoIpsecProfile>\n" +
                "                  <cis1:Name>NOT_USED</cis1:Name>\n" +
                "                  <cis1:Description>NOT_USED</cis1:Description>\n" +
                "                  <cis1:CryptoIpsecTransformSet>\n" +
                "                     <cis1:Name>NOT_USED</cis1:Name>\n" +
                "                     <cis1:Cipher>NOT_USED</cis1:Cipher>\n" +
                "                     <cis1:Bits>NOT_USED</cis1:Bits>\n" +
                "                     <cis1:Mode>NOT_USED</cis1:Mode>\n" +
                "                  </cis1:CryptoIpsecTransformSet>\n" +
                "                  <cis1:PerfectForwardSecrecy>NOT_USED</cis1:PerfectForwardSecrecy>\n" +
                "               </cis1:CryptoIpsecProfile>\n" +
                "               <cis1:IpVirtualReassembly>NOT_USED</cis1:IpVirtualReassembly>\n" +
                "               <cis1:OperationalStatus>NOT_USED</cis1:OperationalStatus>\n" +
                "               <cis1:BgpState>\n" +
                "                  <cis1:Status>NOT_USED</cis1:Status>\n" +
                "                  <cis1:Uptime>NOT_USED</cis1:Uptime>\n" +
                "                  <cis1:NeighborId>${BgpNeighborId}</cis1:NeighborId>\n" +
                "               </cis1:BgpState>\n" +
                "               <cis1:BgpPrefixes>\n" +
                "                  <cis1:Sent>NOT_USED</cis1:Sent>\n" +
                "                  <cis1:Received>NOT_USED</cis1:Received>\n" +
                "               </cis1:BgpPrefixes>\n" +
                "            </cis1:TunnelInterface>\n" +
                "         </cis1:VpnConnection>\n" +
                "      </cis:VpnConnectionRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";

    }

    private final String testSuiteHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<!DOCTYPE TestSuite SYSTEM \"TestSuite-CiscoAsrService.dtd\">\n" +
            "<TestSuite name=\"CiscoAsrService minimal\">\n" +
            "    <Description>This TestSuite tests the functionality of the Cisco Asr1 Service.</Description>\n" +
            "\n";

    private final String testSuiteFooter = "</TestSuite>\n";

    @Test
    public void testStackTraceAnalyzer() {
        try {
            String test = null;

            int length = test.length();
        } catch (Exception e) {
            StackTraceAnalyzer stackTraceAnalyzer = new StackTraceAnalyzer();

            if (stackTraceAnalyzer.isErrorPresentIgnoreCase("NULLPOINTEREXCEPTION", e)) {
                logger.info("Ding!");
            }
        }
    }
}
