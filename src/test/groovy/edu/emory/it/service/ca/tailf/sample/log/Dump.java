package edu.emory.it.service.ca.tailf.sample.log;

public abstract  class Dump {
    private final String indentAmount = "    ";
    protected String indent = "";

    protected void increaseIndent() {
        indent += indentAmount;
    }

    protected void decreaseIndent() {
        int amount = indentAmount.length();
        if (amount < indent.length()) {
            indent = indent.substring(amount);
        } else {
            indent = "";
        }
    }
    protected void dumpHeader(String header) {

        String template = "####################################################################################################";
        template = template.substring(indent.length());
        int templateLength = template.length() ;

        header = " " + header + " ";
        int headerPlusSpacesLength = header.length();
        if (headerPlusSpacesLength > templateLength) {
            System.out.println(indent + header);
        } else {
            int halfLength = ((templateLength / 2 + templateLength % 2) - (headerPlusSpacesLength / 2 + headerPlusSpacesLength % 2)) - indent.length() / 2;
            byte[] bytes = template.getBytes();
            int templateIndex = halfLength;
            for (int n = 0; n < header.length(); n++) {
                bytes[templateIndex++] = (byte)header.charAt(n);
            }

            System.out.println(indent + new String(bytes));
        }
    }
    protected void dump(String value) {
        System.out.println(indent + value);
    }

    protected void dumpNoValues(String message) {
        System.out.println(indent + ">>>>>> NO " + message + " VALUES <<<<<<");
    }

}
