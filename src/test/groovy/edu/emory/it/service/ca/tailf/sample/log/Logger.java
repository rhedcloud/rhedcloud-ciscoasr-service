package edu.emory.it.service.ca.tailf.sample.log;

public class Logger {
    public void info(String message) {
        System.out.println(message);
    }

    public void warn(String message) {
        System.out.println(message);
    }

    public void warn(String message, Exception e) {
        System.out.println(message + "\n");
        e.printStackTrace();
    }

    public void error(String message) {
        System.out.println(message);
    }

    public void error(String message, Exception e) {
        System.out.println(message + "\n");
        e.printStackTrace();
    }
}

