package edu.emory.it.service.ca.provider.mapper

import com.tailf.jnc.Element
import com.tailf.jnc.XMLParser
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection
import edu.emory.moa.objects.resources.v1_0.*
import org.jdom.Document
import org.openeai.layouts.EnterpriseLayoutException
import org.openeai.layouts.EnterpriseLayoutManager
import org.openeai.moa.XmlEnterpriseObject
import spock.lang.Specification

class AbstractMapperSpec extends Specification {


    def "test replaceMe - orignal and value"() {
        given:
        MyAbstractMapper mapper = new MyAbstractMapper()

        when:
        String newValue = mapper.replaceMe("abcd", "efgh")

        then:
        newValue == 'efgh'

        when:
        newValue = mapper.replaceMe('Abc${substitute me}d', "new value")

        then:

        newValue == 'Abcnew valued'

        when:
        newValue = mapper.replaceMe('This ${doit} works', 'really')

        then:
        newValue == 'This really works'

        when:
        newValue = mapper.replaceMe('${ipsec-vpn-research-vpcFIXME_VpcNum-tunFIXME_TunnelNum}B', 'ipsec-vpn-research-vpc001-tun1')

        then:
        println newValue
        newValue == 'ipsec-vpn-research-vpc001-tun1B'
    }

    def "test replaceMe - original, sentinel, value"() {
        given:
        MyAbstractMapper mapper = new MyAbstractMapper()

        when:
        String newValue = mapper.replaceMe("abcd",  '${new value}', "efgh")

        then:
        newValue == 'abcd'

        when:
        newValue = mapper.replaceMe('Abc${substitute me}d', '${different sentinel}',"new value")

        then:

        newValue == 'Abc${substitute me}d'

        when:
        newValue = mapper.replaceMe('Abc${substitute me}d', '${substitute me}', '')

        then:
        newValue == 'Abcd'

        when:
        newValue = mapper.replaceMe('This is ${first} interesting. ${second} works.', '${first}', "really")

        then:

        newValue == 'This is really interesting. ${second} works.'

        when: 'Use prior newValue result'
        newValue = mapper.replaceMe(newValue, '${second}', 'It')

        then:
        newValue == 'This is really interesting. It works.'
    }

    // This test proves that the JNC element objects are quite tolerant to bad xpath and null values.
    // This is important for the mapper strategy hierarchy to have default abstract behavior that can be
    // easily overridden in concrete subclasses for handling structural differences between router version / CSP
    // templates.
    def "test setting null values and bad xpath in templates"() {
        given:
        XMLParser xmlParser = new XMLParser();
        Element shutdownFromXml = xmlParser.parse(_1662AWSShutdownTemplateExample);

        String xpath = 'interface/Tunnel/name'
        String invalidXpath = 'bad/xpath'
        when:
        shutdownFromXml.setValue(xpath, null)
        Object value = shutdownFromXml.getValue(xpath)

        then:
        value == null

        when: 'xpath is valid, and value is not null, we should be able to set/retrieve value'
        shutdownFromXml.setValue(xpath, 'test value')
        value = shutdownFromXml.getValue(xpath)

        then:
        value == 'test value'

        when: 'invalid xpath, i.e., does not exist in Element object, shows that the setValue call succeeds, but getValue retrieves null'
        shutdownFromXml.setValue(invalidXpath, 'test value')
        value = shutdownFromXml.getValue(invalidXpath)

        then:
        value == null

        when: 'null xpath throws a null pointer exception'
        shutdownFromXml.setValue(null, null)

        then:
        thrown NullPointerException
    }

    VpnConnection createVpnConnection() {
        EnterpriseLayoutManager layoutManager = buildLayoutManager();
        VpnConnection vpnConnection = new VpnConnection()
        vpnConnection.setInputLayoutManager(layoutManager)
        vpnConnection.setOutputLayoutManager(layoutManager)
        CryptoKeyring cryptoKeyring = new CryptoKeyring();
        cryptoKeyring.setInputLayoutManager(layoutManager)
        cryptoKeyring.setOutputLayoutManager(layoutManager)
        LocalAddress localAddress = new LocalAddress()
        localAddress.setInputLayoutManager(layoutManager)
        localAddress.setOutputLayoutManager(layoutManager)
        localAddress.setIpAddress("169.254.248.54")
        cryptoKeyring.setLocalAddress(localAddress)
        vpnConnection.setCryptoKeyring(cryptoKeyring)
        CryptoIsakmpProfile cryptoIsakmpProfile = new CryptoIsakmpProfile()
        cryptoIsakmpProfile.setInputLayoutManager(layoutManager)
        cryptoIsakmpProfile.setOutputLayoutManager(layoutManager)
        vpnConnection.setCryptoIsakmpProfile(cryptoIsakmpProfile)
        CryptoIpsecTransformSet cryptoIpsecTransformSet  = new CryptoIpsecTransformSet()
        cryptoIpsecTransformSet.setInputLayoutManager(layoutManager)
        cryptoIpsecTransformSet.setOutputLayoutManager(layoutManager)
        vpnConnection.setCryptoIpsecTransformSet(cryptoIpsecTransformSet)
        CryptoIpsecProfile cryptoIpsecProfile = new CryptoIpsecProfile()
        cryptoIpsecProfile.setInputLayoutManager(layoutManager)
        cryptoIpsecProfile.setOutputLayoutManager(layoutManager)
        vpnConnection.setCryptoIpsecProfile(cryptoIpsecProfile)
        TunnelInterface tunnelInterface = new TunnelInterface()
        tunnelInterface.setInputLayoutManager(layoutManager)
        tunnelInterface.setOutputLayoutManager(layoutManager)
        vpnConnection.addTunnelInterface(tunnelInterface)
        BgpState bgpState = new BgpState()
        bgpState.setInputLayoutManager(layoutManager)
        bgpState.setOutputLayoutManager(layoutManager)
        tunnelInterface.setBgpState(bgpState)



        vpnConnection.setVpcId("vpc-0a855e3ad5a81255e")

        cryptoKeyring.setName("keyring-vpn-research-vpc014-tun1")
        bgpState.setNeighborId("169.254.248.53")
        cryptoIsakmpProfile.setName("isakmp-vpn-research-vpc014-tun1")
        cryptoIpsecTransformSet.setName("ipsec-prop-vpn-research-vpc014-tun1")
        cryptoIpsecProfile.setName("ipsec-vpn-research-vpc014-tun1")
        cryptoIpsecTransformSet.setName("ipsec-prop-vpn-research-vpc014-tun1")
        cryptoKeyring.setPresharedKey("test014")
        tunnelInterface.setName("10014")
        tunnelInterface.setDescription("AWS Research VPC014 Tunnel1 (vpc-0a855e3ad5a81255e)")
        cryptoIpsecProfile.setName("ipsec-vpn-research-vpc014-tun1")


        vpnConnection
    }

    EnterpriseLayoutManager buildLayoutManager() {
        return new EnterpriseLayoutManager()  {
            @Override
            void init(String s, Document document) throws EnterpriseLayoutException {

            }

            @Override
            void buildObjectFromInput(Object o, XmlEnterpriseObject xmlEnterpriseObject) throws EnterpriseLayoutException {

            }

            @Override
            Object buildOutputFromObject(XmlEnterpriseObject xmlEnterpriseObject) throws EnterpriseLayoutException {
                return null
            }

            @Override
            Object buildOutputFromObject(XmlEnterpriseObject xmlEnterpriseObject, String s) throws EnterpriseLayoutException {
                return null
            }

            @Override
            void setLayoutManagerName(String s) {

            }

            @Override
            String getLayoutManagerName() {
                return null
            }

            @Override
            org.jdom.Element getLayoutRoot() {
                return null
            }

            @Override
            void setEnterpriseObjectsUri(String s) {

            }

            @Override
            String getEnterpriseObjectsUri() {
                return null
            }
        }
    }

    String _1662AWSShutdownTemplateExample = '''<native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
    <interface>
      <Tunnel>
        <name>${TunnelId}</name>
        <shutdown xc:operation="create"/>
      </Tunnel>
    </interface>
  </native>
'''

    String _1662CreateTemplateExample = '''            <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
              <crypto>
                <ios-crypto:keyring>
                  <ios-crypto:name>keyring-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>
                  <ios-crypto:vrf>AWS</ios-crypto:vrf>
                  <ios-crypto:description>${ReplaceWith_VpcId}</ios-crypto:description>
                  <ios-crypto:pre-shared-key>
                    <ios-crypto:address>
                      <ios-crypto:ipv4>
                        <ios-crypto:ipv4-addr>${ReplaceWith_RemoteVpnIpAddressA}</ios-crypto:ipv4-addr>
                        <ios-crypto:key xc:operation="create"/>
                        <ios-crypto:unencryt-key>${ReplaceWith_PresharedKeyA}</ios-crypto:unencryt-key>
                      </ios-crypto:ipv4>
                    </ios-crypto:address>
                  </ios-crypto:pre-shared-key>
                </ios-crypto:keyring>
                <ios-crypto:isakmp>
                  <ios-crypto:profile>
                    <ios-crypto:name>isakmp-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>
                    <ios-crypto:description>${ReplaceWith_VpcId}</ios-crypto:description>
                    <ios-crypto:match>
                      <ios-crypto:identity>
                        <ios-crypto:ipv4-address>
                          <ios-crypto:address>${ReplaceWith_RemoteVpnIpAddressA}</ios-crypto:address>
                          <ios-crypto:mask>255.255.255.255</ios-crypto:mask>
                          <ios-crypto:vrf>AWS</ios-crypto:vrf>
                        </ios-crypto:ipv4-address>
                      </ios-crypto:identity>
                    </ios-crypto:match>
                  </ios-crypto:profile>
                </ios-crypto:isakmp>
                <ios-crypto:ipsec>
                  <ios-crypto:transform-set xc:operation="create">
                    <ios-crypto:tag>ipsec-prop-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:tag>
                    <ios-crypto:esp>esp-aes</ios-crypto:esp>
                    <ios-crypto:key-bit>256</ios-crypto:key-bit>
                    <ios-crypto:esp-hmac>esp-sha256-hmac</ios-crypto:esp-hmac>
                    <ios-crypto:mode>
                      <ios-crypto:tunnel xc:operation="create"/>
                    </ios-crypto:mode>
                  </ios-crypto:transform-set>
                  <ios-crypto:profile xc:operation="create">
                    <ios-crypto:name>ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:name>
                    <ios-crypto:description>${ReplaceWith_VpcId}</ios-crypto:description>
                    <ios-crypto:set>
                      <ios-crypto:pfs>
                        <ios-crypto:group>group2</ios-crypto:group>
                      </ios-crypto:pfs>
                      <ios-crypto:transform-set>ipsec-prop-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:transform-set>
                      <ios-crypto:security-association>
                        <ios-crypto:lifetime>
                          <ios-crypto:seconds>3600</ios-crypto:seconds>
                        </ios-crypto:lifetime>
                      </ios-crypto:security-association>
                    </ios-crypto:set>
                  </ios-crypto:profile>
                </ios-crypto:ipsec>
              </crypto>
              <interface>
                <Tunnel>
                  <name>${RouterNumber}0${VpnId-Padded}</name>
                  <description>AWS Research VPC${VpnId-Padded} Tunnel${TunnelNumber} (${VpcId})</description>
                  <ios-tun:tunnel>
                    <ios-tun:destination>${ReplaceWith_RemoteVpnIpAddressA}</ios-tun:destination>
                    <ios-tun:protection>
                      <ios-crypto:ipsec>
                        <ios-crypto:profile>ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:profile>
                      </ios-crypto:ipsec>
                    </ios-tun:protection>
                  </ios-tun:tunnel>
                  <ip>
                    <mtu xc:operation="remove"/>
                    <tcp>
                      <adjust-mss>1387</adjust-mss>
                    </tcp>
                  </ip>
                  <shutdown xc:operation="remove"/>
                </Tunnel>
              </interface>
              <router>
                <ios-bgp:bgp>
                  <ios-bgp:id>3512</ios-bgp:id>
                  <ios-bgp:address-family>
                    <ios-bgp:with-vrf>
                      <ios-bgp:ipv4>
                        <ios-bgp:af-name>unicast</ios-bgp:af-name>
                        <ios-bgp:vrf>
                          <ios-bgp:name>AWS</ios-bgp:name>
                          <ios-bgp:neighbor>
                            <ios-bgp:id>${ReplaceWith_BgpNeighborIdA}</ios-bgp:id>
                            <ios-bgp:peer-group>
                              <ios-bgp:peer-group-name>AWS_RESEARCH_VPCs</ios-bgp:peer-group-name>
                            </ios-bgp:peer-group>
                            <ios-bgp:description>${ReplaceWith_VpcId}</ios-bgp:description>
                          </ios-bgp:neighbor>
                        </ios-bgp:vrf>
                      </ios-bgp:ipv4>
                    </ios-bgp:with-vrf>
                  </ios-bgp:address-family>
                </ios-bgp:bgp>
              </router>
            </native>
'''
    String _1662DeleteTemplateExample = '''<native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
  <router>
    <ios-bgp:bgp>
      <ios-bgp:id>3512</ios-bgp:id>
      <ios-bgp:address-family>
        <ios-bgp:with-vrf>
          <ios-bgp:ipv4>
            <ios-bgp:af-name>unicast</ios-bgp:af-name>
            <ios-bgp:vrf>
              <ios-bgp:name>AWS</ios-bgp:name>
              <ios-bgp:neighbor xc:operation="remove">
                <ios-bgp:id>${ReplaceWith_BgpNeighborIdA}</ios-bgp:id>
              </ios-bgp:neighbor>
            </ios-bgp:vrf>
          </ios-bgp:ipv4>
        </ios-bgp:with-vrf>
      </ios-bgp:address-family>
    </ios-bgp:bgp>
  </router>
  <interface>
    <Tunnel>
      <name>${TunnelId}</name>
      <description>AWS Research VPC${VpnId-Padded} Tunnel${TunnelNumber} (AVAILABLE)</description>
      <ios-tun:tunnel>
        <ios-tun:destination xc:operation="remove"></ios-tun:destination>
        <ios-tun:protection>
          <ios-crypto:ipsec>
            <ios-crypto:profile xc:operation="remove">ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}</ios-crypto:profile>
          </ios-crypto:ipsec>
        </ios-tun:protection>
      </ios-tun:tunnel>
    </Tunnel>
  </interface>
  <crypto>
    <ios-crypto:keyring>
      <ios-crypto:name>keyring-vpn-research-${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:name>
      <ios-crypto:vrf>AWS</ios-crypto:vrf>
      <ios-crypto:description xc:operation="remove"></ios-crypto:description>
      <ios-crypto:pre-shared-key>
        <ios-crypto:address>
          <ios-crypto:ipv4 xc:operation="remove">
            <ios-crypto:ipv4-addr>${ReplaceWith_RemoteVpnIpAddressA}</ios-crypto:ipv4-addr>
            <ios-crypto:key/>
            <ios-crypto:unencryt-key>${ReplaceWith_PresharedKeyA}</ios-crypto:unencryt-key>
          </ios-crypto:ipv4>
        </ios-crypto:address>
      </ios-crypto:pre-shared-key>
    </ios-crypto:keyring>
    <ios-crypto:isakmp>
      <ios-crypto:profile>
        <ios-crypto:name>isakmp-vpn-research-${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:name>
        <ios-crypto:description xc:operation="remove"></ios-crypto:description>
        <ios-crypto:match>
          <ios-crypto:identity>
            <ios-crypto:ipv4-address xc:operation="remove">
              <ios-crypto:address>${ReplaceWith_RemoteVpnIpAddressA}</ios-crypto:address>
              <ios-crypto:mask>255.255.255.255</ios-crypto:mask>
              <ios-crypto:vrf>AWS</ios-crypto:vrf>
            </ios-crypto:ipv4-address>
          </ios-crypto:identity>
        </ios-crypto:match>
      </ios-crypto:profile>
    </ios-crypto:isakmp>
    <ios-crypto:ipsec>
      <ios-crypto:transform-set xc:operation="remove">
        <ios-crypto:tag>ipsec-prop-vpn-research-${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:tag>
      </ios-crypto:transform-set>
      <ios-crypto:profile xc:operation="remove">
        <ios-crypto:name>ipsec-vpn-research-${VpnId-Padded}_VpcNum-tun${TunnelNumber}</ios-crypto:name>
      </ios-crypto:profile>
    </ios-crypto:ipsec>
  </crypto>
</native>
'''
}

