package edu.emory.it.service.ca;

import org.openeai.afa.GenericAppRunner;

public class TestStarter extends Starter {

    public static void main(String[] args) {

        TestStarter testStarter = new TestStarter();

        testStarter.doIt();
    }

    public void doIt() {

        try {

            getEnvironment(LOCAL);

            initializeTest();

            selectTest();

            startTest();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
