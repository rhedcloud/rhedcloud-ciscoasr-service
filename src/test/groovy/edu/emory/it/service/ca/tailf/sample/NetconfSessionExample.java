package edu.emory.it.service.ca.tailf.sample;

import com.tailf.jnc.*;
import edu.emory.it.service.ca.AppConfigWrapper;
import edu.emory.it.service.ca.provider.CiscoAsr1002NetconfStaticNatProvider;
import edu.emory.it.service.ca.provider.CiscoAsr1002NetconfVpnConnectionProvider;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.provider.StaticNatProvider;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.*;
import org.junit.Before;
import org.junit.Test;
import org.openeai.config.AppConfig;
import org.openeai.config.MessageObjectConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.layouts.XmlLayout;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: ##############################################################################################
// 1) Investigate vpn delete failure.
// 2) Query by VpcId
// TODO END ###########################################################################################
/**
 * Virtual Environment NETCONF credentials:
 *
 * NOTE: Secrets moved to AWS Secrets Manager.  See application for examples.
 * user: *****
 * pwd: *****
 *
 */
public class NetconfSessionExample {

    private static boolean virtualMachine = true;

    private String netconfServiceHost;
    private Integer netconfServicePort;
    private String username;
    private String password;

    private NetconfSessionExample.Logger logger;

    String bgp = "<edit-config>\n" +
            "    <target>\n" +
            "      <running/>\n" +
            "    </target>\n" +
            "    <error-option>rollback-on-error</error-option>\n" +
            "    <config>\n" +
            "      <bgp>\n" +
            "        <neighbors>\n" +
            "\t\t\t<neighbor>\n" +
            "\t\t\t  <afi-safi>vpnv4-unicast</afi-safi>\n" +
            "\t\t\t  <vrf-name>AWS</vrf-name>\n" +
            "\t\t\t  <neighbor-id>10.254.0.101</neighbor-id>\n" +
            "\t\t\t  <description>vpc-a1b2c3d4</description>\n" +
            "\t\t\t  <bgp-version>4</bgp-version>\n" +
            "\t\t\t  <link>internal</link>\n" +
            "\t\t\t  <up-time/>      <last-write/>      <last-read/>      <installed-prefixes>0</installed-prefixes>\n" +
            "\t\t\t  <session-state>fsm-nonnegotiated</session-state>\n" +
            "\t\t\t  <negotiated-keepalive-timers>\n" +
            "\t\t\t\t<hold-time>0</hold-time>\n" +
            "\t\t\t\t<keepalive-interval>0</keepalive-interval>\n" +
            "\t\t\t  </negotiated-keepalive-timers>\n" +
            "\t\t\t  <negotiated-cap/>      <bgp-neighbor-counters>\n" +
            "\t\t\t\t<sent>\n" +
            "\t\t\t\t  <opens>0</opens>\n" +
            "\t\t\t\t  <updates>0</updates>\n" +
            "\t\t\t\t  <notifications>0</notifications>\n" +
            "\t\t\t\t  <keepalives>0</keepalives>\n" +
            "\t\t\t\t  <route-refreshes>0</route-refreshes>\n" +
            "\t\t\t\t</sent>\n" +
            "\t\t\t\t<received>\n" +
            "\t\t\t\t  <opens>0</opens>\n" +
            "\t\t\t\t  <updates>0</updates>\n" +
            "\t\t\t\t  <notifications>0</notifications>\n" +
            "\t\t\t\t  <keepalives>0</keepalives>\n" +
            "\t\t\t\t  <route-refreshes>0</route-refreshes>\n" +
            "\t\t\t\t</received>\n" +
            "\t\t\t\t<inq-depth>0</inq-depth>\n" +
            "\t\t\t\t<outq-depth>0</outq-depth>\n" +
            "\t\t\t  </bgp-neighbor-counters>\n" +
            "\t\t\t  <connection>\n" +
            "\t\t\t\t<state>closed</state>\n" +
            "\t\t\t\t<mode>mode-active</mode>\n" +
            "\t\t\t\t<total-established>0</total-established>\n" +
            "\t\t\t\t<total-dropped>0</total-dropped>\n" +
            "\t\t\t\t<last-reset>never</last-reset>\n" +
            "\t\t\t\t<reset-reason/>      </connection>\n" +
            "\t\t\t  <transport>\n" +
            "\t\t\t\t<path-mtu-discovery>true</path-mtu-discovery>\n" +
            "\t\t\t\t<local-port>0</local-port>\n" +
            "\t\t\t\t<foreign-port>0</foreign-port>\n" +
            "\t\t\t\t<mss>0</mss>\n" +
            "\t\t\t  </transport>\n" +
            "\t\t\t  <prefix-activity>\n" +
            "\t\t\t\t<sent>\n" +
            "\t\t\t\t  <current-prefixes>0</current-prefixes>\n" +
            "\t\t\t\t  <total-prefixes>0</total-prefixes>\n" +
            "\t\t\t\t  <implicit-withdraw>0</implicit-withdraw>\n" +
            "\t\t\t\t  <explicit-withdraw>0</explicit-withdraw>\n" +
            "\t\t\t\t  <bestpaths>0</bestpaths>\n" +
            "\t\t\t\t  <multipaths>0</multipaths>\n" +
            "\t\t\t\t</sent>\n" +
            "\t\t\t\t<received>\n" +
            "\t\t\t\t  <current-prefixes>0</current-prefixes>\n" +
            "\t\t\t\t  <total-prefixes>0</total-prefixes>\n" +
            "\t\t\t\t  <implicit-withdraw>0</implicit-withdraw>\n" +
            "\t\t\t\t  <explicit-withdraw>0</explicit-withdraw>\n" +
            "\t\t\t\t  <bestpaths>0</bestpaths>\n" +
            "\t\t\t\t  <multipaths>0</multipaths>\n" +
            "\t\t\t\t</received>\n" +
            "\t\t\t  </prefix-activity>\n" +
            "\t\t\t</neighbor>\n" +
            "        </neighbors>\n" +
            "      </bgp>\n" +
            "    </config>\n" +
            "</edit-config>";

    public void setNetconfServiceHost(String netconfServiceHost) {
        this.netconfServiceHost = netconfServiceHost;
        System.setProperty("netconfRouterHost", this.netconfServiceHost);
    }

    public void setNetconfServicePort(Integer netconfServicePort) {
        this.netconfServicePort = netconfServicePort;
        System.setProperty("netconfRouterPort", String.valueOf(this.netconfServicePort));
    }

    public void setUsername(String username) {
        this.username = username;
        System.setProperty("adminUsername", this.username);
    }

    public void setPassword(String password) {
        this.password = password;
        System.setProperty("adminPassword", this.password);
    }

    @Before
    public void init() {
        logger = new NetconfSessionExample.Logger();
        if (virtualMachine) {
            setupEmoryVirtualMachine();
        } else {
            setupLocalhost();
        }
    }

    private void setupLocalhost() {
        setNetconfServiceHost("0.0.0.0");
        setNetconfServicePort(2022);
        setUsername("admin");
        setPassword("admin");
    }

    private void setupEmoryVirtualMachine() {
        // csrappdev1.cc.emory.edu/10.231.48.245
        // csrappdev2.cc.emory.edu/10.231.48.246
        setNetconfServiceHost("csrappdev1.cc.emory.edu"); // 10.231.48.245
        setNetconfServicePort(830);
        setUsername("*****");
        setPassword("*****");
    }


    private void setupProviderEnvironmentToLocal() {
        System.setProperty("netconfRouterHost", "0.0.0.0"); // 10.231.48.245
        System.setProperty("netconfRouterPort", "2022");
        System.setProperty("adminUsername", "admin");
        System.setProperty("adminPassword", "admin");
    }

    private NetconfSession openNetconfSession(String hostName, Integer port, String username, String password) throws Exception {
        SSHConnection c = new SSHConnection(hostName, port);
        c.authenticateWithPassword(username, password);
        SSHSession ssh = new SSHSession(c);
        NetconfSession session = new NetconfSession(ssh);

        return session;
    }

    private void closeNetconfSession(NetconfSession session) {

        if (session != null) {
            try {
                session.closeSession();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ################################# Raw NETCONF Examples using environment #################################### */
    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ############################################################################################################# */

    private void setupProviderEnvironmentToEmoryDevices() {
        System.setProperty("netconfRouterHost", "csrappdev1.cc.emory.edu"); // 10.231.48.245
        System.setProperty("netconfRouterPort", "830");
        System.setProperty("netconfRouter2Host", "csrappdev2.cc.emory.edu"); // 10.231.48.245
        System.setProperty("netconfRouter2Port", "830");
        System.setProperty("adminUsername", "*****");
        System.setProperty("adminPassword", "*****");

    }

    @Test
    public void updateBgp() throws ProviderException {

        setupProviderEnvironmentToEmoryDevices();
        NetconfSession session = null;

        try {
            session = openNetconfSession(netconfServiceHost, netconfServicePort, username, password);

            NodeSet everything;
            everything = session.get("//*[contains(.,'bgp')]");
            // Pop up notepad as a visual indicator.
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("notepad");

            Thread.sleep(1000);


            System.out.println(everything.toXMLString());

            XMLParser xmlParser = new XMLParser();
            Element bgpElement = xmlParser.parse(bgp);
            bgpElement.removePrefix("nc");

            System.out.println(bgpElement.toXMLString());

            NodeSet reply = session.callRpc(bgpElement);

            System.out.println(reply.toXMLString());

        } catch (Exception e) {
            String errMsg = "Failed to update bgp";
            throw new ProviderException(errMsg, e);
        } finally {
            closeNetconfSession(session);
        }

    }

    @Test
    public void listCapabilities() {
        setupProviderEnvironmentToEmoryDevices();

        NetconfSession session = null;

        try {
            session = openNetconfSession(netconfServiceHost, netconfServicePort, username, password);
            Capabilities capabilities = session.getCapabilities();

            printCapabilities(capabilities);

            System.out.println("Made it.");

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            closeNetconfSession(session);
        }

    }

    @Test
    public void listModuleConfiguration() {
        setupProviderEnvironmentToEmoryDevices();
        NetconfSession session = null;

        String module = "interfaces-state"; // native, interfaces-state, bgp-state-data
        try {
            session = openNetconfSession(netconfServiceHost, netconfServicePort, username, password);
            NodeSet nodeSet;
            nodeSet = session.get(module);
            System.out.println(nodeSet.toXMLString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeNetconfSession(session);
        }
    }

    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ################################### Utility Routines used by tests ########################################## */
    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ############################################################################################################# */


    //#######################################################################################################################################
//    @Test
    public void example1() {
        // Start NETCONF session towards devices
        NetconfSession session = null;
        try {

            session = openNetconfSession(netconfServiceHost, netconfServicePort, username, password);

            Capabilities capabilities = session.getCapabilities();
            String xmlMaybe = capabilities.toString();
            String[] schemes = capabilities.getUrlSchemes();
            NodeSet configs = session.getConfig("/aaa");
            // Get system configuration from session (RUNNING datastore)
            // Read is done using Xpath expression
            Element sys1 = session.getConfig("/system").first();
            // Manipulate the config tree locally
            sys1.setValue("dns", "83.100.1.1");
            sys1.setValue("gateway", "10.0.0.1");
            // Write back the updated element tree to the device
            session.editConfig(sys1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeNetconfSession(session);
        }
    }

    //    @Test
    public void printDeviceConfigAsXml() {

        NetconfSession session = null;
        try {
            session = openNetconfSession(netconfServiceHost, netconfServicePort, username, password);

            Capabilities capabilities = session.getCapabilities();
            printCapabilities(capabilities);
            printUrlSchemes(capabilities.getUrlSchemes());
            session.lock(NetconfSession.CANDIDATE);
            session.copyConfig(NetconfSession.RUNNING, NetconfSession.CANDIDATE);

            String deviceName = "native";
            NodeSet configs = session.getConfig(deviceName);

            if (configs.size() == 1) {
                // Or, specific device config should return only one node
                System.out.println("=============== Device config for " + deviceName + " ===============");
                Element deviceConfig = configs.first();
                System.out.println(deviceConfig.toXMLString());
            } else {
                System.out.println("Device config for " + deviceName + " not found");
            }
            session.unlock(NetconfSession.CANDIDATE);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeNetconfSession(session);
        }

    }

    @Test
    public void testAddPrefix() throws JNCException {
        String defaultNamespace = "http://cisco.com/ns/yang/Cisco-IOS-XE-native";

        Element element = new Element("", "native");

        addPrefix(element, "", defaultNamespace);
        addPrefix(element, "ios-crypto", "http://cisco.com/ns/yang/Cisco-IOS-XE-crypto");

        System.out.println(element.toXMLString());

    }
    //    @Test
    public void printNativeModuleConfigUsingCandidate() {

        NetconfSession session = null;
        try {
            session = openNetconfSession(netconfServiceHost, netconfServicePort, username, password);

            Capabilities capabilities = session.getCapabilities();
            printCapabilities(capabilities);
            printUrlSchemes(capabilities.getUrlSchemes());
            session.lock(NetconfSession.CANDIDATE);
            session.copyConfig(NetconfSession.RUNNING, NetconfSession.CANDIDATE);

            // To print entire device config, use overload:
            // session.getConfig(NetconfSession.CANDIDATE);

            NodeSet configs = session.getConfig(NetconfSession.CANDIDATE, "native");

            if (configs.size() > 0) {
                System.out.println("=============== Device config ================");
                System.out.println(configs.toXMLString());
            } else {
                System.out.println("No config found");
            }
            session.unlock(NetconfSession.CANDIDATE);

            // Commit here, if necessary.

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeNetconfSession(session);
        }
    }


    /** ############################################################################################################# */
    /** ############################################################################################################# */
    /** ###################################### Examples from JNC Docs ############################################### */
    /** ############################################################################################################# */
    /** ############################################################################################################# */

    public void transactionExample() {
        try {
            // Start (NETCONF) sessions towards devices
            SSHConnection c1 = new SSHConnection("127.0.0.1", 3456);
            SSHConnection c2 = new SSHConnection("10.4.5.6", 3456);
            c1.authenticateWithPassword("admin", "pass");
            c2.authenticateWithPassword("admin", "pass");
            SSHSession ssh1 = new SSHSession(c1);
            SSHSession ssh2 = new SSHSession(c2);
            NetconfSession dev1 = new NetconfSession(ssh1);
            NetconfSession dev2 = new NetconfSession(ssh2);
            // take locks on CANDIDATE datastore so that we are not interrupted
            dev1.lock(NetconfSession.CANDIDATE);
            dev2.lock(NetconfSession.CANDIDATE);
            // reset candidates so that CANDIDATE is an exact copy of running
            dev1.copyConfig(NetconfSession.RUNNING, NetconfSession.CANDIDATE);
            dev2.copyConfig(NetconfSession.RUNNING, NetconfSession.CANDIDATE);
            // Get system configuration from dev1
            Element sys1 = dev1.getConfig("/system").first();
            // Manipulate element trees locally
            sys1.setValue("dns", "83.100.1.1");
            sys1.setValue("gateway", "10.0.0.1");
            // Write back the updated element tree to both devices
            dev1.editConfig(NetconfSession.CANDIDATE, sys1);
            dev2.editConfig(NetconfSession.CANDIDATE, sys1);
            // candidates are now updated
            dev1.confirmedCommit(60);
            dev2.confirmedCommit(60);
            // now commit them
            dev1.commit();
            dev2.commit();
            dev1.unlock(NetconfSession.CANDIDATE);
            dev2.unlock(NetconfSession.CANDIDATE);

        } catch (Exception e) {

        }
    }

    private void printUrlSchemes(String[] urlSchemes) {
        System.out.println("=================== URL Schemes ===================");
        for (String urlScheme : Arrays.asList(urlSchemes)) {
            System.out.println(urlScheme);
        }
        System.out.println("================== /URL Schemes ===================");
    }

    private void printCapabilities(Capabilities capabilities) {

        /*
    public static final String NS_NETCONF = "urn:ietf:params:xml:ns:netconf:base:1.0";
    public static final String URN_IETF_PARAMS_XML_NS_NETCONF = "urn:ietf:params:xml:ns:netconf:";
    public static final String NS_NOTIFICATION = "urn:ietf:params:xml:ns:netconf:notification:1.0";
    public static final String NS_PARTIAL_LOCK = "urn:ietf:params:xml:ns:netconf:partial-lock:1.0";
    public static final String URN_IETF_PARAMS = "urn:ietf:params:";
    public static final String NETCONF_BASE_CAPABILITY = "urn:ietf:params:netconf:base:1.0";
    public static final String WRITABLE_RUNNING_CAPABILITY = "urn:ietf:params:netconf:capability:writable-running:1.0";
    public static final String URN_IETF_PARAMS_NETCONF = "urn:ietf:params:netconf:";

         */
        boolean capability;

        System.out.println("=================== Capabilities ===================");
        capability = capabilities.hasWritableRunning(); {
            System.out.print("writable");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasCandidate(); {
            System.out.print("candidate data store");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasConfirmedCommit(); {
            System.out.print("confirmed commit");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasRollbackOnError(); {
            System.out.print("rollback on error");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasValidate(); {
            System.out.print("validate");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasStartup(); {
            System.out.print("startup");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasNotification(); {
            System.out.print("notification");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasInterleave(); {
            System.out.print("interleave");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasUrl(); {
            System.out.print("url");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasXPath(); {
            System.out.print("xPath");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasPartialLock(); {
            System.out.print("partial lock");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasActions(); {
            System.out.print("actions");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasTransactions(); {
            System.out.print("transactions");
            System.out.println(capability ? " enabled" : " disabled");
        }

        capability = capabilities.hasWithDefaults(); {
            System.out.print("with defaults");
            System.out.println(capability ? " enabled" : " disabled");
        }

        System.out.println("=================== /Capabilities ===================");

    }

    private class Logger {
        public void info(String message) {
            System.out.println (message);
        }

        public void error(String message) {
            System.out.println(message);
        }

        public void error(String message, Exception e) {
            System.out.println(message + "\n");
            e.printStackTrace();
        }
    }

    private void addPrefix(Element element, String prefix, String namespace) throws JNCException {

        PrefixMap prefixMap = element.getContextPrefixMap();

        if (!prefixMap.contains(prefix)) {
            Prefix prefixObject = new Prefix(prefix, namespace);
            prefixMap.merge(prefixObject);
            element.setPrefix(prefixMap);
        }

    }
}
