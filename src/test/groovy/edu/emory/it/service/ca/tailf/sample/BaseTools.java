package edu.emory.it.service.ca.tailf.sample;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;
import com.tailf.jnc.JNCException;
import com.tailf.jnc.NetconfSession;
import com.tailf.jnc.SSHConnection;
import com.tailf.jnc.SSHSession;
import edu.emory.it.service.ca.AppConfigWrapper;
import edu.emory.it.service.ca.provider.ProviderException;
import edu.emory.it.service.ca.tailf.sample.log.Logger;
import edu.emory.it.service.ca.util.XmlUtils;
import org.apache.commons.lang.StringUtils;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.MessageObjectConfig;
import org.openeai.config.ProducerConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.utils.lock.DbLockConfig;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseTools {

    protected Logger logger = new Logger();

    String netconfAdminUsername = null;
    String netconfAdminPassword = null;

    protected Router router;

    private static final String testDirectory = "/deploy/build-test";
    protected static final String jndiFile = "deploy/build-test/libs/CiscoAsrService/jndi.properties";


    abstract  protected String getLogtag();

    public void template() {
        Router router = Router.ROUTER1;
        NetconfSession session = null;
        try {
            session = openNetconfSession(router);
            session.lock(NetconfSession.RUNNING);

            logger.info("Do something here...");

        } catch (Exception e) {
            logger.error("Failed ", e);
        } finally {
            unlockDatastore(session, NetconfSession.RUNNING);
            closeNetconfSession(session);
        }
    }

    /**
     * A little "slight of hand" to fabricate a usable AppConfig object
     *
     * 1) Read config doc and parse out all PropertyConfig and MessageObject configuration objects
     * 2) Hand them to an AppConfig wrapper that adds these as general objects in the AppConfig object collection
     * 3) Return AppConfigWrapper that is now usable by a Provider instance
     *
     * See AppConfigWrapper
     *
     * @return
     */
    public static AppConfig createAppConfigFromConfigObjects(String commandName, String routerNumber) {
        String deploymentDescriptor = "configs/CiscoAsr" + routerNumber + "Service.xml";
        XmlUtils xmlUtils = new XmlUtils();

        String cwd = System.getProperty("user.dir");
        System.setProperty("user.dir", cwd + testDirectory);

        List<PropertyConfig> propertyConfigs = xmlUtils.getCommandPropertyConfigList(deploymentDescriptor, commandName);
        List<ProducerConfig> producerConfigs = xmlUtils.getCommandProducerConfigList(deploymentDescriptor, "CiscoAsrNotificationSyncCommand");
        List<MessageObjectConfig> messageObjectConfigs = xmlUtils.getAllMessageObjectConfigs(deploymentDescriptor, commandName);
        List<DbLockConfig> dbLockConfigs = xmlUtils.getAllDbLockConfigs(deploymentDescriptor);

        AppConfigWrapper wrapper = new AppConfigWrapper(propertyConfigs, messageObjectConfigs, dbLockConfigs, producerConfigs);

        return wrapper;
    }

    public Map<String, String> getRouterHostAndPort(Router router) {
        final String routerHostPattern = "(<PropertyName>netconfRouterHost</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
        final String routerPortPattern = "(<PropertyName>netconfRouterPort</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";

        final String appConfig1Path = "/configs/CiscoAsr1Service.xml";
        final String appConfig2Path = "/configs/CiscoAsr2Service.xml";

        String cwd = System.getProperty("user.dir");

        Path configFilePath = router.equals(Router.ROUTER1) ? getPath(appConfig1Path) : getPath(appConfig2Path);

        logger.info("Config file path: " + configFilePath.toString());

        String appConfig = readLines(configFilePath);

        Map<String, String> routerAndPortMap = new HashMap<>();
        Pattern pattern = Pattern.compile(routerHostPattern);
        Matcher matcher = pattern.matcher(appConfig);


        if (matcher.find()) {
            routerAndPortMap.put("routerHost", matcher.group(2));
        }

        pattern = Pattern.compile(routerPortPattern);
        matcher = pattern.matcher(appConfig);

        if (matcher.find()) {
            routerAndPortMap.put("routerPort", matcher.group(2));
        }

        return routerAndPortMap;
    }

    protected String readLines(Path path) {
        String lines;
        try {
            lines = new String(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    protected Path getPath(String configFile) {
        String pwd = System.getProperty("user.dir");

        if (!new File(configFile).exists()) {
            configFile = pwd + configFile;
        }

        Path configFilePath = FileSystems.getDefault().getPath(configFile);
        return configFilePath;
    }

    public static AppConfig createAppConfigFromConfigObjects(String commandName) {
        return createAppConfigFromConfigObjects(commandName, "1");
    }


    protected void getRouterCredentials(AppConfig appConfig) throws ProviderException {

        String accessKeyId;
        String secretAccessKey;
        String secretName;
        String region;

        try {
            Properties generalProperties = appConfig.getProperties("GeneralProperties");
            secretAccessKey = generalProperties.getProperty("routerCredentialsSecretKey");
            accessKeyId = generalProperties.getProperty("routerCredentialsAccessKey");
            secretName = generalProperties.getProperty("routerCredentialsSecretName");
            region = generalProperties.getProperty("awsRegion", "us-east-1");

            if (StringUtils.isEmpty(accessKeyId) || StringUtils.isEmpty(secretAccessKey) || StringUtils.isEmpty(secretName)) {
                String errMsg = "Router configuration secret values incomplete or missing entirely";
                logger.error(getLogtag() + errMsg);
                throw new ProviderException(errMsg);
            }
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Failed to get general properties from app config";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);

        GetSecretValueResult getSecretValueResult = null;

        try {
            logger.info(getLogtag() + "Attempting to get secret value using access key and secret key...");

            BasicAWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
            ;

            AWSSecretsManager clientWithCredentials = AWSSecretsManagerClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(region)
                    .build();

            getSecretValueResult = clientWithCredentials.getSecretValue(getSecretValueRequest);

        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            String errMsg = "Internal service error exception in AWS Secrets Manager";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            String errMsg = "Invalid parameter exception interacting with AWS Secrets Manager";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            String errMsg = "Invalid request exception interacting with AWS Secrets Manager";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            String errMsg = "Resource not found exception interacting with AWS Secrets Manager";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

        try {
            StringReader stringReader = new StringReader(getSecretValueResult.getSecretString());
            Properties routerCredentials = new Properties();
            routerCredentials.load(stringReader);

            netconfAdminUsername = routerCredentials.getProperty("router.username");
            netconfAdminPassword = routerCredentials.getProperty("router.password");

            logger.info(getLogtag() + "Successfully retrieved router credentials from AWS Secrets manager");

        } catch (IOException e) {
            String errMsg = "IO Exception while loading properties from secret value result";
            logger.error(getLogtag() + errMsg, e);
            throw new ProviderException(errMsg, e);
        }

    }

    protected String getNetconfAdminUsername() {
        return netconfAdminUsername;
    }

    protected String getNetconfAdminPassword() {
        return netconfAdminPassword;
    }

    protected NetconfSession openNetconfSession(){
        NetconfSession session = null;
        if (router == null) {
            router = Router.ROUTER1;
        }
        session = openNetconfSession(router);
        return session;
    }

    protected NetconfSession openNetconfSession(Router router){

        String netconfServiceHost;
        int netconfServicePort;

        switch (router.tunnelNumber) {
            case "1":
                netconfServiceHost = "csrappdev1.cc.emory.edu";
                netconfServicePort = 830;
                netconfAdminUsername = netconfAdminUsername;
                netconfAdminPassword = netconfAdminPassword;


                break;
            case "2":
                netconfServiceHost = "csrappdev2.cc.emory.edu";
                netconfServicePort = 830;
                netconfAdminUsername = netconfAdminUsername;
                netconfAdminPassword = netconfAdminPassword;

                break;
            default:
                throw new RuntimeException("Invalid tunnel number: " + router.tunnelNumber);
        }

        NetconfSession session = null;
        try {
            SSHConnection c = new SSHConnection(netconfServiceHost, netconfServicePort);
            c.authenticateWithPassword(netconfAdminUsername, netconfAdminPassword);
            SSHSession ssh = new SSHSession(c);
            session = new NetconfSession(ssh);
            session.setErrorOption(session.ROLLBACK_ON_ERROR);
        } catch (IOException e) {
            String errMsg = "I/O Exception opening netconf session to host: " + netconfServiceHost + " port: " + netconfAdminPassword + " for admin user: " + netconfAdminUsername;
            logger.error(getLogtag() + errMsg, e);
            throw new RuntimeException(errMsg, e);
        } catch (JNCException e) {
            String errMsg = "Java Netconf Client Exception opening netconf session to host: " + netconfServiceHost + " port: " + netconfAdminPassword + " for admin user: " + netconfAdminUsername;
            logger.error(getLogtag() + errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        return session;
    }

    protected void unlockDatastore(NetconfSession netconfSession, int datastoreId){
        String datastoreName;
        switch (datastoreId) {
            case NetconfSession.RUNNING:
                datastoreName = "RUNNING";
                break;
            case NetconfSession.STARTUP:
                datastoreName = "STARTUP";
                break;
            case NetconfSession.CANDIDATE:
                datastoreName = "CANDIDATE";
                break;
            default:
                throw new RuntimeException("Unknown datastore id: " + datastoreId);
        }
        try {
            if (null != netconfSession) {
                netconfSession.unlock(datastoreId);
            }
        } catch (JNCException e) {
            String errMsg = "Java Netconf Client Exception unlocking datastore " + datastoreName;
            logger.warn(getLogtag() + errMsg, e);
        } catch (IOException e ) {
            String errMsg = "I/O Exception unlocking datastore " + datastoreName;
            logger.warn(getLogtag() + errMsg, e);
        }
    }

    protected void closeNetconfSession(NetconfSession netconfSession){
        try {
            if (null != netconfSession) {
                netconfSession.closeSession();
            }
        } catch (JNCException e) {
            String errMsg = "Java Netconf Client Exception closing the netconf session";
            logger.error(getLogtag() + errMsg, e);
            throw new RuntimeException(errMsg, e);
        } catch (IOException e ) {
            String errMsg = "I/O Exception closing the netconf session";
            logger.error(getLogtag() + errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
    }

    protected String labRemoteVpnIpAddress = "72.21.209.225";

    protected enum Router {
        ROUTER1("Cisco ASR1002 Router1", "1"),
        // With Orchestration service refactor, the tunnel index is ALWAYS 0
        ROUTER2("Cisco ASR1002 Router2", "2"),
        BOTH("Both", "");

        private String name;
        private String tunnelNumber;

        Router(String name, String tunnelNumber) {
            this.name = name;
            this.tunnelNumber = tunnelNumber;
        }

        public String getName() {
            return name;
        }

        public String getTunnelNumber() {
            return tunnelNumber;
        }

    }

}
