package edu.emory.it.service.ca.tailf.sample.log;

import edu.emory.moa.jmsobjects.network.v1_0.VpnConnection;
import edu.emory.moa.objects.resources.v1_0.*;

public class VpnConnectionDump extends Dump {

    final private VpnConnection vpnConnection;

    public VpnConnectionDump(VpnConnection vpnConnection) {
        this.vpnConnection = vpnConnection;
    }


    public void dump() {

        dumpHeader("VpnConnection");
        dump("vpcId: " + vpnConnection.getVpcId());
        dump("vpnId: " + vpnConnection.getVpnId());

        CryptoKeyring cryptoKeyring = vpnConnection.getCryptoKeyring();
        dumpCryptoKeyring(cryptoKeyring);

        CryptoIsakmpProfile cryptoIsakmpProfile = vpnConnection.getCryptoIsakmpProfile();

        if (cryptoIsakmpProfile != null) {
            dumpHeader("CryptoIsakmpProfile values");

            if (cryptoIsakmpProfile.getName() == null
                    && cryptoIsakmpProfile.getDescription() == null
                    && cryptoIsakmpProfile.getVirtualRouteForwarding() == null
                    && cryptoIsakmpProfile.getCryptoKeyring() == null
                    && cryptoIsakmpProfile.getMatchIdentity() == null) {
                dumpNoValues("CRYPTO ISAKMP PROFILE");
            } else {
                dump("Name: " + cryptoIsakmpProfile.getName());
                dump("Description: " + cryptoIsakmpProfile.getDescription());
                dump("Vrf: " + cryptoIsakmpProfile.getVirtualRouteForwarding());

                cryptoKeyring = cryptoIsakmpProfile.getCryptoKeyring();
                increaseIndent();
                dumpCryptoKeyring(cryptoKeyring);

                /*
                  <match>
                    <identity>
                      <address>
                        <ip>72.21.209.225</ip>
                        <mask>255.255.255.255</mask>
                        <vrf>AWS</vrf>
                      </address>
                      <address>
                        <ip>169.254.0.1</ip>
                        <mask>255.255.255.255</mask>
                        <vrf>AWS</vrf>
                      </address>
                    </identity>
                  </match>
                 */
                MatchIdentity matchIdentity = cryptoIsakmpProfile.getMatchIdentity();
                if (matchIdentity != null) {
//                    matchIdentity.get
                    dumpHeader("MatchIdentity values");
                    dump("Ip Address: " + matchIdentity.getIpAddress());
                    dump("Netmask: " + matchIdentity.getNetmask());
                    dump("Vrf: " + matchIdentity.getVirtualRouteForwarding());
                } else {
                    dump("No match identity");
                }
            }

        }

        decreaseIndent();

        CryptoIpsecTransformSet cryptoIpsecTransformSet = vpnConnection.getCryptoIpsecTransformSet();

        if (cryptoIpsecTransformSet != null) {
            dumpHeader("CryptoIpsecTransformSet values");
            if (cryptoIpsecTransformSet.getName() == null
                    && cryptoIpsecTransformSet.getBits() == null
                    && cryptoIpsecTransformSet.getCipher() == null
                    && cryptoIpsecTransformSet.getMode() == null) {
                dumpNoValues("CRYPTO IPSEC TRANSFORM SET");
            } else {
                dump("Name: " + cryptoIpsecTransformSet.getName());
                dump("Bits: " + cryptoIpsecTransformSet.getBits());
                dump("Cipher: " + cryptoIpsecTransformSet.getCipher());
                dump("Mode: " + cryptoIpsecTransformSet.getMode());
            }
        } else {
            dump("No crypto ipsec transform set");
        }

        CryptoIpsecProfile cryptoIpsecProfile = vpnConnection.getCryptoIpsecProfile();

        dumpCryptoIpsecProfile(cryptoIpsecProfile);

        TunnelInterface tunnelInterface = vpnConnection.getTunnelInterface(0);
        if (tunnelInterface != null) {
            dumpHeader("TunnelInterface values");
            if (tunnelInterface.getName() == null
                    && tunnelInterface.getDescription() == null
                    && tunnelInterface.getVirtualRouteForwarding() == null
                    && tunnelInterface.getIpAddress() == null
                    && tunnelInterface.getNetmask() == null
                    && tunnelInterface.getTcpMaximumSegmentSize() == null
                    && tunnelInterface.getTunnelSource() == null
                    && tunnelInterface.getTunnelMode() == null
                    && tunnelInterface.getTunnelDestination() == null
                    && tunnelInterface.getIpVirtualReassembly() == null
                    && tunnelInterface.getAdministrativeState() == null
                    && tunnelInterface.getOperationalStatus() == null
                    && tunnelInterface.getCryptoIpsecProfile() == null
                    && tunnelInterface.getBgpState() == null
                    && tunnelInterface.getBgpPrefixes() == null) {
                dumpNoValues("TUNNEL INTERFACE");
            } else {
                dump("Name: " + tunnelInterface.getName());
                dump("Description: " + tunnelInterface.getDescription());
                dump("Vrf: " + tunnelInterface.getVirtualRouteForwarding());
                dump("IpAddress: " + tunnelInterface.getIpAddress());
                dump("Netmask: " + tunnelInterface.getNetmask());
                dump("TcpMaximumSegmentSize: " + tunnelInterface.getTcpMaximumSegmentSize());
                dump("TunnelSource: " + tunnelInterface.getTunnelSource());
                dump("TunnelMode: " + tunnelInterface.getTunnelMode());
                dump("TunnelDestination: " + tunnelInterface.getTunnelDestination());
                dump( "IpVirtualReassembly: " + tunnelInterface.getIpVirtualReassembly());
                dump("AdminstrativeState: " + tunnelInterface.getAdministrativeState());
                dump("OperationalStatus: " + tunnelInterface.getOperationalStatus());

                cryptoIpsecProfile = tunnelInterface.getCryptoIpsecProfile();

                increaseIndent();
                dumpCryptoIpsecProfile(cryptoIpsecProfile);

                BgpState bgpState = tunnelInterface.getBgpState();
                if (bgpState != null) {
                    dumpHeader("BgpState values");
                    if (bgpState.getStatus() == null
                            && bgpState.getUptime() == null) {
                        dumpNoValues("BGP STATE");
                    } else {
                        dump("Status: " + bgpState.getStatus());
                        dump("Uptime: " + bgpState.getUptime());
                        dump("NeighborId: " + bgpState.getNeighborId());
                    }
                }
                BgpPrefixes bgpPrefixes = tunnelInterface.getBgpPrefixes();
                if (bgpPrefixes != null) {
                    dumpHeader("BgpPrefixes values");
                    if (bgpPrefixes.getSent() == null
                            && bgpPrefixes.getReceived() == null) {
                        dumpNoValues("BGP PREFIXES");
                    } else {
                        dump("Sent: " + bgpPrefixes.getSent());
                        dump("Received: " + bgpPrefixes.getReceived());
                    }
                }
            }
        }
    }

    private void dumpCryptoKeyring(CryptoKeyring cryptoKeyring) {
        if (cryptoKeyring != null) {
            dumpHeader("CryptoKeyring values");
            if (cryptoKeyring.getName() == null
                    && cryptoKeyring.getDescription() == null
                    && cryptoKeyring.getPresharedKey() == null
                    && cryptoKeyring.getLocalAddress() == null) {
                dumpNoValues("CRYPTO KEYRING");
            } else {
                dump("Name: " + cryptoKeyring.getName());
                dump("Descripton: " + cryptoKeyring.getDescription());
                dump("Preshared Key: " + cryptoKeyring.getPresharedKey());

                LocalAddress localAddress = cryptoKeyring.getLocalAddress();
                if (localAddress != null) {
                    dump("LocalAdress values");
                    dump("IpAddress: " + localAddress.getIpAddress());
                    dump("vrf: " + localAddress.getVirtualRouteForwarding());
                } else {
                    dump("no crypto keyring local address");
                }
            }
        } else {
            dump("no crypto keyring");
        }

    }

    private void dumpCryptoIpsecProfile(CryptoIpsecProfile cryptoIpsecProfile) {
        if (cryptoIpsecProfile != null) {
            if (cryptoIpsecProfile.getName() == null
                    && cryptoIpsecProfile.getDescription() == null
                    && cryptoIpsecProfile.getPerfectForwardSecrecy() == null) {
                dumpNoValues("CRYPTO IPSEC PROFILE");
            } else {
                dumpHeader("CryptoIpsecProfile values");
                dump("Name: " + cryptoIpsecProfile.getName());
                dump("Description: " + cryptoIpsecProfile.getDescription());
                dump("PerfectForwardSecrecy: " + cryptoIpsecProfile.getPerfectForwardSecrecy());
            }
        } else {
            dump("No crypto ipsec profile");
        }

    }


}

