package edu.emory.it.service.ca;

import org.openeai.config.AppConfig;
import org.openeai.config.MessageObjectConfig;
import org.openeai.config.ProducerConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.utils.lock.DbLock;
import org.openeai.utils.lock.DbLockConfig;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class AppConfigWrapper extends AppConfig {

    private Hashtable<String, Object> objects;

    public AppConfigWrapper(List<PropertyConfig> propertyConfigs, List<MessageObjectConfig> messageObjectConfigs, List<DbLockConfig> dbLockConfigs, List<ProducerConfig> producerConfigs) {
        super();
        setPropertyConfigs(propertyConfigs);
        setMessageObjectConfigs(messageObjectConfigs);
        setProducerConfigs(producerConfigs);
        //setDbLockConfigs(dbLockConfigs);
    }

    private void setPropertyConfigs(List<PropertyConfig> propertyConfigs) {
        objects = new Hashtable<>();

        propertyConfigs.forEach(propertyConfig -> {
            objects.put(propertyConfig.getName().toLowerCase(), propertyConfig);
        });
    }

    private void setDbLockConfigs(List<DbLockConfig> dbLockConfigs) {
        dbLockConfigs.forEach(dbLockConfig -> {
            DbLock dbLock;
            try {
                dbLock = new DbLock(dbLockConfig);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to instantiate db lock " + dbLockConfig.getName());
            }
            objects.put(dbLockConfig.getName().toLowerCase(), dbLock);
        });
    }

    private void setProducerConfigs(List<ProducerConfig> producerConfigs) {

        producerConfigs.forEach(producerConfig -> {
            MessageProducer messageProducer = null;
            if (producerConfig.getProperties().getProperty("ObjectClass").contains("PointToPointProducer")) {
                try {
                    messageProducer = new PointToPointProducer(producerConfig);
                } catch (Exception e) {
                    // TODO: Log
                }
            } else  if (producerConfig.getProperties().getProperty("ObjectClass").contains("PubSubProducer")) {
                try {
                    messageProducer = new PubSubProducer(producerConfig);
                } catch (Exception e) {
                    // TODO: Log
                }
            }
            if (messageProducer != null) {
                List<MessageProducer> producers = new ArrayList<>();
                ProducerPool producerPool = new ProducerPool();
                producers.add(messageProducer);
                producerPool.setProducers(producers);
                objects.put(producerConfig.getName().toLowerCase(), producerPool);
            }
        });
    }

    private void setMessageObjectConfigs(List<MessageObjectConfig> messageObjectConfigElements) {

        messageObjectConfigElements.forEach(messageObjectConfig -> {
            String commandName = (String)messageObjectConfig.getProperties().get("CommandName");;
            String objectClass = (String)messageObjectConfig.getProperties().get("ObjectClass");;

            try {
                Class<?> messageObjectClass = Class.forName(objectClass);
                Class<?>[] classes = new Class<?>[]{messageObjectConfig.getClass()};
                Constructor<?> messageObjectConstructor = messageObjectClass.getConstructor(classes);
                Object[] messageObjectConfigInstance = new Object[]{messageObjectConfig};
                Object messageObject = messageObjectConstructor.newInstance(messageObjectConfigInstance);
                objects.put(commandName.toLowerCase(), messageObject);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Error trying to create object", e);
            }
        });
    }

    /**
     * Override getObjects() to return our property/message object configs. Perfect for provider unit testing.
     *
     * @return Our objects - a hash table of PropertyConfigs from deployment descriptor.
     */
    @Override
    public Hashtable<String, Object> getObjects() {
        return this.objects;
    }
}
