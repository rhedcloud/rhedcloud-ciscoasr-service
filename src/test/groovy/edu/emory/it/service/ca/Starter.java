package edu.emory.it.service.ca;

import edu.emory.it.service.ca.util.TestInitialContextFactoryBuilder;
import org.openeai.afa.GenericAppRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Starter {

    final protected String LOCAL = "/deploy/build-test";
    final protected String DEV = "/deploy/esb-dev";

    final protected String APP1_PROPERTIES_FILE_KEY = "appPropertiesFile";
    final protected String APP2_PROPERTIES_FILE_KEY = "app2PropertiesFile";
    final protected String TEST_PROPERTIES_FILE_KEY = "testPropertiesFile";
    final protected String TEST_JNDI_PROPERTIES_FILE_KEY = "testJndiPropertiesFile";
    final protected String TEST_SUITE_FILE_NAME_ROOT = "TestSuite-CiscoAsrService";

    protected Path testSuitePlaceHolderPath;

    protected String testDirectory;
    private boolean jndiFileProvided = false;
    private Integer serviceNumber = null;
    protected Map<String, File> propertyFilesMap;

    protected void initializeTest() {
        buildPropertiesFileMapForTests();
        setTestWorkingDirectory();

//        if (testDirectory.equals(LOCAL)) {
//            // Rebind jndi properties using props file in deploy/build-test.
//            // Note that java.naming.provider.url in jndi properties file
//            // must match deployment descriptor ProviderUrl config value.
//            TestInitialContextFactoryBuilder.setInitialContextFactoryBuilder(propertyFilesMap.get(TEST_JNDI_PROPERTIES_FILE_KEY));
//        }
    }

    protected void initializeApp() {
        buildPropertiesFileMapForApp();
        setTestWorkingDirectory();

//        if (testDirectory.equals(LOCAL)) {
//            TestInitialContextFactoryBuilder.setInitialContextFactoryBuilder(propertyFilesMap.get(TEST_JNDI_PROPERTIES_FILE_KEY));
//        }

    }

    protected void setTestWorkingDirectory() {

        String cwd = System.getProperty("user.dir");
        System.setProperty("user.dir", cwd + testDirectory);

    }

    private File getApp1PropertiesFile() {
        return propertyFilesMap.get(APP1_PROPERTIES_FILE_KEY);
    }

    private File getApp2PropertiesFile() {
        return propertyFilesMap.get(APP2_PROPERTIES_FILE_KEY);
    }

    private File getTestPropertiesFile() {
        return propertyFilesMap.get(TEST_PROPERTIES_FILE_KEY);
    }

    public void startApp() {

        String[] args1 = {
                getApp1PropertiesFile().getPath()
        };

        String[] args2 = {
                getApp2PropertiesFile().getPath()
        };

        ExecutorService executor
                = Executors.newFixedThreadPool(2);

        executor.submit(() -> {
            try {
                new org.openeai.jms.consumer.MessageConsumerClient(args1);
            } catch (Exception e) {
                log("Error executing MessageConsumerClient for CiscoAsr1Service");
            }
        });

        executor.submit(() -> {
            try {
                new org.openeai.jms.consumer.MessageConsumerClient(args2);
            } catch (Exception e) {
                log("Error executing MessageConsumerClient for CiscoAsr2Service");
            }
        });

        log("Test stopping...");
    }

    public void startTest() {
        String[] args = {
                getTestPropertiesFile().getPath()
        };

        new GenericAppRunner(args);
    }

    protected void buildPropertiesFileMapForTests() {

        String testPropertiesFilePath;
        String testJndiFilePath = testDirectory + "/libs/CiscoAsrService/jndi.properties";
        String testSuitesDir;

        // Running tests.
        testPropertiesFilePath = testDirectory + "/props/TestSuiteApplication-CiscoAsrService.properties";
        String cwd = System.getProperty("user.dir");

         testSuitesDir = testDirectory + "/tests/";

        testSuitePlaceHolderPath = FileSystems.getDefault().getPath(cwd + testSuitesDir + TEST_SUITE_FILE_NAME_ROOT + ".xml");

        propertyFilesMap = new HashMap<>();
        propertyFilesMap.put("testPropertiesFile", new File(cwd + testPropertiesFilePath));
//        if (testDirectory.equals(LOCAL)) {
//            this.jndiFileProvided = true;
//            propertyFilesMap.put("testJndiPropertiesFile", new File(cwd + testJndiFilePath));
//        }
        propertyFilesMap.put("testSuitesDirPath", new File(cwd + testSuitesDir));

        StringBuilder builder = new StringBuilder();
        propertyFilesMap.forEach((k, v ) -> {
            if (!v.exists()) {
                builder.append(k + " does not exist: " + v + "\n");
            }
        });
        if (builder.length() > 0) {
            throw new IllegalStateException("Missing property files: \n\n" + builder.toString());
        }
    }

    protected void buildPropertiesFileMapForApp() {

        String app1PropertiesFilePath;
        String app2PropertiesFilePath;
        String testJndiFilePath = testDirectory + "/libs/CiscoAsrService/jndi.properties";

        app1PropertiesFilePath = testDirectory + "/props/CiscoAsr1Service.properties";
        app2PropertiesFilePath = testDirectory + "/props/CiscoAsr2Service.properties";

        this.serviceNumber = serviceNumber;

        String cwd = System.getProperty("user.dir");

        propertyFilesMap = new HashMap<>();
        propertyFilesMap.put(APP1_PROPERTIES_FILE_KEY, new File(cwd + app1PropertiesFilePath));
        propertyFilesMap.put(APP2_PROPERTIES_FILE_KEY, new File(cwd + app2PropertiesFilePath));

//        if (testDirectory.equals(LOCAL)) {
//            this.jndiFileProvided = true;
//            propertyFilesMap.put("testJndiPropertiesFile", new File(cwd + testJndiFilePath));
//        }

        StringBuilder builder = new StringBuilder();
        propertyFilesMap.forEach((k, v ) -> {
            if (!v.exists()) {
                builder.append(k + " does not exist: " + v + "\n");
            }
        });
        if (builder.length() > 0) {
            throw new IllegalStateException("Missing property files: \n\n" + builder.toString());
        }
    }

    protected void log(String message) {
        System.out.println(message);
    }

    protected void closeReader(BufferedReader br) {
        if (br != null) {
            try {
                br.reset();
                br.close();

            } catch (IOException e) {
                String message = e.getMessage();
                if (!message.equals("Stream not marked")) {
                    log("\n\nError closing buffered reader: " + message);
                }
            }
        }
    }

    protected boolean showPropertiesAndGetApproval() {
        boolean approve = false;
        BufferedReader br = null;

        if (this.serviceNumber != null) {
            log("\nService: CiscoAsr" + serviceNumber + "Service");
        }
        log("Env: " + (testDirectory.equals(LOCAL) ? "LOCAL" : "DEV"));
        log("JNDI properties being used? " + (jndiFileProvided ? "YES" : "NO"));
        log("");
        try {
            br = new BufferedReader(new InputStreamReader(System.in));

            do {
                System.out.print("Continue (Y/N)? ");
                String input = br.readLine();
                input = input.trim();

                if (input.equalsIgnoreCase("Y")) {
                    approve = true;
                    break;
                } else if (input.equalsIgnoreCase("N")) {
                    log("Terminated by request.");
                    break;
                }
            } while (true);

        } catch (IOException e) {
            log("Error trying to read task input: " + e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.reset();
                    br.close();

                } catch (IOException e) {
                    String message = e.getMessage();
                    if (!message.equals("Stream not marked")) {
                        log("\n\nError closing buffered reader: " + message);
                    }
                }
            }
        }

        return approve;
    }

    public void setServiceNumber(Integer serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    protected void getServiceNumber() {
        serviceNumber = -1;

        BufferedReader br = null;
        try {

            do {
                br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("\nService Number (1 or 2): ");
                String input = br.readLine();
                input = input.trim();

                if (input.equals("1") || input.equals("2")) {
                    serviceNumber = Integer.parseInt(input);
                    break;
                } else if (input.equals("")) {
                    break;
                }
            } while (true);

        } catch (IOException e) {
            log("Error trying to read service number input: " + e.getMessage());
        } finally {
            closeReader(br);
        }

        if (serviceNumber < 1) {
            System.out.println("Terminated by request.");
            System.exit(1);
        }

    }

    protected void getEnvironment(String environment) {

        if (environment != null && environment.length() > 0) {
            testDirectory = environment;
        } else {
            testDirectory = null;

            BufferedReader br = null;
            try {

                do {
                    br = new BufferedReader(new InputStreamReader(System.in));
                    System.out.print("\nSelect environment (L)ocal or (D)EV: ");
                    String input = br.readLine();
                    input = input.trim();

                    if (input.equalsIgnoreCase("L") || input.equalsIgnoreCase("D")) {
                        switch (input.toUpperCase()) {
                            case "L":
                                testDirectory = LOCAL;
                                break;
                            case "D":
                                testDirectory = DEV;
                                break;
                        }
                        break;
                    } else if (input.equals("")) {
                        break;
                    }
                } while (true);

            } catch (IOException e) {
                log("Error trying to read environment input: " + e.getMessage());
            } finally {
                closeReader(br);
            }
        }

        if (testDirectory == null) {
            System.out.println("Terminated by request.");
            System.exit(1);
        }
    }

    protected boolean selectTest() {
        boolean approve = false;
        BufferedReader br = null;
        FileSystem fileSystem = FileSystems.getDefault();
        DirectoryStream<Path> testSuitePaths;
        Path testPath = fileSystem.getPath(propertyFilesMap.get("testSuitesDirPath").getPath());
        try {
            testSuitePaths = Files.newDirectoryStream(testPath, TEST_SUITE_FILE_NAME_ROOT + "-*.xml");
        } catch (IOException e) {
            throw new RuntimeException("Failed to list test suite files", e);
        }

        Integer n = 1;
        Map<Integer, Path> fileMap = new HashMap<>();
        for (Path path : testSuitePaths) {
            fileMap.put(n++, path);
        }

        Integer testCount = fileMap.size();

        try {
            br = new BufferedReader(new InputStreamReader(System.in));

            do {
                System.out.println();
                fileMap.forEach((k, v) -> {
                    System.out.println(k + ") " + v.getFileName());
                });
                System.out.print("\nSelect test (Press ENTER for minimal test): ");

                String input = br.readLine();
                input = input.trim();
                Integer selection;
                try {
                    selection = Integer.parseInt(input);
                } catch (NumberFormatException e) {
                    selection = -1;

                    // Use default.
                    System.out.println("\n\nUsing minimal test...");
                    break;
                }

                if (selection > 0 && selection <= testCount) {
                    copyTest(fileMap.get(selection));
                    break;
                } else if (input.length() == 0) {
                    log("Terminated by request.");
                    System.exit(1);
                }
            } while (true);

        } catch (IOException e) {
            log("Error trying to read task input: " + e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.reset();
                    br.close();

                } catch (IOException e) {
                    String message = e.getMessage();
                    if (!message.equals("Stream not marked")) {
                        log("\n\nError closing buffered reader: " + message);
                    }
                }
            }
        }

        return approve;
    }

    private void copyTest(Path selectedTest) {
        log("Copying test: " + selectedTest);

        try {
            Files.copy(selectedTest, testSuitePlaceHolderPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log("Failed to copy " + selectedTest + " over placeholder: " + e.getMessage());
            throw new RuntimeException(e);
        }

    }

}
