package edu.emory.it.service.ca.tools;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class LogAnalyzer {

    private final String[] logFiles = {
            "/doc/logs/monolithic-tests/CiscoAsr1Service-TestRun3.log",  // 0
            "/doc/logs/monolithic-tests/CiscoAsr2Service-TestRun3.log",
            "/doc/logs/monolithic-tests/CiscoAsr1Service-TestRun6.log",
            "/doc/logs/monolithic-tests/CiscoAsr2Service-TestRun6.log",  // 3
            "/doc/logs/monolithic-tests/CiscoAsr1Service-TestRun1.log",
            "/doc/logs/monolithic-tests/CiscoAsr2Service-TestRun1.log",  // 5
            "/doc/logs/monolithic-tests/TestSuiteApplication-CiscoAsrService_TestRun7.log",
            "/doc/logs/monolithic-tests/CiscoAsr1Service-TestRun7.log",  // 7
            "/doc/logs/monolithic-tests/CiscoAsr2Service-TestRun7.log",  // 8
            "/doc/logs/monolithic-tests/TestSuiteApplication-CiscoAsrService_TestRun8-cancelled.log", // 9
            "/doc/logs/monolithic-tests/CiscoAsr1Service_TestRun8-cancelled.log",
            "/doc/logs/monolithic-tests/CiscoAsr2Service_TestRun8-cancelled.log", // 11
            "/doc/logs/monolithic-tests/CiscoAsr1Service.log", // 12
            "/doc/logs/monolithic-tests/CiscoAsr2Service.log", // 13
            "/doc/logs/monolithic-tests/TestSuiteApplication-CiscoAsrService.log", // 14
            "/doc/logs/ASR_1-40-Create_1/TestSuiteApplication-CiscoAsrService.log", // 15
            "/doc/logs/ASR_41-80-Create_1/TestSuiteApplication-CiscoAsrService.log", // 16
            "/doc/logs/ASR_81-120-Create_1/TestSuiteApplication-CiscoAsrService.log", // 17
            "/doc/logs/ASR_121-160-Create_1/TestSuiteApplication-CiscoAsrService.log", // 18
            "/doc/logs/ASR_161-200-Create_1/TestSuiteApplication-CiscoAsrService.log", // 19
            "/doc/logs/ASR_1-200-Delete/TestSuiteApplication-CiscoAsrService.log", // 20
    };

    @Test
    public void deltaTScanner() {
        String logFile = logFiles[18];
        String cwd = System.getProperty("user.dir");

        Path logFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        DeltaTAnalyzer deltaTAnalyzer = new DeltaTAnalyzer();
        // Adds
        try {
            Stream<String> lines = Files.lines(logFilePath);

            Long _10sec = 10000L;
            Long _30sec = 30000L;
            Long _1min = 60000L;
            Long _90sec = 90000L;
            Long _2min = 120000L;
            Long _10mins = 600000L;
            Long _15mins = 900000L;
            Long _30mins = 1800000L;
            Long _1hr = 3600000L;
            Long _2hrs = 7200000L;
            Long _3hrs = 10800000L;
            Long _4hrs = 14400000L;
            Long _5hrs = 18000000L;

            lines.forEach(line -> {
                if (deltaTAnalyzer.linesStartsWithTimestamp(line)){
//                    Long timestamp = getTimestamp(line);
                    LocalDateTime localDateTime = getLocalDateTime(line);
//                    deltaTAnalyzer.setTimestamp(timestamp, line);
                    deltaTAnalyzer.setLocalDateTime(localDateTime, line);
                    Long diff = deltaTAnalyzer.getDeltaT();

                    if (diff > _10sec) {
                        deltaTAnalyzer.accumulateDiff(diff);
                        if (diff > 1000) {
                            log("\n" + diff / 1000 + " sec ");
                        } else {
                            log("\n" + diff + " ms ");
                        }
                        log("difference: " + line + "\n");
                        log("Prior line: " + deltaTAnalyzer.priorLine + "\n");
                        log("Current line: " + deltaTAnalyzer.currentLine + "\n");
                    }
                }
            });

            log("Total run time: " + String.format("%-10.2f", (double)deltaTAnalyzer.getTotalRunTime() / 3600 / 1000) + "\n");

            if (deltaTAnalyzer.diffsFound > 0) {
                Long averageDiffInMilliseconds = deltaTAnalyzer.getTotalDiff() / deltaTAnalyzer.diffsFound;
                Double totalDiffInHours = (double)deltaTAnalyzer.getTotalDiff() / 3600 / 1000;
                log("Total diff in hours: " + String.format("%-10.2f", totalDiffInHours).trim() + "\n");
                if (totalDiffInHours > 24.0) {
                    log("Total diff in days: " + totalDiffInHours / 24L + "\n");
                }
                log("Total diffs within range: " + deltaTAnalyzer.diffsFound + "\n");
                log("Average diff in seconds: " + averageDiffInMilliseconds / 1000 + "\n");
                log("Longest diff in seconds: " + String.format("%-10.0f", (double)deltaTAnalyzer.getLongestDiff() / 1000) + "\n");
            } else {
                log("No diffs found");
            }

        } catch (IOException e) {
            throw new RuntimeException("Failed to read lines from " + logFile);
        }
    }

    @Test
    public void test() {
        String logFile = logFiles[2];
        String cwd = System.getProperty("user.dir");

        Path logFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        DeltaTAnalyzer deltaTAnalyzer = new DeltaTAnalyzer();
        // Adds
        try {
            Stream<String> lines = Files.lines(logFilePath);

            Long _10mins = 600000L;
            Long _15mins = 900000L;
            Long _30mins = 1800000L;
            Long _1hr = 3600000L;
            Long _2hrs = 7200000L;
            Long _3hrs = 10800000L;
            Long _4hrs = 14400000L;
            Long _5hrs = 18000000L;

            AtomicReference<Boolean> startSentinel = new AtomicReference<>(false);

            lines.forEach(line -> {
                if (!startSentinel.get() && line.contains("[TestSuite] Executing all schedules.")) {
                    startSentinel.set(true);
                }
                if (startSentinel.get() && line.contains("[Thread-")) {
                    log(line.substring(30, 42) + "\n");
                }
            });

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void analyzeExecutionTime() {
        String logFile = logFiles[15];
        String cwd = System.getProperty("user.dir");

        Path logFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        ExecutionTimeTabulator tabulator = new ExecutionTimeTabulator();
        // Query-Request execution complete in 1756 ms (Router execution time: 789 ms) with replyContents:
        Pattern pattern = Pattern.compile("(\\w+)-Request execution complete in (\\d+) ms \\(Router execution time: (\\d+) ms\\) with replyContents\\:");
        try {
            Stream<String> lines = Files.lines(logFilePath);

            lines.forEach(line -> {

                Matcher matcher = pattern.matcher(line);

                if (matcher.find()) {
                    Long executionTime = Long.valueOf(matcher.group(2));
                    Long routerExecutionTime = Long.valueOf(matcher.group(3));
                    tabulator.setExecutionTimes(executionTime, routerExecutionTime);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Failed to read log file " + logFile + " lines", e);
        }
        log("\n  Total execution time: " + String.format("%-10d", tabulator.getTotalExecutionTime() / 1000).trim());
        if (tabulator.getTotalExecutionTime() > 3600 * 1000) {
            log(" (" + String.format("%10.2f hours", ((double)tabulator.getTotalExecutionTime() / 1000 / 3600)).trim() + ")");
        }

        log("\n     Total router time: " + String.format("%-10d", tabulator.getRouterExecutionTime() / 1000));
        log("\n         Router exec %: " + String.format("%-10.2f", (double)tabulator.getRouterExecutionTime() / tabulator.getTotalExecutionTime()));
        log("\nLongest execution time: " + String.format("%-10d", tabulator.getLongestExecutionTime() / 1000).trim() + " sec");
//        log("\nTotal execution time in minutes: " + String.format("%-10.2f", (double)tabulator.getTotalExecutionTime() / 1000.0 / 60));
//        log("\nTotal router time in minutes: " + String.format("%-10.2f", tabulator.getRouterExecutionTime() / 1000.0 / 60));
        log("\n        Total requests: " + tabulator.getRequestCount() + "\n");

    }

    @Test
    public void analyzeQueryVpnTime() {
        String logFile = logFiles[2];
        String cwd = System.getProperty("user.dir");

        Path logFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        AddDeleteTabulator tabulator = new AddDeleteTabulator();
        // Adds
        try {
            Stream<String> lines = Files.lines(logFilePath);

            lines.forEach(line -> {

                if (line.contains("VPN provision - query - for vpc id")) {
                    int index = line.indexOf("vpc id:");
                    log(line.substring(index) + "\n");
                    tabulator.newRequest();
                    tabulator.setEditConfigTime(line);
                }
                if (line.contains("Successful vpn provision query after attempt #")) {

                    String attemptNumber = line.substring(line.indexOf("#") + 1);
                    log("Successful query after attempt number " + attemptNumber + "\n");
                    tabulator.calculateEditConfigTime(line);
                    tabulator.setRequestDone(true);
                    // TODO: Fish out attempt number?
                }
                if (tabulator.isRequestDone() && line.contains("-Request execution complete in ")) {
                    Long executionTime = tabulator.getExecutionTime(line);
                    tabulator.calculateTotalExecutionTime(executionTime);
                    log("Total execution time: " + executionTime + " - ");
                    Double routerPercentage = ((double)tabulator.getEditConfigTime() / executionTime) * 100.0;
                    String routerPercentageString = String.format("%4.1f", routerPercentage);
                    log("Router %: " + routerPercentageString + "\n");
                    tabulator.setRequestDone(false);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Failed to read log file " + logFile + " lines", e);
        }
        log("\nTotal router time in minutes: " + String.format("%-10.2f (%-4.2f)", tabulator.getTotalRouterTime() / 1000.0 / 60, ((double)tabulator.getTotalRouterTime() / tabulator.getTotalExecutionTime() * 100.0)));
        log("\nTotal execution time in minutes: " + String.format("%-10.2f", (double)tabulator.getTotalExecutionTime() / 1000.0 / 60));
    }

    @Test
    public void analyzeAddVpnTime() {
        String logFile = logFiles[2];
        String cwd = System.getProperty("user.dir");

        Path logFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        AddDeleteTabulator tabulator = new AddDeleteTabulator();
        // Adds
        try {
            Stream<String> lines = Files.lines(logFilePath);

            lines.forEach(line -> {

                if (line.contains("VPN provision - generate - for owner id")) {
                    int index = line.indexOf("owner id:");
                    log(line.substring(index) + "\n");
                    tabulator.newRequest();
                }
                if (line.contains("native config to send to router")) {
                    tabulator.setEditConfigTime(line);
                } else if (line.contains("Successful vpn provision add after attempt #")) {
                    tabulator.calculateEditConfigTime(line);
                    log("Add edit-config time: " + tabulator.getEditConfigTime() + " - ");
                }

                if (line.contains("Waiting for VPN provision - add - to complete for owner id:")) {
                    tabulator.setWaitForCompleteTime(line);
                } else if (line.contains("seconds for router to settle after add...")) {
                    tabulator.calculateWaitForCompleteTime(line);
                    log("Wait for complete time: " + tabulator.getWaitForCompleteTime() + "\n");
                    tabulator.calculateTotalRouterTime();
                    tabulator.setRequestDone(true);
                }

                if (tabulator.isRequestDone() && line.contains("-Request execution complete in ")) {
                    Long executionTime = tabulator.getExecutionTime(line);
                    tabulator.calculateTotalExecutionTime(executionTime);
                    log("Total execution time: " + executionTime + " - ");
                    Double routerPercentage = (((double)tabulator.getEditConfigTime() + tabulator.getWaitForCompleteTime()) / executionTime) * 100.0;
                    String routerPercentageString = String.format("%4.1f", routerPercentage);
                    log("Router %: " + routerPercentageString + "\n");
                    tabulator.setRequestDone(false);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Failed to read log file " + logFile + " lines", e);
        }
        log("\nTotal router time in minutes: " + String.format("%-10.2f (%-4.2f)", tabulator.getTotalRouterTime() / 1000.0 / 60, ((double)tabulator.getTotalRouterTime() / tabulator.getTotalExecutionTime() * 100.0)));
        log("\nTotal settle time in minutes: " + String.format("%-10.2f", (double)tabulator.getRequestCount() * 30 / 60));
        log("\nTotal execution time in minutes: " + String.format("%-10.2f", (double)tabulator.getTotalExecutionTime() / 1000.0 / 60));
    }

    @Test
    public void analyzeDeleteVpnTime() {
        String logFile = logFiles[2];
        String cwd = System.getProperty("user.dir");

        Path logFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        AddDeleteTabulator tabulator = new AddDeleteTabulator();
        // Deletes
        try {
            Stream<String> lines = Files.lines(logFilePath);

            lines.forEach(line -> {

                if (line.contains("VPN provision - delete - for vpc id")) {
                    int index = line.indexOf("vpc id:");
                    log(line.substring(index) + "\n");
                    tabulator.newRequest();
                }
                if (line.contains("Tunnel shutdown config:")) {
                    tabulator.setEditConfigTime(line);
                } else if (line.contains("Successful delete/shutdown tunnel after attempt #")) {
                    tabulator.calculateEditConfigTime(line);
                    log("Delete edit-config time: " + tabulator.getEditConfigTime() + " - ");
                }

                if (line.contains("Waiting for VPN provision - delete[shutdown tunnel] - to complete for vpc id")) {
                    tabulator.setWaitForCompleteTime(line);
                } else if (line.contains(" seconds for router to settle after delete...")) {
                    tabulator.calculateWaitForCompleteTime(line);
                    log("Wait for complete time: " + tabulator.getWaitForCompleteTime() + "\n");
                    tabulator.calculateTotalRouterTime();
                    tabulator.setRequestDone(true);
                }

                if (tabulator.isRequestDone() && line.contains("-Request execution complete in ")) {
                    Long executionTime = tabulator.getExecutionTime(line);
                    tabulator.calculateTotalExecutionTime(executionTime);
                    log("Total execution time: " + executionTime + " - ");
                    Double routerPercentage = (((double)tabulator.getEditConfigTime() + tabulator.getWaitForCompleteTime()) / executionTime) * 100.0;
                    String routerPercentageString = String.format("%4.1f", routerPercentage);
                    log("Router %: " + routerPercentageString + "\n");
                    tabulator.setRequestDone(false);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Failed to read log file " + logFile + " lines", e);
        }
        log("\nTotal router time in minutes: " + String.format("%-10.2f (%-4.2f)", tabulator.getTotalRouterTime() / 1000.0 / 60, ((double)tabulator.getTotalRouterTime() / tabulator.getTotalExecutionTime() * 100.0)));
        log("\nTotal settle time in minutes: " + String.format("%-10.2f", (double)tabulator.getRequestCount() * 30 / 60));
        log("\nTotal execution time in minutes: " + String.format("%-10.2f", (double)tabulator.getTotalExecutionTime() / 1000.0 / 60));
    }

    private Long getTimestamp(String line) {
        // 2018-09-17 17:40:55,011

        Long timestamp = 0L;
        try {
            String dateString = line.substring(0, 23);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
            Date date = format.parse(dateString);
            timestamp = date.getTime();

        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("Failed to parse date: " + timestamp);
        } catch (Exception e) {
            throw new RuntimeException("Failed to parse date: " + timestamp);
        }
        return timestamp;
    }

    private LocalDateTime getLocalDateTime(String line) {
        // 2018-09-17 17:40:55,011
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");
        LocalDateTime dateTime = LocalDateTime.parse(line.substring(0, 23), formatter);
        return dateTime;
    }

    private void log(String message) {
        System.out.print(message);
    }

    private class AddDeleteTabulator {
        Long requestCount = 0L;
        Long editConfigTime = 0L;
        Long waitForCompleteTime = 0L;
        Long totalRouterTime = 0L;
        Long totalExecutionTime = 0L;
        boolean requestDone = false;

        public void newRequest() {
            editConfigTime = 0L;
            waitForCompleteTime = 0L;
            requestDone = false;
            requestCount++;
        }

        public void setEditConfigTime(String line) {
            editConfigTime = getTimestamp(line);
        }

        public void calculateEditConfigTime(String line) {
            editConfigTime = getTimestamp(line) - editConfigTime;
            totalRouterTime += editConfigTime;
        }

        public void setWaitForCompleteTime(String line) {
            waitForCompleteTime = getTimestamp(line);
        }

        public void calculateWaitForCompleteTime(String line) {
            waitForCompleteTime = getTimestamp(line) - waitForCompleteTime;
        }

        public void calculateTotalRouterTime() {
            totalRouterTime += editConfigTime;
        }

        public void calculateTotalExecutionTime(Long executionTime) {
            totalExecutionTime += executionTime;
        }

        public Long getExecutionTime(String line) {
            Long executionTime = 0L;
            Pattern pattern = Pattern.compile("Request execution complete in (\\d+) ms with replyContents:");
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                executionTime = Long.parseLong(matcher.group(1));
            }

            return executionTime;
        }

        public Long getEditConfigTime() {
            return editConfigTime;
        }

        public Long getRequestCount() {
            return requestCount;
        }

        public Long getWaitForCompleteTime() {
            return waitForCompleteTime;
        }

        public Long getTotalRouterTime() {
            return totalRouterTime;
        }

        public Long getTotalExecutionTime() {
            return totalExecutionTime;
        }

        public void setRequestDone(boolean requestDone) {
            this.requestDone = requestDone;
        }
        public boolean isRequestDone() {
            return requestDone;
        }
    }

    private class ExecutionTimeTabulator {
        Long requestCount = 0L;
        Long totalExecutionTime = 0L;
        Long routerExecutionTime = 0L;
        Long longestExecutionTime = 0L;

        public void setExecutionTimes(Long executionTime, Long routerExecutionTime) {
            this.totalExecutionTime += executionTime;
            this.routerExecutionTime += routerExecutionTime;
            if (this.longestExecutionTime < executionTime) {
                this.longestExecutionTime = executionTime;
            }
            requestCount++;
        }

        public Long getRequestCount() {
            return requestCount;
        }

        public Long getTotalExecutionTime() {
            return totalExecutionTime;
        }

        public Long getRouterExecutionTime() {
            return routerExecutionTime;
        }

        public Long getLongestExecutionTime() {
            return longestExecutionTime;
        }
    }

    class DeltaTAnalyzer {
        LocalDateTime startLocalDateTime;
        LocalDateTime priorLocalDateTime;
        LocalDateTime currentLocalDateTime;
        String priorLine;
        String currentLine;
        Long totalDiff = 0L;
        Long longestDiff = 0L;
        Long diffsFound = 0L;

        public void setLocalDateTime(LocalDateTime localDateTime, String line) {
            if (startLocalDateTime == null) {
                startLocalDateTime = localDateTime;
            }
            priorLocalDateTime = currentLocalDateTime;
            currentLocalDateTime = localDateTime;
            priorLine = currentLine;
            currentLine = line;
        }

        public Long getDeltaT() {
            Long deltaT = -1L;
            if (currentLocalDateTime != null && priorLocalDateTime != null) {
                deltaT = priorLocalDateTime.until(currentLocalDateTime, ChronoUnit.MILLIS);

                if (deltaT > longestDiff) {
                    longestDiff = deltaT;
                }
            }
            return deltaT;
        }

        public void accumulateDiff(Long diff) {
            totalDiff += diff;
            diffsFound++;
        }

        public Long getTotalRunTime() {
            Long deltaT = 0L;
            deltaT = startLocalDateTime.until(currentLocalDateTime, ChronoUnit.MILLIS);
            return deltaT;
        }

        public Long getLongestDiff() {
            return longestDiff;
        }

        public Long getTotalDiff() {
            return totalDiff;
        }
        public boolean linesStartsWithTimestamp(String line) {
            //2018-09-18 14:07:52,057
            Pattern pattern = Pattern.compile("\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d,\\d\\d\\d");
            Matcher matcher = pattern.matcher(line);
            return matcher.find();
        }
    }
}
