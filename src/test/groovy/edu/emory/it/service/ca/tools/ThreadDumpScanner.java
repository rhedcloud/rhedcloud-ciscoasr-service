package edu.emory.it.service.ca.tools;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ThreadDumpScanner {

    private boolean verbose = false;

    private final String[] threadDumpFiles = {
            "/doc/jvisualvm/sample.tdump",  // 0
            "/doc/jvisualvm/intellij.tdump",  // 1
            "/doc/JavaNetconfClient/thread-dumps/DEV/CiscoAsrService_2019-03-26.tda",
            "/doc/JavaNetconfClient/thread-dumps/DEV/CiscoAsrService_2019-04-22.tda",
            "/doc/JavaNetconfClient/thread-dumps/local/CiscoAsrService_2019-04-22.tda", // 4
    };

    @Test
    public void scanner() {
        String logFile = threadDumpFiles[4];
        String cwd = System.getProperty("user.dir");

        Path threadDumpFilePath = FileSystems.getDefault().getPath(cwd + logFile);

        try {
            String allLines = new String(Files.readAllBytes(threadDumpFilePath));

            log("\n=============== Application Threads =================\n");

            List<ThreadInfo> threadInfoList = findApplicationThreadInfo(allLines);

            log(threadInfoList.size() + " application thread infos found:\n\n");

            Map<String, List<ThreadInfo>> collated = collate(threadInfoList);

            collated.forEach((k,v) -> {
                log (v.size() + " " + k + " application thread states\n");
            });

            log("\n\n");

            log("================== JVM Threads ======================\n");

            List<JvmThreadInfo> jvmThreadInfoList = findJvmThreadInfo(allLines);

            log(jvmThreadInfoList.size() + " JVM thread infos found:\n\n");

            Map<String, List<JvmThreadInfo>> collatedJvm = collateJvm(jvmThreadInfoList);

            collatedJvm.forEach((k, v) -> {
                log (v.size() + " " + k + " jvm thread statuses\n");
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private Map<String, List<ThreadInfo>> collate(List<ThreadInfo> threadInfos) {
        Map<String, List<ThreadInfo>> collated = new LinkedHashMap<>();

        threadInfos.forEach(threadInfo -> {

            if (!collated.containsKey(threadInfo.state)) {
                List<ThreadInfo> threadInfoList = new ArrayList<>();
                threadInfoList.add(threadInfo);
                collated.put(threadInfo.state, threadInfoList);
            } else {
                collated.get(threadInfo.state).add(threadInfo);
            }
        });

        return collated;
    }

    private Map<String, List<JvmThreadInfo>> collateJvm(List<JvmThreadInfo> threadInfos) {
        Map<String, List<JvmThreadInfo>> collated = new LinkedHashMap<>();

        threadInfos.forEach(threadInfo -> {

            if (!collated.containsKey(threadInfo.status)) {
                List<JvmThreadInfo> threadInfoList = new ArrayList<>();
                threadInfoList.add(threadInfo);
                collated.put(threadInfo.status, threadInfoList);
            } else {
                collated.get(threadInfo.status).add(threadInfo);
            }
        });

        return collated;
    }

    private void log(String message) {
        System.out.print(message);
    }

    private List<ThreadInfo> findApplicationThreadInfo(String lines) {
        List<ThreadInfo> threadInfos = new ArrayList<>();
        Pattern pattern = Pattern.compile(ThreadInfo.applicationThreadPattern);
        Matcher matcher = pattern.matcher(lines);
        while (matcher.find()) {
            int groupCount = matcher.groupCount();

            if (groupCount == 11) {

                boolean isDaemon = false;
                if ("daemon ".equals(matcher.group(4))) {
                    isDaemon = true;
                }

                ThreadInfo threadInfo = new ThreadInfo(
                        matcher.group(1),
                        matcher.group(2),
                        "daemon ".equals(matcher.group(4)),
                        matcher.group(5),
                        matcher.group(6),
                        matcher.group(7),
                        matcher.group(8),
                        matcher.group(9),
                        matcher.group(10),
                        matcher.group(11)
                );

                threadInfos.add(threadInfo);
                if (verbose) {
                    log("name: " + matcher.group(1) + " id:  " + matcher.group(2) + "  daemon: " + isDaemon + " priority: " + matcher.group(5) + " os priority: " + matcher.group(6));
                    log(" address: " + matcher.group(7) + " os id: " + matcher.group(8) + " status: " + matcher.group(9) + " last known java stack pointer: " + matcher.group(10) + " thread state: " + matcher.group(11));
                    log("\n");
                }
            }
        }


        return threadInfos;
    }


    private List<JvmThreadInfo> findJvmThreadInfo(String lines) {
        List<JvmThreadInfo> threadInfos = new ArrayList<>();
        Pattern pattern = Pattern.compile(JvmThreadInfo.jvmThreadPattern);
        Matcher matcher = pattern.matcher(lines);
        while (matcher.find()) {
            int groupCount = matcher.groupCount();

            if (groupCount == 5) {

                JvmThreadInfo threadInfo = new JvmThreadInfo(
                        matcher.group(1),
                        matcher.group(2),
                        matcher.group(3),
                        matcher.group(4),
                        matcher.group(5)
                );

                threadInfos.add(threadInfo);
                if (verbose) {
                    log("name: " + matcher.group(1) + " os priority: " + matcher.group(2));
                    log(" address: " + matcher.group(3) + " os id: " + matcher.group(4) + " status: " + matcher.group(5));
                    log("\n");
                }
            }
        }

        return threadInfos;
    }


    public class ThreadInfo {
        public final String name;
        public final String id;  // #\d+
        public final Boolean isDaemon;  // daemon
        public final String priority; // prio=[\-]*\d+
        public final String osPriority; // os_prio=[\-]*\d+
        public final String address; // tid=0x[0-9a-fA-F]+
        public final String osId; // nid=0x[0-9a-fA-F]+
        public final String status; // \s[^\[]\[
        public final String lastKnownJavaStackPointer; // \[[a-fA-F0-9]+\]
        public final String state;

        public final static String applicationThreadPattern = "\\n\"([^\"]+?)\"\\s+\\#(\\d+)\\s+((?!daemon)|(daemon\\s{1}))prio=(-?\\d+)\\s+os_prio=(-?\\d+)\\s+tid=0x([0-9a-f]+)\\s+nid=0x([0-9a-f]+)\\s+([^\\[]+)\\[0x([0-9a-f]+)\\][\\S\\s]+?State\\:\\s{1}(.+)";

        // "G1 Refine#0" os_prio=2 tid=0x00000250c367e000 nid=0x3c0c runnable
        public final static String jvmThreadPattern = "\\n\"([^\"]+?)\"\\s+os_prio=(-?\\d+)\\s+tid=0x([0-9a-f]+)\\s+nid=0x([0-9a-f]+)\\s+(.+)";

        public ThreadInfo(String name,
                          String id,
                          Boolean isDaemon,
                          String priority,
                          String osPriority,
                          String address,
                          String osId,
                          String status,
                          String lastKnownJavaStackPointer,
                          String state) {
            this.name = name;
            this.id = id;
            this.isDaemon = isDaemon;
            this.priority = priority;
            this.osPriority = osPriority;
            this.address = address;
            this.osId = osId;
            this.status = status;
            this.lastKnownJavaStackPointer = lastKnownJavaStackPointer;
            this.state = state;
        }

    }

    public class JvmThreadInfo {
        public final String name;
        public final String osPriority; // os_prio=[\-]*\d+
        public final String address; // tid=0x[0-9a-fA-F]+
        public final String osId; // nid=0x[0-9a-fA-F]+
        public final String status; // \s[^\[]\[

        // "G1 Refine#0" os_prio=2 tid=0x00000250c367e000 nid=0x3c0c runnable
        public final static String jvmThreadPattern = "\\n\"([^\"]+?)\"\\s+os_prio=(-?\\d+)\\s+tid=0x([0-9a-f]+)\\s+nid=0x([0-9a-f]+)\\s+(.+)";

        public JvmThreadInfo(String name,
                          String osPriority,
                          String address,
                          String osId,
                          String status) {
            this.name = name;
            this.osPriority = osPriority;
            this.address = address;
            this.osId = osId;
            this.status = status;
        }

    }
}
