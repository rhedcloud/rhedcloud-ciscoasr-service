package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class uses the subversion repository https://svn.service.emory.edu:8443/repos/emoryoit/deployment/esb/dev/Deployments as the keeper of secrets.
 * The requirement is to checkout the directory relative to this project directory so that the relative svn directory ^/deployment/esb/dev/Deployments
 * is located in the relative path ../serviceConfigs/Deployments
 *
 */
public class InstrumentConfigDocs {

    // TODO:  Instrument /deploy/build-test only.  The subversion archive is the secrets keeper (see above)
    // 1) Get config docs from svn Deployments dir
    // 2) Extract secrets:
    //    a) Security credentials (JMS configs)
    //    b) Router credentials AWS secrets (access-key, secret-key and secret-name
    //    c) DB Connect password
    // 3) Insert (2) into deploy/build-test configs

    final private String OBFUSCATION_VALUE = "######";
    final private String JMS_SECURITY_CREDENTIALS_PATTERN = "(<SecurityCredentials>)(.+)(</SecurityCredentials>)";
    final private String ROUTER_CREDENTIALS_ACCESS_KEY_PATTERN = "(<PropertyName>routerCredentialsAccessKey</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
    final private String ROUTER_CREDENTIALS_SECRET_KEY_PATTERN = "(<PropertyName>routerCredentialsSecretKey</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
    final private String ROUTER_CREDENTIALS_SECRET_NAME_PATTERN = "(<PropertyName>routerCredentialsSecretName</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
    final private String DB_CONNECT_PASSWORD_PATTERN = "(dbConnectPassword=\")([^\"]+?)(\")";

    final public String CISCO_ASR1_SERVICE_LOCAL_CONFIG_FILE = "deploy/build-test/configs/CiscoAsr1Service.xml";
    final public String CISCO_ASR2_SERVICE_LOCAL_CONFIG_FILE = "deploy/build-test/configs/CiscoAsr2Service.xml";
    final public String TEST_SUITE_LOCAL_CONFIG_FILE = "deploy/build-test/configs/TestSuiteApplication-CiscoAsrService.xml";

    final public String CISCO_ASR1_SERVICE_CONFIG_FILE = "../../serviceConfigs/Deployments/CiscoAsr1Service.xml";
    final public String CISCO_ASR2_SERVICE_CONFIG_FILE = "../../serviceConfigs/Deployments/CiscoAsr2Service.xml";
    final public String TEST_SUITE_CONFIG_FILE = "../../serviceConfigs/Deployments/TestSuiteApplication-CiscoAsrService.xml";

    String[] localConfigFilesForService1 = {
            CISCO_ASR1_SERVICE_LOCAL_CONFIG_FILE,
            TEST_SUITE_LOCAL_CONFIG_FILE,
    };
    String[] localConfigFilesForService2 = {
            CISCO_ASR2_SERVICE_LOCAL_CONFIG_FILE,
    };

    String[] configFilesForService1 = {
            CISCO_ASR1_SERVICE_CONFIG_FILE,
            TEST_SUITE_CONFIG_FILE,
    };
    String[] configFilesForService2 = {
            CISCO_ASR2_SERVICE_CONFIG_FILE,
    };

    Collection<Path> localConfigFilePathsForService1 = null;
    Collection<Path> localConfigFilePathsForService2 = null;

    Collection<Path> configFilePathsForService1 = null;
    Collection<Path> configFilePathsForService2 = null;

    Map<Path, String> localFileContentsMap = new HashMap<>();
    Map<Path, String> fileContentsMap = new HashMap<>();

    Map<String, Map<String, String>> secretsMap;

    public static void main(String[] args) {
        InstrumentConfigDocs instrumentConfigDocs = new InstrumentConfigDocs();

        try {

            String taskNumber = instrumentConfigDocs.getTask();

            instrumentConfigDocs.log("Task number selected: " + taskNumber);

             instrumentConfigDocs.doIt(taskNumber);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public InstrumentConfigDocs() {
        init();
    }

    // Public API
    public void obfuscateLocal() {
        doObfuscateLines();
    }

    // Public API
    public void setupLocalConfigSecrets() { doSetupLocalConfigSecrets(); }

    // Public API
    public void obfuscate(Map<Path, String> configDocMap) {

        configDocMap.forEach((path, contents) -> {
            String lines;
            lines = obfuscateLines(contents);
            configDocMap.put(path, lines);
        });
    }

    // Public API
    public void instrument(Map<Path, String> configDocMap) {

        Map<String, String> secretsForService1 = secretsMap.get("CiscoAsr1Service.xml");
        Map<String, String> secretsForService2 = secretsMap.get("CiscoAsr2Service.xml");
        Map<String, String> secretsForTestSuiteConfig = secretsMap.get("TestSuiteApplication-CiscoAsrService.xml");

        final String credentialsForService1 = secretsForService1.get("JMS_SECURITY_CREDENTIALS");
        final String credentialsForService2 = secretsForService2.get("JMS_SECURITY_CREDENTIALS");
        final String credentialsForTestSuite = secretsForTestSuiteConfig.get("JMS_SECURITY_CREDENTIALS");

        // Remaining secrets same for both services.
        final String awsAccesKey = secretsForService1.get("AWS_ACCESS_KEY");
        final String awsSecretKey = secretsForService1.get("AWS_SECRET_KEY");
        final String awsSecretName = secretsForService1.get("AWS_SECRET_NAME");
        final String dbConnectPassword = secretsForService1.get("DB_CONNECT_PASSWORD");

        Optional<Path> configDocPath = configDocMap.keySet().stream()
                .filter(path -> path.endsWith("CiscoAsr1Service.xml"))
                .findAny();

        if (configDocMap.isEmpty()) {
            throw new RuntimeException("Can't find CiscoAsr1Service.xml path");
        }

        String lines = configDocMap.get(configDocPath.get());
        lines = instrumentWithCredentials(lines, credentialsForService1);
        lines = instrumentWithAwsAccessKey(lines, awsAccesKey);
        lines = instrumentWithAwsSecretKey(lines, awsSecretKey);
        lines = instrumentWithAwsSecretName(lines, awsSecretName);
        lines = instrumentWithDbConnectPassword(lines, dbConnectPassword);
        configDocMap.put(configDocPath.get(), lines);

        configDocPath = configDocMap.keySet().stream()
                .filter(path -> path.endsWith("CiscoAsr2Service.xml"))
                .findAny();

        if (configDocMap.isEmpty()) {
            throw new RuntimeException("Can't find CiscoAsr2Service.xml path");
        }

        lines = configDocMap.get(configDocPath.get());
        lines = instrumentWithCredentials(lines, credentialsForService2);
        lines = instrumentWithAwsAccessKey(lines, awsAccesKey);
        lines = instrumentWithAwsSecretKey(lines, awsSecretKey);
        lines = instrumentWithAwsSecretName(lines, awsSecretName);
        lines = instrumentWithDbConnectPassword(lines, dbConnectPassword);
        configDocMap.put(configDocPath.get(), lines);

        configDocPath = configDocMap.keySet().stream()
                .filter(path -> path.endsWith("TestSuiteApplication-CiscoAsrService.xml"))
                .findAny();

        if (configDocMap.isEmpty()) {
            throw new RuntimeException("Can't find TestSuiteApplication-CiscoAsrService.xml path");
        }

        lines = configDocMap.get(configDocPath.get());
        lines = instrumentWithCredentials(lines, credentialsForTestSuite);
        configDocMap.put(configDocPath.get(), lines);

    }


    private Map<String, Map<String, String>> obtainSecrets() {
        secretsMap = new HashMap<>();

        fileContentsMap.forEach((path, lines) -> {

            log("Config file path: " + path.toFile().getName());

            Map<String, String> map = new HashMap<>();

            try {
                Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_ACCESS_KEY_PATTERN);
                Matcher matcher = pattern.matcher(lines);
                if (matcher.find()) {
                    map.put("AWS_ACCESS_KEY", matcher.group(2));
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            try {
                Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_KEY_PATTERN);
                Matcher matcher = pattern.matcher(lines);
                if (matcher.find()) {
                    map.put("AWS_SECRET_KEY", matcher.group(2));
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            try {
                Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_NAME_PATTERN);
                Matcher matcher = pattern.matcher(lines);
                if (matcher.find()) {
                    map.put("AWS_SECRET_NAME", matcher.group(2));
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            try {
                Pattern pattern = Pattern.compile(JMS_SECURITY_CREDENTIALS_PATTERN);
                Matcher matcher = pattern.matcher(lines);
                if (matcher.find()) {
                    map.put("JMS_SECURITY_CREDENTIALS", matcher.group(2));
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            try {
                Pattern pattern = Pattern.compile(DB_CONNECT_PASSWORD_PATTERN);
                Matcher matcher = pattern.matcher(lines);
                if (matcher.find()) {
                    map.put("DB_CONNECT_PASSWORD", matcher.group(2));
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            secretsMap.put(path.toFile().getName(), map);

        });


        return secretsMap;
    }

    private void init() {
        setupLocalConfigFilePaths();
        setupConfigFilePaths();
        obtainSecrets();
    }

    private String getTask() {
        String taskNumber = "0";

        BufferedReader br = null;
        try {

            do {
                br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("1) Obfuscate\n2) Setup credentials\n0) Exit\n\nEnter choice: ");
                String input = br.readLine();
                input = input.trim();

                if (input.equals("0") || input.equals("1") || input.equals("2")) {
                    taskNumber = input;
                    break;
                }
            } while (true);

        } catch (IOException e) {
            log("Error trying to read task input: " + e.getMessage());
        } finally {
            closeReader(br);
        }
        return taskNumber;
    }

    private void setupLocalConfigFilePaths() {
        localConfigFilePathsForService1 = new Vector<>();
        localConfigFilePathsForService2 = new Vector<>();

        try {
            FileSystem fileSystem = FileSystems.getDefault();
            for (int n = 0; n < localConfigFilesForService1.length; n++) {
                Path localConfigFile = fileSystem.getPath(localConfigFilesForService1[n]);
                localFileContentsMap.put(localConfigFile, new String(Files.readAllBytes(localConfigFile)));
                localConfigFilePathsForService1.add(localConfigFile);
            }

            for (int n = 0; n < localConfigFilesForService2.length; n++) {
                Path localConfigFile = fileSystem.getPath(localConfigFilesForService2[n]);
                localFileContentsMap.put(localConfigFile, new String(Files.readAllBytes(localConfigFile)));
                localConfigFilePathsForService2.add(localConfigFile);
            }
        } catch (IOException e) {
            log("Error setting up local config paths: " + e.getMessage());
            throw new RuntimeException(e);
        }

    }

    private void setupConfigFilePaths() {
        configFilePathsForService1 = new Vector<>();
        configFilePathsForService2 = new Vector<>();

        try {
            FileSystem fileSystem = FileSystems.getDefault();
            for (int n = 0; n < configFilesForService1.length; n++) {
                Path configFile = fileSystem.getPath(configFilesForService1[n]);
                fileContentsMap.put(configFile, new String(Files.readAllBytes(configFile)));
                configFilePathsForService1.add(configFile);
            }

            for (int n = 0; n < configFilesForService2.length; n++) {
                Path configFile = fileSystem.getPath(configFilesForService2[n]);
                fileContentsMap.put(configFile, new String(Files.readAllBytes(configFile)));
                configFilePathsForService2.add(configFile);
            }
        } catch (IOException e) {
            log("Error setting up config paths: " + e.getMessage());
            throw new RuntimeException(e);
        }

    }

    private void doIt(String taskNumber) {
        switch (taskNumber) {
            case "1":
                doObfuscateLines();
                break;
            case "2":
                doSetupLocalConfigSecrets();
                break;
        }
    }

    private void doObfuscateLines() {

        localConfigFilePathsForService1.addAll(localConfigFilePathsForService2);
        localConfigFilePathsForService1.forEach(path -> {
            String lines = localFileContentsMap.get(path);
            lines = obfuscateLocalLines(lines);
            localFileContentsMap.put(path, lines);
        });

        saveFiles();
    }

    private void doSetupLocalConfigSecrets() {


        Map<String, String> secretsForService1 = secretsMap.get("CiscoAsr1Service.xml");
        Map<String, String> secretsForService2 = secretsMap.get("CiscoAsr2Service.xml");
        Map<String, String> secretsForTestSuiteConfig = secretsMap.get("TestSuiteApplication-CiscoAsrService.xml");

        final String credentialsForService1 = secretsForService1.get("JMS_SECURITY_CREDENTIALS");
        final String credentialsForService2 = secretsForService2.get("JMS_SECURITY_CREDENTIALS");
        final String credentialsForTestSuite = secretsForTestSuiteConfig.get("JMS_SECURITY_CREDENTIALS");

        // Remaining secrets same for both services.
        final String awsAccesKey = secretsForService1.get("AWS_ACCESS_KEY");
        final String awsSecretKey = secretsForService1.get("AWS_SECRET_KEY");
        final String awsSecretName = secretsForService1.get("AWS_SECRET_NAME");
        final String dbConnectPassword = secretsForService1.get("DB_CONNECT_PASSWORD");

        if (credentialsForService1 != null) {
                log("Creds for service1: " + credentialsForService1);
                localConfigFilePathsForService1.forEach(path -> {
//                    String lines = localFileContentsMap.get(path);
//                    lines = instrumentWithCredentials(lines, credentialsForService1);
//                    localFileContentsMap.put(path, lines);
                });

            }
            if (credentialsForService2 != null) {
                log("Creds for service2: " + credentialsForService2);
                localConfigFilePathsForService2.forEach(path -> {
//                    String lines = localFileContentsMap.get(path);
//                    lines = instrumentWithCredentials(lines, credentialsForService2);
//                    localFileContentsMap.put(path, lines);
                });

            }
            if (awsAccesKey != null && dbConnectPassword != null) {
                // Just do every file.
                localConfigFilePathsForService1.addAll(localConfigFilePathsForService2);
                log("AWS Secret Access Key: " + awsAccesKey);
                localConfigFilePathsForService1.forEach(path -> {
                    String lines = localFileContentsMap.get(path);
                    lines = instrumentWithAwsAccessKey(lines, awsAccesKey);
                    lines = instrumentWithAwsSecretKey(lines, awsSecretKey);
                    lines = instrumentWithAwsSecretName(lines, awsSecretName);
//                    lines = instrumentWithDbConnectPassword(lines, dbConnectPassword);
                    localFileContentsMap.put(path, lines);
                });
            }

            saveFiles();
    }

    private String instrumentWithCredentials(String lines, String credentials) {

        try {

            Pattern pattern = Pattern.compile(JMS_SECURITY_CREDENTIALS_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + credentials + matcher.group(3));
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return lines;
    }

    private String instrumentWithAwsAccessKey(String lines, String awsSecretAccessKey) {

        try {
            Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_ACCESS_KEY_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + awsSecretAccessKey + matcher.group(3));
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private String instrumentWithAwsSecretKey(String lines, String awsSecretKey) {

        try {
            Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_KEY_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + awsSecretKey + matcher.group(3));
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private String instrumentWithAwsSecretName(String lines, String awsSecretName) {

        try {
            Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_NAME_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + awsSecretName + matcher.group(3));
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private String instrumentWithDbConnectPassword(String lines, String dbAdminPassword) {
        try {
            Pattern pattern = Pattern.compile(DB_CONNECT_PASSWORD_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + dbAdminPassword + matcher.group(3));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private String obfuscateLines(String lines) {

        Pattern pattern = Pattern.compile(JMS_SECURITY_CREDENTIALS_PATTERN);
        Matcher matcher = pattern.matcher(lines);
        if (matcher.find()) {
            lines = lines.replaceAll(JMS_SECURITY_CREDENTIALS_PATTERN, matcher.group(1) + OBFUSCATION_VALUE + matcher.group(3));
        }

        pattern = Pattern.compile(DB_CONNECT_PASSWORD_PATTERN);
        matcher = pattern.matcher(lines);
        if (matcher.find()) {
            lines = lines.replaceAll(DB_CONNECT_PASSWORD_PATTERN, matcher.group(1) + OBFUSCATION_VALUE + matcher.group(3));
        }

        lines = obfuscateAwsSecrets(lines);

        return lines;
    }

    private String obfuscateLocalLines(String lines) {

        lines = obfuscateAwsSecrets(lines);

        return lines;
    }

    private String obfuscateAwsSecrets(String lines) {
        Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_ACCESS_KEY_PATTERN);
        Matcher matcher = pattern.matcher(lines);
        if (matcher.find()) {
            lines = lines.replaceAll(ROUTER_CREDENTIALS_ACCESS_KEY_PATTERN, matcher.group(1) + OBFUSCATION_VALUE + matcher.group(3));
        }
        pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_KEY_PATTERN);
        matcher = pattern.matcher(lines);
        if (matcher.find()) {
            lines = lines.replaceAll(ROUTER_CREDENTIALS_SECRET_KEY_PATTERN, matcher.group(1) + OBFUSCATION_VALUE + matcher.group(3));
        }
        pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_NAME_PATTERN);
        matcher = pattern.matcher(lines);
        if (matcher.find()) {
            lines = lines.replaceAll(ROUTER_CREDENTIALS_SECRET_NAME_PATTERN, matcher.group(1) + OBFUSCATION_VALUE + matcher.group(3));
        }

        return lines;

    }

    private void saveFiles() {

        StringBuilder builder = new StringBuilder();
        localFileContentsMap.forEach((path, lines) -> {
            if (lines == null | lines.isEmpty()) {
                builder.append("No lines for file " + path.getFileName() + "\n");
            }
        });
        if (builder.length() > 0) {
            throw new RuntimeException("Trying to write empty files: \n" + builder.toString());
        }

        localFileContentsMap.forEach((path, lines) -> {
            log("Saving " + path + "...");
            saveFile(lines, path);

        });
    }

    private void saveFile(String lines, Path file) {
        try {
            Files.write(file, (lines).getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeReader(BufferedReader br) {
        if (br != null) {
            try {
                br.reset();
                br.close();

            } catch (IOException e) {
                String message = e.getMessage();
                if (!message.equals("Stream not marked")) {
                    log("\n\nError closing buffered reader: " + message);
                }
            }
        }
    }

    private void log(String message) {
        System.out.println(message);
    }
}
