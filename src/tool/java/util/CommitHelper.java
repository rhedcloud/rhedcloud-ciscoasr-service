package util;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommitHelper {

    private final String useLockRegex = "(<PropertyName>useLock</PropertyName>[\\S\\s]+?" +
            "<PropertyValue>)(true)(</PropertyValue>)";

    private final String localApp1ConfigFile = "/deploy/build-test/configs/CiscoAsr1Service.xml";
    private final String localApp2ConfigFile = "/deploy/build-test/configs/CiscoAsr2Service.xml";

    private List<Path> configFilePaths = new ArrayList<>();

    public static void main(String[] args) {
        CommitHelper commitHelper = new CommitHelper();

        commitHelper.doIt();
    }

    public void doIt() {

        String cwd = System.getProperty("user.dir");
        FileSystem fileSystem = FileSystems.getDefault();
        configFilePaths.add(fileSystem.getPath(cwd + localApp1ConfigFile));
        configFilePaths.add(fileSystem.getPath(cwd + localApp2ConfigFile));

        chooseExampleProvider();
        obfuscateConfigDocs();
        configureDbForHsqldb();


        Map<Path, String> configDocsMap = new HashMap<>();

        // All modifications go here...
        for (Path configFilePath : configFilePaths) {
            String lines = readLines(configFilePath);
            lines = setUseLockFalse(lines);
            configDocsMap.put(configFilePath, lines);
        }

        saveChanges(configDocsMap);

    }

    private void chooseExampleProvider() {
        ProviderChooser providerChooser = new ProviderChooser();
        log("Setting provider to Example provider...");
        providerChooser.chooseExampleProvider();
    }

    private void obfuscateConfigDocs() {
        InstrumentConfigDocs instrumentConfigDocs = new InstrumentConfigDocs();
        log("Obfuscating secrets...");
        instrumentConfigDocs.obfuscateLocal();
    }

    private void configureDbForHsqldb() {
        DbConfigurer dbConfigurer = new DbConfigurer();
        log("Configuring db for in memory hsqldb...");
        dbConfigurer.configureForDb(DbConfigurer.DB.HSQL_MEM);
    }

    private String setUseLockFalse(String lines) {
        Pattern pattern = Pattern.compile(useLockRegex);
        Matcher matcher = pattern.matcher(lines);
        while (matcher.find()) {
            lines = lines.replaceFirst(matcher.group(), matcher.group(1) + "false" + matcher.group(3));
        }
        return lines;
    }

    private String readLines(Path configFilePath) {
        String lines;
        try {
            lines = new String(Files.readAllBytes(configFilePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private void saveChanges(Map<Path, String> configDocsMap) {
        configDocsMap.forEach((path, l) -> {
            try {
                Files.write(path, l.getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }


    private void log(String message) {
        System.out.println(message);
    }
}
