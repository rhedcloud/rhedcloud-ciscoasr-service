package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbConfigurer {

    public enum DB {
        HSQL_MEM("jdbc:hsqldb:mem:"),
        HSQL("jdbc:hsqldb:"),
        ORACLE("jdbc:oracle:"),
        MY_SQL("jdbc:mysql:"),
        POSTGRES("jdbc:postgresql:");

        public final String prefix;

        DB(String prefix) { this.prefix = prefix;}
    }

    private Map<String, String> hsqldbServiceConfigMap;
    private Map<String, String> postgresqlServiceConfigMap;
    private Map<String, String> oracleServiceConfigMap;

    final private String dbConnectPasswordRegex = "dbConnectPassword\\s*=\\s*\"(.+?)\"";
    final private String dbConnectStringRegex = "dbConnectString\\s*=\\s*\"(.+?)\"";
    final private String dbConnectUserIdRegex = "dbConnectUserId\\s*=\\s*\"(.+?)\"";
    final private String dbDriverNameRegex = "dbDriverName\\s*=\\s*\"(.+?)\"";

//    String oracleConfig =
//            "        <!-- ##### ORACLE ##### -->\n" +
//            "        <!--<property name=\"connection.url\">-->\n" +
//            "            <!--jdbc:oracle:thin:@ldap://oranamesrvr2.cc.emory.edu/esbdev,cn=OracleContext,dc=emory,dc=edu-->\n" +
//            "        <!--</property>-->\n" +
//            "        <!--<property name=\"connection.username\">networkops</property>-->\n" +
//            "        <!--<property name=\"connection.password\">CredentialRedactedAndRotated</property>-->\n" +
//            "        <!--<property name=\"connection.driver_class\">oracle.jdbc.driver.OracleDriver</property>-->\n" +
//            "        <!--<property name=\"dialect\">org.hibernate.dialect.Oracle10gDialect</property>-->\n";
//
//    String hsqldbConfig =
//            "        <!-- ##### HSQLDB ##### -->\n" +
//            "        <property name=\"connection.url\">jdbc:hsqldb:mem:mydb</property>\n" +
//            "        <property name=\"connection.username\">test</property>\n" +
//            "        <property name=\"connection.password\">test</property>\n" +
//            "        <property name=\"connection.driver_class\">org.hsqldb.jdbc.JDBCDriver</property>\n" +
//            "        <property name=\"dialect\">org.hibernate.dialect.HSQLDialect</property>\n";
//
//    String postgresqlConfig =
//            "         <!--##### POSTGRESQL ##### -->\n" +
//            "        <!--<property name=\"connection.url\">jdbc:postgresql://localhost/awsnetops</property>-->\n" +
//            "        <!--<property name=\"connection.username\">postgres</property>-->\n" +
//            "        <!--<property name=\"connection.password\">test</property>-->\n" +
//            "        <!--<property name=\"connection.driver_class\">org.postgresql.Driver</property>-->\n" +
//            "        <!--<property name=\"dialect\">org.hibernate.dialect.PostgreSQLDialect</property>-->\n";

    String hibernateCfgXmlStartSentinel = "<session-factory>";
    String hibernateCfgXmlEndSentinel = "<property name=\"connection.provider_class\">";

    String[] appConfigDbConfigNames = {
        "DbLockConfigs",
        "DbSequenceConfigs",
    };

    List<String> appConfigDbConfigStartTags = new ArrayList<>();
    List<String> appConfigDbConfigEndTags = new ArrayList<>();

    String[] configFilesForService = {
            "deploy/build-test/configs/CiscoAsr1Service.xml",
            "deploy/build-test/configs/CiscoAsr2Service.xml",
    };

    /**
     * Hibernate configuration file for instrumentation starts with DEV, and is output to local.  DEV file is setup
     * per environment for a specific database instance.  Local file can be switched, e.g., hsqldb, postgresql...
     */
//    String hibernateDevConfigResource = "deploy/esb-dev/hibernate.cfg.xml";
//    String hibernateConfigResource = "deploy/build-test/configs/messaging/Environments/Examples/Jars/NetworkOpsService/hibernate/hibernate.cfg.xml";
//    Path hibernateDevConfigResourcePath;
//    Path hibernateConfigResourcePath;

    Collection<Path> configFilePathsForService = null;

    Map<Path, Object> fileContentsMap = new HashMap<>();

    public static void main(String[] args) {
        DbConfigurer dbConfigurer = new DbConfigurer();

        try {


            DB db = dbConfigurer.getDb();

            if (db != null) {
//                dbConfigurer.copyLocalResourceFile();

                dbConfigurer.log("DB selected: " + db);
                dbConfigurer.configureForDb(db);
            } else {
                dbConfigurer.log("Terminated by request.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {


        setupConfigFilePaths();
        setupAppConfigDbConfigNames();
        setupDbConfigMaps();
    }

    private void setupAppConfigDbConfigNames() {
        for (String name : appConfigDbConfigNames) {
            appConfigDbConfigStartTags.add("<" + name + ">");
            appConfigDbConfigEndTags.add("</" + name + ">");
        }
    }

    private void setupDbConfigMaps() {
        hsqldbServiceConfigMap = new HashMap<>();
        hsqldbServiceConfigMap.put("dbConnectString", "jdbc:hsqldb:mem:mydb");
        hsqldbServiceConfigMap.put("dbConnectUserId", "test");
        hsqldbServiceConfigMap.put("dbConnectPassword", "test");
        hsqldbServiceConfigMap.put("dbDriverName", "org.hsqldb.jdbcDriver");

        postgresqlServiceConfigMap = new HashMap<>();
        postgresqlServiceConfigMap.put("dbConnectString", "jdbc:postgresql://localhost/awsnetops");
        postgresqlServiceConfigMap.put("dbConnectUserId", "postgres");
        postgresqlServiceConfigMap.put("dbConnectPassword", "test");
        postgresqlServiceConfigMap.put("dbDriverName", "org.postgresql.Driver");

        oracleServiceConfigMap = new HashMap<>();
        oracleServiceConfigMap.put("dbConnectString", "jdbc:oracle:thin:@ldap://oranamesrvr2.cc.emory.edu/SHRDBDEV,cn=OracleContext,dc=emory,dc=edu");
        oracleServiceConfigMap.put("dbConnectUserId", "awsaccount");
        oracleServiceConfigMap.put("dbConnectPassword", "######");
        oracleServiceConfigMap.put("dbDriverName", "oracle.jdbc.driver.OracleDriver");

    }

    public DbConfigurer() {
        init();
    }

    public void configureForDb(DB db) {
        doIt(db);
        saveFiles();
    }

    public String configureForDb(String lines, String dbAsString) {

        DB db = DB.valueOf(dbAsString);
        setupDbConfigMaps();

        String configuredLines = configureServiceConfigForSelectedDb(lines, db);

        return configuredLines;
    }

    private DB getDb() {

        DB db = null;

        BufferedReader br = null;
        try {

            do {
                br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("1) hsqldb\n2) postgresql\n3) oracle\n0) Exit\n\nEnter choice: ");
                String input = br.readLine();
                input = input.trim();

                boolean quit = true;
                switch (input) {
                    case "1":
                        db = DB.HSQL_MEM;
                        break;
                    case "2":
                        db = DB.POSTGRES;
                        break;
                    case "3":
                        db = DB.ORACLE;
                        break;
                    case "0":
                        break;
                    default:
                        quit = true;
                }
                if (quit) {
                    break;
                }
            } while (true);

        } catch (IOException e) {
            log("Error trying to read db: " + e.getMessage());
        } finally {
            closeReader(br);
        }
        return db;
    }

    private void setupConfigFilePaths() {
        configFilePathsForService = new Vector<>();

        try {
            FileSystem fileSystem = FileSystems.getDefault();
            for (int n = 0; n < configFilesForService.length; n++) {
                Path configFile = fileSystem.getPath(configFilesForService[n]);
                fileContentsMap.put(configFile, new String(Files.readAllBytes(configFile)));
                configFilePathsForService.add(configFile);
            }

            // From -> To : Start with DEV version, write to local version.
//            hibernateDevConfigResourcePath = FileSystems.getDefault().getPath(hibernateDevConfigResource);
//            hibernateConfigResourcePath = FileSystems.getDefault().getPath(hibernateConfigResource);

        } catch (IOException e) {
            log("Error setting up config paths: " + e.getMessage());
            throw new RuntimeException(e);
        }

    }

    private void doIt(DB db) {
        configFilePathsForService.forEach(path -> {
            String lines = (String)fileContentsMap.get(path);
            String configredLines = configureServiceConfigForSelectedDb(lines, db);
            fileContentsMap.put(path, configredLines);
        });

        // Local testing restricted to internal resource

//        try {
//            injectHibernateCfgXmlConfig(hibernateDevConfigResourcePath, hibernateConfigResourcePath, db);
//        } catch (IOException e) {
//            log("Error trying to inject hibernate db config: " + e.getMessage());
//            throw new RuntimeException(e);
//        }

    }

    private String configureServiceConfigForSelectedDb(String lines, DB selectedDb) {

        Pattern pattern;
        Matcher matcher;

        Map<String, String> targetDbConfigMap = null;
        switch(selectedDb) {
            case HSQL_MEM:
                targetDbConfigMap = hsqldbServiceConfigMap;
                break;
            case POSTGRES:
                targetDbConfigMap = postgresqlServiceConfigMap;
                break;
            case ORACLE:
                targetDbConfigMap = oracleServiceConfigMap;
                break;
            default:
                throw new RuntimeException("Unsupported db: " + selectedDb.name());
        }

        pattern = Pattern.compile(dbConnectStringRegex);
        matcher = pattern.matcher(lines);
        String targetReplacement = targetDbConfigMap.get("dbConnectString");
        while (matcher.find()) {
            String replacement = matcher.group(0).replaceFirst(matcher.group(1), targetReplacement);
            lines = lines.replace(matcher.group(0), replacement);
        }

        pattern = Pattern.compile(dbConnectUserIdRegex);
        matcher = pattern.matcher(lines);
        targetReplacement = targetDbConfigMap.get("dbConnectUserId");
        while (matcher.find()) {
            String replacement = matcher.group(0).replaceFirst(matcher.group(1), targetReplacement);
            lines = lines.replace(matcher.group(0), replacement);
        }

        pattern = Pattern.compile(dbConnectPasswordRegex);
        matcher = pattern.matcher(lines);
        targetReplacement = targetDbConfigMap.get("dbConnectPassword");
        while (matcher.find()) {
            String replacement = matcher.group(0).replaceFirst(matcher.group(1), targetReplacement);
            lines = lines.replace(matcher.group(0), replacement);
        }

        pattern = Pattern.compile(dbDriverNameRegex);
        matcher = pattern.matcher(lines);
        targetReplacement = targetDbConfigMap.get("dbDriverName");
        while (matcher.find()) {
            String replacement = matcher.group(0).replaceFirst(matcher.group(1), targetReplacement);
            lines = lines.replace(matcher.group(0), replacement);
        }

        return lines;
    }

//    private void injectHibernateCfgXmlConfig(Path devConfigFilePath, Path localConfigFilePath, DB db) throws IOException {
//        String configLines = null;
//        switch (db) {
//            case HSQL_MEM:
//                configLines = hsqldbConfig;
//                break;
//            case POSTGRES:
//                configLines = postgresqlConfig;
//                break;
//            case ORACLE:
//                configLines = oracleConfig;
//                break;
//            default:
//                throw new RuntimeException("Unsupported hibernate db config " + db.prefix);
//        }
//
//        List<String> lines = Files.readAllLines(devConfigFilePath);
//        List<String> outputLines = new ArrayList<>();
//        int n;
//        for (n = 0; n < lines.size(); n++) {
//            String line = lines.get(n);
//            if (line.contains(hibernateCfgXmlStartSentinel)) {
//                outputLines.add(line);
//                outputLines.add("");
//                break;
//            }
//            outputLines.add(line);
//        }
//
//        for (; n < lines.size(); n++) {
//            if (lines.get(n).contains(hibernateCfgXmlEndSentinel)) {
//                break;
//            }
//        }
//        if (n >= lines.size()) {
//            throw new RuntimeException("Failed to find hibernate config file end sentinel: " + hibernateCfgXmlEndSentinel);
//        }
//
//        outputLines.add(configLines);
//
//        for (; n < lines.size(); n++) {
//            outputLines.add(lines.get(n));
//        }
//
//        StringBuilder builder = new StringBuilder();
//        outputLines.forEach(line -> {
//            builder.append(line + "\n");
//            log(line);
//        });
//
//        fileContentsMap.put(localConfigFilePath, builder.toString());
//    }


    private void saveFiles() {

        StringBuilder builder = new StringBuilder();
        fileContentsMap.forEach((path, lines) -> {
            if (lines instanceof List) {
                if (lines == null | ((List)lines).isEmpty()) {
                    builder.append("No lines for file " + path.getFileName() + "\n");
                }
            } else if (lines instanceof String) {
                if (lines == null | ((String) lines).isEmpty()) {
                    builder.append("No lines for file " + path.getFileName() + "\n");
                }
            }
        });
        if (builder.length() > 0) {
            throw new RuntimeException("Trying to write empty files: \n" + builder.toString());
        }

        fileContentsMap.forEach((path, lines) -> {
            log("Saving " + path + "...");
            saveFile(lines, path);

        });
    }

    private void saveFile(Object lines, Path file) {
        try {
            if (lines instanceof String) {
                Files.write(file, ((String)lines).getBytes());
            } else if (lines instanceof List) {
                Files.write(file, (List)lines);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeReader(BufferedReader br) {
        if (br != null) {
            try {
                br.reset();
                br.close();

            } catch (IOException e) {
                String message = e.getMessage();
                if (!message.equals("Stream not marked")) {
                    log("\n\nError closing buffered reader: " + message);
                }
            }
        }
    }

    private void log(String message) {
        System.out.println(message);
    }

}
