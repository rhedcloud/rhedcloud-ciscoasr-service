package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProviderChooser {

    public enum PROVIDER_TYPE {
        EXAMPLE,
        ACTUAL,
        NONE
    }

    final String ciscoAsr1ConfigFile = "deploy/build-test/configs/CiscoAsr1Service.xml";
    final String ciscoAsr2ConfigFile = "deploy/build-test/configs/CiscoAsr2Service.xml";
    private Path ciscoAsr1ConfigPath;
    private Path ciscoAsr2ConfigPath;

    public static void main(String args[]) {
        ProviderChooser providerChooser = new ProviderChooser();

        providerChooser.doIt();

    }

    public ProviderChooser() {
        init();
    }

    // Public API
    public void chooseExampleProvider() {
        PROVIDER_TYPE provider_type = PROVIDER_TYPE.EXAMPLE;
        doIt(provider_type);
    }

    // Public API
    public void chooseActualProvider() {
        PROVIDER_TYPE provider_type = PROVIDER_TYPE.ACTUAL;
        doIt(provider_type);
    }

    // Public API
    public String choose(String providerTypeString, Path configFilePath) {
        PROVIDER_TYPE provider_type = PROVIDER_TYPE.valueOf(providerTypeString);

        String lines = readLines(configFilePath);
        lines = chooseProviders(lines, provider_type);
        return lines;
    }

    public void doIt() {

        PROVIDER_TYPE provider_type = getProviderType();
        doIt(provider_type);

    }

    private void doIt(PROVIDER_TYPE provider_type) {
        String lines = readLines(ciscoAsr1ConfigPath);
        lines = chooseProviders(lines, provider_type);
        saveChanges(lines, ciscoAsr1ConfigPath);

        lines = readLines(ciscoAsr2ConfigPath);
        lines = chooseProviders(lines, provider_type);
        saveChanges(lines, ciscoAsr2ConfigPath);
    }

    private void init() {
        ciscoAsr1ConfigPath = getPath(ciscoAsr1ConfigFile);
        ciscoAsr2ConfigPath = getPath(ciscoAsr2ConfigFile);
    }

    private String readLines(Path configFilePath) {
        String lines;
        try {
            lines = new String(Files.readAllBytes(configFilePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private Path getPath(String configFile) {
        String pwd = System.getProperty("user.dir");

        if (!new File(configFile).exists()) {
            configFile = pwd + "/" +configFile;
        }

        Path configFilePath = FileSystems.getDefault().getPath(configFile);
        return configFilePath;
    }

    private void saveChanges(String lines, Path configFilePath) {
        try {
            Files.write(configFilePath, lines.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected PROVIDER_TYPE getProviderType() {

        PROVIDER_TYPE provider_type = PROVIDER_TYPE.NONE;

        BufferedReader br = null;
        try {

            do {
                br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("\nProvider Type (E)xample or (A)ctual: ");
                String input = br.readLine();
                input = input.trim();

                if (input.equalsIgnoreCase("E") || input.equalsIgnoreCase("A")) {
                    switch (input.toUpperCase()) {
                        case "E":
                            provider_type = PROVIDER_TYPE.EXAMPLE;
                            break;
                        case "A":
                            provider_type = PROVIDER_TYPE.ACTUAL;
                            break;
                    }
                    break;
                } else if (input.equals("")) {
                    break;
                }
            } while (true);

        } catch (IOException e) {
            log("Error trying to read environment input: " + e.getMessage());
        } finally {
            closeReader(br);
        }

        if (provider_type == PROVIDER_TYPE.NONE) {
            System.out.println("Terminated by request.");
            System.exit(1);
        }

        return provider_type;
    }

    protected void closeReader(BufferedReader br) {
        if (br != null) {
            try {
                br.reset();
                br.close();

            } catch (IOException e) {
                String message = e.getMessage();
                if (!message.equals("Stream not marked")) {
                    log("\n\nError closing buffered reader: " + message);
                }
            }
        }
    }

    private String chooseProviders(String lines, PROVIDER_TYPE provider_type) {
        final String PROVIDER_CLASS_NAME_PATTERN = "<PropertyName>[\\s\\w]+?ProviderClassName</PropertyName>[\\S\\s]+?" +
                "((?:<!--)?<PropertyValue>(.+?Provider)</PropertyValue>(?:-->)?)[\\S\\s]+?" +
                "((?:<!--)?<PropertyValue>(.+?Provider)</PropertyValue>(?:-->)?)";

        Pattern providerClassNamePatter = Pattern.compile(PROVIDER_CLASS_NAME_PATTERN);

        Matcher matcher = providerClassNamePatter.matcher(lines);

        while (matcher.find()) {
            lines = choose(lines, matcher, provider_type);
        }

        return lines;
    }

    private String choose(String lines, Matcher matcher, PROVIDER_TYPE provider_type) {
        String exampleProperty = matcher.group(1).contains(".Example") ? matcher.group(1) : matcher.group(3);
        String actualProperty = matcher.group(1).contains(".Example") ? matcher.group(3) : matcher.group(1);
        String exampleClassName = matcher.group(2).contains(".Example") ? matcher.group(2) : matcher.group(4);
        String actualClassName = matcher.group(2).contains(".Example") ? matcher.group(4) : matcher.group(2);

//        log("\nExample property: " + exampleProperty);
//        log("Example class name: " + exampleClassName);
//        log("Actual property: " + actualProperty);
//        log("Actual class name: " + actualClassName);
        if (provider_type == PROVIDER_TYPE.ACTUAL) {
            if (!exampleProperty.contains("<!--") && !exampleProperty.contains("-->")) {
                lines = lines.replaceFirst(exampleProperty, "<!--" + exampleProperty + "-->");
            }
            String uncommented = actualProperty.replace("<!--", "").replace("-->", "");
            lines = lines.replaceFirst(actualProperty, uncommented);
        } else {
            if (!actualProperty.contains("<!--") && !actualProperty.contains("-->")) {
                lines = lines.replaceFirst(actualProperty, "<!--" + actualProperty + "-->");
            }
            String uncommented = exampleProperty.replace("<!--", "").replace("-->", "");
            lines = lines.replaceFirst(exampleProperty, uncommented);
        }
        return lines;
    }

    private void log(String message) {
        System.out.println(message);
    }


}
