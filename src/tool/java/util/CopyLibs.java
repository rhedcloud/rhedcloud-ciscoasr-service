package util;

//import org.junit.Test;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


public class CopyLibs {

    private final String systemPathRegexForLocalLibs = "\\<systemPath\\>(\\$\\{project.basedir\\}\\/lib\\/.+)\\<\\/systemPath\\>";
    private final String systemPathRegexForEnvLibs = "\\<systemPath\\>(\\$\\{project.basedir\\}\\/deploy\\/build-test\\/libs\\/.+)\\<\\/systemPath\\>";
    private final Pattern systemPathPatternForLocalLibs = Pattern.compile(systemPathRegexForLocalLibs);
    private final Pattern systemPathPatternForEnvLibs = Pattern.compile(systemPathRegexForEnvLibs);

    private final List<String> localLibFileNamesFromPom = new ArrayList<>();
    private final List<String> envLibFileNamesFromPom = new ArrayList<>();

    List<String> compileTimeLibFileNames = null;
    List<String> buildTestLibFileNames = null;
    List<String> devLibFileNames = null;
    List<String> testLibFileNames = null;
    List<String> stageLibFileNames = null;
    List<String> prodLibFileNames = null;

    String pomFilePath = null;
    String libsDirFragment = null;
    String buildTestLibDir = null;
    String devLibDir = null;
    String testLibDir = null;
    String stageLibDir = null;
    String prodLibDir = null;
    String libDir = null;

    List<FileInfo> versionedFiles = null;

    private boolean verbose = true;

    public static void main(String[] args) {
        CopyLibs copyLibs = new CopyLibs();
        try {
            copyLibs.copy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    @Test
    public void copy() {
        // This is the one, module location-dependent implementation. copy-files-tool module is a subdir of the project.
        String cwd = System.getProperty("user.dir").replaceAll("\\\\", "/").replaceAll("[A-Z]\\:", "") + "/..";
        cwd = System.getProperty("user.dir");

        pomFilePath = cwd + "/pom.xml";
        libsDirFragment = "/libs/CiscoAsrService/";
        buildTestLibDir = cwd + "/deploy/build-test" + libsDirFragment;
        devLibDir = cwd + "/deploy/esb-dev" + libsDirFragment;
        testLibDir = cwd + "/deploy/esb-test" + libsDirFragment;
        stageLibDir = cwd + "/deploy/esb-stage" + libsDirFragment;
        prodLibDir = cwd + "/deploy/esb-prod" + libsDirFragment;
        libDir = cwd + "/lib/";

        if (verbose) {
            log("cwd: " + cwd);
            log("buildTestLibDir: " + buildTestLibDir);
            log("devLibDir: " + devLibDir);
            log("testLibDir: " + testLibDir);
            log("stageLibDir: " + stageLibDir);
            log("prodLibDir: " + prodLibDir);
            log("libDir: " + libDir);

        }

        // TODO:
        // 1) Copy all jars from lib -> deploy/build-test/libs/CiscoAsrService/.
        // 2) Check for multiple jar entries - if more than one, keep latest version.
        // 3) Delete all jars from esb-dev, esb-test, esb-stage, esb-prod - LEAVE jndi.properties file!
        // 4) Copy all jars from deploy/build-test/libs/CiscoAsrService/. -> esb-dev, esb-test, esb-stage, esb-prod
        // 5) Leave all copies of jndi.properties - these are manually maintained

        compileTimeLibFileNames = getDirectoryListing(libDir);
        buildTestLibFileNames = getDirectoryListing(buildTestLibDir);
        devLibFileNames = getDirectoryListing(devLibDir);
        testLibFileNames = getDirectoryListing(testLibDir);
        stageLibFileNames = getDirectoryListing(stageLibDir);
        prodLibFileNames = getDirectoryListing(prodLibDir);


        getPomSystemFilenames(pomFilePath);

        if (verbose ) {
            log("\n\nCompile time libs:");
            localLibFileNamesFromPom.forEach(path -> {
                log("    " + path);
            });

            log("\n\nRuntime libs:");
            envLibFileNamesFromPom.forEach(file -> {
                log("    " + file);
            });

        }

        checkPomToCompileTimeLibReferences();

        log("\nCompile time lib dir matches pom.xml entries. Copying to environmental lib dirs...");


        log("\nCopying lib dir files to build-test and env dirs...");

        copyFiles(compileTimeLibFileNames, libDir, buildTestLibDir);
        copyFiles(compileTimeLibFileNames, libDir, devLibDir);
        copyFiles(compileTimeLibFileNames, libDir, testLibDir);
        copyFiles(compileTimeLibFileNames, libDir, stageLibDir);
        copyFiles(compileTimeLibFileNames, libDir, prodLibDir);

        log("\nRemoving duplicates - typically emory-moa jar revs...");

        removeDuplicatesKeepLatestVersions(buildTestLibFileNames, FileSystems.getDefault().getPath(buildTestLibDir));
        removeDuplicatesKeepLatestVersions(buildTestLibFileNames, FileSystems.getDefault().getPath(devLibDir));
        removeDuplicatesKeepLatestVersions(buildTestLibFileNames, FileSystems.getDefault().getPath(testLibDir));
        removeDuplicatesKeepLatestVersions(buildTestLibFileNames, FileSystems.getDefault().getPath(stageLibDir));
        removeDuplicatesKeepLatestVersions(buildTestLibFileNames, FileSystems.getDefault().getPath(prodLibDir));

        log("\nDone.");

    }

    private void getPomSystemFilenames(String pomFilePath) {

        try {
            Path path = FileSystems.getDefault().getPath(pomFilePath);
            List<String> lines = Files.readAllLines(path);
            lines.forEach(line -> {
                Matcher matcher = systemPathPatternForLocalLibs.matcher(line);
                if (matcher.find()) {
                    String fileName = matcher.group(1);
                    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                    localLibFileNamesFromPom.add(fileName);
                }
                matcher = systemPathPatternForEnvLibs.matcher(line);
                if (matcher.find()) {
                    String fileName = matcher.group(1);
                    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                    envLibFileNamesFromPom.add(fileName);
                }

            });
        } catch (Exception e) {
            log(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void checkPomToCompileTimeLibReferences() {
        if (verbose) {
            log("Checking compile time libs against POM references...");
        }
        final List<String> unreferencedCompileTimeJars = new ArrayList<>();
        final List<String> missingCompileTimeJars = new ArrayList<>();
        compileTimeLibFileNames.forEach(fileRef -> {
            if (verbose) {
                log("   " + fileRef);
            }
            if (!localLibFileNamesFromPom.contains(fileRef)) {
                unreferencedCompileTimeJars.add(fileRef);
            }
        });

        if (verbose) {
            log("Checking POM references against compile time libs...");
        }
        localLibFileNamesFromPom.forEach(fileRef -> {
            if (verbose) {
                log("   " + fileRef);
            }
            if (!compileTimeLibFileNames.contains(fileRef)) {
                missingCompileTimeJars.add(fileRef);
            }
        });
        StringBuilder builder = new StringBuilder();
        if (unreferencedCompileTimeJars.size() > 0) {
            builder.append("Unreferenced compile time jars found:\n");
            unreferencedCompileTimeJars.forEach(unrefJar -> {
                builder.append("-> " + unrefJar + "\n");
            });
        }
        if (missingCompileTimeJars.size() > 0) {
            builder.append("Missing (pom referencing non-existant) compile time jars:\n");
            missingCompileTimeJars.forEach(missingJar -> {
                builder.append("-> " + missingJar + "\n");
            });
        }
        if (builder.length() > 0) {
            throw new RuntimeException(builder.toString());
        }
    }

    private void copyFiles(final List<String> sourceFiles, final String sourceDirectoryPath, final String destinationDirectoryPath) {
        FileSystem fileSystem = FileSystems.getDefault();

        sourceFiles.forEach(file -> {
            Path source = fileSystem.getPath(sourceDirectoryPath + file);
            Path destination = fileSystem.getPath(destinationDirectoryPath + file);
            try {
                Files.copy(source, destination, REPLACE_EXISTING);
            } catch (IOException e) {
                String errMsg = "IOException while copying file [" + file + "]: ";
                log(errMsg, e);
                throw new RuntimeException(errMsg, e);
            }
        });
    }

    private void removeDuplicatesKeepLatestVersions(List<String> files, Path envDir) {
        List<FileInfo> nameToVersionMap = new ArrayList<>();

        files.forEach(file -> {

            Pattern pattern;

            pattern = Pattern.compile("((.+?)(\\-)(\\d)(.+)(\\.jar))");
            Matcher matcher = pattern.matcher(file);

            if (matcher.matches()) {
                int count = matcher.groupCount();
                String baseName;

                if (count == 6) {
                    baseName = matcher.group(2);
                    String version = matcher.group(4) + matcher.group(5);
                    nameToVersionMap.add(new FileInfo(baseName, version));
                }
            } else {
                nameToVersionMap.add(new FileInfo(file.replace(".jar", ""), ""));
            }
        });

        if (verbose) {
            log("\n====================== All files count: [" + nameToVersionMap.size() + "]\n");
        }

        versionedFiles = nameToVersionMap
                .stream()
                .filter(fileInfo -> !fileInfo.version.isEmpty())
                .collect(Collectors.toList());

        if (verbose) {
            log("\n====================== Versioned files size: [" + versionedFiles.size() + "]\n");
        }

        versionedFiles
                .stream()
                .filter(fileInfo -> Collections.frequency(versionedFiles, fileInfo) > 1)
                .collect(Collectors.toSet())
                .forEach(fileInfo -> {keepLatestVersionInBuildTestLib(fileInfo, envDir);});
    }

    private void keepLatestVersionInBuildTestLib(FileInfo fileInfo, Path envDir) {
        String fileWildcard = fileInfo.root + "-*.jar";
        try {
            DirectoryStream<Path> paths = Files.newDirectoryStream(envDir, fileWildcard);

            String keepFileName = "";

            for (Path path : paths) {
                String fileName = path.getFileName().toString();
                if (fileName.compareTo(keepFileName) > 0) {
                    keepFileName = fileName;
                }
            };

            paths = Files.newDirectoryStream(envDir, fileWildcard);

            for (Path path : paths)  {
                if (!path.getFileName().toString().equals(keepFileName)) {
                    // TODO: This is the DELETE
                    Files.delete(path);
                }
            };
        } catch (IOException e) {
            throw new RuntimeException("Failed to get build-test lib dir files for " + fileWildcard, e);
        }
    }

    private List<String> getDirectoryListing(String dirPath) {
        List<String> fileNames = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(dirPath))) {
            for (Path path : directoryStream) {
                fileNames.add(path.getFileName().toString());
            }
        } catch (IOException ex) {}
        return fileNames;
    }

    private class FileInfo {
        int hashCode;
        private final String root;
        private final String version;

        public FileInfo(String root, String version) {
            this.root = root;
            this.version = version;
            this.hashCode = root.hashCode();
        }
        @Override
        public boolean equals(Object obj){
            if(obj == null)
                return false;
            else if(this==obj)
                return true;
            else
                return this.hashCode() == ((FileInfo)obj).hashCode();
        }
        @Override
        public int hashCode() {
            return hashCode;
        }
    }

    private void log(String msg) {
        System.out.println(msg);
    }

    private void log(String msg, Exception e) {
        System.out.println(msg);
        e.printStackTrace();
    }

    private void log(FileInfo fileInfo) {
        log("root: " + fileInfo.root + "  version: " + fileInfo.version);
    }
}
