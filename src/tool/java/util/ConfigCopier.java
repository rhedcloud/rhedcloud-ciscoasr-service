package util;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * This class uses the subversion repository https://svn.service.emory.edu:8443/repos/emoryoit/deployment/esb/dev/Deployments as the keeper of secrets.
 * The requirement is to checkout the directory relative to this project directory so that the relative svn directory ^/deployment/esb/dev/Deployments
 * is located in the relative path ../serviceConfigs/Deployments
 *
 */
public class ConfigCopier {

    private final String serviceConfigsRoot = "/../../serviceConfigs";

    private final String jmsInitialContextFactoryRegex = "<InitialContextFactory>.+?</InitialContextFactory>";
    private final String jmsProviderUrlRegex = "<ProviderURL>.+?</ProviderURL>";
    private final String useLockRegex = "(<PropertyName>useLock</PropertyName>[\\S\\s]+?" +
            "<PropertyValue>)(false)(</PropertyValue>)";
    private final String securityPrincipalRegex = "<SecurityPrincipal>.+?</SecurityPrincipal>";

    private final String jmsInitialContextFactory = "<InitialContextFactory>com.sonicsw.jndi.mfcontext.MFContextFactory</InitialContextFactory>";
    private final String jmsProviderUrl = "<ProviderURL>tcp://intsonicdev1.cc.emory.edu:2506</ProviderURL>";

    private final String localCiscoAsr1ConfigFile = "/deploy/build-test/configs/CiscoAsr1Service.xml";
    private final String localCiscoAsr2ConfigFile = "/deploy/build-test/configs/CiscoAsr2Service.xml";
    private final String devCiscoAsr1ConfigFile = "/deploy/esb-dev/configs/CiscoAsr1Service.xml";
    private final String devCiscoAsr2ConfigFile = "/deploy/esb-dev/configs/CiscoAsr2Service.xml";
    private final String serviceConfigsCiscoAsr1ConfigFile = serviceConfigsRoot + "/Deployments/CiscoAsr1Service.xml";
    private final String serviceConfigsCiscoAsr2ConfigFile = serviceConfigsRoot + "/Deployments/CiscoAsr2Service.xml";

    private final String localTestSuiteConfigFile = "/deploy/build-test/configs/TestSuiteApplication-CiscoAsrService.xml";
    private final String devTestSuiteConfigFile = "/deploy/esb-dev/configs/TestSuiteApplication-CiscoAsrService.xml";
    private final String serviceConfigsTestSuiteConfigFile = serviceConfigsRoot + "/Deployments/TestSuiteApplication-CiscoAsrService.xml";

    private final String localTestSuiteTestsDir = "/deploy/build-test/tests/";
    private final String devTestSuiteTestsDir = "/deploy/esb-dev/tests/";
    private final String serviceConfigsTestSuiteTestsDir = serviceConfigsRoot + "/TestSuites/CiscoAsrService/";

    private final String testSuiteRootFilename = "TestSuite-CiscoAsrService";

    private Map<String, String> jndiNamingMap;


    public static void main(String[] args) {
        ConfigCopier configCopier = new ConfigCopier();

        configCopier.doIt();
    }

    String jmsPassword;

    private void doIt() {

        initJndiNamingMap();

        FileSystem fileSystem = FileSystems.getDefault();
        String cwd = System.getProperty("user.dir");

        Path localCiscoAsr1ConfigFilePath = fileSystem.getPath(cwd + localCiscoAsr1ConfigFile);
        Path localCiscoAsr2ConfigFilePath = fileSystem.getPath(cwd + localCiscoAsr2ConfigFile);
        Path localTestSuiteConfigFilePath = fileSystem.getPath(cwd + localTestSuiteConfigFile);
        Path serviceConfigsCiscoAsr1ConfigFilePath = fileSystem.getPath(cwd + serviceConfigsCiscoAsr1ConfigFile);
        Path serviceConfigsCiscoAsr2ConfigFilePath = fileSystem.getPath(cwd + serviceConfigsCiscoAsr2ConfigFile);
        Path serviceConfigsTestSuiteConfigFilePath = fileSystem.getPath(cwd + serviceConfigsTestSuiteConfigFile);

        String localCiscoAsr1ConfigDoc;
        String localCiscoAsr2ConfigDoc;
        String localTestSuiteConfigDoc;

        try {
            localTestSuiteConfigDoc = new String(Files.readAllBytes(localTestSuiteConfigFilePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        ProviderChooser providerChooser = new ProviderChooser();
        localCiscoAsr1ConfigDoc = providerChooser.choose("ACTUAL", localCiscoAsr1ConfigFilePath);
        localCiscoAsr2ConfigDoc = providerChooser.choose("ACTUAL", localCiscoAsr2ConfigFilePath);

        DbConfigurer dbConfigurer = new DbConfigurer();
        localCiscoAsr1ConfigDoc = dbConfigurer.configureForDb(localCiscoAsr1ConfigDoc, "ORACLE");
        localCiscoAsr2ConfigDoc = dbConfigurer.configureForDb(localCiscoAsr2ConfigDoc, "ORACLE");
        localTestSuiteConfigDoc = dbConfigurer.configureForDb(localTestSuiteConfigDoc, "ORACLE");

        localTestSuiteConfigDoc = convertTestConfigJndiNaming(localTestSuiteConfigDoc);
        localTestSuiteConfigDoc = setTestConfigSecurityPrincipal(localTestSuiteConfigDoc);

        Map<Path, String> configDocsMap = new HashMap<>();

        boolean doInstrumentConfigDocs = true;
        InstrumentConfigDocs instrumentConfigDocs = null;

        configDocsMap.put(serviceConfigsCiscoAsr1ConfigFilePath, localCiscoAsr1ConfigDoc);
        configDocsMap.put(serviceConfigsCiscoAsr2ConfigFilePath, localCiscoAsr2ConfigDoc);
        configDocsMap.put(serviceConfigsTestSuiteConfigFilePath, localTestSuiteConfigDoc);
        if (doInstrumentConfigDocs) {
            instrumentConfigDocs = new InstrumentConfigDocs();
            instrumentConfigDocs.instrument(configDocsMap);
        }
        // Miscellaneous stuff...
        configDocsMap.forEach((path, lines) -> {
            lines = setupJmsProvider(lines);
            lines = convertPaths(lines);
            lines = setUseLockTrue(lines);

            configDocsMap.put(path, lines);
        });

        // Write to svn dir...
        configDocsMap.forEach((path, lines) -> {
            try {
                Files.write(path, lines.getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        boolean doDev = false;
        if (doDev) {
            // Obfuscate and copy svn dir app config files processed above to dev
            updateDevEnvironmentAppConfigs(instrumentConfigDocs);

            // Copy test suites from local to svn and dev locations
            DirectoryStream<Path> localTestSuiteFilePaths = getLocalTestSuiteFilePaths();

            Map<Path, String> serviceConfigsTestSuitesMap = getTestSuiteFilePathMap(localTestSuiteFilePaths, serviceConfigsTestSuiteTestsDir);
            localTestSuiteFilePaths = getLocalTestSuiteFilePaths();
            Map<Path, String> devTestSuitesMap = getTestSuiteFilePathMap(localTestSuiteFilePaths, devTestSuiteTestsDir);

            convertTestSuitePaths(serviceConfigsTestSuitesMap);
            convertTestSuitePaths(devTestSuitesMap);

            serviceConfigsTestSuitesMap.forEach((path, lines) -> {
                try {
                    Files.write(path, lines.getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

            devTestSuitesMap.forEach((path, lines) -> {
                try {
                    Files.write(path, lines.getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    /**
     * <ConnectionFactoryName>TestSuiteAppProducerQCF</ConnectionFactoryName> -> <ConnectionFactoryName>cn=NetworkOpsServiceConsumerQCF</ConnectionFactoryName>
     * <ConnectionFactoryName>TestSuiteAppConsumerTCF</ConnectionFactoryName> -> <ConnectionFactoryName>cn=NetworkOpsServiceProducerTCF</ConnectionFactoryName>
     * <ConnectionFactoryName>TestSuiteAppProducerTCF</ConnectionFactoryName> -> <ConnectionFactoryName>cn=NetworkOpsServiceProducerTCF</ConnectionFactoryName>
     * <DestinationName>TestSuiteAppTopic</DestinationName> -> <DestinationName>cn=NetworkOpsServiceProducerTCF</DestinationName>
     */
    private void initJndiNamingMap() {
        jndiNamingMap = new HashMap<>();
        jndiNamingMap.put(
                "<ConnectionFactoryName>TestSuiteApp1ProducerQCF</ConnectionFactoryName>",
                "<ConnectionFactoryName>cn=CiscoAsr1ServiceConsumerQCF</ConnectionFactoryName>");
        jndiNamingMap.put(
                "<ConnectionFactoryName>TestSuiteApp2ProducerQCF</ConnectionFactoryName>",
                "<ConnectionFactoryName>cn=CiscoAsr2ServiceConsumerQCF</ConnectionFactoryName>");
        jndiNamingMap.put(
                "<ConnectionFactoryName>TestSuiteApp1ConsumerTCF</ConnectionFactoryName>",
                "<ConnectionFactoryName>cn=CiscoAsr1ServiceProducerTCF</ConnectionFactoryName>");
        jndiNamingMap.put(
                "<ConnectionFactoryName>TestSuiteApp2ConsumerTCF</ConnectionFactoryName>",
                "<ConnectionFactoryName>cn=CiscoAsr2ServiceProducerTCF</ConnectionFactoryName>");
        jndiNamingMap.put(
                "<ConnectionFactoryName>TestSuiteApp1ProducerTCF</ConnectionFactoryName>",
                "<ConnectionFactoryName>cn=CiscoAsr1ServiceProducerTCF</ConnectionFactoryName>");
        jndiNamingMap.put(
                "<ConnectionFactoryName>TestSuiteApp2ProducerTCF</ConnectionFactoryName>",
                "<ConnectionFactoryName>cn=CiscoAsr2ServiceProducerTCF</ConnectionFactoryName>");
        jndiNamingMap.put(
                "<DestinationName>TestSuiteApp1Topic</DestinationName>",
                "<DestinationName>cn=CiscoAsr1ServiceProducerTCF</DestinationName>");
        jndiNamingMap.put(
                "<DestinationName>TestSuiteApp2Topic</DestinationName>",
                "<DestinationName>cn=CiscoAsr2ServiceProducerTCF</DestinationName>");
    }

    private String setupJmsProvider(String lines) {

        lines = lines.replaceAll(jmsInitialContextFactoryRegex, jmsInitialContextFactory);
        lines = lines.replaceAll(jmsProviderUrlRegex, jmsProviderUrl);
        return lines;
    }

    private String convertPaths(String lines) {
        String localPath = "docUri=\"eos/";
        String devPath = "docUri=\"configs/messaging/Environments/Examples/EnterpriseObjects/3.0/";
        lines = lines.replace(localPath, devPath);

        lines = lines.replace("<PropertyValue>logs/CiscoAsrService.log</PropertyValue>",
                "<PropertyValue>/var/log/tomcat8/CiscoAsrService.log</PropertyValue>");
        return lines;

    }

    private DirectoryStream<Path> getLocalTestSuiteFilePaths() {
        String cwd = System.getProperty("user.dir");
        FileSystem fileSystem = FileSystems.getDefault();
        DirectoryStream<Path> localTestSuiteTestPaths;
        Path localTestSuitesPath = fileSystem.getPath(cwd + localTestSuiteTestsDir);
        try {
            localTestSuiteTestPaths = Files.newDirectoryStream(localTestSuitesPath, testSuiteRootFilename + "-*.xml");
        } catch (IOException e) {
            throw new RuntimeException("Failed to list test suite files", e);
        }

        return localTestSuiteTestPaths;
    }

    private Map<Path, String> getTestSuiteFilePathMap(DirectoryStream<Path> localTestSuitePaths, String targetDir) {
        String cwd = System.getProperty("user.dir");
        FileSystem fileSystem = FileSystems.getDefault();
        Map<Path, String> testSuiteMap = new HashMap<>();

        localTestSuitePaths.forEach(path -> {
            Path configDirPath = fileSystem.getPath(cwd + targetDir + path.getFileName());

            try {
                testSuiteMap.put(configDirPath, new String(Files.readAllBytes(path)));
            } catch (IOException e) {
                throw new RuntimeException("Failed to read test suite file " + path.getFileName(), e);
            }

        });
        return testSuiteMap;
    }

    private void updateDevEnvironmentAppConfigs(InstrumentConfigDocs instrumentConfigDocs) {
        FileSystem fileSystem = FileSystems.getDefault();
        String cwd = System.getProperty("user.dir");

        try {
            Path serviceConfigFile1Path = fileSystem.getPath(cwd + serviceConfigsCiscoAsr1ConfigFile);
            Path serviceConfigFile2Path = fileSystem.getPath(cwd + serviceConfigsCiscoAsr2ConfigFile);
            Path testSuiteConfigPath = fileSystem.getPath(cwd + serviceConfigsTestSuiteConfigFile);

            String ciscoAsr1Config = new String(Files.readAllBytes(serviceConfigFile1Path));
            String ciscoAsr2Config = new String(Files.readAllBytes(serviceConfigFile2Path));
            String testSuiteConfig = new String(Files.readAllBytes(testSuiteConfigPath));
            Map<Path, String> configFilePathMap = new HashMap<>();

            Path devConfigFile1Path = fileSystem.getPath(cwd + devCiscoAsr1ConfigFile);
            Path devConfigFile2Path = fileSystem.getPath(cwd + devCiscoAsr2ConfigFile);
            Path devTestSuiteConfigPath = fileSystem.getPath(cwd + devTestSuiteConfigFile);
            configFilePathMap.put(devConfigFile1Path, ciscoAsr1Config);
            configFilePathMap.put(devConfigFile2Path, ciscoAsr2Config);
            configFilePathMap.put(devTestSuiteConfigPath, testSuiteConfig);
            instrumentConfigDocs.obfuscate(configFilePathMap);

            configFilePathMap.forEach((path, lines) -> {
                try {
                    Files.write(path, lines.getBytes());
                } catch (IOException e) {
                    throw new RuntimeException("Failed to write obfuscated config file to esb-dev", e);
                }
            });

        } catch (IOException e) {
            throw new RuntimeException("Error while reading service config files", e);
        }
    }

    private void copyFile(String sourceFile, String destinationFile) {
        String cwd = System.getProperty("user.dir");
        FileSystem fileSystem = FileSystems.getDefault();
        Path sourcePath = fileSystem.getPath(cwd + sourceFile);
        Path destPath = fileSystem.getPath(cwd + destinationFile);

        try {
            Files.copy(sourcePath, destPath, REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Failed to copy file", e);
        }
    }

    private void convertTestSuitePaths(Map<Path, String> testSuitePathMap) {
        String configDirTestSuiteDir = "configs/messaging/Environments/Examples/TestSuites/CiscoAsrService/";

        testSuitePathMap.forEach((path, string) -> {
            string = string.replaceAll("xslUri=\"tests/", "xslUri=\"" + configDirTestSuiteDir);
            testSuitePathMap.put(path, string);
        });
    }

    private String convertTestConfigJndiNaming(String testConfig) {

        for (Map.Entry<String, String> entry : jndiNamingMap.entrySet()) {
            testConfig = testConfig.replaceAll(entry.getKey(), entry.getValue());
        }

        return testConfig;
    }

    private String setTestConfigSecurityPrincipal(String testConfig) {
        return testConfig.replaceAll(securityPrincipalRegex, "<SecurityPrincipal>CiscoAsr1Service</SecurityPrincipal>");
    }

    private String setUseLockTrue(String lines) {
        Pattern pattern = Pattern.compile(useLockRegex);
        Matcher matcher = pattern.matcher(lines);
        while (matcher.find()) {
            lines = lines.replaceFirst(matcher.group(), matcher.group(1) + "true" + matcher.group(3));
        }
        return lines;
    }


}
