package util;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetupForLocalTests {

    private final String useLockRegex = "(<PropertyName>useLock</PropertyName>[\\S\\s]+?" +
            "<PropertyValue>)(false)(</PropertyValue>)";

    private final String localAppConfigFile1 = "/deploy/build-test/configs/CiscoAsr1Service.xml";
    private final String localAppConfigFile2 = "/deploy/build-test/configs/CiscoAsr2Service.xml";

    public static void main(String[] args) {
        SetupForLocalTests setupForLocalTests = new SetupForLocalTests();

        setupForLocalTests.doIt();
    }

    public void doIt() {

        try {
            ProviderChooser providerChooser = new ProviderChooser();
            providerChooser.chooseActualProvider();
        } catch (Exception e) {
            System.err.println("Failed to choose provider");
            throw new RuntimeException(e);
        }

        try {
            DbConfigurer dbConfigurer = new DbConfigurer();
            dbConfigurer.configureForDb(DbConfigurer.DB.POSTGRES);
        } catch (Exception e) {
            System.err.println("Failed to configure db for postgres");
            throw new RuntimeException(e);
        }

        try {
            InstrumentConfigDocs instrumentConfigDocs = new InstrumentConfigDocs();
            instrumentConfigDocs.setupLocalConfigSecrets();
        } catch (Exception e) {
            System.err.println("Failed to setup local config secrets");
            throw new RuntimeException(e);
        }

        String cwd = System.getProperty("user.dir");
        Path localConfigFilePath1 = FileSystems.getDefault().getPath(cwd + localAppConfigFile1);
        Path localConfigFilePath2 = FileSystems.getDefault().getPath(cwd + localAppConfigFile2);

        Map<Path, String> configDocsMap = new HashMap<>();

        // All modifications go here...
        String lines = readLines(localConfigFilePath1);
        lines = setUseLockToTrue(lines);
        configDocsMap.put(localConfigFilePath1, lines);

        lines = readLines(localConfigFilePath2);
        lines = setUseLockToTrue(lines);
        configDocsMap.put(localConfigFilePath2, lines);

        saveChanges(configDocsMap);

    }

    private String setUseLockToTrue(String lines) {

        lines = setUseLockTrue(lines);

        return lines;
    }


    private String setUseLockTrue(String lines) {
        Pattern pattern = Pattern.compile(useLockRegex);
        Matcher matcher = pattern.matcher(lines);
        while (matcher.find()) {
            lines = lines.replaceFirst(matcher.group(), matcher.group(1) + "true" + matcher.group(3));
        }
        return lines;
    }

    private String readLines(Path configFilePath) {
        String lines;
        try {
            lines = new String(Files.readAllBytes(configFilePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private void saveChanges(Map<Path, String> configDocsMap) {
        configDocsMap.forEach((path, l) -> {
            try {
                Files.write(path, l.getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }


}
