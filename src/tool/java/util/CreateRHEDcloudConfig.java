package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateRHEDcloudConfig {

    final private String SERVICE_NAME_PATTERN = "Emory Network Operations Service";
    final private String NEW_SERVICE_NAME = "RHEDcloud Network Operations Service";
    final private String LOG_FILE_PATH_PATTERN = "logs/CiscoAsrService.log";
    final private String NEW_LOG_FILE_PATH = "/var/log/tomcat8/CiscoAsrService.log";
    final private String JMS_PROVIDER_URL = "tcp://localhost:61616";
    final private String NEW_JMS_PROVIDER_URL = "ssl://b-a4c98873-ebf6-4a53-9144-11ca245173e1-1.mq.us-east-1.amazonaws.com:61617";
    final private String JMS_SECURITY_PRINCIPAL_PATTERN = "(<SecurityPrincipal>)(.+?)(</SecurityPrincipal>)";
    private String NEW_JMS_SECURITY_PRINCIPAL = "";
    final private String JMS_SECURITY_CREDENTIALS_PATTERN = "(<SecurityCredentials>)(.+?)(</SecurityCredentials>)";
    private String NEW_JMS_SECURITY_CREDENTIALS = "";
    final private String JMS_USERNAME_PATTERN = "(<Username>)(.+?)(</Username>)";
    private String NEW_JMS_USERNAME = "";
    final private String JMS_PASSWORD_PATTERN = "(<Password>)(.+?)(</Password>)";
    private String NEW_JMS_PASSWORD = "";
    final private String DB_DRIVER_NAME_PATTERN = "(dbDriverName=\")([^\"?]+)(\")";
    final private String NEW_DB_DRIVER_NAME = "oracle.jdbc.driver.OracleDriver";
    final private String DB_CONNECT_STRING_PATTERN = "(dbConnectString=\")([^\"?]+)(\")";
    final private String NEW_DB_CONNECT_STRING = "jdbc:oracle:thin:@rhedcloud-dev-1.cqbnsdsexba3.us-east-1.rds.amazonaws.com:1521:RHEDCLD1";
    final private String DB_CONNECT_USER_ID_PATTERN = "(dbConnectUserId=\")([^\"?]+)(\")";
    private String NEW_DB_CONNECT_USER_ID = "";
    final private String DB_CONNECT_PASSWORD_PATTERN = "(dbConnectPassword=\")([^\"?]+)(\")";
    private String NEW_DB_CONNECT_PASSWORD = "";
    final private String ROUTER_CREDENTIALS_SECRET_ACCESS_KEY_PATTERN = "(<PropertyName>routerCredentialsSecretAccessKey</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
    final private String ROUTER_CREDENTIALS_SECRET_KEY_PATTERN = "(<PropertyName>routerCredentialsSecretKey</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
    final private String ROUTER_CREDENTIALS_SECRET_NAME_PATTERN = "(<PropertyName>routerCredentialsSecretName</PropertyName>[\\S\\s]+?<PropertyValue>)([^<?]+)(</PropertyValue>)";
    private String NEW_ROUTER_CREDENTIALS_SECRET_ACCESS_KEY;
    private String NEW_ROUTER_CREDENTIALS_SECRET_KEY;
    private String NEW_ROUTER_CREDENTIALS_SECRET_NAME;

            /*
                                                   <Property>
                                            <PropertyName>routerCredentialsSecretAccessKey</PropertyName>
                                            <PropertyValue>######</PropertyValue>
                                        </Property>
                                        <Property>
                                            <PropertyName>routerCredentialsSecretKey</PropertyName>
                                            <PropertyValue>######</PropertyValue>
                                        </Property>
                                        <Property>
                                            <PropertyName>routerCredentialsSecretName</PropertyName>
                                            <PropertyValue>######</PropertyValue>
                                        </Property>

             */
    private final String credentialsFile = "/deploy/sjbtest/credentials.txt";
    private final String localAppConfigFile1 = "/deploy/build-test/configs/CiscoAsr1Service.xml";
    private final String localAppConfigFile2 = "/deploy/build-test/configs/CiscoAsr2Service.xml";
    private final String devAppConfigFile1 = "/deploy/sjbtest/CiscoAsr1Service-dev-rhedcloud.xml";
    private final String devAppConfigFile2 = "/deploy/sjbtest/CiscoAsr2Service-dev-rhedcloud.xml";

    public static void main(String[] args) {
        CreateRHEDcloudConfig creator = new CreateRHEDcloudConfig();

        creator.doIt();
    }

    private void doIt() {
        String cwd = System.getProperty("user.dir");

        Path localConfigFilePath1 = FileSystems.getDefault().getPath(cwd + localAppConfigFile1);
        Path localConfigFilePath2 = FileSystems.getDefault().getPath(cwd + localAppConfigFile2);
        Path devAppConfigFilePath1 = FileSystems.getDefault().getPath(cwd + devAppConfigFile1);
        Path devAppConfigFilePath2 = FileSystems.getDefault().getPath(cwd + devAppConfigFile2);

        processConfigFile(localConfigFilePath1, devAppConfigFilePath1, cwd);
        processConfigFile(localConfigFilePath2, devAppConfigFilePath2, cwd);
    }

    private void processConfigFile(Path localConfigFilePath1, Path devAppConfigFilePath, String cwd) {
        Properties credentials = readCredentials(cwd);

        NEW_JMS_SECURITY_PRINCIPAL = credentials.getProperty("jms.security.principal");
        NEW_JMS_SECURITY_CREDENTIALS = credentials.getProperty("jms.security.credentials");
        NEW_JMS_USERNAME = credentials.getProperty("jms.username");
        NEW_JMS_PASSWORD = credentials.getProperty("jms.password");
        NEW_DB_CONNECT_USER_ID = credentials.getProperty("db.connect.userid");
        NEW_DB_CONNECT_PASSWORD = credentials.getProperty("db.connect.password");
        NEW_ROUTER_CREDENTIALS_SECRET_ACCESS_KEY = credentials.getProperty("router.secrets.secret_access_key");
        NEW_ROUTER_CREDENTIALS_SECRET_KEY = credentials.getProperty("router.secrets.secret_key");
        NEW_ROUTER_CREDENTIALS_SECRET_NAME = credentials.getProperty("router.secrets.secret_name");


        String configDoc;

        ProviderChooser providerChooser = new ProviderChooser();
        configDoc = providerChooser.choose("ACTUAL", localConfigFilePath1);

        // <Deployment name="Emory Network Operations Service" baseURI="none" status="planned" type="development">
        // <FullName>Emory Network Operations Service</FullName>
        // <Description>The Emory Network Operations Service (NOS) is an ESB service
        configDoc = changeServiceName(configDoc, NEW_SERVICE_NAME);


        // <PropertyValue>logs/org.rhedcloud.NetworkOpsService.log</PropertyValue>  ->  <PropertyValue>/var/log/tomcat8/NetworkOpsService.log</PropertyValue>
        configDoc = changeLogFilePath(configDoc, NEW_LOG_FILE_PATH);

        // <ProviderURL>tcp://localhost:61616</ProviderURL>  ->  <ProviderURL>ssl://b-a4c98873-ebf6-4a53-9144-11ca245173e1-1.mq.us-east-1.amazonaws.com:61617</ProviderURL>
        configDoc = changeJmsProviderURL(configDoc, NEW_JMS_PROVIDER_URL);

        // <SecurityPrincipal>NetworkOpsService</SecurityPrincipal>  ->  <SecurityPrincipal>NetworkOpsService</SecurityPrincipal>
        configDoc = changeJmsSecurityPrincipal(configDoc, NEW_JMS_SECURITY_PRINCIPAL);
        configDoc = changeJmsUsername(configDoc, NEW_JMS_USERNAME);

        // <SecurityCredentials>not used</SecurityCredentials>  ->  <SecurityCredentials>NetworkOpsService22</SecurityCredentials>
        configDoc = changeJmsSecurityCredentials(configDoc, NEW_JMS_SECURITY_CREDENTIALS);
        configDoc = changeJmsPassword(configDoc, NEW_JMS_PASSWORD);

        // dbDriverName="org.postgresql.Driver"  ->  dbDriverName="oracle.jdbc.driver.OracleDriver"
        configDoc = changeDbDriverName(configDoc, NEW_DB_DRIVER_NAME);

        // dbConnectString="jdbc:postgresql://localhost/awsnetops"  ->  dbConnectString="jdbc:oracle:thin:@rhedcloud-dev-1.cqbnsdsexba3.us-east-1.rds.amazonaws.com:1521:RHEDCLD1"
        configDoc = changeDbConnectString(configDoc, NEW_DB_CONNECT_STRING);

        // dbConnectUserId="postgres"  ->  dbConnectUserId="networkops_dev"
        configDoc = changeDbConnectUserId(configDoc, NEW_DB_CONNECT_USER_ID);

        // dbConnectPassword="test"  ->   dbConnectPassword="Titnospw21"
        configDoc = changeDbConnectPassword(configDoc, NEW_DB_CONNECT_PASSWORD);

        configDoc = changeRouterCredsSecretKey(configDoc,NEW_ROUTER_CREDENTIALS_SECRET_KEY);
        configDoc = changeRouterCredsSecretAccessKey(configDoc, NEW_ROUTER_CREDENTIALS_SECRET_ACCESS_KEY);
        configDoc = changeRouterCredsSecretName(configDoc, NEW_ROUTER_CREDENTIALS_SECRET_NAME);

        saveFile(configDoc, devAppConfigFilePath);


    }

    private Properties readCredentials(String cwd) {
        try (InputStream input = new FileInputStream(cwd + credentialsFile)) {

            Properties prop = new Properties();

            prop.load(input);

            return prop;
        } catch (IOException ex) {
            throw new RuntimeException("Failed to read credentials file", ex);
        }
    }

    private String changeServiceName(String lines, String newName) {
        try {

            Pattern pattern = Pattern.compile(SERVICE_NAME_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), newName);
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeLogFilePath(String lines, String newPath) {

        try {

            Pattern pattern = Pattern.compile(LOG_FILE_PATH_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), newPath);
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeJmsProviderURL(String lines, String newURL) {

        try {

            Pattern pattern = Pattern.compile(JMS_PROVIDER_URL);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), newURL);
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeJmsSecurityPrincipal(String lines, String newPrincipal) {

        try {

            Pattern pattern = Pattern.compile(JMS_SECURITY_PRINCIPAL_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newPrincipal + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeJmsSecurityCredentials(String lines, String newCredentials) {

        try {

            Pattern pattern = Pattern.compile(JMS_SECURITY_CREDENTIALS_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newCredentials + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeJmsUsername(String lines, String newUsername) {

        try {

            Pattern pattern = Pattern.compile(JMS_USERNAME_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newUsername + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeJmsPassword(String lines, String newPassword) {

        try {

            Pattern pattern = Pattern.compile(JMS_PASSWORD_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newPassword + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeDbDriverName(String lines, String newDbDriverName) {

        try {

            Pattern pattern = Pattern.compile(DB_DRIVER_NAME_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newDbDriverName + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeDbConnectString(String lines, String newDbConnectString) {

        try {

            Pattern pattern = Pattern.compile(DB_CONNECT_STRING_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newDbConnectString + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeDbConnectUserId(String lines, String newDbConnectUserId) {

        try {

            Pattern pattern = Pattern.compile(DB_CONNECT_USER_ID_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newDbConnectUserId + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeDbConnectPassword(String lines, String newDbConnectPassword) {

        try {

            Pattern pattern = Pattern.compile(DB_CONNECT_PASSWORD_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newDbConnectPassword + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeRouterCredsSecretAccessKey(String lines, String newSecretAccessKey) {
        try {
            Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_ACCESS_KEY_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newSecretAccessKey + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeRouterCredsSecretKey(String lines, String newSecretKey) {
        try {
            Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_KEY_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newSecretKey + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private String changeRouterCredsSecretName(String lines, String newSecretName) {
        try {
            Pattern pattern = Pattern.compile(ROUTER_CREDENTIALS_SECRET_NAME_PATTERN);
            Matcher matcher = pattern.matcher(lines);
            while (matcher.find()) {
                lines = lines.replaceFirst(matcher.group(), matcher.group(1) + newSecretName + matcher.group(3));
            }

            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private void saveFile(String lines, Path file) {
        try {
            Files.write(file, (lines).getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}

