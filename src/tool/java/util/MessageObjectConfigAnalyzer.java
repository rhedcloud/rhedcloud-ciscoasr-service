package util;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageObjectConfigAnalyzer {

    private final String localAppConfigFile1 = "/deploy/build-test/configs/CiscoAsr1Service.xml";
    private final String localAppConfigFile2 = "/deploy/build-test/configs/CiscoAsr2Service.xml";

    public static void main(String[] args) {
        MessageObjectConfigAnalyzer analyzer = new MessageObjectConfigAnalyzer();

        analyzer.doIt();
    }

    public void doIt() {

        String cwd = System.getProperty("user.dir");
        FileSystem fileSystem = FileSystems.getDefault();
        Path appConfigPath1 = fileSystem.getPath(cwd + localAppConfigFile1);
        Path appConfigPath2 = fileSystem.getPath(cwd + localAppConfigFile2);

        scanConfigFile(appConfigPath1);
        scanConfigFile(appConfigPath2);

    }

    private void scanConfigFile(Path configFilePath) {

        log("\nScanning " + configFilePath.toFile().getName() + "...\n");

        List<String> lines = readLines(configFilePath);

        List<String> appMessageObjects = new ArrayList<>();
        List<String> servletMessageObjects = new ArrayList<>();

        final Pattern messageObjectConfigPattern = Pattern.compile("\\<MessageObjectConfig\\s+((?!name).)*name=\"([^\"]+)\"");

        boolean processingAppLines = true;
        for (String line : lines) {
            if (line.contains("</ConsumerConfigs>")) {
                processingAppLines = false;
            }

            // <MessageObjectConfig name="VpnConnectionProfileAssignmentQuerySpecification.v1_0" ...

            final Matcher messageObjectConfigMatcher = messageObjectConfigPattern.matcher(line);

            if (messageObjectConfigMatcher.find()) {
                if (processingAppLines) {
                    appMessageObjects.add(messageObjectConfigMatcher.group(2));
                } else {
                    servletMessageObjects.add(messageObjectConfigMatcher.group(2));
                }
            }
        }

        if (!appMessageObjects.isEmpty()) {
            log("App message objects not found in servlet list:\n" );

            appMessageObjects.forEach(line -> {
                if (!servletMessageObjects.contains(line)) {
                    log(line);
                }

            });

        }

    }


    private void log(String message) {
        System.out.println(message);
    }

    private String readText(Path configFilePath) {
        String lines;
        try {
            lines = new String(Files.readAllBytes(configFilePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private List<String> readLines(Path configFilePath) {
        List<String> lines;
        try {
            lines = Files.readAllLines(configFilePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }


}
