<?xml version="1.0"?>

<!--
testing at http://xslttest.appspot.com/
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" omit-xml-declaration="no" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*"/>

    <!-- the identity template (copies your input verbatim) -->
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*" />
        </xsl:copy>
    </xsl:template>

    <!-- Variable data from router must be replaced with known responses - coupled with Example provider and test config -->

    <xsl:template match="VpnConnection/TunnelInterface/IpVirtualReassembly/text()">
        <xsl:text>[unknown]</xsl:text>
    </xsl:template>

    <xsl:template match="VpnConnection/TunnelInterface/BgpState/Status/text()">
        <xsl:text>fsm-established</xsl:text>
    </xsl:template>
    <xsl:template match="VpnConnection/TunnelInterface/BgpState/Uptime/text()">
        <xsl:text>00:00:00</xsl:text>
    </xsl:template>

    <xsl:template match="VpnConnection/TunnelInterface/BgpPrefixes/Sent/text()">
        <xsl:text>0</xsl:text>
    </xsl:template>
    <xsl:template match="VpnConnection/TunnelInterface/BgpPrefixes/Received/text()">
        <xsl:text>0</xsl:text>
    </xsl:template>

</xsl:stylesheet>
