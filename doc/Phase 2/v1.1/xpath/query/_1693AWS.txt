native/crypto/isakmp/profile/name -> isakmp-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
native/crypto/ipsec/transform-set/tag -> ipsec-prop-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
interfaces-state/interface/name -> Tunnel${TunnelId}
    ${TunnelId}
native/crypto/keyring/name -> keyring-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/id -> ${BgpNeighborId}
    ${BgpNeighborId}
bgp-state-data/neighbors/neighbor/neighbor-id -> ${BgpNeighborId}
    ${BgpNeighborId}
native/crypto/ipsec/profile/name -> ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
native/interface/Tunnel/name -> ${RouterNumber}${TunnelNumberZeroBased}${VpnId-Padded}
    ${RouterNumber}
    ${TunnelNumberZeroBased}
    ${VpnId-Padded}
native/crypto/isakmp/profile/name -> isakmp-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
native/crypto/ipsec/transform-set/tag -> ipsec-prop-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
interfaces-state/interface/name -> Tunnel${TunnelId}
    ${TunnelId}
native/crypto/keyring/name -> keyring-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor/id -> ${BgpNeighborId}
    ${BgpNeighborId}
bgp-state-data/neighbors/neighbor/neighbor-id -> ${BgpNeighborId}
    ${BgpNeighborId}
native/crypto/ipsec/profile/name -> ipsec-vpn-research-vpc${VpnId-Padded}-tun${TunnelNumber}
    ${VpnId-Padded}
    ${TunnelNumber}
native/interface/Tunnel/name -> ${RouterNumber}${TunnelNumberZeroBased}${VpnId-Padded}
    ${RouterNumber}
    ${TunnelNumberZeroBased}
    ${VpnId-Padded}
