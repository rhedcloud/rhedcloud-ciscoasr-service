 #!/bin/bash

# Jimmy's templates are missing xc xmlns - this script replaces first line with one that includes it.

var='<native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-crypto="http://cisco.com/ns/yang/Cisco-IOS-XE-crypto" xmlns:ios-tun="http://cisco.com/ns/yang/Cisco-IOS-XE-tunnel" xmlns:ios-bgp="http://cisco.com/ns/yang/Cisco-IOS-XE-bgp" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">'

find . -name "Tunnel*" -print | xargs sed -i "1s|.*|${var}|"

var='<native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-nat="http://cisco.com/ns/yang/Cisco-IOS-XE-nat" xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">'
find . -name "NAT" -print | xargs sed -i "1s|.*|${var}|"
