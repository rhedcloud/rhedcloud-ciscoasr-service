# Create new module in Project Structure -> Modules
   1) Set project root to folder of choice.  This can even be a sibling to /src/main and /src/test - "src/tool" for tool module
   2) Select/create source folder - "src/tool/java" for tool module (Note: an existing directory can be converted into a new module root and source folder)
   3) In "Paths" tab, select "Use module compile output path" and specify $MODULE_DIR$\out and $MODULE_DIR$\out\test for output path and test output path (anor any other subdirectories in your module)
   4) Add your output root directory as a class path directory (see adding-classpath-dir.txt)
   5) Add libraries as needed - "Libraries..." will allow you to share dependencies from main project, e.g. JUnit.jar
