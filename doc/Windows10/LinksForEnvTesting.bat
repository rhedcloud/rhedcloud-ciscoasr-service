@echo off
REM Run from current directory (doc\Windows10)

REM deploy/build-test

echo "esb-dev links..."

if not exist %cd%\..\..\deploy\esb-dev\message (
    echo "Creating message link..."
    mklink /D %cd%\..\..\deploy\esb-dev\message %cd%\..\..\deploy\build-test\message
)

if not exist %cd%\..\..\deploy\esb-dev\eos (
    echo "Creating eos link..."
    mklink /D %cd%\..\..\deploy\esb-dev\eos %cd%\..\..\deploy\build-test\eos
)

REM deploy/build-test/configs

if not exist %cd%\..\..\deploy\esb-dev\configs\messaging (
    echo "Creating configs\messaging link... "
    mklink /D %cd%\..\..\deploy\esb-dev\configs\messaging %cd%\..\..\deploy\build-test\configs\messaging
)

if exist %cd%\..\..\deploy\esb-test\configs (
    echo "esb-test links..."

    if not exist %cd%\..\..\deploy\esb-test\message (
        echo "Creating message link..."
        mklink /D %cd%\..\..\deploy\esb-test\message %cd%\..\..\deploy\build-test\message
    )

    if not exist %cd%\..\..\deploy\esb-test\eos (
        echo "Creating eos link..."
        mklink /D %cd%\..\..\deploy\esb-test\eos %cd%\..\..\deploy\build-test\eos
    )

    REM deploy/build-test/configs

    if not exist %cd%\..\..\deploy\esb-test\configs\messaging (
        echo "Creating configs\messaging link... "
        mklink /D %cd%\..\..\deploy\esb-test\configs\messaging %cd%\..\..\deploy\build-test\configs\messaging
    )
)

if exist %cd%\..\..\deploy\esb-stage\configs (
    echo "esb-stage links..."

    if not exist %cd%\..\..\deploy\esb-stage\message (
        echo "Creating message link..."
        mklink /D %cd%\..\..\deploy\esb-stage\message %cd%\..\..\deploy\build-test\message
    )

    if not exist %cd%\..\..\deploy\esb-stage\eos (
        echo "Creating eos link..."
        mklink /D %cd%\..\..\deploy\esb-stage\eos %cd%\..\..\deploy\build-test\eos
    )

    REM deploy/build-test/configs

    if not exist %cd%\..\..\deploy\esb-stage\configs\messaging (
        echo "Creating configs\messaging link... "
        mklink /D %cd%\..\..\deploy\esb-stage\configs\messaging %cd%\..\..\deploy\build-test\configs\messaging
    )
)
