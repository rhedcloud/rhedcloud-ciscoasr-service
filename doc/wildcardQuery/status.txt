
I've completed my research and have an implementation that is working with the Lab Routers.

Basic conclusion:  I've found a way to implement a VpnConnection wildcard query, however, the jury is out whether the xpath filters will be viable in PROD.

Jimmy, you'll have to evaluate the queries below and determine on a query by query basis whether this is a viable solution. I refer to the "index-scan" problem we saw on PROD with an inappropriate xpath filter.

1) Queries

There are 8 separate queries required to gather all the config sections and tunnel state needed to construct VpnConnection instances:

a) Native interface tunnel query

   session.get("native/interface/Tunnel[starts-with(name, '" + tunnelNumber + "')]/../Tunnel[not (contains(description, 'AVAILABLE'))]")

   This query returns all active tunnels on the current router, and testing reveals that the number returned matches router state. This list is used to query for the remaining 7 sections/state, by iterating over all active tunnels for the required search context needed to obtain the section/state data. The second half of the xpath filter eliminates unprovisioned tunnels.

   This query is fast and again returns exactly the number of active tunnels, so I'm assuming it's an index-seek type lookup, but you'll need to confirm this.


b) "Safe" queries for native config sections

   session.get("native/crypto/keyring[starts-with(name, 'keyring-vpn-research-vpc')]")
   session.get("native/crypto/isakmp/profile[starts-with(name, 'isakmp-vpn-research-vpc')]")
   session.get("native/crypto/ipsec/transform-set[starts-with(tag, 'ipsec-prop-vpn-research-vpc')]")
   session.get("native/crypto/ipsec/profile[starts-with(name, 'ipsec-vpn-research-vpc')]")

   These bring back n results per section, where n is the number of profiles on the router.  For the Lab Routers, n = 200. Each of these results are searched using the corresponding interface tunnel search context to obtain the corresponding "section." All query times are subsecond.

   These look safe to me considering the return count and query times. You'll need to confirm that this is equivalent to an index seek (as opposed to an index scan).

c) interfaces-state for interface

   session.get("interfaces-state/interface[starts-with(name, 'Tunnel" + tunnelNumber + "')]")

   This is the first suspect query, in that I assume that the xpath filter "interfaces-state/interface" corresponds to objects in the routers other than tunnels. That's why I included the 'Tunnel' child qualifier. As you'll see later, this is the longest query of all.

   I'm sure this will require a "index-seek" style lookup to prevent buffer overflows on the routers in PROD. Please confirm.

d) bgp config and state

   session.get("native/router/bgp/address-family/with-vrf/ipv4/vrf/neighbor[starts-with(id, '" + ipRange + "')]")
   session.get("bgp-state-data/neighbors/neighbor[starts-with(neighbor-id, '" + ipRange + "')]")

   We look up this information in the current query implementation using a specific ip address range calculated from the primary ip addresses obtained from the returned native interface config. You'll notice the "ipRange" value I'm using here - this is calculated from all primary ips in query (1a) by finding the greatest common ip address (see (2b)).  So far, this has been limited to the three-octet value "169.254.248." for router 1, and "169.254.252." for router 2. Response times are around 1 second or less - see query times below.

   My guess is that since we're currently using this query, and the response times are fast, it must be safe. Please confirm.

2) Algorithm

   Using the queries from (1), a list of VpnConnection objects can be constructed, each VpnConnection encapsulating an active tunnel's config and state. I've confirmed that the results are correct by comparing against the original query results for associated vpc-ids.

   There are a couple of interesting implementations to find all the required config sections and state:

   a) Only 8 queries per router are required

      The remaining collation is performed in memory by iterating over the first collection, native/interface/Tunnel context and filtering each remaining collection for current context related nodes.

   b) Calculating the greatest common ip range from all neighbor ids in the native interface tunnel configuration.

      This is done by accumulating a list of all neighbor ids from (1a), and keeping all common octets, starting from the most significant (6th power or first from left) octet.

   c) Building Netconf Nodes for each individual config/state element in the list

      Just safe node traversal to the root node, and creating Netconf nodes for use in the matching algorithm already in place in the provider.

   d) Java 8 streams for filtering

      Quickly find matching nodes in each of the 7 collections from the searches above.

3) Performance

   Note that all times below are calculated from test execution on the Lab Router environment.

		[Netconf Tools] vpn connection wildcard query: 33 vpn connections hydrated:
		   Section query times (milliseconds):
			  interface tunnel: 287
					   keyring: 335
				isakmp profile: 607
				 transform set: 100
				 ipsec profile: 114
			   interface state: 5160
					bgp config: 515
				bgp state data: 1156
			  ----------------------
			  Total query time: 8274
					Total time: 8690

		[Netconf Tools] 33 active tunnels found for wildcard query

   As stated above, the tests were run on the Lab Routers containing 33 tunnels each.  Note that collation time, the in memory portion of the processing, is around 0.5 second.

4) Conclusions

   a) No changes required to NetworkOpsService Provisioning - only a refactor of the VpnConnectionProvisioning Reconciler if we proceed with the wildcard query.
   b) No changes required to CiscoAsrService VpnConnectionQuerySpecificationEO.xml - both vpnId and vpcId keys are currently not required.
   c) Minimal changes required to CiscoAsrService to support a List return from query.
   d) Local JMS send client sending a VpnConnection.Query-Request with no vpc-id results in a list of VpnConnection object returned.

   If this is a viable approach, here are the following benefits to the VpnConnectionProvisioning Reconciler:
   
      a) Reducing the number of calls to CiscoAsrService from n to 1 per router, where n = the number of registered profile ids
	  b) Significantly reducing the probability of receiving "Connection Reset" exceptions during the router state acquisition phase, by virtue of (a)
	  c) Reducing the router state acquisition time for both routers from 48 MINUTES to 16 SECONDS, given the current query modulation delay of 6 seconds on the Lab Routers

   The "Connection Reset" issue is at this point in the network stack, while attempting to open a Netconf session, so reducing the Netconf session calls by an order of magnitude should have a significant positive impact.
   
   Note that my next test will be create a feature branch in NetworkOpsService and use the wildcard query for the VpnConnection Reconciler scheduled task.
   
   The question remains:  Are the queries viable in PROD?  It's up to Jimmy to decide if we'll have runaway Index-Scan behavior for any of the queries listed above.


