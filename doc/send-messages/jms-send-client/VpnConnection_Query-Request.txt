---------------------- Message Body ---------------------------

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE edu.emory.Network.VpnConnection.Query SYSTEM "../dtd/Query-Request.dtd">
<edu.emory.Network.VpnConnection.Query>
	<ControlAreaRequest messageCategory="edu.emory.Network" messageObject="VpnConnection"
		messageAction="Query" messageRelease="1.0" messagePriority="9" messageType="Request">
		<Sender>
			<MessageId>
				<SenderAppId>edu.emory.TestSuiteApplication</SenderAppId>
				<ProducerId>4860446b-1e16-4bc0-8d0a-af7675956591 </ProducerId>
				<MessageSeq>1024</MessageSeq>
			</MessageId>
			<Authentication>
				<AuthUserId>TestSuiteApp</AuthUserId>
			</Authentication>
		</Sender>
		<Datetime>
			<Year>2017</Year>
			<Month>9</Month>
			<Day>17</Day>
			<Hour>13</Hour>
			<Minute>47</Minute>
			<Second>30</Second>
			<SubSecond>456</SubSecond>
			<Timezone>6:00-GMT</Timezone>
		</Datetime>
		<ExpectedReplyFormat messageCategory="edu.emory.Network" messageObject="VpnConnection"
			messageRelease="1.0" messageAction="Provide" messageType="Reply"/>
	</ControlAreaRequest>
	<DataArea>
		<VpnConnectionQuerySpecification>
		    <VpcId>emory-testnum1</VpcId>
			<VpnId>1</VpnId>
		</VpnConnectionQuerySpecification>
	</DataArea>
</edu.emory.Network.VpnConnection.Query>

--------------------------- Properties ---------------------------------

COMMAND_NAME  VpnConnectionRequestCommand
MESSAGE_NAME  VpnConnectionRequestCommand
