#!/bin/bash
export CURL_HOME=`pwd`
curl --max-time 100 --header "Content-Type: text/xml;charset=UTF-8" --header "SOAPAction: http://www.emory.edu/CiscoAsr1Service/" --data @$CURL_HOME/data/VpnConnection-Wildcard-Query.xml http://emory-rhedcloud-ciscoasr-service-dev.us-east-1.elasticbeanstalk.com/services/CiscoAsr1Service
unset CURL_HOME
