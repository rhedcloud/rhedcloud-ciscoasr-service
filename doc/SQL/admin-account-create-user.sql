-- In admin account:
-- DEV
create user ciscoasr_dev identified by Titcaspw2_1 profile default;

grant connect to ciscoasr_dev;
grant resource to ciscoasr_dev;
grant unlimited tablespace to ciscoasr_dev;

-- PROD
create user ciscoasr_prod identified by CbATw_tcTHL profile default;

grant connect to ciscoasr_prod;
grant resource to ciscoasr_prod;
grant unlimited tablespace to ciscoasr_prod;

